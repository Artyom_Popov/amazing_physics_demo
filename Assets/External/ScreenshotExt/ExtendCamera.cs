﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;

namespace Capture
{
    public class ExtendCamera : MonoBehaviour
    {
        [SerializeField]
        private Settings _settings;

        private Vector3 _position;

        public Camera CustomCamera { get; private set; }
        public bool IsEnable { get; set; }

        private void Awake()
        {
            CustomCamera = GetComponent<Camera>();
        }

        private void Start()
        {
            IsEnable = true;

            _settings.target.targetDistance = _settings.target.currentDistance;

            if (_settings.axisX.invertAxisX)
            {
                _settings.axisX.invertXValue = -1;
            }
            else
            {
                _settings.axisX.invertXValue = 1;
            }
            if (_settings.axisY.invertAxisY)
            {
                _settings.axisY.invertYValue = -1;
            }
            else
            {
                _settings.axisY.invertYValue = 1;
            }
            if (_settings.zoom.invertAxisZoom)
            {
                _settings.zoom.invertZoomValue = -1;
            }
            else
            {
                _settings.zoom.invertZoomValue = 1;
            }

            _settings.axisX.x = _settings.axisX.initialAngleX;
            _settings.axisY.y = _settings.axisY.initialAngleY;

            transform.Rotate(new Vector3(0f, _settings.axisX.initialAngleX, 0f), Space.World);
            transform.Rotate(new Vector3(_settings.axisY.initialAngleY, 0f, 0f), Space.Self);

            _position = ((transform.rotation * new Vector3(0f, 0f, -_settings.target.currentDistance))) + _settings.target.target.position;
        }

        private void Update()
        {
            if (_settings.target.target != null)
            {
                if (_settings.control.mouseControl)
                {
                    if ((!_settings.control.clickToRotate || (_settings.control.leftClickToRotate && Input.GetMouseButton(1))) || (_settings.control.rightClickToRotate && Input.GetMouseButton(1)))
                    {
                        _settings.axisX.xVelocity += (Input.GetAxis(_settings.axisX.mouseAxisX) * _settings.axisX.xSpeed) * _settings.axisX.invertXValue;
                        _settings.axisY.yVelocity -= (Input.GetAxis(_settings.axisY.mouseAxisY) * _settings.axisY.ySpeed) * _settings.axisY.invertYValue;
                    }
                }

                _settings.zoom.zoomVelocity -= (Input.GetAxis(_settings.zoom.mouseAxisZoom) * _settings.zoom.zoomSpeed) * _settings.zoom.invertZoomValue;
            }


            if (_settings.axisX.limitX)
            {
                if ((_settings.axisX.x + _settings.axisX.xVelocity) < (_settings.axisX.xMinLimit + _settings.axisX.xLimitOffset))
                {
                    _settings.axisX.xVelocity = (_settings.axisX.xMinLimit + _settings.axisX.xLimitOffset) - _settings.axisX.x;
                }
                else if ((_settings.axisX.x + _settings.axisX.xVelocity) > (_settings.axisX.xMaxLimit + _settings.axisX.xLimitOffset))
                {
                    _settings.axisX.xVelocity = (_settings.axisX.xMaxLimit + _settings.axisX.xLimitOffset) - _settings.axisX.x;
                }

                _settings.axisX.x += _settings.axisX.xVelocity;

                transform.Rotate(new Vector3(0f, _settings.axisX.xVelocity, 0f), Space.World);
            }
            else
            {
                transform.Rotate(new Vector3(0f, _settings.axisX.xVelocity, 0f), Space.World);
            }


            if (_settings.axisY.limitY)
            {
                if ((_settings.axisY.y + _settings.axisY.yVelocity) < (_settings.axisY.yMinLimit + _settings.axisY.yLimitOffset))
                {
                    _settings.axisY.yVelocity = (_settings.axisY.yMinLimit + _settings.axisY.yLimitOffset) - _settings.axisY.y;
                }
                else if ((_settings.axisY.y + _settings.axisY.yVelocity) > (_settings.axisY.yMaxLimit + _settings.axisY.yLimitOffset))
                {
                    _settings.axisY.yVelocity = (_settings.axisY.yMaxLimit + _settings.axisY.yLimitOffset) - _settings.axisY.y;
                }

                _settings.axisY.y += _settings.axisY.yVelocity;

                transform.Rotate(new Vector3(_settings.axisY.yVelocity, 0f, 0f), Space.Self);
            }
            else
            {
                transform.Rotate(new Vector3(_settings.axisY.yVelocity, 0f, 0f), Space.Self);
            }


            if ((_settings.target.targetDistance + _settings.zoom.zoomVelocity) < _settings.target.minDistance)
            {
                _settings.zoom.zoomVelocity = _settings.target.minDistance - _settings.target.targetDistance;
            }
            else if ((_settings.target.targetDistance + _settings.zoom.zoomVelocity) > _settings.target.maxDistance)
            {
                _settings.zoom.zoomVelocity = _settings.target.maxDistance - _settings.target.targetDistance;
            }


            _settings.target.targetDistance += _settings.zoom.zoomVelocity;
            _settings.target.currentDistance = Mathf.Lerp(_settings.target.currentDistance, _settings.target.targetDistance, _settings.zoom.smoothingZoom);


            if (_settings.collision.cameraCollision)
            {
                Vector3 vector = this.transform.position - _settings.target.target.position;
                Ray ray = new Ray(_settings.target.target.position, vector.normalized);
                RaycastHit hit;
                if (Physics.SphereCast(ray.origin, _settings.collision.collisionRadius, ray.direction, out hit, _settings.target.currentDistance))
                {
                    _settings.target.currentDistance = hit.distance;
                }
            }

                       

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
            {
                if (_settings.control.keyboardControl)
                {
                    _settings.target.target.rotation = transform.rotation;
                    _settings.target.target.position += _settings.target.target.forward * _settings.control.keyBoardSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
                    _settings.target.target.position += _settings.target.target.right * _settings.control.keyBoardSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;

                    _position = _settings.target.target.position - (transform.rotation * Vector3.forward * _settings.target.currentDistance);
                }
            }
            else if (!Input.GetMouseButton(2))
            {
                //_position = ((transform.rotation * new Vector3(0f, 0f, -_settings.target.currentDistance))) + _settings.target.target.position;
                _position = ((transform.rotation * new Vector3(0f, 0f, - _settings.target.currentDistance))) + GetTargetPosition();
            }
            else
            {
                _settings.target.target.rotation = transform.rotation;
                _settings.target.target.Translate(Vector3.right * -Input.GetAxis(_settings.axisX.mouseAxisX) * _settings.control.panSpeed);
                _settings.target.target.Translate(transform.up * -Input.GetAxis(_settings.axisY.mouseAxisY) * _settings.control.panSpeed, Space.World);

                _position = _settings.target.target.position - (transform.rotation * Vector3.forward * _settings.target.currentDistance);
            }

            transform.position = _position;


            _settings.axisX.xVelocity *= _settings.axisX.dampeningX;
            _settings.axisY.yVelocity *= _settings.axisY.dampeningY;
            _settings.zoom.zoomVelocity = 0f;
        }

        private Vector3 GetTargetPosition()
        {
            if (_settings.target.isMovingTarget)
            {
                if (Mathf.Abs(_settings.target.target.position.x - _settings.target.newTargetPos.x) > .001f)
                {
                    _settings.target.target.position = Vector3.MoveTowards(_settings.target.target.position, _settings.target.newTargetPos, _settings.target.movingTargetSpeed * Time.deltaTime);

                    print("moving to next target position");
                }
                else
                {
                    _settings.target.isMovingTarget = false;
                }
            }

            return _settings.target.target.position;
        }

        public void SetTargetPos(Vector3 position)
        {
            _settings.target.isMovingTarget = true;
            _settings.target.newTargetPos = position;
        }

        [Serializable]
        public class Settings
        {
            public Target target;
            public Control control;
            public AxisX axisX;
            public AxisY axisY;
            public Zoom zoom;
            public Collision collision;

            [Serializable]
            public class Target
            {
                public Transform target;
                public float targetDistance;
                public float currentDistance = 10f;
                public float maxDistance = 5f;
                public float minDistance = 0.5f;
                public float movingTargetSpeed = 1.5f;
                [HideInInspector] public Vector3 newTargetPos;
                [HideInInspector] public bool isMovingTarget;
            }

            [Serializable]
            public class Control
            {
                public bool keyboardControl = true;
                public bool mouseControl = true;
                public bool clickToRotate = true;
                public bool leftClickToRotate;
                public bool rightClickToRotate = true;
                public float panSpeed = 0.05f;
                public float keyBoardSpeed = 10f;
            }

            [Serializable]
            public class AxisX
            {
                public float x;
                public float xLimitOffset;
                public bool limitX;
                public float xMaxLimit = 60f;
                public float xMinLimit = -60f;
                public float xSpeed = 1f;
                public float xVelocity;
                public float dampeningX = 0.9f;
                public float initialAngleX;
                public bool invertAxisX;
                public int invertXValue = 1;
                public string kbPanAxisX = "Horizontal";
                public string mouseAxisX = "Mouse X";
            }

            [Serializable]
            public class AxisY
            {
                public float y;
                public float yLimitOffset;
                public bool limitY = true;
                public float yMaxLimit = 60f;
                public float yMinLimit = -60f;
                public float ySpeed = 1f;
                public float yVelocity;
                public float dampeningY = 0.9f;
                public float initialAngleY;
                public bool invertAxisY;
                public int invertYValue = 1;
                public string kbPanAxisY = "Vertical";
                public string mouseAxisY = "Mouse Y";
            }

            [Serializable]
            public class Zoom
            {
                public float zoomSpeed = 5f;
                public float zoomVelocity;
                public int invertZoomValue = 1;
                public bool invertAxisZoom;
                public bool kbUseZoomAxis;
                public string kbZoomAxisName = string.Empty;
                public string mouseAxisZoom = "Mouse ScrollWheel";
                public float smoothingZoom = 0.1f;
            }

            [Serializable]
            public class Collision
            {
                public bool cameraCollision;
                public float collisionRadius = 0.25f;
            }
        }
    }
}
