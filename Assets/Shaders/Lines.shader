﻿Shader "Custom/Lines"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1)
	}

	SubShader
	{
		Color[_Color]

		Pass
		{
			//Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			ZTest Always
			Cull Off
			Fog { Mode Off }

			/*BindChannels
			{
			  Bind "vertex", vertex
			  Bind "color", color
			}*/
		}
	}
}