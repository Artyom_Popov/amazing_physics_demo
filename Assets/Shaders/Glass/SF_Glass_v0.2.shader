// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.35 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.35;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:2,spmd:0,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:True,rprd:True,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2865,x:34699,y:32850,varname:node_2865,prsc:2|diff-7987-OUT,spec-194-OUT,gloss-6781-OUT,normal-7430-OUT,alpha-421-OUT,refract-7288-OUT;n:type:ShaderForge.SFN_Fresnel,id:3703,x:31680,y:33213,varname:node_3703,prsc:2|NRM-9609-OUT,EXP-2129-OUT;n:type:ShaderForge.SFN_Vector3,id:2440,x:31680,y:33715,varname:node_2440,prsc:2,v1:0,v2:0,v3:5;n:type:ShaderForge.SFN_NormalVector,id:1566,x:30792,y:33215,prsc:2,pt:True;n:type:ShaderForge.SFN_Slider,id:6070,x:30709,y:33770,ptovrint:False,ptlb:Refraction,ptin:_Refraction,varname:_Refraction,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1268866,max:1;n:type:ShaderForge.SFN_Multiply,id:6830,x:31998,y:33289,varname:node_6830,prsc:2|A-3703-OUT,B-6070-OUT;n:type:ShaderForge.SFN_Color,id:7121,x:32323,y:32962,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3960785,c3:0.4431373,c4:0.328;n:type:ShaderForge.SFN_OneMinus,id:2129,x:31348,y:33424,varname:node_2129,prsc:2|IN-6070-OUT;n:type:ShaderForge.SFN_Clamp01,id:421,x:34455,y:33033,varname:node_421,prsc:2|IN-9759-OUT;n:type:ShaderForge.SFN_Slider,id:5095,x:31001,y:33063,ptovrint:False,ptlb:Frestnel,ptin:_Frestnel,varname:_Frestnel,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:5;n:type:ShaderForge.SFN_Add,id:126,x:32846,y:33188,varname:node_126,prsc:2|A-7121-A,B-394-OUT;n:type:ShaderForge.SFN_Slider,id:6781,x:34236,y:32645,ptovrint:False,ptlb:Glossiness,ptin:_Glossiness,varname:_Glossiness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Power,id:394,x:32323,y:33183,varname:node_394,prsc:2|VAL-3703-OUT,EXP-8657-OUT;n:type:ShaderForge.SFN_Vector1,id:8657,x:31998,y:33129,varname:node_8657,prsc:2,v1:3;n:type:ShaderForge.SFN_Fresnel,id:9119,x:31680,y:33012,varname:node_9119,prsc:2|NRM-9609-OUT,EXP-5095-OUT;n:type:ShaderForge.SFN_Slider,id:6433,x:32335,y:32568,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:_Specular,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Append,id:7288,x:33294,y:33437,varname:node_7288,prsc:2|A-2374-OUT,B-647-OUT;n:type:ShaderForge.SFN_Multiply,id:2374,x:32846,y:33349,varname:node_2374,prsc:2|A-4644-R,B-3173-OUT;n:type:ShaderForge.SFN_Multiply,id:647,x:32846,y:33541,varname:node_647,prsc:2|A-4644-G,B-3173-OUT;n:type:ShaderForge.SFN_ComponentMask,id:4644,x:31998,y:33724,varname:node_4644,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-4834-XYZ;n:type:ShaderForge.SFN_Transform,id:4834,x:31680,y:33536,varname:node_4834,prsc:2,tffrom:0,tfto:3|IN-9609-OUT;n:type:ShaderForge.SFN_Multiply,id:8432,x:32846,y:32748,varname:node_8432,prsc:2|A-6433-OUT,B-9872-OUT;n:type:ShaderForge.SFN_Add,id:3173,x:32323,y:33348,varname:node_3173,prsc:2|A-6830-OUT,B-6602-OUT;n:type:ShaderForge.SFN_Vector1,id:2497,x:31680,y:33436,varname:node_2497,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:6602,x:31998,y:33490,varname:node_6602,prsc:2|A-2497-OUT,B-6070-OUT;n:type:ShaderForge.SFN_RemapRange,id:2071,x:31998,y:32883,varname:node_2071,prsc:2,frmn:0,frmx:0.5,tomn:0,tomx:1|IN-9119-OUT;n:type:ShaderForge.SFN_Power,id:9872,x:32323,y:32811,varname:node_9872,prsc:2|VAL-2071-OUT,EXP-8657-OUT;n:type:ShaderForge.SFN_Clamp01,id:6162,x:33320,y:32727,varname:node_6162,prsc:2|IN-8432-OUT;n:type:ShaderForge.SFN_Tex2d,id:3723,x:30787,y:33496,ptovrint:True,ptlb:Normal,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_NormalBlend,id:7707,x:31087,y:33311,varname:node_7707,prsc:2|BSE-1566-OUT,DTL-3723-RGB;n:type:ShaderForge.SFN_Lerp,id:7430,x:31998,y:33906,varname:node_7430,prsc:2|A-2440-OUT,B-3723-RGB,T-6070-OUT;n:type:ShaderForge.SFN_Lerp,id:9609,x:31348,y:33219,varname:node_9609,prsc:2|A-1566-OUT,B-7707-OUT,T-6070-OUT;n:type:ShaderForge.SFN_Tex2d,id:9997,x:32846,y:32960,ptovrint:False,ptlb:Dirt,ptin:_Dirt,varname:_Dirt,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Blend,id:7987,x:34315,y:32777,varname:node_7987,prsc:2,blmd:6,clmp:True|SRC-7121-RGB,DST-2740-OUT;n:type:ShaderForge.SFN_Blend,id:4180,x:33294,y:33171,varname:node_4180,prsc:2,blmd:5,clmp:True|SRC-9997-R,DST-9997-G;n:type:ShaderForge.SFN_Blend,id:518,x:33520,y:33101,varname:node_518,prsc:2,blmd:5,clmp:True|SRC-4180-OUT,DST-9997-B;n:type:ShaderForge.SFN_SwitchProperty,id:8467,x:33742,y:33042,ptovrint:False,ptlb:Dirt Alpha from Grayscale,ptin:_DirtAlphafromGrayscale,varname:_DirtAlphafromGrayscale,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-9997-A,B-518-OUT;n:type:ShaderForge.SFN_Blend,id:9759,x:34185,y:33034,varname:node_9759,prsc:2,blmd:6,clmp:True|SRC-126-OUT,DST-6786-OUT;n:type:ShaderForge.SFN_Multiply,id:6786,x:33968,y:33223,varname:node_6786,prsc:2|A-8467-OUT,B-3265-OUT;n:type:ShaderForge.SFN_Slider,id:3265,x:33411,y:32924,ptovrint:False,ptlb:Dirt Visiblity,ptin:_DirtVisiblity,varname:_DirtVisiblity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:2740,x:34094,y:32836,varname:node_2740,prsc:2|A-9997-RGB,B-3265-OUT;n:type:ShaderForge.SFN_Slider,id:1743,x:33228,y:32544,ptovrint:False,ptlb:Specular Multiplier,ptin:_SpecularMultiplier,varname:_SpecularMultiplier,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:194,x:33734,y:32728,varname:node_194,prsc:2|A-6162-OUT,B-8432-OUT,T-1743-OUT;n:type:ShaderForge.SFN_Negate,id:4546,x:31093,y:33819,varname:node_4546,prsc:2|IN-6070-OUT;n:type:ShaderForge.SFN_Vector1,id:7386,x:34455,y:33203,varname:node_7386,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Vector3,id:8771,x:34393,y:32444,varname:node_8771,prsc:2,v1:0.07843138,v2:0.3960785,v3:0.4431373;n:type:ShaderForge.SFN_Vector1,id:1269,x:34455,y:32892,varname:node_1269,prsc:2,v1:1;proporder:7121-6070-6433-1743-6781-5095-3723-9997-8467-3265;pass:END;sub:END;*/

Shader "Shader Forge/SF_Glass_v0.2" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3960785,0.4431373,0.328)
        _Refraction ("Refraction", Range(0, 1)) = 0.1268866
        _Specular ("Specular", Range(0, 1)) = 1
        _SpecularMultiplier ("Specular Multiplier", Range(0, 1)) = 0
        _Glossiness ("Glossiness", Range(0, 1)) = 1
        _Frestnel ("Frestnel", Range(0, 5)) = 0
        _BumpMap ("Normal", 2D) = "bump" {}
        _Dirt ("Dirt", 2D) = "white" {}
        [MaterialToggle] _DirtAlphafromGrayscale ("Dirt Alpha from Grayscale", Float ) = 0
        _DirtVisiblity ("Dirt Visiblity", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform float _Refraction;
            uniform float4 _Color;
            uniform float _Frestnel;
            uniform float _Glossiness;
            uniform float _Specular;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform sampler2D _Dirt; uniform float4 _Dirt_ST;
            uniform fixed _DirtAlphafromGrayscale;
            uniform float _DirtVisiblity;
            uniform float _SpecularMultiplier;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                UNITY_FOG_COORDS(8)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD9;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = lerp(float3(0,0,5),_BumpMap_var.rgb,_Refraction);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 node_7707_nrm_base = normalDirection + float3(0,0,1);
                float3 node_7707_nrm_detail = _BumpMap_var.rgb * float3(-1,-1,1);
                float3 node_7707_nrm_combined = node_7707_nrm_base*dot(node_7707_nrm_base, node_7707_nrm_detail)/node_7707_nrm_base.z - node_7707_nrm_detail;
                float3 node_7707 = node_7707_nrm_combined;
                float3 node_9609 = lerp(normalDirection,node_7707,_Refraction);
                float2 node_4644 = mul( UNITY_MATRIX_V, float4(node_9609,0) ).xyz.rgb.rg;
                float node_3703 = pow(1.0-max(0,dot(node_9609, viewDirection)),(1.0 - _Refraction));
                float node_3173 = ((node_3703*_Refraction)+(1.0*_Refraction));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + float2((node_4644.r*node_3173),(node_4644.g*node_3173));
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Glossiness;
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float node_8657 = 3.0;
                float node_8432 = (_Specular*pow((pow(1.0-max(0,dot(node_9609, viewDirection)),_Frestnel)*2.0+0.0),node_8657));
                float node_194 = lerp(saturate(node_8432),node_8432,_SpecularMultiplier);
                float3 specularColor = float3(node_194,node_194,node_194);
                float3 directSpecular = attenColor * pow(max(0,dot(reflect(-lightDirection, normalDirection),viewDirection)),specPow)*specularColor;
                float3 indirectSpecular = (gi.indirect.specular)*specularColor;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float4 _Dirt_var = tex2D(_Dirt,TRANSFORM_TEX(i.uv0, _Dirt));
                float3 diffuseColor = saturate((1.0-(1.0-_Color.rgb)*(1.0-(_Dirt_var.rgb*_DirtVisiblity))));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,saturate(saturate((1.0-(1.0-(_Color.a+pow(node_3703,node_8657)))*(1.0-(lerp( _Dirt_var.a, saturate(max(saturate(max(_Dirt_var.r,_Dirt_var.g)),_Dirt_var.b)), _DirtAlphafromGrayscale )*_DirtVisiblity)))))),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform float _Refraction;
            uniform float4 _Color;
            uniform float _Frestnel;
            uniform float _Glossiness;
            uniform float _Specular;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform sampler2D _Dirt; uniform float4 _Dirt_ST;
            uniform fixed _DirtAlphafromGrayscale;
            uniform float _DirtVisiblity;
            uniform float _SpecularMultiplier;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                LIGHTING_COORDS(8,9)
                UNITY_FOG_COORDS(10)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = lerp(float3(0,0,5),_BumpMap_var.rgb,_Refraction);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 node_7707_nrm_base = normalDirection + float3(0,0,1);
                float3 node_7707_nrm_detail = _BumpMap_var.rgb * float3(-1,-1,1);
                float3 node_7707_nrm_combined = node_7707_nrm_base*dot(node_7707_nrm_base, node_7707_nrm_detail)/node_7707_nrm_base.z - node_7707_nrm_detail;
                float3 node_7707 = node_7707_nrm_combined;
                float3 node_9609 = lerp(normalDirection,node_7707,_Refraction);
                float2 node_4644 = mul( UNITY_MATRIX_V, float4(node_9609,0) ).xyz.rgb.rg;
                float node_3703 = pow(1.0-max(0,dot(node_9609, viewDirection)),(1.0 - _Refraction));
                float node_3173 = ((node_3703*_Refraction)+(1.0*_Refraction));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + float2((node_4644.r*node_3173),(node_4644.g*node_3173));
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Glossiness;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float node_8657 = 3.0;
                float node_8432 = (_Specular*pow((pow(1.0-max(0,dot(node_9609, viewDirection)),_Frestnel)*2.0+0.0),node_8657));
                float node_194 = lerp(saturate(node_8432),node_8432,_SpecularMultiplier);
                float3 specularColor = float3(node_194,node_194,node_194);
                float3 directSpecular = attenColor * pow(max(0,dot(reflect(-lightDirection, normalDirection),viewDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _Dirt_var = tex2D(_Dirt,TRANSFORM_TEX(i.uv0, _Dirt));
                float3 diffuseColor = saturate((1.0-(1.0-_Color.rgb)*(1.0-(_Dirt_var.rgb*_DirtVisiblity))));
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * saturate(saturate((1.0-(1.0-(_Color.a+pow(node_3703,node_8657)))*(1.0-(lerp( _Dirt_var.a, saturate(max(saturate(max(_Dirt_var.r,_Dirt_var.g)),_Dirt_var.b)), _DirtAlphafromGrayscale )*_DirtVisiblity))))),0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Refraction;
            uniform float4 _Color;
            uniform float _Frestnel;
            uniform float _Glossiness;
            uniform float _Specular;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform sampler2D _Dirt; uniform float4 _Dirt_ST;
            uniform float _DirtVisiblity;
            uniform float _SpecularMultiplier;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float4 _Dirt_var = tex2D(_Dirt,TRANSFORM_TEX(i.uv0, _Dirt));
                float3 diffColor = saturate((1.0-(1.0-_Color.rgb)*(1.0-(_Dirt_var.rgb*_DirtVisiblity))));
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 node_7707_nrm_base = normalDirection + float3(0,0,1);
                float3 node_7707_nrm_detail = _BumpMap_var.rgb * float3(-1,-1,1);
                float3 node_7707_nrm_combined = node_7707_nrm_base*dot(node_7707_nrm_base, node_7707_nrm_detail)/node_7707_nrm_base.z - node_7707_nrm_detail;
                float3 node_7707 = node_7707_nrm_combined;
                float3 node_9609 = lerp(normalDirection,node_7707,_Refraction);
                float node_8657 = 3.0;
                float node_8432 = (_Specular*pow((pow(1.0-max(0,dot(node_9609, viewDirection)),_Frestnel)*2.0+0.0),node_8657));
                float node_194 = lerp(saturate(node_8432),node_8432,_SpecularMultiplier);
                float3 specColor = float3(node_194,node_194,node_194);
                float roughness = 1.0 - _Glossiness;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
