// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.35 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.35;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:2,spmd:0,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2865,x:33730,y:33165,varname:node_2865,prsc:2|diff-848-OUT,spec-358-OUT,gloss-1813-OUT,normal-3346-OUT,transm-4496-OUT,lwrap-4496-OUT,alpha-4969-OUT,refract-5804-OUT;n:type:ShaderForge.SFN_Multiply,id:6343,x:31192,y:33286,varname:node_6343,prsc:2|A-7736-RGB,B-6665-RGB;n:type:ShaderForge.SFN_Color,id:6665,x:30909,y:33373,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1960784,c2:0.2941177,c3:0.3607843,c4:0.125;n:type:ShaderForge.SFN_Tex2d,id:7736,x:30069,y:33096,ptovrint:True,ptlb:Dirt,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5964,x:30909,y:33980,ptovrint:True,ptlb:Normal Map,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Slider,id:358,x:32351,y:33210,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:_Specular,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:1813,x:32351,y:33384,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Gloss,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Fresnel,id:3998,x:31667,y:33627,varname:node_3998,prsc:2|EXP-5074-OUT;n:type:ShaderForge.SFN_Append,id:2783,x:32555,y:33988,varname:node_2783,prsc:2|A-4707-OUT,B-2060-OUT;n:type:ShaderForge.SFN_Multiply,id:2060,x:32283,y:34080,varname:node_2060,prsc:2|A-9936-G,B-797-OUT;n:type:ShaderForge.SFN_Slider,id:797,x:31240,y:33993,ptovrint:False,ptlb:Refraction,ptin:_Refraction,varname:_Refraction,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1,max:1;n:type:ShaderForge.SFN_Multiply,id:4707,x:32283,y:33914,varname:node_4707,prsc:2|A-9936-R,B-797-OUT;n:type:ShaderForge.SFN_NormalVector,id:6840,x:30909,y:34235,prsc:2,pt:True;n:type:ShaderForge.SFN_Transform,id:3082,x:31806,y:34135,varname:node_3082,prsc:2,tffrom:3,tfto:0|IN-5871-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9936,x:32033,y:34135,varname:node_9936,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-3082-XYZ;n:type:ShaderForge.SFN_Negate,id:5804,x:32773,y:33968,varname:node_5804,prsc:2|IN-2783-OUT;n:type:ShaderForge.SFN_Slider,id:5074,x:31240,y:33649,ptovrint:False,ptlb:Frestnel Deepness,ptin:_FrestnelDeepness,varname:_FrestnelDeepness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:5,max:10;n:type:ShaderForge.SFN_Slider,id:6086,x:31240,y:33820,ptovrint:False,ptlb:Frestnel Power,ptin:_FrestnelPower,varname:_FrestnelPower,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:4150,x:31971,y:33723,varname:node_4150,prsc:2|IN-3998-OUT,IMIN-3458-OUT,IMAX-7427-OUT,OMIN-6086-OUT,OMAX-7427-OUT;n:type:ShaderForge.SFN_Vector1,id:3458,x:31667,y:33825,varname:node_3458,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:7427,x:31667,y:33956,varname:node_7427,prsc:2,v1:0;n:type:ShaderForge.SFN_Add,id:7369,x:32299,y:33581,varname:node_7369,prsc:2|A-4150-OUT,B-4703-OUT;n:type:ShaderForge.SFN_Clamp01,id:4969,x:32528,y:33555,varname:node_4969,prsc:2|IN-7369-OUT;n:type:ShaderForge.SFN_Add,id:9521,x:33088,y:33396,varname:node_9521,prsc:2|A-8882-RGB,B-4969-OUT;n:type:ShaderForge.SFN_Multiply,id:4496,x:33346,y:33400,varname:node_4496,prsc:2|A-9521-OUT,B-5306-OUT;n:type:ShaderForge.SFN_Slider,id:5306,x:32975,y:33581,ptovrint:False,ptlb:Inner Lighting,ptin:_InnerLighting,varname:_InnerLighting,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_LightColor,id:8882,x:32809,y:33401,varname:node_8882,prsc:2;n:type:ShaderForge.SFN_NormalBlend,id:5383,x:31318,y:34144,varname:node_5383,prsc:2|BSE-6840-OUT,DTL-5964-RGB;n:type:ShaderForge.SFN_Slider,id:4671,x:30831,y:33786,ptovrint:False,ptlb:Normal Influence,ptin:_NormalInfluence,varname:_NormalInfluence,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Lerp,id:3346,x:31667,y:33447,varname:node_3346,prsc:2|A-3026-OUT,B-5964-RGB,T-4671-OUT;n:type:ShaderForge.SFN_Lerp,id:5871,x:31599,y:34135,varname:node_5871,prsc:2|A-6840-OUT,B-5383-OUT,T-4671-OUT;n:type:ShaderForge.SFN_Vector3,id:3026,x:30909,y:33620,varname:node_3026,prsc:2,v1:0,v2:0,v3:5;n:type:ShaderForge.SFN_SwitchProperty,id:9212,x:30909,y:33141,ptovrint:False,ptlb:Dirt Alpha from Grayscale,ptin:_DirtAlphafromGrayscale,varname:_DirtAlphafromGrayscale,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-7736-A,B-7480-OUT;n:type:ShaderForge.SFN_Blend,id:8432,x:30426,y:33166,varname:node_8432,prsc:2,blmd:5,clmp:True|SRC-7736-R,DST-7736-G;n:type:ShaderForge.SFN_Blend,id:5565,x:30655,y:33166,varname:node_5565,prsc:2,blmd:5,clmp:True|SRC-8432-OUT,DST-7736-B;n:type:ShaderForge.SFN_Blend,id:848,x:32270,y:32832,varname:node_848,prsc:2,blmd:6,clmp:True|SRC-6665-RGB,DST-7161-OUT;n:type:ShaderForge.SFN_Depth,id:5268,x:30329,y:32647,varname:node_5268,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3818,x:31421,y:32862,varname:node_3818,prsc:2|A-7736-RGB,B-7971-OUT;n:type:ShaderForge.SFN_OneMinus,id:7971,x:31117,y:32927,varname:node_7971,prsc:2|IN-3090-OUT;n:type:ShaderForge.SFN_Multiply,id:733,x:31421,y:33264,varname:node_733,prsc:2|A-9212-OUT,B-7971-OUT;n:type:ShaderForge.SFN_Slider,id:6723,x:30250,y:32994,ptovrint:False,ptlb:Dirt Z Depth,ptin:_DirtZDepth,varname:_DirtZDepth,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:10,max:10;n:type:ShaderForge.SFN_Clamp01,id:3090,x:30876,y:32902,varname:node_3090,prsc:2|IN-7507-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:7507,x:30638,y:32724,varname:node_7507,prsc:2|IN-5268-OUT,IMIN-4659-OUT,IMAX-6626-OUT,OMIN-4659-OUT,OMAX-6723-OUT;n:type:ShaderForge.SFN_Vector1,id:4659,x:30329,y:32788,varname:node_4659,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:6626,x:30329,y:32897,varname:node_6626,prsc:2,v1:1;n:type:ShaderForge.SFN_Add,id:4703,x:32000,y:33470,varname:node_4703,prsc:2|A-6665-A,B-8943-OUT;n:type:ShaderForge.SFN_Add,id:5155,x:30426,y:33369,varname:node_5155,prsc:2|A-7736-R,B-7736-G;n:type:ShaderForge.SFN_Add,id:7480,x:30655,y:33369,varname:node_7480,prsc:2|A-5155-OUT,B-7736-B;n:type:ShaderForge.SFN_Multiply,id:8943,x:31683,y:33264,varname:node_8943,prsc:2|A-733-OUT,B-2533-OUT;n:type:ShaderForge.SFN_Slider,id:2533,x:31343,y:33079,ptovrint:False,ptlb:Dirt Opacity,ptin:_DirtOpacity,varname:_DirtOpacity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:7161,x:31686,y:32864,varname:node_7161,prsc:2|A-3818-OUT,B-2533-OUT;proporder:6665-5964-358-1813-797-4671-6086-5074-5306-7736-9212-2533-6723;pass:END;sub:END;*/

Shader "Shader Forge/SF_Glass_v1.1" {
    Properties {
        _Color ("Color", Color) = (0.1960784,0.2941177,0.3607843,0.125)
        _BumpMap ("Normal Map", 2D) = "bump" {}
        _Specular ("Specular", Range(0, 1)) = 1
        _Gloss ("Gloss", Range(0, 1)) = 1
        _Refraction ("Refraction", Range(0, 1)) = 0.1
        _NormalInfluence ("Normal Influence", Range(0, 1)) = 0.5
        _FrestnelPower ("Frestnel Power", Range(0, 1)) = 1
        _FrestnelDeepness ("Frestnel Deepness", Range(0, 10)) = 5
        _InnerLighting ("Inner Lighting", Range(0, 1)) = 0.5
        _MainTex ("Dirt", 2D) = "white" {}
        [MaterialToggle] _DirtAlphafromGrayscale ("Dirt Alpha from Grayscale", Float ) = 0
        _DirtOpacity ("Dirt Opacity", Range(0, 1)) = 1
        _DirtZDepth ("Dirt Z Depth", Range(0, 10)) = 10
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Refraction;
            uniform float _FrestnelDeepness;
            uniform float _FrestnelPower;
            uniform float _InnerLighting;
            uniform float _NormalInfluence;
            uniform fixed _DirtAlphafromGrayscale;
            uniform float _DirtZDepth;
            uniform float _DirtOpacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                float4 projPos : TEXCOORD8;
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = lerp(float3(0,0,5),_BumpMap_var.rgb,_NormalInfluence);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float3 node_5383_nrm_base = normalDirection + float3(0,0,1);
                float3 node_5383_nrm_detail = _BumpMap_var.rgb * float3(-1,-1,1);
                float3 node_5383_nrm_combined = node_5383_nrm_base*dot(node_5383_nrm_base, node_5383_nrm_detail)/node_5383_nrm_base.z - node_5383_nrm_detail;
                float3 node_5383 = node_5383_nrm_combined;
                float2 node_9936 = mul( float4(lerp(normalDirection,node_5383,_NormalInfluence),0), UNITY_MATRIX_V ).xyz.rgb.rg;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (-1*float2((node_9936.r*_Refraction),(node_9936.g*_Refraction)));
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 directSpecular = attenColor * pow(max(0,dot(reflect(-lightDirection, normalDirection),viewDirection)),specPow)*specularColor;
                float3 indirectSpecular = (gi.indirect.specular)*specularColor;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                float node_3458 = 1.0;
                float node_7427 = 0.0;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_4659 = 0.0;
                float node_7971 = (1.0 - saturate((node_4659 + ( (partZ - node_4659) * (_DirtZDepth - node_4659) ) / (1.0 - node_4659))));
                float node_4969 = saturate(((_FrestnelPower + ( (pow(1.0-max(0,dot(normalDirection, viewDirection)),_FrestnelDeepness) - node_3458) * (node_7427 - _FrestnelPower) ) / (node_7427 - node_3458))+(_Color.a+((lerp( _MainTex_var.a, ((_MainTex_var.r+_MainTex_var.g)+_MainTex_var.b), _DirtAlphafromGrayscale )*node_7971)*_DirtOpacity))));
                float3 node_4496 = ((_LightColor0.rgb+node_4969)*_InnerLighting);
                float3 w = node_4496*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * node_4496;
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuseColor = saturate((1.0-(1.0-_Color.rgb)*(1.0-((_MainTex_var.rgb*node_7971)*_DirtOpacity))));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,node_4969),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Refraction;
            uniform float _FrestnelDeepness;
            uniform float _FrestnelPower;
            uniform float _InnerLighting;
            uniform float _NormalInfluence;
            uniform fixed _DirtAlphafromGrayscale;
            uniform float _DirtZDepth;
            uniform float _DirtOpacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                float4 projPos : TEXCOORD8;
                LIGHTING_COORDS(9,10)
                UNITY_FOG_COORDS(11)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = lerp(float3(0,0,5),_BumpMap_var.rgb,_NormalInfluence);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float3 node_5383_nrm_base = normalDirection + float3(0,0,1);
                float3 node_5383_nrm_detail = _BumpMap_var.rgb * float3(-1,-1,1);
                float3 node_5383_nrm_combined = node_5383_nrm_base*dot(node_5383_nrm_base, node_5383_nrm_detail)/node_5383_nrm_base.z - node_5383_nrm_detail;
                float3 node_5383 = node_5383_nrm_combined;
                float2 node_9936 = mul( float4(lerp(normalDirection,node_5383,_NormalInfluence),0), UNITY_MATRIX_V ).xyz.rgb.rg;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (-1*float2((node_9936.r*_Refraction),(node_9936.g*_Refraction)));
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 directSpecular = attenColor * pow(max(0,dot(reflect(-lightDirection, normalDirection),viewDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                float node_3458 = 1.0;
                float node_7427 = 0.0;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_4659 = 0.0;
                float node_7971 = (1.0 - saturate((node_4659 + ( (partZ - node_4659) * (_DirtZDepth - node_4659) ) / (1.0 - node_4659))));
                float node_4969 = saturate(((_FrestnelPower + ( (pow(1.0-max(0,dot(normalDirection, viewDirection)),_FrestnelDeepness) - node_3458) * (node_7427 - _FrestnelPower) ) / (node_7427 - node_3458))+(_Color.a+((lerp( _MainTex_var.a, ((_MainTex_var.r+_MainTex_var.g)+_MainTex_var.b), _DirtAlphafromGrayscale )*node_7971)*_DirtOpacity))));
                float3 node_4496 = ((_LightColor0.rgb+node_4969)*_InnerLighting);
                float3 w = node_4496*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * node_4496;
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float3 diffuseColor = saturate((1.0-(1.0-_Color.rgb)*(1.0-((_MainTex_var.rgb*node_7971)*_DirtOpacity))));
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * node_4969,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _DirtZDepth;
            uniform float _DirtOpacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float4 projPos : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_4659 = 0.0;
                float node_7971 = (1.0 - saturate((node_4659 + ( (partZ - node_4659) * (_DirtZDepth - node_4659) ) / (1.0 - node_4659))));
                float3 diffColor = saturate((1.0-(1.0-_Color.rgb)*(1.0-((_MainTex_var.rgb*node_7971)*_DirtOpacity))));
                float3 specColor = float3(_Specular,_Specular,_Specular);
                float roughness = 1.0 - _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
