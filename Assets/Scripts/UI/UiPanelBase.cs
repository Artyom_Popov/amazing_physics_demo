﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class UiPanelBase : MonoBehaviour
    {
        [SerializeField]
        protected GameObject _root;
        
        public virtual void SetActivePanel(bool value)
        {
            _root.SetActive(value);
        }        
    }
}
