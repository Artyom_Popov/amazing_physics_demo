﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AmazyingPhysics;

namespace AmazyingPhysics
{
    public class ExperimentPanel : UiPanelBase
    {
        public PhysicsCategorySettings PhysicsCategory;
        public ExperimentSettings Experiment;
        public BackSettings Back;
        public ApplySettings Apply;
        public AimTextSettings AimText;

        public void Constructor(Dictionary<int, string> physicsCategories, int[] physicsCategotyIds, Dictionary<int, Experiment> experiments)
        {
            PhysicsCategory.Constructor(physicsCategories);
            Experiment.Constructor(physicsCategotyIds, experiments);
        }

        [Serializable]
        public class PhysicsCategorySettings
        {
            [SerializeField]
            private GameObject _prefPhysicsCategoryElmnt;
            
            [SerializeField]
            private GameObject _physicsCategoryContainer;

            private int _currentPhysicsCategoryElmntId;

            public void Constructor(Dictionary<int, string> physicsCategories)
            {
                InitPhysicsCategoryPanel(physicsCategories);

                _currentPhysicsCategoryElmntId = -1;
            }

            private Dictionary<int, LabModuleElmnt> PhysicsCategoryElmnts { get; set; }

            public event Action<int> OnClickPhysicsCategory;

            private void InitPhysicsCategoryPanel(Dictionary<int, string> physicsCategories)
            {
                PhysicsCategoryElmnts = new Dictionary<int, LabModuleElmnt>();

                foreach (var item in physicsCategories)
                {
                    GameObject go = Instantiate(_prefPhysicsCategoryElmnt) as GameObject;
                    go.transform.SetParent(_physicsCategoryContainer.transform, false);

                    var elmnt = go.GetComponent<LabModuleElmnt>();
                    elmnt.SetId(item.Key);
                    elmnt.SetText(item.Value);
                    elmnt.SetColor(ColorManager.Instance.Normal2, ColorManager.Instance.Transparency);
                    elmnt.OnClick += PhysicsCategoryElmnt_OnClick;

                    PhysicsCategoryElmnts.Add(item.Key, elmnt);
                }
            }

            private void PhysicsCategoryElmnt_OnClick(int elmntId)
            {
                if (OnClickPhysicsCategory != null)
                    OnClickPhysicsCategory(elmntId);

                SelectPhysicsCategoryElmnt(elmntId);
            }

            private void SelectPhysicsCategoryElmnt(int elmntId)
            {
                if (_currentPhysicsCategoryElmntId != -1)
                {
                    PhysicsCategoryElmnts[_currentPhysicsCategoryElmntId].SetColor(ColorManager.Instance.Normal2, ColorManager.Instance.Transparency);
                }

                PhysicsCategoryElmnts[elmntId].SetColor(ColorManager.Instance.Select1, ColorManager.Instance.Normal1);

                _currentPhysicsCategoryElmntId = elmntId;
            }
        }

        [Serializable]
        public class ExperimentSettings
        {
            public GameObject _prefExperimentElmnt;

            [SerializeField]
            private GameObject _prefExperimentContainer;

            [SerializeField]
            private Transform _experimentContainer;

            private int _currentGroupExperimentId;
            private int _currentExperimentElmntId;
            private Dictionary<int, Transform> _experimentContainers;
            
            public void Constructor(int[] physicsCategotyIds, Dictionary<int, Experiment> experiments)
            {
                InitExperimentContainers(physicsCategotyIds, _prefExperimentElmnt, _experimentContainer);
                InitExperimentPanel(experiments);

                _currentExperimentElmntId = -1;
            }

            private Dictionary<int, LabWorkElmnt> ExperimentElmnts { get; set; }

            public event Action<int> OnClickExperiment;

            private void InitExperimentContainers(int[] physicsCategotyIds, GameObject prefab, Transform parent)
            {
                _experimentContainers = new Dictionary<int, Transform>();

                foreach (var item in physicsCategotyIds)
                {
                    var container = GetExperimentContainer(prefab, parent);
                    container.gameObject.SetActive(false);
                    _experimentContainers.Add(item, container);
                }
            }

            private Transform GetExperimentContainer(GameObject prefab, Transform parent)
            {
                var container = Instantiate(_prefExperimentContainer);
                container.transform.SetParent(parent, false);

                return container.transform;
            }

            private void InitExperimentPanel(Dictionary<int, Experiment> experiments)
            {
                ExperimentElmnts = new Dictionary<int, LabWorkElmnt>();
                
                foreach (var item in experiments)
                {
                    GameObject go = Instantiate(_prefExperimentElmnt) as GameObject;
                    go.transform.SetParent(_experimentContainers[item.Value.PhysicsCategoryId], false);

                    var elmnt = go.GetComponent<LabWorkElmnt>();
                    elmnt.SetId(item.Key);
                    elmnt.SetText(item.Value.NameRu);
                    elmnt.SetColor(ColorManager.Instance.Normal2, ColorManager.Instance.Transparency);
                    elmnt.OnClick += Elmnt_OnClickExperiment;
                    elmnt.SetInteractable(false);
                    
                    ExperimentElmnts.Add(item.Key, elmnt);
                }

                ExperimentElmnts[17].SetInteractable(true);
                ExperimentElmnts[18].SetInteractable(true);
                ExperimentElmnts[28].SetInteractable(true);
            }            

            private void Elmnt_OnClickExperiment(int elmntId)
            {
                if (OnClickExperiment != null)
                    OnClickExperiment(elmntId);

                SelectExperimentElmnt(elmntId);
            }

            public void SelectGroupExperiment(int physicsCategory)
            {
                if (_currentGroupExperimentId != -1)
                {
                    _experimentContainers[_currentGroupExperimentId].gameObject.SetActive(false);
                }

                _experimentContainers[physicsCategory].gameObject.SetActive(true);

                _currentGroupExperimentId = physicsCategory;
            }            

            private void SelectExperimentElmnt(int elmntId)
            {
                if (_currentExperimentElmntId != -1)
                {
                    ExperimentElmnts[_currentExperimentElmntId].SetColor(ColorManager.Instance.Normal2, ColorManager.Instance.Transparency);
                }

                ExperimentElmnts[elmntId].SetColor(ColorManager.Instance.Select1, ColorManager.Instance.Normal1);

                _currentExperimentElmntId = elmntId;
            }

            public bool IsSelectExperiment()
            {
                var value = _currentExperimentElmntId == -1 ? false : true;
                return value;
            }
        }

        [Serializable]
        public class BackSettings
        {
            [SerializeField]
            private ExperimentControlElmnt _backToMenuElmnt;
            
            public ExperimentControlElmnt BackToMenuElmnt
            {
                get
                {
                    return _backToMenuElmnt;
                }
            }
        }

        [Serializable]
        public class ApplySettings
        {
            [SerializeField]
            private ExperimentControlElmnt _startExperimentElmnt;
            
            public ExperimentControlElmnt StartExperimentElmnt
            {
                get
                {
                    return _startExperimentElmnt;
                }
            }
        }

        [Serializable]
        public class AimTextSettings
        {
            [SerializeField]
            private Text _txtAimExperiment;
            
            /// <summary>
            /// установить текст цели экспериментаы
            /// </summary>
            /// <param name="text"></param>
            public void SetTextAimExperiment(string text)
            {
                _txtAimExperiment.text = text;
            }
        }
    }
}
