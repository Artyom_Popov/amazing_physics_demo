﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

namespace AmazyingPhysics
{
    public class LabModuleElmnt : MonoBehaviour
    {
        [SerializeField]
        private Button _button;

        [SerializeField]
        private Text _text;
        
        private int _elmntId;
        
        public event Action<int> OnClick;

        private void Start()
        {
            //_button.gameObject.GetEventTrigger();
            //_button.gameObject.GetComponent<EventTrigger>().AddEventListener(EventTriggerType.PointerEnter, (BaseEventData b) => SetActiveLine(true));
            //_button.gameObject.GetComponent<EventTrigger>().AddEventListener(EventTriggerType.PointerExit, (BaseEventData b) => SetActiveLine(false));
            
            _button.onClick.AddListener(() =>
            {
                if (OnClick != null)
                    OnClick(_elmntId);
            });
        }

        public void SetId(int elmntId)
        {
            _elmntId = elmntId;
        }

        public void SetText(string text)
        {
            _text.text = text;
        }       

        public void SetColor(Color textColor, Color imageColor)
        {
            _text.color = textColor;
            _button.image.color = imageColor;
        }
    }
}