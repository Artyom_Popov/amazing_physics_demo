﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

namespace AmazyingPhysics
{
    public class ExperimentControlElmnt : MonoBehaviour
    {
        [SerializeField]
        private Button _button;

        [SerializeField]
        private Text _text;

        public event Action OnClick;

        private void Start()
        {
            _button.onClick.AddListener(() =>
            {
                if (OnClick != null)
                    OnClick();
            });
        }

        public void SetText(string text)
        {
            _text.text = text;
        }        
    }
}