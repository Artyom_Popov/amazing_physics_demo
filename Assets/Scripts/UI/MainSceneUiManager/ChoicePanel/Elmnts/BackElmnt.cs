﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;

namespace AmazyingPhysics
{
    public class BackElmnt : MonoBehaviour
    {
        [SerializeField] private Button _button;
        //[SerializeField] private GameObject _line;
        
        public event Action OnClickBack;

        private void Start()
        {
            //_button.gameObject.GetEventTrigger();
            //_button.gameObject.GetComponent<EventTrigger>().AddEventListener(EventTriggerType.PointerEnter, (BaseEventData b) => SetActiveLine(true));
            //_button.gameObject.GetComponent<EventTrigger>().AddEventListener(EventTriggerType.PointerExit, (BaseEventData b) => SetActiveLine(false));


            _button.onClick.AddListener(() =>
            {
                if (OnClickBack != null)
                    OnClickBack();
            });
        }

        public void SetActiveElmnt(bool value)
        {
            _button.gameObject.SetActive(value);
        }

        public void SetText(string text)
        {
            _button.GetComponentInChildren<Text>().text = text;
        }

        //public void SetActiveLine(bool value)
        //{
        //    _line.SetActive(value);
        //}        
    }
}