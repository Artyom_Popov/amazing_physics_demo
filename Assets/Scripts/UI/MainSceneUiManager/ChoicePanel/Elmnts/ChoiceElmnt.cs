﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;

namespace AmazyingPhysics
{
    public class ChoiceElmnt : MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private GameObject _line;

        private int _idElmnt;

        public event Action<int> OnClickChoice;

        private void Start()
        {
            _button.gameObject.GetEventTrigger();
            _button.gameObject.GetComponent<EventTrigger>().AddEventListener(EventTriggerType.PointerEnter, (BaseEventData b) => SetActiveLine(true));
            _button.gameObject.GetComponent<EventTrigger>().AddEventListener(EventTriggerType.PointerExit, (BaseEventData b) => SetActiveLine(false));


            _button.onClick.AddListener(() =>
            {
                if (OnClickChoice != null)
                    OnClickChoice(_idElmnt);
            });
        }

        public void SetText(string text)
        {
            _button.GetComponentInChildren<Text>().text = text;
        }
        
        public void SetID(int idElmnt)
        {
            _idElmnt = idElmnt;
        }

        public void SetActiveLine(bool value)
        {
            _line.SetActive(value);
        }        
    }
}