﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    [Serializable]
    public class Choice
    {
        public int ID;
        public string Name;
    }

    public class ChoicePanel : MonoBehaviour
    {
        [SerializeField] private GameObject _root;
        [SerializeField] private Transform _container;
        [SerializeField] private GameObject _prefChoiceElmnt;

        [SerializeField] private BackElmnt _backElmnt;

        private ChoiceElmnt[] _choiceElmnts = new ChoiceElmnt[0];

        private List<Choice> _data = new List<Choice>();
        
        public ChoiceElmnt[] ChoiceElmnts
        {
            get
            {
                return _choiceElmnts;
            }
        }

        public BackElmnt BackElmnt
        {
            get
            {
                return _backElmnt;
            }
        }

        public void SetActivePanel(bool value)
        {
            _root.SetActive(value);
            _backElmnt.SetActiveElmnt(!value);
        }

        public void InitChoice()
        {
            LoadData();

            _choiceElmnts = new ChoiceElmnt[_data.Count];
            
            for (int i = 0; i < _data.Count; i++)
            {
                GameObject go = Instantiate(_prefChoiceElmnt) as GameObject;
                go.transform.SetParent(_container, false);

                _choiceElmnts[i] = go.GetComponent<ChoiceElmnt>();
                _choiceElmnts[i].SetID(_data[i].ID);
                _choiceElmnts[i].SetText(_data[i].Name);   
                
                if(i == 1 || i == 3)
                {
                    GetDivider();
                }                       
            }

            _backElmnt.OnClickBack += () => SetActivePanel(true);
        } 
        
        private GameObject GetDivider()
        {
            GameObject divider = new GameObject("Divider");
            divider.transform.SetParent(_container, false);
            divider.AddComponent<LayoutElement>().preferredHeight = 30;

            return divider;
        }
        
        private void LoadData()
        {
            _data.Add(new Choice { ID = 1, Name = "ОПЫТЫ" });
            _data.Add(new Choice { ID = 2, Name = "ДЕМОНСТРАЦИИ" });
            _data.Add(new Choice { ID = 3, Name = "ЛИЧНЫЙ КАБИНЕТ" });
            _data.Add(new Choice { ID = 4, Name = "НАСТРОЙКИ" });
            _data.Add(new Choice { ID = 5, Name = "ВЫХОД" });

            SetActivePanel(true);
        }
    }
}
