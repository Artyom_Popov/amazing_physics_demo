﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmazyingPhysics
{
    public class MainSceneUIManager : MonoBehaviour
    {
        private ChoicePanel _choicePanel;
        private ExperimentPanel _labPanel;

        [SerializeField]
        private GameObject _root;

        private void Awake()
        {
            _choicePanel = FindChildTransform("ChoicePanel").GetComponent<ChoicePanel>();
            _labPanel = FindChildTransform("ExperimentPanel").GetComponent<ExperimentPanel>();            
        }

        public void SetActiveUI(bool value)
        {
            _root.SetActive(value);
        }

        public ExperimentPanel ExperimentPanel
        {
            get
            {
                return _labPanel;
            }
        }

        public ChoicePanel ChoicePanel
        {
            get
            {
                return _choicePanel;
            }
        }

        private Transform FindChildTransform(string name)
        {
            Transform[] transforms = GetComponentsInChildren<Transform>();
            Transform trans = null;

            foreach (var item in transforms)
            {
                if (item.name == name)
                {
                    trans = item;
                    break;
                }
            }

            if (trans == null)
                Debug.LogError("cant find obj " + name);

            return trans;
        }
    }
}
