﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;

namespace AmazyingPhysics
{
    public class TaskPanel : MonoBehaviour, IActivationPanel
    {
        #region Fields

        [SerializeField]
        private ManageSettings _settings;      

        [SerializeField]
        private Text _prefTextTask;

        [SerializeField]
        private GameObject _prefFormulaEditor;

        [SerializeField]
        private GameObject _prefVariableContent;

        [SerializeField]
        private Image _prefImage;

        [SerializeField]
        private Canvas _canvas;

        [SerializeField]
        private Font _myFont;

        [SerializeField]
        private NumerableManager _numerableManager;

        [SerializeField]
        private GameObject _conclusionUI;

        [SerializeField]
        private TaskChecker _taskChecker;

        private string _tagTable = "t"; // тег для инстанцирования таблицы
        private string _tagFormula = "f";// тег для инстанцирования формулы
        private string _tagImage = "i";// тег для инстанцирования изображения      

        public Transform _variablesContent { get; set; }

        //+++++++++++++++++++++++
        public ElementContainer _pElementContainer;

        public FormulaEditor _aFormulaEditor { get; private set; }
        public ElementContainer _aElementContainer { get; private set; }

        #endregion

        public void SetActivePanel(bool value)
        {
            _settings.root.SetActive(value);
        }

        public void Constructor(int experimentId, string[] task, TaskFormula[] taskFormula, FormulaOperator[] formulaOperator, FormulaOperand[] formulaOperand, TaskConclusion[] taskConclusion, TaskVariable[] taskVariable)
        {
            if(task == null)
            {
                return;
            }

            var trimSign = "\n".ToCharArray();

            for (int i = 0; i < task.Length; i++)
            {
                #region Formula
                if (task[i].StartsWith(_tagFormula))
                {
                    var trimData = "f[]".ToCharArray();

                    var id = task[i].Trim(trimData);

                    var formula = taskFormula
                        .Where(x => x.Id == id)
                        .First();

                    var go = Instantiate(_prefFormulaEditor);
                    go.transform.SetParent(_settings.scrollRect.content, false);

                    var varIds = formula.Variables;
                    varIds = varIds.Replace("\n", "");
                    char[] charVarIds = varIds.ToCharArray();

                    var formulaEditor = go.GetComponent<FormulaEditor>();

                    formulaEditor.Init(this, formula.Name + " = ", formula.Id);

                    _taskChecker.AddFormula(formulaEditor, formula.CheckNumber);

                    string varId = string.Empty;
                    bool isVarId = true;
                    string numId = string.Empty;
                    bool isNumId = false;
                    for (int j = 0; j < charVarIds.Length; j++)
                    {
                        if (charVarIds[j] == '~')
                        {
                            isNumId = true;
                            isVarId = false;
                        }
                        if (charVarIds[j] == ';')
                        {
                            varId = varId.Replace(";", "");
                            numId = numId.Replace("~", "");
                            formulaEditor.AddVariables(varId, numId);
                            varId = string.Empty;
                            numId = string.Empty;

                            isVarId = true;
                            isNumId = false;
                        }
                        if (isVarId)
                        {
                            varId += charVarIds[j];
                        }
                        if (isNumId)
                        {
                            numId += charVarIds[j];
                        }
                    }

                    formulaEditor.SetNumerableManager(_numerableManager);
                }
                #endregion

                #region Table
                else if (task[i].StartsWith(_tagTable))
                {
                    var trimData = "t[]".ToCharArray();
                    var tableId = task[i].Trim(trimData);

                    var table = Resources.Load("UI/Tables/" + tableId, typeof(GameObject)) as GameObject;

                    var go = Instantiate(table);
                    var _tableEditor = go.GetComponent<TableEditor>();
                    _tableEditor.Init(this, _numerableManager);

                    go.transform.SetParent(_settings.scrollRect.content, false);
                }
                #endregion

                #region Image
                else if (task[i].StartsWith(_tagImage))
                {
                    var trimData = "i[]".ToCharArray();
                    var imageId = task[i].Trim(trimData);

                    var image = _prefImage;
                    _prefImage.sprite = Resources.Load<Sprite>("UI/Images/img_" + imageId);

                    var go = Instantiate(image);

                    go.transform.SetParent(_settings.scrollRect.content, false);
                }
                #endregion

                #region Just Text
                else
                {
                    task[i] = task[i].TrimEnd(trimSign);
                    var text = Instantiate(_prefTextTask);
                    text.transform.SetParent(_settings.scrollRect.content, false);
                    text.text = task[i];
                }
                #endregion
            }

            InitVariablesForChecking(experimentId, taskVariable);
            InitConclusion(experimentId, taskConclusion);
            InitElementContainer(formulaOperator, formulaOperand);
            InitSettingsPanel();
        }

        private void InitVariablesForChecking(int experimentId, TaskVariable[] taskVariable)
        {
            foreach (var item in taskVariable)
            {
                if(item.ExperimentId == experimentId)
                {
                    _taskChecker.AddVariable(item);
                }
            }            
        }

        private void InitConclusion(int experimentId, TaskConclusion[] taskConclusion)
        {
            var conclusion = taskConclusion
                .Where(x => x.ExperimentId == experimentId)
                .FirstOrDefault();

            if(conclusion == null)
            {
                return;
            }
            var toggleGroup = gameObject.AddComponent<ToggleGroup>();

            var conclusions = new List<Transform>();

            for (int j = 3; j > 0; j--)
            {
                var go = Instantiate(_conclusionUI);
                conclusions.Add(go.transform);
                var toggle = go.GetComponent<Toggle>();
                var taskConclusionUI = go.GetComponent<ConclusionUI>();
                taskConclusionUI._taskChecker = _taskChecker;
                toggle.group = toggleGroup;

                toggle.onValueChanged.AddListener((bool on) => {
                    if (on)
                    {
                        taskConclusionUI.SetConclusion();
                    }
                });

                switch (j)
                {
                    case 3:
                        taskConclusionUI.SetText(conclusion.Right);
                        taskConclusionUI._isRight = true;
                        _taskChecker.SetRightConclusion(conclusion.Right);
                        break;
                    case 2:
                        taskConclusionUI.SetText(conclusion.Wrong1);
                        break;
                    case 1:
                        taskConclusionUI.SetText(conclusion.Wrong2);
                        break;
                    default:
                        break;
                }
            }

            while (conclusions.Count != 0)
            {
                var index = UnityEngine.Random.Range(0, conclusions.Count);
                conclusions[index].SetParent(_settings.scrollRect.content, false);
                conclusions.RemoveAt(index);
            }
        }

        private void InitElementContainer(FormulaOperator[] formulaOperator, FormulaOperand[] formulaOperand)
        {
            _aElementContainer = Instantiate(_pElementContainer);
            _aElementContainer._transform.SetParent(DynamicRoot.Instance.DynamicRootTransform, false);

            _aElementContainer.AddOperator("+", () => GetFormulaEditor().AddOperator("+"));
            _aElementContainer.AddOperator("-", () => GetFormulaEditor().AddOperator("-"));
            _aElementContainer.AddOperator("*", () => GetFormulaEditor().AddOperator("*"));
            _aElementContainer.AddOperator("/", () => GetFormulaEditor().AddDivision());
            _aElementContainer.AddOperator("√", () => GetFormulaEditor().AddRoot());
            _aElementContainer.AddOperator("sin", () => GetFormulaEditor().AddSin());
            _aElementContainer.AddOperator("x²", () => GetFormulaEditor().AddSquare());
            _aElementContainer.AddOperator("( )", () => GetFormulaEditor().AddBrackets());

            for (int i = 0; i < 10; i++)
            {
                var text = i.ToString();
                _aElementContainer.AddNumber(text, () => GetFormulaEditor().AddNumber(text));
            }

            for (int i = 0; i < formulaOperand.Length; i++)
            {
                var text = formulaOperand[i].Operand;
                var id = formulaOperand[i].Id.ToString();
                _aElementContainer.AddOperand(text, () => GetFormulaEditor().AddOperand(text, id));
            }

            _aElementContainer._OnContainerEnter += () => GetFormulaEditor().SetClickable(false);
            _aElementContainer._OnContainerExit += () => GetFormulaEditor().SetClickable(true);
        }

        private FormulaEditor GetFormulaEditor()
        {
            return _aFormulaEditor;
        }

        public void SetFormulaEditor(FormulaEditor formulaEditor)
        {
            _aFormulaEditor = formulaEditor;
        }

        private void InitSettingsPanel()
        {
            _settings.Constructor();
            _settings.btnShowHide.onClick.AddListener(() => StartCoroutine(_settings.CorShowHidePanel()));
        }

        /// <summary>
        /// установить фокус камеры на таблицу
        /// </summary>
        public void SetFocusTable()
        {
            StartCoroutine(CorSetFocusTable());
        }

        /// <summary>
        /// коротина установки фокуса камеры на таблицу
        /// </summary>
        /// <returns></returns>
        private IEnumerator CorSetFocusTable()
        {
            float velocity = -1;
            float smoothTime = 0.25f;

            while (velocity < -0.01f)
            {
                _settings.scrollRect.verticalScrollbar.value = Mathf.SmoothDamp(_settings.scrollRect.verticalScrollbar.value, 0, ref velocity, smoothTime);
                print(velocity);
                yield return null;
            }

            _settings.scrollRect.verticalScrollbar.value = 0;
        }

        [Serializable]
        public class ManageSettings
        {
            public GameObject root;
            public Text txtHeader;
            public ScrollRect scrollRect;
            public Button btnShowHide;
            public bool statePanel = true;

            private RectTransform rootTransform;
            private float maxPos;           

            public void Constructor()
            {
                btnShowHide.image.color = ColorManager.Instance.Color1;
                rootTransform = root.transform as RectTransform;
                maxPos = rootTransform.rect.width;
            }

            public IEnumerator CorShowHidePanel()
            {
                statePanel = !statePanel;

                float dampTime = .2f;
                float velocity = -1;               
                float currentPosX = rootTransform.anchoredPosition.x;               
                float targetPosX = statePanel ? maxPos : 0;
                btnShowHide.transform.localRotation = statePanel ? Quaternion.AngleAxis(180, btnShowHide.transform.forward) : Quaternion.AngleAxis(0, btnShowHide.transform.forward);
                while ((int)velocity != 0)
                {
                    currentPosX = Mathf.SmoothDamp(currentPosX, targetPosX, ref velocity, dampTime);
                    rootTransform.anchoredPosition = new Vector2(currentPosX, rootTransform.anchoredPosition.y);                   
                    yield return null;
                }

                rootTransform.anchoredPosition = new Vector2(targetPosX, rootTransform.anchoredPosition.y);                
            }
        }
    }
}
