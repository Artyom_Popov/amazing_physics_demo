﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContentController : MonoBehaviour {

    public RectTransform _rect { get; set; }

    public List<Unit> _units = new List<Unit>();

    //In future there is should be infinity actions, so this string [_id] for limited will be deleted.
    public string _id;

    private void Awake()
    {
        _rect = GetComponent<RectTransform>();
    }
}
