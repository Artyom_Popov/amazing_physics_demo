﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Caret : MonoBehaviour
{
    public RectTransform _rect { get; set; }

    private Image _caret;

    private IEnumerator _coroutine;

    void Awake()
    {
        _rect = GetComponent<RectTransform>();
        _caret = GetComponent<Image>();
        _coroutine = CoroutineAnimation();
    }

    public void Enable()
    {
        _caret.enabled = true;

        StartAnimation();
    }

    public void Disable()
    {
        StopAnimation();

        _caret.enabled = false;
    }

    public void StartAnimation()
    {
        StopAnimation();

        _coroutine = CoroutineAnimation();
        StartCoroutine(_coroutine);
    }

    public void StopAnimation()
    {
        StopCoroutine(_coroutine);
    }

    IEnumerator CoroutineAnimation()
    {
        while (true)
        {
            _caret.enabled = true;
            yield return new WaitForSeconds(0.6f);
            _caret.enabled = false;
            yield return new WaitForSeconds(0.6f);
        }
    }

    public void ToLeft(Unit unit)
    {
        _rect.SetParent(unit._rect, false);
        _rect.anchorMin = new Vector2(0.0f, 0.5f);
        _rect.anchorMax = new Vector2(0.0f, 0.5f);
        _rect.anchoredPosition = Vector3.zero;
        StartAnimation();
    }

    public void ToRight(Unit unit)
    {
        _rect.SetParent(unit._rect, false);
        _rect.anchorMin = new Vector2(1.0f, 0.5f);
        _rect.anchorMax = new Vector2(1.0f, 0.5f);
        _rect.anchoredPosition = Vector3.zero;
        StartAnimation();
    }

}
