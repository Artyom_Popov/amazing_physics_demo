﻿using UnityEngine;
using System.Collections.Generic;

public class Unit : MonoBehaviour {

    public RectTransform _rect { get; private set; }

    public Unit _parent { get; private set; }

    public List<Unit> _childs { get; private set; }

    private void Awake()
    {
        _rect = GetComponent<RectTransform>();
        _childs = new List<Unit>();
    }

    public void AddChild(Unit unit, int sibling)
    {
        _childs.Insert(sibling, unit);
        unit._rect.SetParent(_rect, false);
        unit._rect.SetSiblingIndex(sibling);
        unit.SetParent(this);
    }

    public void SetParent(Unit parent)
    {
        _parent = parent;
    }

    public void Delete()
    {
        _parent._childs.Remove(this);
        Destroy(gameObject);
    }
}
