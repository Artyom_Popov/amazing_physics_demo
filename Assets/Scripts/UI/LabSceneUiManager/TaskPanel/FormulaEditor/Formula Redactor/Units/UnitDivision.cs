﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDivision : Unit {
    public Unit _top;
    public Unit _bot;

    private void Start()
    {
        _top.SetParent(this);
        _bot.SetParent(this);
    }
}
