﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBrackets : Unit {
    public Unit _container;

    private void Start()
    {
        _container.SetParent(this);
    }
}
