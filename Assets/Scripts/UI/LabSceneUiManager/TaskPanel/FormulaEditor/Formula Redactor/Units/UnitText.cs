﻿using UnityEngine.UI;

public class UnitText : Unit {

    public Text _text;

    public string _id{ get; set; }

    public enum Type { isOperator, isNumber, isOperand};

    public Type _type { get; set; }

    public void SetText(string text)
    {
        _text.text = text;
    }

    public void SetType(Type type)
    {
        _type = type;
    }

    public void SetId(string id)
    {
        _id = id;
    }
}
