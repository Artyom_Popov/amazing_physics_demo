﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitOperator : Unit
{
    public Text _text;

    public string _id { get; set; }

    public void SetText(string text)
    {
        _text.text = text;
    }
}
