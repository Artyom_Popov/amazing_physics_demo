﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitOperand : Unit
{
    public Text _text;

    public string _id { get; set; }

    public void SetText(string text)
    {
        _text.text = text;
    }

    public void SetId(string id)
    {
        _id = id;
    }
}
