﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitRoot : Unit {
    public Unit _container;

    //delete in future
    public RectTransform _middleLineRect;

    private void Start()
    {
        _container.SetParent(this);
    }

    public void ReRotateLine()
    {
        _middleLineRect.rotation = Quaternion.Euler(0.0f, 0.0f, -7.0f);
    }
}
