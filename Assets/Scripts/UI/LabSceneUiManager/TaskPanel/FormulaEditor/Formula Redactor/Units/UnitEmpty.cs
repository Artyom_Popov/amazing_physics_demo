﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitEmpty : Unit {

    public Image _image;
    public LayoutElement _layout;

    public void SetWidth(float width)
    {
        _layout.preferredWidth = width;
    }

    public void SetHeight(float height)
    {
        _layout.preferredHeight = height;
    }

    public void SetSize(float width, float height)
    {
        _layout.preferredWidth = width;
        _layout.preferredHeight = height;
    }

    public void SetColor(Color32 color)
    {
        _image.color = color;
    }
}
