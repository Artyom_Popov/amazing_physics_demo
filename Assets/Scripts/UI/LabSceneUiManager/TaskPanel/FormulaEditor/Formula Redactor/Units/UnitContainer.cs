﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitContainer : Unit {

    public UnitEmpty _pUnitEmpty;
    public float _width;
    public float _height;
    public Color32 color;

    public UnitEmpty AddUnitEmpty()
    {
        var unitEmpty = Instantiate(_pUnitEmpty);
        AddChild(unitEmpty, 0);
        unitEmpty.SetSize(_width, _height);
        unitEmpty.SetColor(color);
        return unitEmpty;
    }
}
