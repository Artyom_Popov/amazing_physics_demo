﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AmazyingPhysics;

public class FormulaEditor : MonoBehaviour
{
    public Image _image;
    public Button _button;
    public Text _text;
    public Caret _caret;
    public Transform _elementParent;

    private TaskPanel _taskPanel;
    public string _id { get; private set; }

    private bool _isEnable;
    private bool _isClickable;

    public UnitOperator _pUnitOperator;
    public UnitNumber _pUnitNumber;
    public UnitOperand _pUnitOperand;
    public UnitDivision _pUnitDivision;
    public UnitRoot _pUnitRoot;
    public UnitSin _pUnitSin;
    public UnitSquare _pUnitSquare;
    public UnitBrackets _pUnitBrackets;

    public UnitContainer _content;

    private Unit _aParent;
    private Unit _aChild;

    private IEnumerator _delete;

    enum CheckedUnit { isNull, isSimple, isEmpty, isLeftNull, isLeftRightNull };

    public void Init(TaskPanel taskPanel, string text, string id)
    {
        _taskPanel = taskPanel;
        _text.text = text;
        _id = id;
    }

    public void SetClickable(bool isClickable)
    {
        _isClickable = isClickable;
    }

    private void Start()
    {
        _button.onClick.AddListener(() => StartSolve());

        _aParent = _content;
        _aChild = _content.AddUnitEmpty();
    }

    private void Update()
    {
        if (_isEnable)
        {
            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                _delete = BackSpace();

                StartCoroutine(_delete);
            }

            if (Input.GetKeyUp(KeyCode.Backspace))
            {
                StopCoroutine(_delete);
            }

            if (_isClickable)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    SelectUnit();
                }
            }
        }
    }

    public void PointerDown() {

        var unitEmpty = _content._childs[0] as UnitEmpty;

        if (unitEmpty == null)
        {
            if (!_isEnable)
            {
                if (_taskPanel._aFormulaEditor != null)
                {
                    _taskPanel._aFormulaEditor.EndSolve();
                }

                _isEnable = true;

                _taskPanel.SetFormulaEditor(this);

                _taskPanel._aElementContainer._transform.SetParent(_elementParent, false);

                SelectUnit();
            }
        }
    }

    public void StartSolve()
    {
        _isEnable = true;

        if (_taskPanel._aFormulaEditor != null)
        {
            _taskPanel._aFormulaEditor.EndSolve();
        }

        _taskPanel.SetFormulaEditor(this);

        _taskPanel._aElementContainer._transform.SetParent(_elementParent, false);

        _button.gameObject.SetActive(false);

        _image.enabled = true;

        SelectUnit();
    }

    public void EndSolve()
    {
        _isEnable = false;

        var unitEmpty = _content._childs[0] as UnitEmpty;

        if (unitEmpty != null)
        {
            _button.gameObject.SetActive(true);

            _image.enabled = false;
        }

        _caret.Disable();
    }

    public Unit AddUnit(Unit unit)
    {
        var child = Instantiate(unit);

        var checkedUnit = Check(_aChild);

        if (checkedUnit == CheckedUnit.isNull)
        {
            _aParent.AddChild(child, 0);
            _aChild = child;
            _caret.ToRight(child);
        }
        if (checkedUnit == CheckedUnit.isSimple)
        {
            _aParent.AddChild(child, _aChild._rect.GetSiblingIndex() + 1);
            _aChild = child;
            _caret.ToRight(child);
        }
        if (checkedUnit == CheckedUnit.isEmpty)
        {
            _aChild.Delete();
            _aParent.AddChild(child, 0);
            _aChild = child;
            _caret.ToRight(child);
        }
        if (checkedUnit == CheckedUnit.isLeftNull)
        {
            _aParent.AddChild(child, _aChild._rect.GetSiblingIndex() + 1);
            _aChild = child;
            _caret.ToRight(child);
        }
        if (checkedUnit == CheckedUnit.isLeftRightNull)
        {
            _aParent.AddChild(child, _aChild._rect.GetSiblingIndex() + 1);
            _aChild = child;
            _caret.ToRight(child);
        }

        return child;
    }

    private void SelectUnit()
    {
        var mousePosition = Input.mousePosition;
        var oldDistance = float.PositiveInfinity;

        var units = GetUnitsFrom(_content);

        var isLeft = false;
        var isRight = false;

        Unit gottedUnit = null;

        for (int i = 0; i < units.Count; i++)
        {
            var unit = units[i];

            var sibling = unit._rect.GetSiblingIndex();

            // cheking | LEFT position for every unit in the content
            var width = unit._rect.rect.width / 2;
            var position = unit._rect.TransformPoint(new Vector2(-width, 0.0f));
            var newDistance = Vector2.Distance(mousePosition, position);

            if (newDistance < oldDistance)
            {
                oldDistance = newDistance;

                isLeft = true;
                isRight = false;

                gottedUnit = unit;
            }

            // cheking | RIGHT position for last unit in the content
            if (sibling == unit._parent._rect.childCount - 1)
            {
                position = unit._rect.TransformPoint(new Vector2(width, 0.0f));
                newDistance = Vector2.Distance(mousePosition, position);

                if (newDistance < oldDistance)
                {
                    oldDistance = newDistance;

                    isLeft = false;
                    isRight = true;

                    gottedUnit = unit;
                }
            }
        }

        var checkedUnit = Check(gottedUnit);

        if (checkedUnit == CheckedUnit.isSimple)
        {
            if (isLeft)
            {
                var sibling = gottedUnit._parent._childs.IndexOf(gottedUnit);
                var child = gottedUnit._parent._childs[sibling - 1];

                _aParent = child._parent;
                _aChild = child;

                _caret.ToRight(_aChild);
            }
            if (isRight)
            {
                _aParent = gottedUnit._parent;
                _aChild = gottedUnit;

                _caret.ToRight(_aChild);
            }
        }
        if (checkedUnit == CheckedUnit.isEmpty)
        {
            _aParent = gottedUnit._parent;
            _aChild = gottedUnit;

            _caret.ToLeft(_aChild);
        }
        if (checkedUnit == CheckedUnit.isLeftNull)
        {
            _aParent = gottedUnit._parent;
            _aChild = null;

            _caret.ToLeft(gottedUnit);
        }
        if (checkedUnit == CheckedUnit.isLeftRightNull)
        {
            if (isLeft)
            {
                _aParent = gottedUnit._parent;
                _aChild = null;

                _caret.ToLeft(gottedUnit);
            }
            if (isRight)
            {
                _aParent = gottedUnit._parent;
                _aChild = gottedUnit;

                _caret.ToRight(_aChild);
            }
        }
    }

    private void DeleteUnit()
    {
        var checkedUnit = Check(_aChild);

        if (checkedUnit == CheckedUnit.isSimple)
        {
            var sibling = _aChild._parent._childs.IndexOf(_aChild);
            var child = _aChild._parent._childs[sibling - 1];

            _aChild.Delete();
            _aChild = child;

            _caret.ToRight(child);
        }
        if (checkedUnit == CheckedUnit.isLeftNull)
        {
            var sibling = _aChild._parent._childs.IndexOf(_aChild);
            var child = _aChild._parent._childs[sibling + 1];

            _aChild.Delete();

            _caret.ToLeft(child);
        }
        if (checkedUnit == CheckedUnit.isLeftRightNull)
        {
            var container = _aParent as UnitContainer;
            var child = container.AddUnitEmpty();

            _aChild.Delete();
            _aChild = child;

            _caret.ToLeft(child);
        }


        SetFormula();
    }

    private List<Unit> GetUnitsFrom(Unit Unit)
    {
        var unitsList = new List<Unit>();

        foreach (var unit in Unit._childs)
        {
            var unitOperator = unit as UnitOperator;
            var unitNumber = unit as UnitNumber;
            var unitOperand = unit as UnitOperand;
            var unitDivision = unit as UnitDivision;
            var unitRoot = unit as UnitRoot;
            var unitSin = unit as UnitSin;
            var unitSquare = unit as UnitSquare;
            var unitBrackets = unit as UnitBrackets;
            var unitEmpty = unit as UnitEmpty;

            if (unitOperator != null)
            {
                unitsList.Add(unitOperator);
            }

            if (unitNumber != null)
            {
                unitsList.Add(unitNumber);
            }

            if (unitOperand != null)
            {
                unitsList.Add(unitOperand);
            }

            if (unitDivision != null)
            {
                unitsList.Add(unitDivision);

                var topList = GetUnitsFrom(unitDivision._top);
                var botList = GetUnitsFrom(unitDivision._bot);

                foreach (var item in topList)
                {
                    unitsList.Add(item);
                }

                foreach (var item in botList)
                {
                    unitsList.Add(item);
                }
            }

            if (unitRoot != null)
            {
                unitsList.Add(unitRoot);

                var list = GetUnitsFrom(unitRoot._container);

                foreach (var item in list)
                {
                    unitsList.Add(item);
                }
            }

            if (unitSin != null)
            {
                unitsList.Add(unitSin);

                var list = GetUnitsFrom(unitSin._container);

                foreach (var item in list)
                {
                    unitsList.Add(item);
                }
            }

            if (unitBrackets != null)
            {
                unitsList.Add(unitBrackets);

                var list = GetUnitsFrom(unitBrackets._container);

                foreach (var item in list)
                {
                    unitsList.Add(item);
                }
            }

            if (unitSquare != null)
            {
                unitsList.Add(unitSquare);
            }

            if (unitEmpty != null)
            {
                unitsList.Add(unitEmpty);
            }
        }

        return unitsList;
    }

    private CheckedUnit Check(Unit unit)
    {
        CheckedUnit checkedUnit = CheckedUnit.isNull;

        if (unit != null)
        {
            checkedUnit = CheckedUnit.isSimple;

            var unitEmpty = unit as UnitEmpty;
            if (unitEmpty != null)
            {
                checkedUnit = CheckedUnit.isEmpty;
            }
            else
            {
                var sibling = unit._parent._childs.IndexOf(unit);
                var count = unit._parent._childs.Count;

                if (sibling == 0 && count > 1)
                {
                    checkedUnit = CheckedUnit.isLeftNull;
                }
                if (sibling == 0 && count == 1)
                {
                    checkedUnit = CheckedUnit.isLeftRightNull;
                }
            }
        }

        return checkedUnit;
    }

    IEnumerator BackSpace()
    {
        DeleteUnit();

        yield return new WaitForSeconds(0.3f);

        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            DeleteUnit();
        }
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++

    private Dictionary<string, string> _variables = new Dictionary<string, string>();
    private NumerableManager _numerableManager;

    public void AddOperator(string text)
    {
        if (_aChild != null)
        {
            var unit = _aChild as UnitOperator;

            if (unit != null)
            {
                return;
            }
        }

        var unitOperator = AddUnit(_pUnitOperator) as UnitOperator;
        unitOperator.SetText(text);

        SetFormula();
    }

    public void AddNumber(string text)
    {
        var unitNumber = AddUnit(_pUnitNumber) as UnitNumber;

        unitNumber.SetText(text);

        SetFormula();
    }

    public void AddOperand(string text, string id)
    {
        var unitOperand = AddUnit(_pUnitOperand) as UnitOperand;
        unitOperand.SetText(text);
        unitOperand.SetId(id);

        SetFormula();
    }

    public void AddDivision()
    {
        if (_aChild != null)
        {
            var unitContainer = _aChild._parent as UnitContainer;

            if (unitContainer != null)
            {
                var parentDivision = unitContainer._parent as UnitDivision;
                var parentRoot = unitContainer._parent as UnitRoot;

                if (parentDivision != null)
                {
                    return;
                }
                if (parentRoot != null)
                {
                    //return;
                    parentRoot.ReRotateLine();
                }
            }
        }

        var unitDivision = AddUnit(_pUnitDivision) as UnitDivision;

        var topContainer = unitDivision._top as UnitContainer;
        var childTop = topContainer.AddUnitEmpty();

        var botContainer = unitDivision._bot as UnitContainer;
        var childBot = botContainer.AddUnitEmpty();

        _aParent = unitDivision._top;
        _aChild = childTop;

        _caret.ToLeft(childTop);

        SetFormula();
    }

    public void AddRoot()
    {
        if (_aChild != null)
        {
            var unitContainer = _aChild._parent as UnitContainer;

            if (unitContainer != null)
            {
                var parentDivision = unitContainer._parent as UnitDivision;
                var parentRoot = unitContainer._parent as UnitRoot;

                if (parentDivision != null)
                {
                    return;
                }
                if (parentRoot != null)
                {
                    return;
                }
            }
        }

        var unitRoot = AddUnit(_pUnitRoot) as UnitRoot;

        var container = unitRoot._container as UnitContainer;
        var child = container.AddUnitEmpty();

        _aParent = container;
        _aChild = child;

        _caret.ToLeft(child);

        SetFormula();
    }

    public void AddSin()
    {
        var unitSin = AddUnit(_pUnitSin) as UnitSin;

        var container = unitSin._container as UnitContainer;
        var child = container.AddUnitEmpty();

        _aParent = container;
        _aChild = child;

        _caret.ToLeft(child);

        SetFormula();
    }

    public void AddSquare()
    {
        if (_aChild != null)
        {
            var aUnitOperator = _aChild as UnitOperator;
            var aUnitContainer = _aChild._parent as UnitContainer;
            var aUnitSquare = _aChild as UnitSquare;
            var aUnitEmpty = _aChild as UnitEmpty;

            if (aUnitOperator != null)
            {
                return;
            }

            if (aUnitContainer != null)
            {
                var parentRoot = aUnitContainer._parent as UnitRoot;

                if (parentRoot != null)
                {
                    return;
                }
            }

            if(aUnitSquare != null)
            {
                return;
            }

            if(aUnitEmpty != null)
            {
                return;
            }
        }

        var unitSquare = AddUnit(_pUnitSquare) as UnitSquare;

        SetFormula();
    }

    public void AddBrackets()
    {
        var unitBrackets = AddUnit(_pUnitBrackets) as UnitBrackets;

        var container = unitBrackets._container as UnitContainer;
        var child = container.AddUnitEmpty();

        _aParent = container;
        _aChild = child;

        _caret.ToLeft(child);

        SetFormula();
    }

    public void AddVariables(string varId, string numId)
    {
        _variables.Add(varId, numId);
    }

    public void SetNumerableManager(NumerableManager numerableManager)
    {
        _numerableManager = numerableManager;
    }

    private void SetFormula()
    {
        _numerableManager.AddFormula(_id, GetFormula(_content, false));
        _numerableManager.ChangeValues();
    }

    public string Check()
    {
        return GetFormula(_content, true);
    }

    private string GetFormula(Unit container, bool isCheck)
    {
        string formula = null;

        foreach (var unit in container._childs)
        {
            Unit pUnit = null;

            var sibling = unit._parent._childs.IndexOf(unit);
            if (sibling > 0)
            {
                pUnit = unit._parent._childs[sibling - 1];
            }

            var pUnitOperator = pUnit as UnitOperator;
            var pUnitNumber = pUnit as UnitNumber;
            var pUnitOperand = pUnit as UnitOperand;
            var pUnitDivision = pUnit as UnitDivision;
            var pUnitRoot = pUnit as UnitRoot;
            var pUnitSin = pUnit as UnitSin;
            var pUnitSquare = pUnit as UnitSquare;
            var pUnitBrackets = pUnit as UnitBrackets;
            var pUnitEmpty = pUnit as UnitEmpty;

            var unitOperator = unit as UnitOperator;
            var unitNumber = unit as UnitNumber;
            var unitOperand = unit as UnitOperand;
            var unitDivision = unit as UnitDivision;
            var unitRoot = unit as UnitRoot;
            var unitSin = unit as UnitSin;
            var unitSquare = unit as UnitSquare;
            var unitBrackets = unit as UnitBrackets;
            var unitEmpty = unit as UnitEmpty;

            if (unitOperator != null)
            {
                formula += unitOperator._text.text;
            }

            if (unitNumber != null)
            {
                if (pUnitOperand != null || pUnitDivision != null || pUnitRoot != null || pUnitSin != null || pUnitSquare != null || pUnitBrackets != null)
                {
                    formula += "*";
                }

                formula += unitNumber._text.text;
            }

            if (unitOperand != null)
            {
                if (pUnitOperand != null || pUnitNumber != null || pUnitDivision || pUnitRoot != null || pUnitSin != null || pUnitSquare != null || pUnitBrackets != null)
                {
                    formula += "*";
                }

                if (isCheck)
                {
                    formula += "(" + unitOperand._id + "+ 100)";
                }
                else
                {
                    formula += "(" + _variables[unitOperand._id] + ")";
                }
            }

            if (unitDivision != null)
            {
                if (pUnitOperand != null || pUnitNumber != null || pUnitDivision || pUnitRoot != null || pUnitSin != null || pUnitSquare != null || pUnitBrackets != null)
                {
                    formula += "*";
                }

                var topText = GetFormula(unitDivision._top, isCheck);
                var botText = GetFormula(unitDivision._bot, isCheck);

                if (topText == null || botText == null)
                {
                    return null;
                }

                var text = "(" + "(" + topText + ")" + "/" + "(" + botText + ")" + ")";

                formula += text;
            }

            if (unitRoot != null)
            {
                if (pUnitOperand != null || pUnitNumber != null || pUnitDivision || pUnitRoot != null || pUnitSin != null || pUnitSquare != null || pUnitBrackets != null)
                {
                    formula += "*";
                }

                var containerText = GetFormula(unitRoot._container, isCheck);

                if (containerText == null)
                {
                    return null;
                }

                var text = '(' + "sqrt(" + containerText + ')' + ')';

                formula += text;
            }

            if (unitSin != null)
            {
                if (pUnitOperand != null || pUnitNumber != null || pUnitDivision || pUnitRoot != null || pUnitSin != null || pUnitSquare != null || pUnitBrackets != null)
                {
                    formula += "*";
                }

                var containerText = GetFormula(unitSin._container, isCheck);

                if (containerText == null)
                {
                    return null;
                }

                var text = '(' + "sin(" + containerText + ')' + ')';

                formula += text;
            }


            if (unitSquare != null)
            {
                var text = "^2";

                formula += text;
            }

            if (unitBrackets != null)
            {
                if (pUnitOperand != null || pUnitNumber != null || pUnitDivision || pUnitRoot != null || pUnitSin != null || pUnitSquare != null || pUnitBrackets != null)
                {
                    formula += "*";
                }

                var containerText = GetFormula(unitBrackets._container, isCheck);

                if (containerText == null)
                {
                    return null;
                }

                var text = '(' + "(" + containerText + ')' + ')';

                formula += text;
            }

            if (unitEmpty != null)
            {
                return null;
            }
        }

        //delete in future
        var fUnitOperator = container._childs[0] as UnitOperator;
        if (fUnitOperator != null)
        {
            return null;
        }

        //delete in future
        var lUnitOperator = container._childs[container._childs.Count - 1] as UnitOperator;
        if(lUnitOperator != null)
        {
            return null;
        }

        //delete in future 
        var fUnitSquare = container._childs[0] as UnitSquare;
        if (fUnitSquare != null)
        {
            return null;
        }

        return formula;
    }
}
