﻿using System;
using UnityEngine;
using AmazyingPhysics;

public class ElementContainer : MonoBehaviour
{
    public Transform _transform;

    public Transform _containerOperator;
    public Transform _containerNumber;
    public Transform _containerOperand;

    public Color32 _colorOperator;
    public Color32 _colorNumber;
    public Color32 _colorOperand;

    public ElementButton _pElementButton;

    public event Action _OnContainerEnter;
    public event Action _OnContainerExit;

    public void AddOperator(string text, Action action)
    {
        Instantiate(_pElementButton, _containerOperator).InitElement(_colorOperator, text, action);
    }

    public void AddNumber(string text, Action action)
    {
        Instantiate(_pElementButton, _containerNumber).InitElement(_colorNumber, text, action);
    }

    public void AddOperand(string text, Action action)
    {
        Instantiate(_pElementButton, _containerOperand).InitElement(_colorOperand, text, action);
    }

    public void OnPointerEnter()
    {
        if(_OnContainerEnter != null)
        {
            _OnContainerEnter();
        }
    }

    public void OnPointerExit()
    {
        if (_OnContainerExit != null)
        {
            _OnContainerExit();
        }
    }
}
