﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ElementButton : MonoBehaviour
{
    public event Action OnButtonDown;

    public Image _image;

    public Text _text;

    public void OnPointerDown()
    {
        if (OnButtonDown != null)
        {
            OnButtonDown();
        }
    }

    public void InitElement(Color32 color, string text, Action action)
    {
        _image.color = color;
        _text.text = text;
        OnButtonDown += action;
    }
}
