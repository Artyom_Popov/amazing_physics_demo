﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NumerableManager : MonoBehaviour {
    private Dictionary<string, double> _variables = new Dictionary<string, double>();
    private Dictionary<string, string> _formuls = new Dictionary<string, string>();
    private Dictionary<string, string> _textedVariabled = new Dictionary<string, string>();

    public event Action OnChange;

    public void ChangeValues()
    {
        if (OnChange != null)
        {
            OnChange();
        }
    }

    public void AddNumber(string id, double number)
    {
        if (_variables.ContainsKey(id))
        {
            _variables[id] = number;
        }
        else
        {
            _variables.Add(id, number);
        }
    }

    public double GetNumber(string id, string lineId = "")
    {
        double number = 0.0;

        if (_variables.ContainsKey(id))
        {
            number = _variables[id];
        }
        else
        {
            if (_formuls.ContainsKey(id))
            {
                number = ReplaceAction(_formuls[id], lineId);
            }
        }

        return number;
    }

    public void AddFormula(string id, string formula)
    {
        if (_formuls.ContainsKey(id))
        {
            _formuls[id] = formula;
        }
        else
        {
            _formuls.Add(id, formula);
        }
    }

    public string GetFormula(string id)
    {
        string formula = string.Empty;

        if (_formuls.ContainsKey(id))
        {
            formula = _formuls[id];
        }

        return formula;
    }

    public void AddTextedVariable(string id, string text)
    {
        if (_textedVariabled.ContainsKey(id))
        {
            _textedVariabled[id] = text;
        }
        else
        {
            _textedVariabled.Add(id, text);
        }
    }

    public string GetTextedVariable(string id)
    {
        string text = null;

        if (_textedVariabled.ContainsKey(id))
        {
            text = _textedVariabled[id];
        }

        return text;
    }

    public double ReplaceAction(string action, string lineId = "")
    {
        if (action == "" || action == null)
        {
            return 0;
        }

        action = ReplaceId(action, lineId);

        var number = SolveString(action);

        return number;
    }

    private string ReplaceId(string action, string lineId = "")
    {
        char[] chars = action.ToCharArray();

        bool isId = false;
        string toReplace = string.Empty;
        for (int i = 0; i < chars.Length; i++)
        {
            if (chars[i] == '[')
            {
                isId = true;
            }

            if (chars[i] == ']')
            {
                toReplace += ']';
                isId = false;

                var id = toReplace.Replace("[", "").Replace("]", "");
                var number = 0.0;

                if (!id.Contains("l"))
                {
                    print(id + lineId + " " + GetNumber(id + lineId));
                    number = GetNumber(id + lineId);
                }
                else
                {
                    number = GetNumber(id, lineId);
                }

                action = action.Replace(toReplace, number.ToString());

                toReplace = string.Empty;
            }

            if (isId)
            {
                toReplace += chars[i];
            }
        }

        return action;
    }

    public double SolveString(string action)
    {
        if(action == null)
        {
            return 0.0;
        }

        AK.ExpressionSolver solver = new AK.ExpressionSolver();
        double number = solver.EvaluateExpression(action); // Also returns 2.0

        if (double.IsNaN(number))
        {
            number = 0;
        }
        if (double.IsInfinity(number))
        {
            number = 0;
        }

        return number;
    }
}
