﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormulaNumeration : MonoBehaviour {
    public NumerableManager _numerableManager;

    public Dictionary<string, string> _variables = new Dictionary<string, string>();

    public string Numerate(Transform content, bool isCheck = false)
    {
        var formula = string.Empty;

        for (int i = 0; i < content.childCount; i++)
        {
            var action = content.GetChild(i).name;

            var beforeAction = string.Empty;
            var nextAction = string.Empty;

            if (i != 0)
            {
                beforeAction = content.GetChild(i - 1).name;
            }

            if (i != content.childCount - 1)
            {
                nextAction = content.GetChild(i + 1).name;
            }

            if (action.Contains("Number:"))
            {
                action = action.Replace("Number:", "");

                if (!beforeAction.Contains("Number:"))
                {
                    action = '(' + action;
                }
                if (!nextAction.Contains("Number:"))
                {
                    action = action + ')';
                }

                if (beforeAction == "Square")
                {
                    action = '*' + action;
                }
            }

            if (action.Contains("ID:"))
            {
                action = action.Replace("ID:", "");
                if (!isCheck)
                {
                    action = '(' + _variables[action] + ')';
                }
                if (isCheck)
                {
                    action = '(' + action + "100" + ')';
                }

                if (beforeAction == "Square")
                {
                    action = '*' + action;
                }
            }

            if (action == "Division")
            {
                action = string.Empty;

                var division = content.GetChild(i);

                var top = division.GetChild(1);
                var bot = division.GetChild(2);

                if (top.childCount > 0 && bot.childCount > 0)
                {
                    var topAction = string.Empty;
                    var botAction = string.Empty;

                    topAction = '(' + Numerate(top, isCheck) + ')';

                    botAction = '(' + Numerate(bot, isCheck) + ')';

                    action = '(' + topAction + '/' + botAction + ')';
                }
            }

            if (action == "Square")
            {
                action = string.Empty;

                action += "^2";
            }

            if (action == "Radical")
            {
                action = string.Empty;

                var radical = content.GetChild(i).GetChild(3);

                action = Numerate(radical, isCheck);

                action = '(' + "sqrt(" + action + ')' + ')';
            }

            if(action == "sin(")
            {
                action = "(sin(";
                if(nextAction == ")")
                {
                    return "0";
                }
            }

            if(action == "(")
            {
                action = "((";
                if (nextAction == ")")
                {
                    return "0";
                }
            }

            if(action == ")")
            {
                action = "))";
            }

            formula += action;
        }

        formula = formula.Replace(")(", ")*(");

        formula = formula.Replace("*)", ")").Replace("-)", ")").Replace("+)", ")");

        formula = formula.Replace("()", "0").Replace("()", "0");

        if (formula.Contains("++") || formula.Contains("--") || formula.Contains("**"))
        {
            formula = ReplaceSimpleActions(formula);
        }

        if(formula != "")
        {
            var firstC = formula[0];
            if (firstC == '*')
            {
                return "0";
            }
        }

        if(formula != "")
        {
            var lastC = formula[formula.Length - 1];
            if (lastC == '+' || lastC == '-' || lastC == '*')
            {
                return "0";
            }
        }

        return formula;
    }

    private string ReplaceSimpleActions(string formula)
    {
        bool isDone = true;

        if (formula.Contains("--"))
        {
            isDone = false;
            formula = formula.Replace("--", "+");
        }

        if (formula.Contains("+-"))
        {
            isDone = false;
            formula = formula.Replace("+-", "-");
        }

        if (formula.Contains("-+"))
        {
            isDone = false;
            formula = formula.Replace("-+", "-");
        }

        if (formula.Contains("++"))
        {
            isDone = false;
            formula = formula.Replace("++", "+");
        }

        if (!isDone)
        {
            formula = ReplaceSimpleActions(formula);
            isDone = true;
        }

        return formula;
    }


}