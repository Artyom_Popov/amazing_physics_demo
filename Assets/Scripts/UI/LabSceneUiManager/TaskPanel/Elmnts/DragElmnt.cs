﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using AmazyingPhysics.Core;
using System.Collections;

namespace AmazyingPhysics
{
    public class DragElmnt : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private GameObject _root;
        [SerializeField] private Image _imgIcon;
        [SerializeField] private Image _imgBackground;

        public event Action OnClick;

        private bool _isClick;

        public void OnPointerDown(PointerEventData eventData)
        {
            StopAllCoroutines();
            StartCoroutine(CorClickOnElmnt());
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _isClick = false;
        }

        public void SetActivePanel(bool value)
        {
            _root.SetActive(value);
            _imgBackground.enabled = !value;
            _imgIcon.enabled = !value;
        }

        public void SetImageBackground(Sprite sprite)
        {
            _imgBackground.sprite = sprite;
        }

        private IEnumerator CorClickOnElmnt()
        {
            _isClick = true;

            yield return new WaitForSeconds(.15f);

            if(!_isClick)
            {
                if (OnClick != null)
                    OnClick();

                SetActivePanel(true);
            }
        }
    }
}
