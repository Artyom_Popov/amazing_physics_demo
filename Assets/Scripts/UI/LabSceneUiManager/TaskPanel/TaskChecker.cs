﻿using UnityEngine;
using System.Collections.Generic;
using AmazyingPhysics;

public class TaskChecker : MonoBehaviour {
    public NumerableManager _numerableManager;
    public TableResultPanel _tableResultPanel;

    private float _resultPercentage;

    private float _formulaPercentage;
    private float _tablePercentage;
    private float _conclusionPercantage; 

    //Class for saving data of formula with boxes and number for checking;
    private class Formula
    {
        public FormulaEditor formulaEditor;
        public string target;
    }
    //Saves Formulas Results
    private List<Formula> _formulas = new List<Formula>();

    //Class for saving data of variable with min and max value;
    private class Variable
    {
        public string variable;
        public string view;
        public float min;
        public float max;
        public float target;
        public string measurement;
        public string text;
    }

    //Saves Variables Results
    private List<Variable> _variables = new List<Variable>();

    //Class for saving data of variables with actual and right text;
    private class Conclusion
    {
        public string actual;
        public string right;
        public bool isRight;
    }

    //Saves Conclusion Result
    private Conclusion _conclusion = new Conclusion();

    private void Start()
    {
        _tableResultPanel.OnClick += SetResults;
    }

    private void OnDestroy()
    {
        _tableResultPanel.OnClick -= SetResults;
    }

    //Add Variable for checking at the start
    public void AddVariable(TaskVariable variable)
    {
        var Variable = new Variable();

        Variable.variable = variable.Variable;
        Variable.view = variable.View;
        Variable.min = variable.Min;
        Variable.max = variable.Max;
        Variable.target = variable.Target;
        Variable.measurement = variable.Measurement;
        Variable.text = variable.Text;

        _variables.Add(Variable);
    }

    //Add Formula for checking at the start
    public void AddFormula(FormulaEditor formulaEditor, string target)
    {
        var Formula = new Formula();

        Formula.formulaEditor = formulaEditor;
        Formula.target = target;

        _formulas.Add(Formula);
    }

    //Set Right Conclusion at the start
    public void SetRightConclusion(string right)
    {
        _conclusion.right = right;
    }

    //Set Actual Conclusion at any time
    public void SetActualConclusion(bool isRight, string actual)
    {
        _conclusion.actual = actual;
        _conclusion.isRight = isRight;
    }

    //Set Result of formula, variables and conclusion to TableResultPanel on event OnClick
    private void SetResults()
    {
        _resultPercentage = 0.0f;
        _formulaPercentage = 40.0f;
        _tablePercentage = 40.0f;
        _conclusionPercantage = 20.0f; 

        //////Checking For Formulas
        if(_formulas.Count >= 1)
        {
            var textFormula = _tableResultPanel.AddTextCategory();
            textFormula.text = "Формулы";
        }
        if (_formulas.Count == 0)
        {
            _conclusionPercantage += 40.0f;
        }

        for (int i = 0; i < _formulas.Count; i++)
        {
            if(_formulas[i].target != "null")
            {
                var aFormula = _formulas[i].formulaEditor.Check();
                if (aFormula != null)
                {
                    var resultUI = _tableResultPanel.AddResultUI();

                    var actual = _numerableManager.SolveString(aFormula).ToString() + "c";
                    var target = _formulas[i].target;

                    _formulas[i].formulaEditor._caret.Disable();

                    var text = Instantiate(_formulas[i].formulaEditor._text);
                    text.transform.SetParent(resultUI._actualFormulaContainer, false);
                    var content = Instantiate(_formulas[i].formulaEditor._content);
                    content._rect.SetParent(resultUI._actualFormulaContainer, false);

                    if (actual == target)
                    {
                        resultUI.SetResultFormula(true);

                        _resultPercentage += _formulaPercentage / _formulas.Count;
                    }
                    else
                    {
                        resultUI.SetResultFormula(false);
                        var resource = Resources.Load("UI/Formula/" + _formulas[i].formulaEditor._id, typeof(GameObject)) as GameObject;
                        if(resource != null)
                        {
                            var rightFormula = Instantiate(resource).transform;
                            rightFormula.SetParent(resultUI._rectContainerRight, false);
                            rightFormula.SetSiblingIndex(0);
                        }

                        print(actual);
                        //00FF95FF
                    }
                }
                else
                {
                    var resultUI = _tableResultPanel.AddResultUI();
                    var text = Instantiate(_formulas[i].formulaEditor._text);
                    text.transform.SetParent(resultUI._actualFormulaContainer, false);
                    resultUI.SetResultFormula(false);
                    var resource = Resources.Load("UI/Formula/" + _formulas[i].formulaEditor._id, typeof(GameObject)) as GameObject;
                    if (resource != null)
                    {
                        var rightFormula = Instantiate(resource).transform;
                        rightFormula.SetParent(resultUI._rectContainerRight, false);
                        rightFormula.SetSiblingIndex(0);
                    }
                }
            }
        }

        if (_variables.Count >= 1)
        {
            var textVariable = _tableResultPanel.AddTextCategory();
            textVariable.text = "Табличные значения";
        }
        if (_variables.Count == 0)
        {
            _conclusionPercantage += 40.0f;
        }

        //Checking For Variables
        for (int i = 0; i < _variables.Count; i++)
        {
            var resultUI = _tableResultPanel.AddResultUI();
            var view = _variables[i].view;
            var text = _variables[i].text;

            if (text == null)
            {
                var number = (float)_numerableManager.GetNumber(_variables[i].variable);
                var target = _variables[i].target;
                var min = _variables[i].min;
                var max = _variables[i].max;
                var measuremt = _variables[i].measurement;

                if (min <= number && number <= max)
                {
                    resultUI.SetResult(view + " равно " + number.ToString() + ' ' + measuremt);

                    _resultPercentage += _tablePercentage / _variables.Count;
                }
                else
                {
                    resultUI.SetResult(view + " равно " + number.ToString() + ' ' + measuremt, view + "\nдолжно быть равно " + target.ToString() + ' ' + measuremt);
                }
            }

            if (text != null)
            {
                var aText = _numerableManager.GetTextedVariable(_variables[i].variable);
                if(aText != null)
                {
                    if (text == aText)
                    {
                        resultUI.SetResult(view + " " + aText);

                        _resultPercentage += _tablePercentage / _variables.Count;
                    }
                    else
                    {
                        resultUI.SetResult(view + " " + aText, view + "\nдолжно быть " + text);
                    }
                }
                else
                {
                    resultUI.SetResult("Результат наблюдения не выбран", view + "\nдолжно быть " + text);
                }
            }
        }

        var textConclusion = _tableResultPanel.AddTextCategory();
        textConclusion.text = "Вывод";

        //Checking For Conclusion
        if (_conclusion.actual != null)
        {
            var resultUI = _tableResultPanel.AddResultUI();

            if (_conclusion.isRight)
            {
                resultUI.SetResult(_conclusion.right);

                _resultPercentage += _conclusionPercantage;
            }
            else
            {
                resultUI.SetResult(_conclusion.actual, _conclusion.right);
            }
        }
        else
        {
            var resultUI = _tableResultPanel.AddResultUI();

            resultUI.SetResult(_conclusion.actual, _conclusion.right);
        }

        _tableResultPanel.SetResultPercentage((int)_resultPercentage);
    }
}