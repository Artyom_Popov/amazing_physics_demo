﻿using UnityEngine;
using System.Collections;

namespace AmazyingPhysics
{
    public class FormulaPanel : MonoBehaviour, IActivationPanel
    {
        [SerializeField]
        public GameObject _root;

        public void SetActivePanel(bool value)
        {
            _root.SetActive(value);
        }
    }
}
