﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Linq;
using System;

public class DragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {
	
	private Vector2 originalLocalPointerPosition;
	private Vector3 originalPanelLocalPosition;
	/*[SerializeField] */private RectTransform panelRectTransform;
    /*[SerializeField] */private RectTransform parentRectTransform;
    
    private string _tagDraggableUI = "DraggableUI";
    private bool _isObj;

    void Awake()
    {
        panelRectTransform = transform as RectTransform;
        parentRectTransform = panelRectTransform.parent as RectTransform;
    }

    public void OnPointerDown (PointerEventData data) {
		originalPanelLocalPosition = panelRectTransform.localPosition;
		RectTransformUtility.ScreenPointToLocalPointInRectangle (parentRectTransform, data.position, data.pressEventCamera, out originalLocalPointerPosition);

        if (data.pointerCurrentRaycast.gameObject != null)
            _isObj = CheckObj(data.pointerCurrentRaycast.gameObject.tag);
    }
	
	public void OnDrag (PointerEventData data) {
		if (panelRectTransform == null || parentRectTransform == null)
			return;

        if (!_isObj)
            return;
		
		Vector2 localPointerPosition;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (parentRectTransform, data.position, data.pressEventCamera, out localPointerPosition)) {
			Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;
			panelRectTransform.localPosition = originalPanelLocalPosition + offsetToOriginal;
		}
		
		ClampToWindow ();
	}

    public void OnPointerUp(PointerEventData eventData)
    {
        _isObj = false;
    }
	
	// Clamp panel to area of parent
	void ClampToWindow () {
		Vector3 pos = panelRectTransform.localPosition;
		
		Vector3 minPosition = parentRectTransform.rect.min - panelRectTransform.rect.min;
		Vector3 maxPosition = parentRectTransform.rect.max - panelRectTransform.rect.max;
		
		pos.x = Mathf.Clamp (panelRectTransform.localPosition.x, minPosition.x, maxPosition.x);
		pos.y = Mathf.Clamp (panelRectTransform.localPosition.y, minPosition.y, maxPosition.y);
		
		panelRectTransform.localPosition = pos;
	}

    private bool CheckObj(string tag)
    {
        bool value = false;

        if (_tagDraggableUI == tag)
            value = true;

        return value;
    }    
}
