﻿using UnityEngine;
using UnityEngine.UI;

public class ConclusionUI : MonoBehaviour{
    public TaskChecker _taskChecker;
    public bool _isRight;
    public Text _text;

    public void SetText(string text)
    {
        _text.text = text;
    }

    public void SetConclusion()
    {
        _taskChecker.SetActualConclusion(_isRight, _text.text);
    }
}
