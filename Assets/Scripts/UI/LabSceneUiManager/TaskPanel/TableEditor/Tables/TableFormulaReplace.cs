﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TableFormulaReplace : MonoBehaviour
{
    public TableEditor _tableEditor;

    public string _lineId;
    public string _id;
    public string _formulaId;

    public Text _txt;

    public string old_replace;
    public string new_replace;

    private void Start()
    {
        _tableEditor.OnChange += ChangleValues;
    }

    private void OnDestroy()
    {
        _tableEditor.OnChange -= ChangleValues;
    }

    private void ChangleValues()
    {
        var numerableManager = _tableEditor._numerableManager;

        var action = numerableManager.GetFormula(_formulaId);

        if(action != null)
        {
            action = action.Replace(old_replace, new_replace);
        }

        var number = numerableManager.ReplaceAction(action, _lineId);

        number = Math.Round(number, 2);

        _txt.text = number.ToString();

        numerableManager.AddNumber(_id, number);
    }
}
