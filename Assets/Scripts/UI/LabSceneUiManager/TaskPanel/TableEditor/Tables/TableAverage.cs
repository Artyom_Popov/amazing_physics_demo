﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TableAverage : MonoBehaviour {
    public TableEditor _tableEditor;

    public List<string> _ids = new List<string>();

    public string _lineId;
    public string _id;

    public Text _txt;

    private void Start()
    {
        _tableEditor.OnChange += ChangeValues;
    }

    private void OnDestroy()
    {
        _tableEditor.OnChange -= ChangeValues;
    }

    private void ChangeValues()
    {
        var numerableManager = _tableEditor._numerableManager;

        var action = "";

        var count = 0;

        for(int i = 0; i < _ids.Count; i++)
        {
            var n = numerableManager.GetNumber(_ids[i]);
            if(n != 0)
            {
                count += 1;
                if (count > 1)
                {
                    action += '+';
                }
                action += '(';
                action += n;
                action += ')';
            }
        }

        if(action != "")
        {
            action = '(' + action + ')' + '/' + count.ToString();
        }
        else
        {
            action = "0";
        }

        var number = numerableManager.ReplaceAction(action, _lineId);

        number = Math.Round(number, 2);

        _txt.text = number.ToString();

        numerableManager.AddNumber(_id, number);
    }


}
