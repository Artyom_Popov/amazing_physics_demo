﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TableSimple : MonoBehaviour {
    public TableEditor _tableEditor;

    public string _lineId;
    public string _id;
    public string _action;

    public Text _txt;

    private void Start()
    {
        _tableEditor.OnChange += ChangeValues;
    }

    private void OnDestroy()
    {
        _tableEditor.OnChange -= ChangeValues;
    }

    private void ChangeValues()
    {
        var numerableManager = _tableEditor._numerableManager;

        var number = numerableManager.ReplaceAction(_action, _lineId);

        number = Math.Round(number, 2);

        _txt.text = number.ToString();

        numerableManager.AddNumber(_id, number);
    }
}
