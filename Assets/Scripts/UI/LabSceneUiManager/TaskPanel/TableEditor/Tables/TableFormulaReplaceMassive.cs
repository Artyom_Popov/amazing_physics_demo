﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TableFormulaReplaceMassive : MonoBehaviour
{
    public TableEditor _tableEditor;

    public string _lineId;
    public string _id;
    public string _formulaId;

    public Text _txt;

    public string[] old_replace;
    public string[] new_replace;

    private void Start()
    {
        _tableEditor.OnChange += ChangleValues;
    }

    private void OnDestroy()
    {
        _tableEditor.OnChange -= ChangleValues;
    }

    private void ChangleValues()
    {
        var numerableManager = _tableEditor._numerableManager;

        var action = numerableManager.GetFormula(_formulaId);

        for (int i = 0; i < old_replace.Length; i++)
        {
            action = action.Replace(old_replace[i], new_replace[i]);
        }

        var number = numerableManager.ReplaceAction(action, _lineId);

        number = Math.Round(number, 2);

        _txt.text = number.ToString();

        numerableManager.AddNumber(_id, number);
    }
}
