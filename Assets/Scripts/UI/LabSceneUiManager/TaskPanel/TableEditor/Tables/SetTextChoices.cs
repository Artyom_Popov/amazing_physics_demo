﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using AmazyingPhysics;

public class SetTextChoices : MonoBehaviour {
    public TableEditor _tableEditor;
    public RectTransform _choices;
    private Text _text;
    private string _id;

    public void SetUiText(Text text)
    {
        _text = text;
        _choices.localPosition = text.transform.parent.localPosition;
        _choices.gameObject.SetActive(true); 
    }

    public void SetText(Text text)
    {
        _text.text = text.text;

        _tableEditor._numerableManager.AddTextedVariable(_id, _text.text);

        _choices.gameObject.SetActive(false);
    }

    public void SetId(string id)
    {
        _id = id;
    }
}
