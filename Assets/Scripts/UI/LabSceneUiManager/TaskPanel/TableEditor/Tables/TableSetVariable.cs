﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TableSetVariable : MonoBehaviour
{
    public TableEditor _tableEditor;

    public string _id;
    public double _number;

    private void Start()
    {
        _tableEditor.OnChange += ChangleValues;
    }

    private void OnDestroy()
    {
        _tableEditor.OnChange -= ChangleValues;
    }

    private void ChangleValues()
    {
        var numerableManager = _tableEditor._numerableManager;
        numerableManager.AddNumber(_id, _number);
    }
}
