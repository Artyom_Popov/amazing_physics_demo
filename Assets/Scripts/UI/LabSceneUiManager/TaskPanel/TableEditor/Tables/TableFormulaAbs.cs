﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TableFormulaAbs : MonoBehaviour
{
    public TableEditor _tableEditor;

    public string _lineId;
    public string _id;
    public string _formulaId;

    public Text _txt;

    private void Start()
    {
        _tableEditor.OnChange += ChangleValues;
    }

    private void OnDestroy()
    {
        _tableEditor.OnChange -= ChangleValues;
    }

    private void ChangleValues()
    {
        var numerableManager = _tableEditor._numerableManager;

        var action = numerableManager.GetFormula(_formulaId);

        var number = numerableManager.ReplaceAction(action, _lineId);

        number = Math.Round(number, 2);

        _txt.text = Mathf.Abs((float)number).ToString();

        numerableManager.AddNumber(_id, number);
    }
}
