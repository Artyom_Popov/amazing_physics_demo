﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TableEditorInputField : MonoBehaviour {
    private InputField _inputField;
    public TableEditor _tableEditor;

    public string _id;
    public string _myId;
    public double _number;

    void Awake()
    {
        gameObject.AddComponent<EventTrigger>();
        _inputField = GetComponent<InputField>();
    }

    public void Start()
    {
        _inputField.onValueChanged.AddListener(delegate { ValueChangeCheck(); });

        AddEventPonterDown();

        SetId();
    }

    private void AddEventPonterDown()
    {
        EventTrigger trigger = GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => { OnPointerDown((PointerEventData)data); });
        trigger.triggers.Add(entry);
    }

    private void SetId()
    {
        string tableId = "t" + transform.parent.parent.parent.parent.name.Replace("(Clone)", "");
        string colomnId = "c" + transform.parent.parent.parent.name;
        string lineId = "l" + transform.parent.name;

        _id = tableId + '_' + colomnId;
        _myId = tableId + '_' + colomnId + '_' + lineId;
    }

    public void OnPointerDown(PointerEventData data)
    {
        _tableEditor.InputFieldPointerDown();
    }

    public void ValueChangeCheck()
    {
        Numerate();
        _tableEditor.ChangeValues();
    }

    private void Numerate()
    {
        var text = _inputField.text;

        if (text == "")
        {
            _number = 0;
        }
        else
        {
            double textNumber;
            if(double.TryParse(_inputField.text, out textNumber))
            {
                _number = double.Parse(_inputField.text);
            }
        }

        _tableEditor.AddNumber(_myId, _number);
    }
}
