﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AmazyingPhysics;
using System;

public class TableEditor : MonoBehaviour {
    private Transform _transform;

    private TaskPanel _taskPanel;

    public NumerableManager _numerableManager;

    public event Action OnChange;

    public void Init(TaskPanel taskPanel, NumerableManager numerableManager)
    {
        _taskPanel = taskPanel;
        _numerableManager = numerableManager;
        _numerableManager.OnChange += ChangeValues;
    }

    private void OnDestroy()
    {
        _numerableManager.OnChange -= ChangeValues;
    }

    public void InputFieldPointerDown()
    {
        if(_taskPanel._aFormulaEditor != null)
        {
            _taskPanel._aFormulaEditor.EndSolve();
        }
    }

    public void ChangeValues()
    {
        if (OnChange != null)
        {
            OnChange();
        }
    }

    public void AddNumber(string id, double number)
    {
        _numerableManager.AddNumber(id, number);
    }
}
