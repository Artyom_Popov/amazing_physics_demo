﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

namespace AmazyingPhysics
{
    public class ControlAppElmnt : MonoBehaviour
    {
        [SerializeField]
        private Button _button;
        
        public event Action<EControlApp> OnClick;

        private EControlApp _controlApp;

        private void Start()
        {
            _button.onClick.AddListener(() =>
            {
                if (OnClick != null)
                    OnClick(_controlApp);
            });
        }

        public void SetType(EControlApp controlApp)
        {
            _controlApp = controlApp;
        }

        public void SetText(string text)
        {
            _button.GetComponentInChildren<Text>().text = text;
        }        
    }
}