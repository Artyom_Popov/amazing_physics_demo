﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AmazyingPhysics.Core;

namespace AmazyingPhysics
{
    public enum EControlApp { Repeat, ExitToMainMenu, ExitFromApp }

    public class ControlAppPanel : UiPanelBase
    {
        [SerializeField]
        private GameObject _prefControlAppElmnt;

        [SerializeField]
        private Transform _container;

        public Dictionary<EControlApp, ControlAppElmnt> ControlAppElmnts;
        public event Action<EControlApp> OnClickElmnt;

        void Awake()
        {
            InitPanel();
        }
        
        public void InitPanel()
        {
            var data = new[]
            {
                new { ControlApp = EControlApp.Repeat, Text = "НАЧАТЬ ЗАНОВО" },
                new { ControlApp = EControlApp.ExitToMainMenu, Text = "ВЫЙТИ В ГЛАВНОЕ МЕНЮ" },
                new { ControlApp = EControlApp.ExitFromApp, Text = "ВЫЙТИ ИЗ ПРИЛОЖЕНИЯ" }                
            };

            ControlAppElmnts = new Dictionary<EControlApp, ControlAppElmnt>();
            
            for (int i = 0; i < data.Length; i++)
            {
                GameObject go = Instantiate(_prefControlAppElmnt) as GameObject;
                go.transform.SetParent(_container, false);

                var elmnt = go.GetComponent<ControlAppElmnt>();
                elmnt.SetType(data[i].ControlApp);
                elmnt.SetText(data[i].Text);
                elmnt.OnClick += Elmnt_OnClick;

                ControlAppElmnts.Add(data[i].ControlApp, elmnt);
            }            
        }

        private void Elmnt_OnClick(EControlApp controlApp)
        {
            if (OnClickElmnt != null)
                OnClickElmnt(controlApp);
        }        
    }
}
