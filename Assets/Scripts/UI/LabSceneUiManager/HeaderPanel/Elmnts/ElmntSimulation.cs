﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace AmazyingPhysics
{
    public class ElmntSimulation : MonoBehaviour
    {
        [SerializeField]
        private Button _button;  
        
        [SerializeField]
        private Sprite _sprtPlaySimulation;

        [SerializeField]
        private Sprite _sprtStopSimulation;

        public event Action<bool> OnPlaySimulation;

        private bool _isSimulation;
        private Color _sourceColor;

        private void Start()
        {
           _button.image.color = ColorManager.Instance.Color1;

            _button.onClick.AddListener(() =>
            {
                if (OnPlaySimulation != null)
                {
                    _isSimulation = !_isSimulation;
                    OnPlaySimulation(_isSimulation);
                    SetSprite(_isSimulation);
                }               
            });
        }

        private void SetSprite(bool value)
        {
            if (value)
            {
                _button.image.sprite = _sprtStopSimulation;
                _button.image.color = ColorManager.Instance.Normal1;
            }
            else
            {
                _button.image.sprite = _sprtPlaySimulation;
                _button.image.color = ColorManager.Instance.Color1;
            }
        }

        public void SetColor(Color color)
        {
            _button.image.color = color;
        }       
    }
}