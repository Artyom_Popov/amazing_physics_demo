﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AmazyingPhysics.Core;

namespace AmazyingPhysics
{
    public class ExitToMainSceneElmnt : MonoBehaviour
    {
        [SerializeField] private Button _button;        

        public event Action OnExit;

        private void Start()
        {
            _button.onClick.AddListener(() =>
            {
                if (OnExit != null)
                    OnExit();                
            });
        }
    }        
}
