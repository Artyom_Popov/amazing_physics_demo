﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;
using AmazyingPhysics.Core;

namespace AmazyingPhysics
{
    public class LaunchItemElmnt : MonoBehaviour
    {
        [SerializeField]
        private Button _button;

        [SerializeField]
        private Image _imgBackground;

        private EInventoryLaunchMode _eItemLaunchMode;

        public event Action<EInventoryLaunchMode> OnClick;
        
        private void Start()
        {
            _button.onClick.AddListener(() =>
            {
                if (OnClick != null)
                    OnClick(_eItemLaunchMode);
            });
        }

        public void SetType(EInventoryLaunchMode eItemLaunchMode)
        {
            _eItemLaunchMode = eItemLaunchMode;
        }

        public void SetColor(Color color)
        {
            if(_button.interactable)
                _button.image.color = color;
        }

        public void SetSprite(Sprite sprite)
        {
            _imgBackground.sprite = sprite;
        }

        public void SetInteractable(bool value, Color colorActive, Color colorInActive)
        {
            _button.interactable = value;

            if (value)
                _button.image.color = colorActive;
            else
                _button.image.color = colorInActive;
        }
    }
}