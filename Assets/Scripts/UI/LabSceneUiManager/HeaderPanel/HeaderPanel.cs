﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class HeaderPanel : MonoBehaviour
    {
        [SerializeField]
        public Settings SettingsPanel;

        public void Constructor(string nameExperiment)
        {
            SettingsPanel.txtExperiment.text = nameExperiment;
            SettingsPanel.Constructor();            
        }

        public void SetActivePanel(bool value)
        {
            SettingsPanel.root.SetActive(value);
        }

        [Serializable]
        public class Settings
        {
            public GameObject root;
            public Text txtExperiment;
            public Button btnSettings;
            public Button btnClose;
            public event Action OnSettings;
            public event Action OnClose;

            public void Constructor()
            {
                btnSettings.image.color = ColorManager.Instance.Color1;

                btnSettings.onClick.AddListener(() =>
                {
                    if (OnSettings != null)
                    {
                        OnSettings();
                    }
                });

                btnClose.image.color = ColorManager.Instance.Color1;

                btnClose.onClick.AddListener(() =>
                {
                    if (OnClose != null)
                    {
                        OnClose();
                    }
                });
            }
        }
    }
}
