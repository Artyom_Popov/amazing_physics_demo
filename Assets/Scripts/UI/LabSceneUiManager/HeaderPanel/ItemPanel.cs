﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AmazyingPhysics.Core;

namespace AmazyingPhysics
{
    public class ItemPanel : MonoBehaviour
    {
        [SerializeField]
        private GameObject _root;

        [SerializeField] private ExitToMainSceneElmnt _appSettingsPanel;

        [SerializeField] private Button _btnPlayPhysics;
        [SerializeField] private Button _btnCreate;
        [SerializeField] private Button _btnTransform;
        [SerializeField] private Button _btnSettings;
        [SerializeField] private Button _btnDelete;
        [SerializeField] private Button _btnNotebook;

        private Color _colorSource;
        private Color _colorSelect;

        private Dictionary<EInventoryLaunchMode, Button> _btnItemStates;

        [SerializeField] private Sprite _sprtStopPhysics;
        private Sprite _sprtPlayPhysics;

        public event Action<bool> OnPlayPhysics;
        public event Action<EInventoryLaunchMode> OnItemStage;
        public event Action<bool> OnNotebook;
        // public event Action OnCreate;
        // public event Action OnTransform;
        // public event Action OnSettings;
        // public event Action OnDelete;

        private bool _isPlayPhysic;
        private bool _isNotebook;

        public ExitToMainSceneElmnt AppSettingsPanel
        {
            get
            {
                return _appSettingsPanel;
            }
        }

        //private void Start()
        //{
        //    _sprtPlayPhysics = _btnPlayPhysics.image.sprite;

        //    _btnItemStates = new Dictionary<EItemLaunchMode, Button>();
        //    _btnItemStates.Add(EItemLaunchMode.Create, _btnCreate);
        //    _btnItemStates.Add(EItemLaunchMode.Transform, _btnTransform);
        //    _btnItemStates.Add(EItemLaunchMode.Settings, _btnSettings);
        //    _btnItemStates.Add(EItemLaunchMode.Erase, _btnDelete);

        //    _colorSource = _btnCreate.image.color;
        //    _colorSelect = Color.blue;

        //    InitManagePanel();
        //}

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _btnPlayPhysics.onClick.Invoke();
            }
        }

        public void SetActivePanel(bool value)
        {
            _root.SetActive(value);
        }

        public void InitManagePanel()
        {
            _btnPlayPhysics.onClick.AddListener(() =>
            {
                _isPlayPhysic =! _isPlayPhysic;

                if (OnPlayPhysics != null)
                    OnPlayPhysics(_isPlayPhysic);

                ChangeSpritePlayStopPhysics(_isPlayPhysic);
            });

            _btnCreate.onClick.AddListener(() =>
            {
                if (OnItemStage != null)
                    OnItemStage(EInventoryLaunchMode.Create);

                // if (OnCreate != null)
                //     OnCreate();

                // SelectItemState(0);
            });

            _btnTransform.onClick.AddListener(() =>
            {
                 if (OnItemStage != null)
                    OnItemStage(EInventoryLaunchMode.Transform);

                // if (OnTransform != null)
                //     OnTransform();

                // SelectItemState(1);
            });

            _btnSettings.onClick.AddListener(() =>
            {
                 if (OnItemStage != null)
                    OnItemStage(EInventoryLaunchMode.Settings);

                // if (OnSettings != null)
                //     OnSettings();

                // SelectItemState(2);
            });

            _btnDelete.onClick.AddListener(() =>
            {
                 if (OnItemStage != null)
                    OnItemStage(EInventoryLaunchMode.Erase);

                // if (OnDelete != null)
                //     OnDelete();

                // SelectItemState(3);
            });

            _btnNotebook.onClick.AddListener(() =>
            {
                _isNotebook = !_isNotebook;

                if (OnNotebook != null)
                    OnNotebook(_isNotebook);
            });    

            // подписка на события при нажатии на кнопки управления предметом
            // OnCreate += () => SelectItemState(0);   
            // OnTransform += () => SelectItemState(1);   
            // OnSettings += () => SelectItemState(2);   
            // OnDelete += () => SelectItemState(3);   

            OnItemStage += (EInventoryLaunchMode itemStage) => SelectItemState(itemStage);

            SetItemStage(EInventoryLaunchMode.Create);
        }

        private void ChangeSpritePlayStopPhysics(bool value)
        {
            if(value)
            {
                _btnPlayPhysics.image.sprite = _sprtStopPhysics;
            }
            else
            {
                _btnPlayPhysics.image.sprite = _sprtPlayPhysics;
            }
        }

        // private void SelectItemState(int i)
        // {
        //     foreach (var item in _btnItemStates)
        //     {
        //         item.image.color = _colorSource;
        //     }

        //     _btnItemStates[i].image.color = _colorSelect;
        // }

         private void SelectItemState(EInventoryLaunchMode itemStage)
        {
            foreach (var item in _btnItemStates)
            {
                item.Value.image.color = _colorSource;
            }

            _btnItemStates[itemStage].image.color = _colorSelect;
        }

        public void SetItemStage(EInventoryLaunchMode itemMode)
        {
            OnItemStage(itemMode);

            // switch (itemMode)
            // {
            //     case EItemMode.Create:
            //         _btnCreate.onClick.Invoke();
            //         break;
            //     case EItemMode.Transform:
            //         _btnTransform.onClick.Invoke();
            //         break;
            //     case EItemMode.Settings:
            //         _btnSettings.onClick.Invoke();
            //         break;
            //     case EItemMode.Erase:
            //         _btnDelete.onClick.Invoke();
            //         break;
            //     default:
            //         break;
            // }
        }
    }
}
