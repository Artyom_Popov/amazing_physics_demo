﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;

namespace AmazyingPhysics
{
    public class ItemSettingsPanel : MonoBehaviour
    {
        [SerializeField] private GameObject _root;
        
        public void SetActivePanel(bool value)
        {
            _root.SetActive(value);
        }        
    }
}