﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace AmazyingPhysics
{
    public class InventoryElmnt : MonoBehaviour
    {
        [SerializeField]
        Button _button;

        [SerializeField]
        private Image _imgBackground;

        [SerializeField]
        private Text _txtItemName;

        public event Action<Inventory> OnClick;

        private Inventory _inventory;
        
        private void Start()
        {
            _button.onClick.AddListener(() =>
            {
                if (OnClick != null)
                    OnClick(_inventory);                
            });
        }

        public void SetInventory(Inventory inventory)
        {
            _inventory = inventory;
            _txtItemName.text = inventory.NameRu;
        }

        public void SetColor(Color color)
        {
            _button.image.color = color;
        }

        public void SetSprite(Sprite sprite)
        {
            _imgBackground.sprite = sprite;
        }     
    }
}