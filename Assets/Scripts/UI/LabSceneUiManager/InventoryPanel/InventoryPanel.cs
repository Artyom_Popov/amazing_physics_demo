﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;

namespace AmazyingPhysics
{
    public class InventoryPanel : UiPanelBase
    {
        [SerializeField]
        private ManageSettings _settings;

        [SerializeField]
        private GameObject _prefInventoryElmnt;
        
        [SerializeField]
        private Color _source;

        [SerializeField]
        private Color _select;

        private string _pathItems = "ImgItems/sprt_btn_inv_";
        private Dictionary<int, InventoryElmnt> _inventoryElmnts;
        private int _currentElmntId = -1;     
        
        public event Action<Inventory> OnClickElmnt;

        private void Start()
        {
            _settings.Constructor();
            _settings.btnShowHide.onClick.AddListener(ShowHidePanel);
        }

        public void InitPanel(Dictionary<int, Inventory> data)
        {
            _inventoryElmnts = new Dictionary<int, InventoryElmnt>();
            
            foreach (var item in data)
            {
                GameObject go = Instantiate(_prefInventoryElmnt) as GameObject;
                go.transform.SetParent(_settings.scrollRect.content, false);

                var elmnt = go.GetComponent<InventoryElmnt>();
                elmnt.SetInventory(item.Value);
                elmnt.SetColor(_source);

                Sprite sprite = Resources.Load<Sprite>(_pathItems + item.Value.Id.ToInventoryNameId());
                elmnt.SetSprite(sprite);

                elmnt.OnClick += Elmnt_OnClick;

                _inventoryElmnts.Add(item.Key, elmnt);
            }            
        }

        private void Elmnt_OnClick(Inventory inventory)
        {
            if (OnClickElmnt != null)
                OnClickElmnt(inventory);
            
            SelectElmnt(inventory.Id);
        }

        private void SelectElmnt(int elmntId)
        {
            if (_currentElmntId != -1)
            {
                _inventoryElmnts[_currentElmntId].SetColor(_source);
            }

            _inventoryElmnts[elmntId].SetColor(_select);

            _currentElmntId = elmntId;
        }

        public void ShowHidePanel()
        {
            StopAllCoroutines();
            StartCoroutine(_settings.CorShowHidePanel());
        }

        public void ShowHidePanel(bool value)
        {
            //_settings.btnShowHide.onClick.RemoveAllListeners();

            if (value)
            {
                _settings.btnShowHide.gameObject.SetActive(false);

                _settings.statePanel = false;

                StopAllCoroutines();
                StartCoroutine(_settings.CorShowHidePanel());
            }
            else
            {
                _settings.btnShowHide.gameObject.SetActive(true);
                //_settings.btnShowHide.onClick.AddListener(ShowHidePanel);
            }           
        }

        [Serializable]
        public class ManageSettings
        {
            public GameObject root;            
            public ScrollRect scrollRect;
            public Button btnShowHide;
            public bool statePanel = true;

            private RectTransform rootTransform;
            private float maxPos;

            public void Constructor()
            {
                btnShowHide.image.color = ColorManager.Instance.Color1;
                rootTransform = root.transform as RectTransform;
                maxPos = rootTransform.rect.width;
            }           

            public IEnumerator CorShowHidePanel()
            {
                statePanel = !statePanel;

                float dampTime = .2f;
                float velocity = -1;
                float currentPosX = rootTransform.anchoredPosition.x;
                float targetPosX = statePanel ? 0 :  - maxPos;
                btnShowHide.transform.localRotation = statePanel ? Quaternion.AngleAxis(180, btnShowHide.transform.forward) : Quaternion.AngleAxis(0, btnShowHide.transform.forward);
                while ((int)velocity != 0)
                {
                    currentPosX = Mathf.SmoothDamp(currentPosX, targetPosX, ref velocity, dampTime);
                    rootTransform.anchoredPosition = new Vector2(currentPosX, rootTransform.anchoredPosition.y);
                    yield return null;
                }

                rootTransform.anchoredPosition = new Vector2(targetPosX, rootTransform.anchoredPosition.y);
            }
        }
    }
}