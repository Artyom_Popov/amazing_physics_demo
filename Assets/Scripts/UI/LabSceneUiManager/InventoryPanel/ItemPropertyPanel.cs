﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;

namespace AmazyingPhysics
{
    public class ItemPropertyPanel : MonoBehaviour
    {
        //[SerializeField] private GameObject _root;
        //[SerializeField] private GameObject _node;
        //[SerializeField] private Transform _transGroup;
        //[SerializeField] private Transform _transItem;

        //private Button _btnInventoryPref;
        
        //private string _pathGroups = "ImgItemsGroups/sprt_btn_inv_group_";
        //private string _pathItems = "ImgItems/sprt_btn_inv_";
        //private string _pathBtnInventory = "UI/LabScene/btnInventory";

        //private Button[] _btnGroups;
        //public Action[] OnSelectGroups;

        //[SerializeField] private ItemSettings[] _itemsGroup;

        //public ItemSettings[] ItemsGroup
        //{
        //    get
        //    {
        //        return _itemsGroup;
        //    }           
        //}

        //public void SetActivePanel(bool value)
        //{
        //    _root.SetActive(value);
        //}

        //private void LoadBtnPrefab()
        //{
        //    _btnInventoryPref = Resources.Load<Button>(_pathBtnInventory) as Button;

        //    if (_btnInventoryPref == null)
        //        Debug.LogError("btnInventory не загружен");
        //}

        //public void InitPanel(GroupItem[] groupItems, Inventory[] items)
        //{
        //    LoadBtnPrefab();
        //    InitGroupPanel(groupItems, items);
        //}

        //private void InitGroupPanel(GroupItem[] groupItems, Inventory[] items)
        //{
        //    _btnGroups = new Button[groupItems.Length];
        //    OnSelectGroups = new Action[groupItems.Length];
        //    _itemsGroup = new ItemSettings[groupItems.Length];

        //    for (int i = 0; i < groupItems.Length; i++)
        //    {
        //        _btnGroups[i] = Instantiate(_btnInventoryPref) as Button;
        //        _btnGroups[i].transform.SetParent(_transGroup, false);                
        //        Sprite sprite = Resources.Load<Sprite>(_pathGroups + groupItems[i].ID);
        //        _btnGroups[i].image.sprite = sprite;

        //        int buffer = i;
        //        _btnGroups[buffer].onClick.AddListener(() =>
        //        {
        //            if (OnSelectGroups[buffer] != null)
        //                OnSelectGroups[buffer]();

        //            SetActiveGroupItems(buffer);
        //        });

        //        _itemsGroup[i] = new ItemSettings();
        //        Inventory[] sortedItems = GetSortedItemsByIdGroup(items, groupItems[i].ID);
        //        InitItemsGroup(_itemsGroup[i], sortedItems);
        //    }

        //    SetActiveGroupItems(0);
        //}

        //private Inventory[] GetSortedItemsByIdGroup(Inventory[] items, string idGroup)
        //{
        //    List<Inventory> sortedItems = new List<Inventory>();

        //    foreach (var item in items)
        //    {
        //        if (item.IdPhysicsCategory == idGroup)
        //        {
        //            sortedItems.Add(item);
                    
        //        }
        //    }

        //    return sortedItems.ToArray();
        //}

        //private void SetActiveGroupItems(int idGroup)
        //{
        //    foreach (var item in _itemsGroup)
        //    {
        //        item.itemGroup.SetActive(false);
        //    }

        //    _itemsGroup[idGroup].itemGroup.SetActive(true);
        //}

        //private void InitItemsGroup(ItemSettings itemsGroup, Inventory[] items)
        //{
        //    itemsGroup.itemGroup = new GameObject();

        //    HorizontalLayoutGroup horizontalLayoutGroup = itemsGroup.itemGroup.AddComponent<HorizontalLayoutGroup>();
        //    horizontalLayoutGroup.childForceExpandHeight = false;
        //    horizontalLayoutGroup.childForceExpandWidth = false;
        //    horizontalLayoutGroup.spacing = 1;

        //    itemsGroup.itemGroup.transform.SetParent(_transItem, false);

        //    itemsGroup.btnItems = new Button[items.Length];
        //    itemsGroup.OnSelectItems = new Action<Inventory>[items.Length];

        //    for (int i = 0; i < items.Length; i++)
        //    {
        //        itemsGroup.btnItems[i] = Instantiate(_btnInventoryPref) as Button;
        //        itemsGroup.btnItems[i].transform.SetParent(itemsGroup.itemGroup.transform, false);
        //        Sprite sprite = Resources.Load<Sprite>(_pathItems + items[i].ID);
        //        itemsGroup.btnItems[i].image.sprite = sprite;

        //        int buffer = i;
        //        itemsGroup.btnItems[buffer].onClick.AddListener(() =>
        //        {
        //            if (itemsGroup.OnSelectItems[buffer] != null)
        //                itemsGroup.OnSelectItems[buffer](items[buffer]);
        //        });
        //    }
        //}

        //public void HidePanel(bool value)
        //{
        //    StopAllCoroutines();
        //    StartCoroutine(CorHidePanel(value));
        //}

        //private IEnumerator CorHidePanel(bool value)
        //{
        //    float step = 3f;            
        //    float increment;
        //    float speed = 1000;          
            
        //    if (value)
        //    {
        //        increment = _node.transform.localPosition.y;
        //        float endPos = -51;

        //        while (_node.transform.localPosition.y >= endPos)
        //        {
        //            Vector3 position = new Vector3(0, increment, 0);

        //            _node.transform.localPosition = Vector3.Lerp(_node.transform.localPosition, position, Time.deltaTime * speed);

        //            increment -= step;

        //            yield return null;
        //        }
        //    }
        //    else
        //    {
        //        increment = _node.transform.localPosition.y;

        //        while (_node.transform.localPosition.y < 0)
        //        {
        //            Vector3 position = new Vector3(0, increment, 0);

        //            _node.transform.localPosition = Vector3.Lerp(_node.transform.localPosition, position, Time.deltaTime * speed);

        //            increment += step;

        //            yield return null;
        //        }
        //    }
        //}
        
        //[Serializable]
        //public class ItemSettings
        //{
        //    public GameObject itemGroup;
        //    public Button[] btnItems;
        //    public Action<Inventory>[] OnSelectItems;
        //}
    }
}