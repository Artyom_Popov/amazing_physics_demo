﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmazyingPhysics
{
    public class LabSceneUIManager : MonoBehaviour
    {
        public HeaderPanel HeaderPanel { get; private set; }
        public TaskPanel TaskPanel { get; private set; }
        public InventoryPanel InventoryPanel { get; private set; }
        public InventoryActPanel InventoryActPanel { get; private set; }
        public ControlAppPanel ControlAppPanel { get; private set; }
        public TableResultPanel TableResultPanel { get; private set; }
        public FooterPanel FooterPanel { get; private set; }        

        [SerializeField]
        private GameObject _root;

        private void Awake()
        {
            HeaderPanel = transform.FindChildTransform("HeaderPanel").GetComponent<HeaderPanel>();
            TaskPanel = transform.FindChildTransform("TaskPanel").GetComponent<TaskPanel>();
            InventoryPanel = transform.FindChildTransform("InventoryPanel").GetComponent<InventoryPanel>();
            InventoryActPanel = transform.FindChildTransform("InventoryActPanel").GetComponent<InventoryActPanel>();
            ControlAppPanel = transform.FindChildTransform("ControlAppPanel").GetComponent<ControlAppPanel>();
            TableResultPanel = transform.FindChildTransform("TableResultPanel").GetComponent<TableResultPanel>();
            FooterPanel = transform.FindChildTransform("FooterPanel").GetComponent<FooterPanel>();            
        }

        public void SetActiveUI(bool value)
        {
            _root.SetActive(value);
        }        
    }
}
