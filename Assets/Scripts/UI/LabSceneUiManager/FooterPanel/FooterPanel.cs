﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class FooterPanel : MonoBehaviour
    {
        [SerializeField]
        public Settings SettingsPanel;

        public void Start()
        {
            SettingsPanel.Constructor();
        }

        public void SetActivePanel(bool value)
        {
            SettingsPanel.root.SetActive(value);
        }

        [Serializable]
        public class Settings
        {
            public GameObject root;
            public ElmntSimulation elmntSimulation;
            public Button btnMainView;
            public Button btnCheckExperiment;
            public event Action OnCheckExperiment;
            public event Action OnMainView;

            public void Constructor()
            {
                btnMainView.image.color = ColorManager.Instance.Color1;

                btnMainView.onClick.AddListener(() =>
                {
                    if (OnMainView != null)
                    {
                        OnMainView();
                    }
                });
                
                btnCheckExperiment.image.color = ColorManager.Instance.Color1;

                btnCheckExperiment.onClick.AddListener(() =>
                {
                    if (OnCheckExperiment != null)
                    {
                        OnCheckExperiment();
                    }
                });
            }
        }
    }
}
