﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

namespace AmazyingPhysics
{
    public class CheckExperimentElmnt : MonoBehaviour
    {
        [SerializeField]
        private Button _button;
        
        public event Action OnClick;

        private void Start()
        {
            _button.onClick.AddListener(() =>
            {
                if (OnClick != null)
                    OnClick();                
            });
        }

        public void SetText(string text)
        {
            _button.GetComponentInChildren<Text>().text = text;
        }        
    }
}