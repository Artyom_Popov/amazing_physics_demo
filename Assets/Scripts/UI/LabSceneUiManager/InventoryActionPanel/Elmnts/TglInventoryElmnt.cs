﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using System.Collections;

namespace AmazyingPhysics
{
    public class TglInventoryElmnt : MonoBehaviour
    {
        [SerializeField]
        private Toggle _toggle;

        [SerializeField]
        private Text _text;

        private string[] _texts;

        public event Action<bool> OnClick;
        
        public void Constructor(bool state)
        {
            _toggle.isOn = state;
            _toggle.onValueChanged.AddListener((bool value) => 
            {
                if(OnClick != null)
                {
                    OnClick(value);
                    _text.text = !_toggle.isOn ? _texts[0] : _texts[1];
                }
            });
        }

        public void SetText(string[] texts)
        {
            _texts = texts;
            _text.text = !_toggle.isOn ? texts[0] : texts[1];            
        }        
    }
}