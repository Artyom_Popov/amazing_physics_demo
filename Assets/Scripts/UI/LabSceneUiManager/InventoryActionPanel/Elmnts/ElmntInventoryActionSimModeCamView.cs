﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace AmazyingPhysics
{
    public class ElmntInventoryActionSimModeCamView : MonoBehaviour
    {
        [SerializeField]
        private Button _button;

        private CameraViewSettings _cameraView;
        
        public event Action<CameraViewSettings> OnClick;
        
        public void Constructor(CameraViewSettings cameraView)
        {
            _cameraView = cameraView;

            _button.onClick.AddListener(() =>
            {
                if (OnClick != null)
                {
                    OnClick(_cameraView);
                }
            });
        }
        
        public void SetParent(Transform parent)
        {
            transform.SetParent(parent, false);
        }

        public void SetActive(bool value)
        {
            _button.gameObject.SetActive(value);
        }
    }
}