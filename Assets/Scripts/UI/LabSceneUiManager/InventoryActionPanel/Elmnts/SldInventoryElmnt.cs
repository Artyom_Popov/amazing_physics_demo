﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace AmazyingPhysics
{
    public class SldInventoryElmnt : MonoBehaviour
    {
        [SerializeField]
        private Slider _slider;

        [SerializeField]
        private Text _txtActionName;

        [SerializeField]
        private Text _txtMeasure;

        private string[] _actionNames;

        public event Action<float> OnClick;
        
        private void Start()
        {
            _slider.onValueChanged.AddListener((float value) =>
            {
                if (OnClick != null)
                {
                    OnClick(value);
                    _txtMeasure.text = String.Format("{0} {1}", _slider.value.ToString("0.0"), _actionNames[1]);                    
                }
            });            
        }

        public void SetSettings(SliderSettings settings)
        {
            _slider.minValue = settings.Min;
            _slider.maxValue = settings.Max;
            _slider.value = settings.Value;
        }

        public void SetText(string[] texts)
        {
            _actionNames = texts;
            _txtActionName.text = _actionNames[0];
            _txtMeasure.text = String.Format("{0} {1}", _slider.value.ToString("0.0"), _actionNames[1]);
        }        
    }
}