﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace AmazyingPhysics
{
    public class ElmntInventoryActionSimModeMenu : MonoBehaviour
    {
        [SerializeField]
        private Button _button;
        
        public event Action<PickableInventoryBase> OnClick;

        private PickableInventoryBase _inventory;
        
        public void Constructor(PickableInventoryBase inventory)
        {
            _inventory = inventory;

            _button.onClick.AddListener(() =>
            {
                if (OnClick != null)
                {
                    OnClick(_inventory);
                }
            });
        }     

        public void SetActive(bool value)
        {
            _button.gameObject.SetActive(value);
        }
    }
}