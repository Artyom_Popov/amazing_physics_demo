﻿namespace AmazyingPhysics
{
    public enum EInventoryActionUiType
    {
        Button,
        Toggle,
        Slider
    }

    public class InventoryAction
    {
        public EInventoryActionUiType Type;
        public BtnInventoryElmnt Button;
        public SldInventoryElmnt Slider;
    }
}