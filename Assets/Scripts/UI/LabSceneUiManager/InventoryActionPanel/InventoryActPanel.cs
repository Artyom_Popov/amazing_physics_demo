﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;

namespace AmazyingPhysics
{
    public class InventoryActPanel : UiPanelBase
    {
        [SerializeField]
        private GameObject _prefBtnInventoryElmnt;

        [SerializeField]
        private GameObject _prefSldInventoryElmnt;

        [SerializeField]
        private GameObject _prefTglInventoryElmnt;

        [SerializeField]
        private Transform _container;

        [SerializeField]
        private Canvas _canvas;

        private List<BtnInventoryElmnt> _buttons = new List<BtnInventoryElmnt>();
        private List<SldInventoryElmnt> _sliders = new List<SldInventoryElmnt>();
        private List<TglInventoryElmnt> _toggles = new List<TglInventoryElmnt>();

        private float _angle = 360;
        private float _distance = 200;

        private bool _isShowingActions;

        public event Action OnDeactivatePanel;

        public void Init(IinventoryAct[] inventoryActs)
        {
            if (inventoryActs == null)
                return;

            _isShowingActions = true;

            DestroyElmnts();

            _root.SetActive(true);
            _buttons = new List<BtnInventoryElmnt>();
            _sliders = new List<SldInventoryElmnt>();
            _toggles = new List<TglInventoryElmnt>();

            Vector2 pos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvas.transform as RectTransform, Input.mousePosition, _canvas.worldCamera, out pos);
            var mousePosition = _canvas.transform.TransformPoint(pos);
            //_root.transform.position = mousePosition;

            print(mousePosition);

            var a = _angle * Mathf.Deg2Rad;

            for (int i = 1; i < inventoryActs.Length + 1; i++)
            {
                var posX = Mathf.Cos(a / inventoryActs.Length * i) * _distance;
                var posY = Mathf.Sin(a / inventoryActs.Length * i) * _distance;
                var vec = new Vector3(posX, posY, 0);

                switch (inventoryActs[i - 1].Type)
                {
                    case EInventoryActionType.Button:
                        CreateButton(inventoryActs[i - 1], mousePosition, vec);
                        break;
                    case EInventoryActionType.Slider:
                        CreateSlider(inventoryActs[i - 1], mousePosition, vec);
                        break;
                    case EInventoryActionType.Toggle:
                        CreateToggle(inventoryActs[i - 1], mousePosition, vec);
                        break;
                    default:
                        break;
                }
            }
        }

        public void Deactivate()
        {
            _isShowingActions = false;
            _root.SetActive(false);
            DestroyElmnts();

            if(OnDeactivatePanel != null)
            {
                OnDeactivatePanel();
            }
        }

        private void CreateButton(IinventoryAct button, Vector3 startPosition, Vector3 actualPosition)
        {
            var go = Instantiate(_prefBtnInventoryElmnt) as GameObject;
            go.transform.SetParent(_container, false);
            //go.transform.position = startPosition;
            //go.transform.localPosition += actualPosition;

            var elmnt = go.GetComponent<BtnInventoryElmnt>();
            elmnt.SetText(button.Button.ActionName);            
            elmnt.OnClick += button.Button.Action;
            elmnt.OnClick += Deactivate;


            _buttons.Add(elmnt);
        }

        private void CreateSlider(IinventoryAct slider, Vector3 startPosition, Vector3 actualPosition)
        {
            var go = Instantiate(_prefSldInventoryElmnt) as GameObject;
            go.transform.SetParent(_container, false);
            //go.transform.position = startPosition;
            //go.transform.localPosition += actualPosition;

            var elmnt = go.GetComponent<SldInventoryElmnt>();
            elmnt.SetSettings(slider.Slider.Settings);
            elmnt.SetText(slider.Slider.ActionName);
            elmnt.OnClick += slider.Slider.Action;           
            
            _sliders.Add(elmnt);
        }

        private void CreateToggle(IinventoryAct toggle, Vector3 startPosition, Vector3 actualPosition)
        {
            var go = Instantiate(_prefTglInventoryElmnt) as GameObject;
            go.transform.SetParent(_container, false);
            //go.transform.position = startPosition;
            //go.transform.localPosition += actualPosition;

            var elmnt = go.GetComponent<TglInventoryElmnt>();
            elmnt.Constructor(toggle.Toggle.State);
            elmnt.SetText(toggle.Toggle.ActionName);            
            elmnt.OnClick += toggle.Toggle.Action;

            _toggles.Add(elmnt);
        }

        private void DestroyElmnts()
        {
            if(_buttons.Count > 0)
            {
                foreach (var b in _buttons)
                {
                    Destroy(b.gameObject);
                }
            }

            _buttons.Clear();

            if (_sliders.Count > 0)
            {
                foreach (var s in _sliders)
                {
                    Destroy(s.gameObject);
                }
            }

            _sliders.Clear();

            if (_toggles.Count > 0)
            {
                foreach (var t in _toggles)
                {
                    Destroy(t.gameObject);
                }
            }

            _toggles.Clear();
        }        
    }
}