﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace AmazyingPhysics
{
    public class InventoryActionsSimModePanel : MonoBehaviour
    {
        [SerializeField]
        private GameObject Root;

        public ElmntInventoryActionSimModeMenu ActionsMenu;
        public ElmntInventoryActionSimModeCamView CamView;

        public void SetParent(Transform parent)
        {
            transform.SetParent(parent, false);
        }

        public void SetActive(bool value)
        {
            Root.SetActive(value);
        }
    }
}