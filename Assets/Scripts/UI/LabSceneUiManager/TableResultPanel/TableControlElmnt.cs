﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

namespace AmazyingPhysics
{
    public class TableControlElmnt : MonoBehaviour
    {
        [SerializeField]
        private Button _button;
        
        public event Action<ETableControl> OnClick;

        private ETableControl _tableControl;

        private void Start()
        {
            _button.onClick.AddListener(() =>
            {
                if (OnClick != null)
                    OnClick(_tableControl);
            });
        }

        public void SetType(ETableControl tableControl)
        {
            _tableControl = tableControl;
        }

        public void SetText(string text)
        {
            _button.GetComponentInChildren<Text>().text = text;
        }        
    }
}