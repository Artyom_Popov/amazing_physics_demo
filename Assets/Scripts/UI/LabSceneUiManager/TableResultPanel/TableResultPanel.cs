﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public enum ETableControl { ShowMistake, Repeat, ExitToMainMenu }

    public class TableResultPanel : UiPanelBase
    {
        [SerializeField]
        private GameObject _prefTableResultElmnt, _prefTableControlElmnt;

        [SerializeField]
        private Transform _resultContainerUp, _resultContainerDown, _controlContainer;

        public Dictionary<ETableControl, TableControlElmnt> TableControlElmnts;
        public event Action<ETableControl> OnClickControlElmnt;

        [SerializeField]
        private Text _formulsText, tableVariablesText, _answerText;

        [SerializeField]
        private Transform _resultContainer;

        [SerializeField]
        private GameObject _prefabResultUI;
        
        public event Action OnClick;

        public Text _textCategory;
        public Text _resultPercentage;

        private void Elmnt_OnClick(ETableControl tableControl)
        {
            if (OnClickControlElmnt != null)
            {
                OnClickControlElmnt(tableControl);
            }
        }

        public override void SetActivePanel(bool value)
        {
            base.SetActivePanel(value);

            if (OnClick != null)
            {
                OnClick();
            }
        }

        public ResultUI AddResultUI()
        {
            var go = Instantiate(_prefabResultUI);
            go.transform.SetParent(_resultContainer, false);

            var resultUI = go.GetComponent<ResultUI>();
            return resultUI;
        }

        public Text AddTextCategory()
        {
            var textCategory = Instantiate(_textCategory);
            textCategory.transform.SetParent(_resultContainer, false);

            var text = textCategory.GetComponent<Text>();
            return text;
        }

        public void SetResultPercentage(int percent)
        {
            _resultPercentage.text = percent.ToString() + " / 100";
        }
    }
}
