﻿using UnityEngine;
using UnityEngine.UI;
using System;
using AmazyingPhysics.Data;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

namespace AmazyingPhysics
{
    public class TableResultElmnt : MonoBehaviour
    {
        [SerializeField]
        private Text _text;
        
        public void SetText(string text)
        {
            _text.text = text;
        }        
    }
}