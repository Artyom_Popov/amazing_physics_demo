﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultUI : MonoBehaviour {
    public Text _actualText;
    public Text _rightText;

    public GameObject _containerRight;
    public RectTransform _rectContainerRight;

    public RectTransform _actualFormulaContainer;

    public Text _resultIcon;

    public Color32 _rightColor;
    public Color32 _wrongColor;

    public int _fontSize;

    public void SetResult(string actual)
    {
        _actualText.text = actual;
        _actualText.fontSize = _fontSize;

        _resultIcon.text = "Верно";
        _resultIcon.color = _rightColor;
    }

    public void SetResult(string actual, string right)
    {
        _actualText.text = actual;
        _actualText.fontSize = _fontSize;

        _containerRight.SetActive(true);

        _rightText.text = right;
        _rightText.fontSize = _fontSize;

        _resultIcon.text = "Неверно";
        _resultIcon.color = _wrongColor;
    }

    public void SetResultFormula(bool isRight)
    {
        _actualText.gameObject.SetActive(false);
        _actualFormulaContainer.gameObject.SetActive(true);

        if (isRight)
        {
            _resultIcon.text = "Верно";
            _resultIcon.color = _rightColor;
        }
        else
        {
            _containerRight.SetActive(true);
            _resultIcon.text = "Неверно";
            _resultIcon.color = _wrongColor;
        }
    }
}
