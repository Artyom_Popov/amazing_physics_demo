﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class ColorManager : Singleton<ColorManager>
    {
        public Color Select1;
        public Color Select2;
        public Color Normal1;
        public Color Normal2;
        public Color BackGround;
        public Color Transparency;

        public Color Color1;
        public Color Color2;
        public Color Color3;
        public Color Color4;
        public Color Color5;
        public Color Color6;
    }
}