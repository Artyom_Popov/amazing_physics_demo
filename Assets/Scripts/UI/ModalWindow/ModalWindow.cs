﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace AmazyingPhysics
{
    public class ModalWindow : MonoBehaviour
    {
        [SerializeField] private Text _txtInfo;
        [SerializeField] private Button _btnYes;
        [SerializeField] private Button _btnNo;

        public void InitWindow(string text)
        {
            _txtInfo.text = text;

            _btnYes.GetComponentInChildren<Text>().text = "OK";
            _btnYes.onClick.RemoveAllListeners();
            _btnYes.onClick.AddListener(CloseWindow);

            _btnNo.gameObject.SetActive(false);
        }

        public void InitWindow(string text, Action action)
        {
            _txtInfo.text = text;

            _btnYes.GetComponentInChildren<Text>().text = "OK";
            _btnYes.onClick.RemoveAllListeners();
            _btnYes.onClick.AddListener(() =>
            {
                if (action != null)
                {
                    action();
                    CloseWindow();
                }
            });

            _btnNo.gameObject.SetActive(true);
            _btnNo.onClick.RemoveAllListeners();
            _btnNo.GetComponentInChildren<Text>().text = "NO";
        }

        private void CloseWindow()
        {
            Destroy(gameObject);
        }
    }
}
