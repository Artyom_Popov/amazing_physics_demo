﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AmazyingPhysics;

namespace AmazyingPhysics
{
    public class ModalWindowManager : Singleton<ModalWindowManager>
    {        
        [SerializeField] private GameObject _prefModalWindow;

        private void Update()
        {
            if (Input.GetKeyDown("x"))
                CreateModelWindow();
        }

        public ModalWindow CreateModelWindow()
        {
            GameObject go = Instantiate(_prefModalWindow) as GameObject;
            ModalWindow modalWindow = go.GetComponent<ModalWindow>();           
            return modalWindow;
        }        
    }
}
