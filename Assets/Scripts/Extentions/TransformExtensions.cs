﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AmazyingPhysics;

public static class TransformExtensions
{
    public static Transform FindChildTransform(this Transform transform, string name)
    {
        Transform[] transforms = transform.GetComponentsInChildren<Transform>();
        Transform trans = null;

        foreach (var item in transforms)
        {
            if (item.name == name)
            {
                trans = item;
                break;
            }
        }

        if (trans == null)
            Debug.LogError("cant find obj " + name);

        return trans;
    }

    public static Transform FindInventory(this GameObject gameobject, string name)
    {
        Transform[] transforms = DynamicRoot.Instance.DynamicRootTransform.GetComponentsInChildren<Transform>();
        Transform trans = null;

        foreach (var item in transforms)
        {
            if (item.name == name)
            {
                trans = item;
                break;
            }
        }
        
        return trans;
    }
}
