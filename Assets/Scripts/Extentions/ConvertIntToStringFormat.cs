﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ConvertIntToStringFormat
{
    /// <summary>
    /// перевести числовое значение Id инвентаря к строковому виду значения из базы данных
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static string ToInventoryNameId(this int value)
    {
        return value.ToString("000");
    }
}
