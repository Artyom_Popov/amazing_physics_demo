﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExtensionCoroutine : Singleton<ExtensionCoroutine>
{
    public IEnumerator StartCorExtendedCoroutine(IEnumerator enumerator)
    {
        yield return StartCoroutine(enumerator);
    }

    public void StartExtendedCoroutine(IEnumerator enumerator)
    {
        StartCoroutine(enumerator);
    }

    public void StartExtendedCoroutineNoWait(IEnumerator enumerator)
    {
        StartCoroutine(enumerator);
    }
}
