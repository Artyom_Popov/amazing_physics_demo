﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Numbers
{
    public List<int> RandomNumbers(int max)
    {
        List<int> lstNumbers = new List<int>();
        
        Random rndNumber = new Random();

        int number = rndNumber.Next(0, max);
        
        lstNumbers.Add(number);
        
        int count = 0;

        do 
        {
            number = rndNumber.Next(0, max);

            if (!lstNumbers.Contains(number))
            {                
                lstNumbers.Add(number);
            }

            count++;
        }
        while (count <= 10 * max);

        return lstNumbers;
    }
}

