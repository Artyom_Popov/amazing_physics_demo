﻿using System.Collections.Generic;
using UnityEngine.Events;

namespace UnityEngine.EventSystems
{
    public static class EventTriggerExtension
    {
        public static EventTrigger GetEventTrigger(this GameObject gameObject)
        {
            EventTrigger trigger = gameObject.GetComponent<EventTrigger>();
            if (trigger == null) trigger = gameObject.AddComponent<EventTrigger>();
            return trigger;
        }

        public static void AddEventListener(this EventTrigger trigger, EventTriggerType eventType, UnityAction<BaseEventData> callback)
        {
            if (trigger == null)
                return;

            if (trigger.triggers == null)
                trigger.triggers = new List<EventTrigger.Entry>();

            EventTrigger.Entry entry = trigger.triggers.Find(e => e.eventID == eventType);

            if (entry == null)
            {
                entry = new EventTrigger.Entry();
                entry.eventID = eventType;
                entry.callback = new EventTrigger.TriggerEvent();
                trigger.triggers.Add(entry);
            }

            entry.callback.AddListener(callback);
        }
    }
}
