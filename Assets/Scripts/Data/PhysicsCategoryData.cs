﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AmazyingPhysics
{   
    [Serializable]
    public class PhysicsCategory
    {
        public int Id;
        public string NameRu;
    }
}

namespace AmazyingPhysics.Data
{
    public class PhysicsCategoryData
    {
        public Dictionary<int, string> PhysicsCategories { get; private set; }

        public PhysicsCategoryData()
        {
            LoadData();
        }

        private void LoadData()
        {
            string path = Application.streamingAssetsPath + "/Data/PhysicsCategory.json";
            string file = File.ReadAllText(path);
            string json = "{ \"Items\": " + file + "}";
            var data = JsonHelper.FromJson<PhysicsCategory>(json);

            PhysicsCategories = new Dictionary<int, string>();

            foreach (var item in data)
            {
                PhysicsCategories.Add(item.Id, item.NameRu);
            }      
        }        
    }
}
