﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace AmazyingPhysics
{
    [Serializable]
    public class Inventory
    {
        public int Id;
        public string NameRu;        
        public int[] PhysicsCategoryId;
        public bool CanStayOnTable;
        public ParentInventory[] ParentInventory;
    }

    [Serializable]
    public class ParentInventory
    {
        public int Id;
        public int ConnectorId;
    }
}

namespace AmazyingPhysics.Data
{
    public class InventoryData
    {
        public Dictionary<int, Inventory> Inventories { get; set; }

        public InventoryData(int physicsCategoryId)
        {
            LoadData(physicsCategoryId);
        }

        private void LoadData(int physicsCategoryId)
        {
            string path = Application.streamingAssetsPath + "/Data/Inventory.json";
            string file = File.ReadAllText(path);
            string json = "{ \"Items\": " + file + "}";
            var data = JsonHelper.FromJson<Inventory>(json);

            //Debug.Log("physicsCategoryId " + physicsCategoryId);

            data = data
                .Where(x => x.PhysicsCategoryId.Contains(physicsCategoryId))
                .ToArray();

            Inventories = new Dictionary<int, Inventory>();

            foreach (var item in data)
            {
                Inventories.Add(item.Id, item);
                Debug.Log(item.NameRu);
            }                       
        }
    }
}
