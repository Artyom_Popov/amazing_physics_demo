﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace AmazyingPhysics
{
    [Serializable]
    public class ExperimentTask
    {
        public int Id;
        public string NameRu;        
        public int ExperimentId;        
    }
}

namespace AmazyingPhysics.Data
{
    public class ExperimentTaskData
    {
        public string Task { get; set; }

        public ExperimentTaskData(int experimentId)
        {
            LoadData(experimentId);
        }

        private void LoadData(int experimentId)
        {
            string path = Application.streamingAssetsPath + "/Data/ExperimentTask.json";
            string file = null;

            if (Application.platform == RuntimePlatform.Android)
            {
                WWW reader = new WWW(path);
                while (!reader.isDone) { }

                file = reader.text;
            }
            else
            {
                file = File.ReadAllText(path);
            }

            string json = "{ \"Items\": " + file + "}";
            var data = JsonHelper.FromJson<ExperimentTask>(json);

            Task = data
                .Where(x => x.ExperimentId == experimentId)
                .Select(t => t.NameRu)
                .First();                          
        }
    }
}
