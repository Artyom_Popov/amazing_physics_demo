﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace AmazyingPhysics
{
    [Serializable]
    public class TaskFormula
    {
        public string Id;
        public string Name;
        public string Variables;
        public string CheckNumber;
    }

    [Serializable]
    public class FormulaOperator
    {
        public int Id;
        public string Operator;
        public string NameRu;
    }

    [Serializable]
    public class FormulaOperand
    {
        public int Id;
        public string Operand;
        public string NameRu;
        public string Interpretation;
        public int PhysicsCategoryId;
        public int[] ExperimentId;
    }

    [Serializable]
    public class TaskConclusion
    {
        public int Id;
        public int ExperimentId;
        public string Right;
        public string Wrong1;
        public string Wrong2;
    }

    [Serializable]
    public class TaskVariable
    {
        public int Id;
        public int ExperimentId;
        public string Variable;
        public string View;
        public float Min;
        public float Max;
        public float Target;
        public string Measurement;
        public string Text;
    }
}

namespace AmazyingPhysics.Data
{
    public class FormulaData
    {
        public FormulaOperator[] FormulaOperator { get; private set; }
        public FormulaOperand[] FormulaOperand { get; private set; }
        public TaskFormula[] TaskFormula { get; private set; }
        public TaskConclusion[] TaskConclusion { get; private set; }
        public TaskVariable[] TaskVariable { get; private set; }

        public FormulaData(int physicsCategoryId, int experimentId)
        {
            LoadData(physicsCategoryId, experimentId);
        }

        private void LoadData(int physicsCategoryId, int experimentId)
        {
            var dataTaskFormula = GetData<TaskFormula>("TaskFormula");
            TaskFormula = dataTaskFormula
                .ToArray();

            var dataFormulaOperator = GetData<FormulaOperator>("FormulaOperator");
            FormulaOperator = dataFormulaOperator
                .ToArray();

            var dataFormulaOperand = GetData<FormulaOperand>("FormulaOperand");
            FormulaOperand = dataFormulaOperand
                .Where(x => x.ExperimentId.Contains(experimentId))
                .ToArray();

            var dataTaskConclusion = GetData<TaskConclusion>("TaskConclusion");
            TaskConclusion = dataTaskConclusion
                .ToArray();

            var dataTaskVariable = GetData<TaskVariable>("TaskVariable");
            TaskVariable = dataTaskVariable
                .ToArray();
        }

        private T[] GetData<T>(string nameFile)
        {
            string path = Application.streamingAssetsPath + "/Data/" + nameFile + ".json";
            string file = File.ReadAllText(path);
            string newJson = "{ \"Items\": " + file + "}";

            var data = JsonHelper.FromJson<T>(newJson);

            return data;
        }
    }
}
