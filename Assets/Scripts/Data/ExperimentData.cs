﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AmazyingPhysics
{
    [Serializable]
    public class Experiment
    {
        public int Id;
        public string NameRu;
        public string DescriptionRu;
        public int PhysicsCategoryId;
    }
}

namespace AmazyingPhysics.Data
{
    [Serializable]
    public class ExperimentData
    {
        public Dictionary<int, Experiment> Experiments { get; private set; }

        public ExperimentData()
        {
            LoadData();
        }
        
        private void LoadData()
        {
            string path = Application.streamingAssetsPath + "/Data/Experiment.json";
            string file = null;
            
            if (Application.platform == RuntimePlatform.Android)
            {
                WWW reader = new WWW(path);
                while (!reader.isDone) { }

                file = reader.text;
            }
            else
            {
                file = File.ReadAllText(path);
            }

            string json = "{ \"Items\": " + file + "}";
            var data = JsonHelper.FromJson<Experiment>(json);

            Experiments = new Dictionary<int, Experiment>();

            foreach (var item in data)
            {
                Experiments.Add(
                   item.Id,
                   new Experiment
                   {
                       Id = item.Id,
                       NameRu = item.NameRu,
                       DescriptionRu = item.DescriptionRu,
                       PhysicsCategoryId = item.PhysicsCategoryId,
                   });
            }
        }        
    }
}
