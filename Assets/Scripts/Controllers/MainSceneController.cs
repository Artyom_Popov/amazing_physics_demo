﻿using System.Collections;
using System.Linq;
using AmazyingPhysics.Core;

namespace AmazyingPhysics
{
    public class MainSceneController
    {
        private readonly MainSceneUIManager _uiManager;
        private readonly ExperimentManagerCore _experimentManagerCore;

        public MainSceneController(ExperimentManagerCore experimentManagerCore, MainSceneUIManager uiManager)
        {
            _experimentManagerCore = experimentManagerCore;
            _uiManager = uiManager;

            Init();
        }

        private void Init()
        {
            _uiManager.ChoicePanel.InitChoice();
            _uiManager.ChoicePanel.ChoiceElmnts[0].OnClickChoice += ChoicePanel_OnClickChoice;
            _uiManager.ChoicePanel.ChoiceElmnts[4].OnClickChoice += ChoicePanel_OnExit;

            var ids = _experimentManagerCore.PhysicsCategoryData.PhysicsCategories.Select(c => c.Key).ToArray();
            _uiManager.ExperimentPanel.Constructor(_experimentManagerCore.PhysicsCategoryData.PhysicsCategories, ids, _experimentManagerCore.ExperimentData.Experiments);
            _uiManager.ExperimentPanel.PhysicsCategory.OnClickPhysicsCategory += ExperimentPanel_OnClickPhysicsCategory;
            _uiManager.ExperimentPanel.Experiment.OnClickExperiment += ExperimentPanel_OnClickExperiment;
            _uiManager.ExperimentPanel.Apply.StartExperimentElmnt.OnClick += () => StartExperimentElmnt_OnClick();
            
            ScenesManager.Instance.OnSetActiveMainScene += _uiManager.SetActiveUI;
        }

        #region Experiment block

        private void ExperimentPanel_OnClickPhysicsCategory(int physicsCategoryId)
        {
            _experimentManagerCore.CurrentPhysicsCategoryId = physicsCategoryId;

            _uiManager.ExperimentPanel.Experiment.SelectGroupExperiment(physicsCategoryId);            
        }

        private void ExperimentPanel_OnClickExperiment(int experimentId)
        {
            _experimentManagerCore.CurrentExperimentId = experimentId;

            _uiManager.ExperimentPanel.AimText.SetTextAimExperiment("Цель работы:\n" + _experimentManagerCore.CurrentExperiment.DescriptionRu);
        }

        private void StartExperimentElmnt_OnClick()
        {
            if(_uiManager.ExperimentPanel.Experiment.IsSelectExperiment())
            {
                ExtensionCoroutine.Instance.StartExtendedCoroutine(CorLoadLabScene());
            }
            else
            {
                var m = ModalWindowManager.Instance.CreateModelWindow();
                m.InitWindow("Пожалуйста, выберите эксперимент");
            }                   
        }

        private IEnumerator CorLoadLabScene()
        {
            _uiManager.SetActiveUI(false);            
            yield return ExtensionCoroutine.Instance.StartCorExtendedCoroutine(ScenesManager.Instance.CorLoadLabScene(_experimentManagerCore.CurrentExperiment));            
        }

        #endregion

        #region ChoicePanel block

        private void ChoicePanel_OnClickChoice(int elmntId)
        {
            _uiManager.ChoicePanel.SetActivePanel(false);
            _uiManager.ExperimentPanel.SetActivePanel(true);
            _uiManager.ChoicePanel.BackElmnt.OnClickBack += ChoicePanel_OnClickBack;
        }

        private void ChoicePanel_OnClickBack()
        {
            _uiManager.ExperimentPanel.SetActivePanel(false);
            _uiManager.ChoicePanel.BackElmnt.OnClickBack -= ChoicePanel_OnClickBack;
        }

        private void ChoicePanel_OnExit(int elmntId)
        {
            ScenesManager.Instance.Quit();
        }

        #endregion
    }
}
