﻿using AmazyingPhysics.Core;
using System.Linq;

namespace AmazyingPhysics
{
    public class InventoryController
    {
        private readonly InventoryCore _inventoryCore;
        private readonly ExperimentTaskCore _experimentTaskCore;
        private readonly PickableInventoryManager _pickableInventoryManager;
        private readonly CameraManager _cameraManager;
        private readonly SimulationManager _simulationManager;
        private readonly Gizmo _gizmo;
        private readonly MouseHoverState _mouseHoverState;
        private readonly LabSceneUIManager _uiManager;

        public InventoryController (
                InventoryCore inventoryCore, 
                ExperimentTaskCore experimentTaskCore, 
                PickableInventoryManager pickableInventoryManager, 
                CameraManager cameraManager, 
                SimulationManager simulationManager, 
                Gizmo gizmo, 
                MouseHoverState mouseHoverState, 
                LabSceneUIManager uiManager
            )
        {
            _inventoryCore = inventoryCore;
            _experimentTaskCore = experimentTaskCore;
            _pickableInventoryManager = pickableInventoryManager;
            _cameraManager = cameraManager;
            _simulationManager = simulationManager;
            _gizmo = gizmo;
            _mouseHoverState = mouseHoverState;
            _uiManager = uiManager;

            Init();
        }

        public void Init()
        {
            _uiManager.HeaderPanel.Constructor(_inventoryCore.Experiment.NameRu);
            _uiManager.HeaderPanel.SettingsPanel.OnSettings += SettingsPanel_OnSettings;
            _uiManager.HeaderPanel.SettingsPanel.OnClose += SettingsPanel_OnClose;

            _uiManager.TaskPanel.Constructor (
                _inventoryCore.Experiment.Id,
                _experimentTaskCore.Task,
                _experimentTaskCore.FormulaData.TaskFormula,
                _experimentTaskCore.FormulaData.FormulaOperator,
                _experimentTaskCore.FormulaData.FormulaOperand,
                _experimentTaskCore.FormulaData.TaskConclusion,
                _experimentTaskCore.FormulaData.TaskVariable
            );

            _uiManager.InventoryPanel.InitPanel(_inventoryCore.InventoryData.Inventories);
            _uiManager.InventoryPanel.OnClickElmnt += InventoryPanel_OnClickElmnt;
            _uiManager.InventoryActPanel.OnDeactivatePanel += InventoryActPanel_OnDeactivatePanel;

            _uiManager.ControlAppPanel.OnClickElmnt += ControlAppPanel_OnClickElmnt;

            _uiManager.TableResultPanel.OnClickControlElmnt += TableResultPanel_OnClickControlElmnt;

            _uiManager.FooterPanel.SettingsPanel.elmntSimulation.OnPlaySimulation += SimulationLaunchElmnt_OnPlaySimulation;
            _uiManager.FooterPanel.SettingsPanel.OnMainView += SettingsPanel_OnMainView;
            _uiManager.FooterPanel.SettingsPanel.OnCheckExperiment += CheckExperimentElmnt_OnClick;
            
            _pickableInventoryManager.gameObject.SetActive(true);
            _pickableInventoryManager.Constructor(_simulationManager, _cameraManager, _gizmo, _mouseHoverState);
            _pickableInventoryManager.OnSelectInventory += PickableInventoryManager_OnSelectInventory;

            _gizmo.Constructor(_cameraManager.ExtendCamera.CustomCamera);
            _gizmo.OnFocus += Gizmo_OnFocus;

            _cameraManager.ExtendCamera.OnChangeTargetPos.OnValueChange += _uiManager.InventoryActPanel.Deactivate;
        }

        private void Gizmo_OnFocus()
        {
            _cameraManager.ExtendCamera.SetTargetPos(_pickableInventoryManager.InventoryPointer.CurrentInventory.CenterItem);
        }

        private void SettingsPanel_OnMainView()
        {
            _cameraManager.ExtendCamera.SetView(0);
        }

        private void SettingsPanel_OnSettings()
        {
        }

        private void SettingsPanel_OnClose()
        {
            _uiManager.ControlAppPanel.SetActivePanel(true);           
        }

        private void InventoryPanel_OnClickElmnt(Inventory inventory)
        {
            var item = _pickableInventoryManager.InventoryCreator.Create(inventory, _inventoryCore.Experiment.Id);

            if(item.InventorySettings.InventoryActionsSimModePanel != null)
            {
                item.InventorySettings.InventoryActionsSimModePanel.CamView.OnClick += _cameraManager.ExtendCamera.SetView;
                item.InventorySettings.InventoryActionsSimModePanel.ActionsMenu.OnClick += ActionsMenu_OnClick;
            }            
        }

        private void ActionsMenu_OnClick(PickableInventoryBase inventory)
        {
            _pickableInventoryManager.SelectInventoryPointer(inventory);
            PickableInventoryManager_OnSelectInventory(inventory);
        }

        private void TableResultPanel_OnClickControlElmnt(ETableControl tableControl)
        {
            switch (tableControl)
            {
                case ETableControl.ShowMistake:
                    break;
                case ETableControl.Repeat:
                    ExtensionCoroutine.Instance.StartExtendedCoroutine(ScenesManager.Instance.CorReLoadLabScene());
                    break;
                case ETableControl.ExitToMainMenu:
                    ExtensionCoroutine.Instance.StartExtendedCoroutine(ScenesManager.Instance.CorBackToMainScene());
                    break;
                default:
                    break;
            }
        }

        private void ControlAppPanel_OnClickElmnt(EControlApp controlApp)
        {
            switch (controlApp)
            {
                case EControlApp.Repeat:
                    ExtensionCoroutine.Instance.StartExtendedCoroutine(ScenesManager.Instance.CorReLoadLabScene());
                    break;
                case EControlApp.ExitToMainMenu:
                    ExtensionCoroutine.Instance.StartExtendedCoroutine(ScenesManager.Instance.CorBackToMainScene());
                    break;
                case EControlApp.ExitFromApp:
                    ScenesManager.Instance.Quit();
                    break;
                default:
                    break;
            }
        }

        private void CheckExperimentElmnt_OnClick()
        {
            _uiManager.TableResultPanel.SetActivePanel(true);
        }

        private void InventoryActPanel_OnDeactivatePanel()
        {
            if(!_pickableInventoryManager.InventoryPointer.PointerItemIsNull() && _pickableInventoryManager.InventoryPointer.CurrentInventory.InventorySettings.InventoryActionsSimModePanel != null)
            {
                _pickableInventoryManager.InventoryPointer.CurrentInventory.InventorySettings.InventoryActionsSimModePanel.SetActive(true);
            }
            _pickableInventoryManager.DeselectInventoryPointer();              
        }

        private void PickableInventoryManager_OnSelectInventory(PickableInventoryBase inventory)
        {
            if(_simulationManager.IsEnableSimulation)
            {
                if (inventory != null)
                {
                    _uiManager.InventoryActPanel.Init(inventory.GetInventoryActions().ToArray());

                    if (_pickableInventoryManager.InventoryPointer.CurrentInventory.InventorySettings.InventoryActionsSimModePanel != null)
                    {
                        _pickableInventoryManager.InventoryPointer.CurrentInventory.InventorySettings.InventoryActionsSimModePanel.SetActive(false);
                    }
                }                           
            }        
        }
               
        private void SimulationLaunchElmnt_OnPlaySimulation(bool value)
        {
            if (value)
            {
                _pickableInventoryManager.DeleteCreatedInventory();                
            }
            else
            {
                _uiManager.InventoryActPanel.Deactivate();
            }
          
            _simulationManager.LaunchSimulation(value);
            _uiManager.InventoryPanel.ShowHidePanel(value);
        }        
    }
}