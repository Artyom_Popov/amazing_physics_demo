﻿using AmazyingPhysics.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

namespace AmazyingPhysics
{
    public class ElectricCircuitController
    {
        private readonly ElectricCircuitCore _electricCircuitCore;
        private readonly PickableInventoryManager _pickableInventoryManager;
        private readonly SimulationManager _simulationManager;
        private readonly WireManager _wireManager;

        public ElectricCircuitController(ElectricCircuitCore electricCircuitCore, PickableInventoryManager pickableInventoryManager, SimulationManager simulationManager, WireManager wireManager)
        {
            _electricCircuitCore = electricCircuitCore;
            _pickableInventoryManager = pickableInventoryManager;
            _simulationManager = simulationManager;
            _wireManager = wireManager;

            Init();
        }   
        
        public void Init()
        {
            _pickableInventoryManager.OnSelectPlug += PickableInventoryManager_OnPlug;
            _pickableInventoryManager.OnMissPlug += PickableInventoryManager_OnMissPlug;

            _simulationManager.OnActiveSimulation += SimulationManager_OnActiveSimulation;
        }      

        private void SimulationManager_OnActiveSimulation(bool value)
        {
            if(value)
            {
                foreach (var item in _pickableInventoryManager.InventoryCreator.CreatedInventory.Inventories)
                {
                    IElectricElmntBase electricElmnt = item as IElectricElmntBase;

                    if(electricElmnt != null)
                    {
                        _electricCircuitCore.CreatedElectricElmnts.Add(electricElmnt);
                        //Debug.Log("id  electric elmnt: " + electricElmnt.ElectricElmntId);
                    }
                }
                                
                _electricCircuitCore.EnableSimulation();
            }
            else
                _electricCircuitCore.DisableSimulation();
        }

        private void PickableInventoryManager_OnPlug(Plug plug, Transform end)
        {
            _electricCircuitCore.GetPlug(plug, end);
        }

        private void PickableInventoryManager_OnMissPlug()
        {
            _electricCircuitCore.ResetSelectPlug();
        }
    }
}