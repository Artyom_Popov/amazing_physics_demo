﻿using AmazyingPhysics.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace AmazyingPhysics
{
    public enum EInventoryLaunchMode { Default, Create, Transform, Settings, Erase, Measure }
}

namespace AmazyingPhysics.Core
{
    public class InventoryCore
    {
        public EInventoryLaunchMode InventoryLaunchMode { get; set; }
        public InventoryData InventoryData { get; private set; } 
        public Experiment Experiment { get; private set; }      

        public InventoryCore(Experiment experiment)
        {
            InventoryData = new InventoryData(experiment.PhysicsCategoryId);
            Experiment = experiment;
        }
    }
}
