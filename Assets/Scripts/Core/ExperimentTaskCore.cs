﻿using AmazyingPhysics.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace AmazyingPhysics.Core
{
    public class ExperimentTaskCore
    {
        private ExperimentTaskData ExperimentTask { get; set; }
        public FormulaData FormulaData { get; private set; }
        
        public string[] Task { get; private set; }        

        public ExperimentTaskCore(Experiment experiment)
        {
            ExperimentTask = new ExperimentTaskData(experiment.Id);
            GenerateTask();

            FormulaData = new FormulaData(experiment.PhysicsCategoryId, experiment.Id);
        }

        private void GenerateTask()
        {
            var task = ExperimentTask.Task;   
            
            if(task != null)
            {
                char[] delimiter = "$".ToCharArray();
                Task = task.Split(delimiter);
            }          
        }
    }
}
