﻿using AmazyingPhysics.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AmazyingPhysics.Core
{
    public class ExperimentManagerCore
    {
        public ExperimentManagerCore()
        {
            ExperimentData = new ExperimentData();
            PhysicsCategoryData = new PhysicsCategoryData();
        }

        public ExperimentData ExperimentData { get; private set; }

        public Experiment CurrentExperiment
        {
            get
            {
                return ExperimentData.Experiments[CurrentExperimentId];
            }            
        }

        public int CurrentExperimentId { get; set; }

        public Dictionary<int, Experiment> GetExperimentsByPhysicsCategoryId(int physicsCategoryId)
        {
            return ExperimentData.Experiments
                .Where(e => e.Value.PhysicsCategoryId == physicsCategoryId)
                .ToDictionary(k => k.Key, v => v.Value);
        }

        public PhysicsCategoryData PhysicsCategoryData { get; private set; }

        public string CurrentPhysicsCategory
        {
            get
            {
                return PhysicsCategoryData.PhysicsCategories[CurrentPhysicsCategoryId];
            }
        }

        public int CurrentPhysicsCategoryId { get; set; }      
    }    
}
