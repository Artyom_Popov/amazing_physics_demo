using System;

public enum EInventoryActionType
{
    Button,
    Slider,
    Toggle,
}

public interface IButtonInventory
{
    string ActionName { get; set; }
    Action Action { get; set; }
}

public class ButtontnInventory : IButtonInventory
{
    public ButtontnInventory(string actionName, Action action)
    {
        ActionName = actionName;
        Action = action;
    }

    public Action Action { get; set; }    
    public string ActionName { get; set; }
}

public class ButtonInventoryFactory : IinventoryAct
{
    public ButtonInventoryFactory(string actionName, Action action)
    {
        Type = EInventoryActionType.Button;
        Button = new ButtontnInventory(actionName, action);        
    }

    public EInventoryActionType Type { get; private set; }
    public IButtonInventory Button { get; set; }
    ISliderInventory IinventoryAct.Slider { get; set; }
    IToggleInventory IinventoryAct.Toggle { get; set; }
}

public interface ISliderInventory
{
    string[] ActionName { get; set; }
    Action<float> Action { get; set; }    
    SliderSettings Settings { get; set; }
}

public class SliderInventory : ISliderInventory
{
    public SliderInventory(string[] actionName, Action<float> action, SliderSettings settings)
    {
        ActionName = actionName;
        Action = action;
        Settings = settings;
    }
    
    public string[] ActionName { get; set; }
    public Action<float> Action { get; set; }    
    public SliderSettings Settings { get; set; }
}

public class SliderInventoryFactory : IinventoryAct
{
    public SliderInventoryFactory(string[] actionName, Action<float> action, SliderSettings settings)
    {
        Type = EInventoryActionType.Slider;
        Slider = new SliderInventory(actionName, action, settings);        
    }

    public EInventoryActionType Type { get; private set; }
    public ISliderInventory Slider { get; set; }
    IButtonInventory IinventoryAct.Button { get; set; }
    IToggleInventory IinventoryAct.Toggle { get; set; }

}

public interface IToggleInventory
{
    string[] ActionName { get; set; }
    Action<bool> Action { get; set; }
    bool State { get; set; }
}

public class ToggleInventory : IToggleInventory
{
    public ToggleInventory(string[] actionName, Action<bool> action, bool state)
    {
        ActionName = actionName;
        Action = action;
        State = state;
    }

    public Action<bool> Action { get; set; }
    public string[] ActionName { get; set; }
    public bool State { get; set; }
}

public class ToggleInventoryFactory : IinventoryAct
{
    public ToggleInventoryFactory(string[] actionName, Action<bool> action, bool state)
    {
        Type = EInventoryActionType.Toggle;
        Toggle = new ToggleInventory(actionName, action, state);
    }

    public EInventoryActionType Type { get; private set; }    
    public IToggleInventory Toggle { get; set; }
    ISliderInventory IinventoryAct.Slider { get; set; }    
    IButtonInventory IinventoryAct.Button { get; set; }

}

public interface IinventoryAct
{
    EInventoryActionType Type { get; }
    IButtonInventory Button { get; set; }
    ISliderInventory Slider { get; set; }
    IToggleInventory Toggle { get; set; }
}

public class SliderSettings
{
    public SliderSettings(float value, float min, float max)
    {
        Value = value;
        Min = min;
        Max = max;
    }

    public float Value { get; set; }
    public float Min { get; set; }
    public float Max { get; set; }
}