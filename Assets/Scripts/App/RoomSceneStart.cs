﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class RoomSceneStart : MonoBehaviour
    {
        [SerializeField]
        private GameObject _markerBoard;

        public void Constructor(int experimentId)
        {
            SetImageTask(experimentId);
        }

        private void SetImageTask(int experimentId)
        {
            var texture = Resources.Load<Texture>("MarkerBoardImgs/txt_exp_" + experimentId.ToInventoryNameId());

            var mats = _markerBoard.GetComponent<MeshRenderer>().materials;
            mats[1].mainTexture = texture;
        }        
    }
}
