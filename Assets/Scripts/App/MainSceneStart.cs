﻿using AmazyingPhysics.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

namespace AmazyingPhysics
{
    public class MainSceneStart : MonoBehaviour
    {
        [Inject]
        private MainSceneUIManager _uiManager;

        private void Start()
        {
            var experimentManagerCore = new ExperimentManagerCore();
            var mainSceneController = new MainSceneController(experimentManagerCore, _uiManager);
        }        
    }
}
