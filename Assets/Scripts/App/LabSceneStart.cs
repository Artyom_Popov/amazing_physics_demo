﻿using UnityEngine;
using System.Collections;
using AmazyingPhysics;
using Zenject;
using System;
using AmazyingPhysics.Core;

namespace AmazyingPhysics
{
    public class LabSceneStart : MonoBehaviour
    {
        #region Fields

        [Inject]
        private LabSceneUIManager _uiManager;
        
        [Inject]
        private PickableInventoryManager _pickableInventoryManager;

        [Inject]
        private CameraManager _cameraManager;

        [Inject]
        private SimulationManager _simulationManager;
        
        [Inject]
        private WireManager _wireManager;

        [Inject]
        private Gizmo _gizmo;

        [Inject]
        private MouseHoverState _mouseHoverState;

        #endregion

        ////для тестирования
        //private void Start()
        //{
        //    Experiment experiment = new Experiment();

        //    // электрика
        //    //experiment.Id = 13;
        //    //experiment.NameRu = "Test";
        //    //experiment.PhysicsCategoryId = 4;

        //    //// оптика
        //    experiment.Id = 17;
        //    experiment.NameRu = "Test";
        //    experiment.PhysicsCategoryId = 6;

        //    // механика
        //    //experiment.Id = 5;
        //    //experiment.NameRu = "Test";
        //    //experiment.PhysicsCategoryId = 2;

        //    // термодинамика
        //    //experiment.Id = 23;
        //    //experiment.NameRu = "Test";
        //    //experiment.PhysicsCategoryId = 3;

        //    Constructor(experiment);
        //}

        public void Constructor(Experiment experiment)
        {
            var inventoryCore = new InventoryCore(experiment);
            var experimentTaskCore = new ExperimentTaskCore(experiment);
            var inventoryController = new InventoryController(
                    inventoryCore, 
                    experimentTaskCore, 
                    _pickableInventoryManager, 
                    _cameraManager, 
                    _simulationManager, 
                    _gizmo, 
                    _mouseHoverState, 
                    _uiManager
                );
            
            // постоянный электрический ток и электричество и магнетизм
            if (experiment.PhysicsCategoryId == 4 || experiment.PhysicsCategoryId == 5)
            {
                var electricCircuitCore = new ElectricCircuitCore(_wireManager);
                var electricCircuitController = new ElectricCircuitController(electricCircuitCore, _pickableInventoryManager, _simulationManager, _wireManager);
            }
        }
    }
}
