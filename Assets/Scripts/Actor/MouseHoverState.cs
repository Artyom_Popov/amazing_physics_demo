﻿using UnityEngine;

namespace AmazyingPhysics
{
    public enum EItemStage
    {
        Default,
        Take,
        MoveOnTable,
        MoveOnAbstractArea,
        Connect,       
        Gizmo
    };

    public class MouseHoverState : MonoBehaviour
    {
        public Texture2D _take;
        public Texture2D _moveOnTable;
        public Texture2D _moveOnAbstractArea;
        public Texture2D _connect;        
        public Texture2D _gizmo;
        
        private bool _isCrosshair;
        private Texture2D _currentTexture;
        
        void OnGUI()
        {
            if (_isCrosshair)
            {
                var mousePos = Input.mousePosition;
                Rect rect = new Rect(mousePos.x- _currentTexture.width/2, Screen.height - mousePos.y - _currentTexture.height/2, _currentTexture.width, _currentTexture.height);                
                GUI.DrawTexture(rect, _currentTexture);
            }
        }        

        public void SetCrosshair(EItemStage itemStage)
        {            
            SetActiveCursor(false);
            
            switch (itemStage)
            {
                case EItemStage.Default:
                    SetActiveCursor(true);                   
                    break;
                case EItemStage.Take:
                    _currentTexture = _take;
                    break;
                case EItemStage.MoveOnTable:
                    _currentTexture = _moveOnTable;
                    break;
                case EItemStage.MoveOnAbstractArea:
                    _currentTexture = _moveOnAbstractArea;
                    break;
                case EItemStage.Connect:
                    _currentTexture = _connect;
                    break;              
                case EItemStage.Gizmo:
                    _currentTexture = _gizmo;
                    break;
                default:
                    break;
            }
        }

        private void SetActiveCursor(bool value)
        {
            Cursor.visible = value;
            _isCrosshair = !value;            
        }

        private void OnDisable()
        {
            SetActiveCursor(true);
        }
    }
}
