﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Zenject;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AmazyingPhysics
{
    public class ScenesManager : Singleton<ScenesManager>
    {
        private string _mainScene = "_Main";
        private string _labScene = "_Lab";
        private string _roomScene = "_Room";
        private Experiment _bufferExperiment;

        public GameObject _loader;

        public event Action<bool> OnSetActiveMainScene;
        
        public bool IsLoaded { get; private set; }
        //public GameObject GoLabSceneApp { get; private set; }
        //public GameObject GoRoomSceneApp { get; private set; }

        /// <summary>
        /// вернуться к главной сцене
        /// </summary>
        /// <returns></returns>
        public IEnumerator CorBackToMainScene()
        {
            IsLoaded = false;
            SetActiveLoader(true);

            Scene scene = SceneManager.GetSceneByName(_mainScene);
            SceneManager.SetActiveScene(scene);
            
            yield return StartCoroutine(UnloadScene(_roomScene));
            yield return StartCoroutine(UnloadScene(_labScene));

            if (OnSetActiveMainScene != null)
                OnSetActiveMainScene(true);

            SetActiveLoader(false);
            IsLoaded = true;
        }

        /// <summary>
        /// загрузить сцену с лабораторией
        /// </summary>
        /// <returns></returns>
        public IEnumerator CorLoadLabScene(Experiment experiment)
        {
            IsLoaded = false;
            SetActiveLoader(true);

            yield return StartCoroutine(LoadAdditiveScene(_labScene));
            var _labSceneStart = GetStartScript<LabSceneStart>(_labScene, "LabSceneApp");          
            _labSceneStart.Constructor(experiment);

            yield return StartCoroutine(LoadAdditiveScene(_roomScene));            
            var _roomSceneApp = GetStartScript<RoomSceneStart>(_roomScene, "RoomSceneApp");
            _roomSceneApp.Constructor(experiment.Id);
            
            Scene scene = SceneManager.GetSceneByName(_roomScene);
            SceneManager.SetActiveScene(scene);

            _bufferExperiment = experiment;

            SetActiveLoader(false);
            IsLoaded = true;
        }

        /// <summary>
        /// перегрузить сцену с лабораторией
        /// </summary>
        /// <returns></returns>
        public IEnumerator CorReLoadLabScene()
        {
            IsLoaded = false;
            SetActiveLoader(true);

            Scene scene = SceneManager.GetSceneByName(_mainScene);
            SceneManager.SetActiveScene(scene);

            // выгрузить сцены
            yield return StartCoroutine(UnloadScene(_roomScene));            
            yield return StartCoroutine(UnloadScene(_labScene));

            yield return StartCoroutine(CorLoadLabScene(_bufferExperiment));            
        }

        /// <summary>
        /// загрузит аддитивно сцну
        /// </summary>
        /// <param name="scene">имя сцены</param>
        /// <returns></returns>
        private IEnumerator LoadAdditiveScene(string scene)
        {
            AsyncOperation async = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
            
            while (!async.isDone)
            {
                Debug.Log("loading scene: " + _labScene);
                yield return null;
            }
        }

        /// <summary>
        /// выгрузить сцену
        /// </summary>
        /// <param name="scene">имя сцены</param>
        /// <returns></returns>
        private IEnumerator UnloadScene(string scene)
        {        
            var async = SceneManager.UnloadSceneAsync(scene);
            
            while(true)
            {
                if (async != null)
                {
                    if (async.isDone)
                    {
                        var asyncUnload = Resources.UnloadUnusedAssets();
                        yield return asyncUnload;
                        yield break;                        
                    }                        
                } 
                              
                yield return null;
            }            
        }

        public void Quit()
        {
            Application.Quit();
        }

        private void SetActiveLoader(bool value)
        {
            _loader.SetActive(value);
        }

        private T GetStartScript<T>(string sceneName, string nameSceneApp)
        {
            Scene scene = SceneManager.GetSceneByName(sceneName);

            var obj = scene.GetRootGameObjects()
                .Where(n => n.name == nameSceneApp)
                .Select(g => g.gameObject)
                .First();

            T startScript = obj.GetComponent<T>();

            return startScript;
        }
    }
}