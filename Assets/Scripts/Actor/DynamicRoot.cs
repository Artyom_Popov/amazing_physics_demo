﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmazyingPhysics
{
    public class DynamicRoot : Singleton<DynamicRoot>
    {
        private Transform _transform;

        public Transform DynamicRootTransform
        {
            get { return _transform; }            
        }

        private void Awake()
        {
            _transform = GetComponent<Transform>();
        }
    }
}
