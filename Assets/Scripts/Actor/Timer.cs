﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public class Timer : Singleton<Timer>
    {
        [SerializeField]
        private GameObject _root;

        [SerializeField]
        private GameObject _txtTimerPref;
        
        private Text _txtTimer;
        private List<GameObject> _timerValues = new List<GameObject>();
        private int _minutes;
        private int _seconds;
        private int _fraction;
        private float _startTime;
        private float _playTime;
        private bool _isTimerEnable;

        void Update()
        {         
            if (_isTimerEnable)
            {
                _playTime = Time.time - _startTime;
                _minutes = (int)(_playTime / 60f);
                _seconds = (int)(_playTime % 60f);
                _fraction = (int)((_playTime * 10) % 10);
                _txtTimer.text = string.Format("{0}\'{1}\"{2}", _minutes, _seconds, _fraction);                
            }
        }

        private void Start()
        {
            var txtTimerInstance = Instantiate(_txtTimerPref);
            txtTimerInstance.transform.SetParent(_root.transform, false);
            _txtTimer = txtTimerInstance.GetComponent<Text>();
            _txtTimer.color = Color.yellow;
        }

        private void SetActive(bool value)
        {
            _root.SetActive(value);
        }

        public void StartTimer()
        {
            SetActive(true);
            //Time.timeScale = 1;
            _startTime = Time.time;
            _isTimerEnable = true;
        }

        public void StopTimer()
        {
            SetActive(false);
            //Time.timeScale = 0;
            _startTime = Time.time;
            _isTimerEnable = false;

            foreach (var item in _timerValues)
            {
                Destroy(item);
            }
            _timerValues.Clear();
        }

        public void PinTimer()
        {
            var txtTimerInstance = Instantiate(_txtTimerPref);
            txtTimerInstance.transform.SetParent(_root.transform, false);
            txtTimerInstance.GetComponent<Text>().color = Color.red;
            txtTimerInstance.GetComponent<Text>().text = _txtTimer.text;
            _timerValues.Add(txtTimerInstance);
        }
    }
}