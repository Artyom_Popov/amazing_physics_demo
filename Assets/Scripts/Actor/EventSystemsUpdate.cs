﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public class EventSystemsUpdate : MonoBehaviour
    {
        [SerializeField]
        private GameObject _root;

        private void Start()
        {
            SetActive(false);
            SetActive(true);
        }

        private void SetActive(bool value)
        {
            _root.SetActive(value);
        }        
    }
}