﻿using UnityEngine;
using System.Collections;
using System;

namespace AmazyingPhysics
{
    public class FlyCamera : MonoBehaviour
    {
        public Camera camera;

        public float cameraSensitivity = 90;
        public float climbSpeed = 4;
        public float normalMoveSpeed = 10;
        public float slowMoveFactor = 0.25f;
        public float fastMoveFactor = 3;

        private float rotationX = 0.0f;
        private float rotationY = 0.0f;
        
        public bool IsCamerEnable { get; set; }       

        void Update()
        {
            if (IsCamerEnable == false) return;

            rotationX += Input.GetAxis("Mouse X") * cameraSensitivity * Time.deltaTime;
            rotationY += Input.GetAxis("Mouse Y") * cameraSensitivity * Time.deltaTime;
            rotationY = Mathf.Clamp(rotationY, -90, 90);

            camera.transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
            camera.transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);

            //if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            //{
            //    transform.position += transform.forward * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
            //    transform.position += transform.right * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
            //}
            //else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            //{
            //    transform.position += transform.forward * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
            //    transform.position += transform.right * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
            //}
            //else
            //{
            camera.transform.position += camera.transform.forward * normalMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
            camera.transform.position += camera.transform.right * normalMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
            //}


            if (Input.GetKey(KeyCode.Q)) { camera.transform.position += camera.transform.up * climbSpeed * Time.deltaTime; }
            if (Input.GetKey(KeyCode.E)) { camera.transform.position -= camera.transform.up * climbSpeed * Time.deltaTime; }

            

            //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //RaycastHit hit;

            //if (Physics.Raycast(ray, out hit))
            //{
            //    Debug.DrawLine(Camera.main.transform.position, hit.point);
            //}
        }
    }
}