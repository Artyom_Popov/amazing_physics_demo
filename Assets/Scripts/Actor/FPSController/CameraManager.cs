﻿using System.Collections;
using UnityEngine;

namespace AmazyingPhysics
{
    public class CameraManager : MonoBehaviour
    {
        [SerializeField]
        private Transform _posToView;
        
        private Vector3 _currentCamPos;
        private Quaternion _currentCamRot;        
        private bool _isCamStayToView;
        private float _journeyTime = 1;
        private float _startTime;

        public ExtendCamera ExtendCamera;
        
        private void Start()
        {
            _currentCamPos = ExtendCamera.CustomCamera.transform.position;           
        }
        
        private void Update()
        {
            if (Input.GetKey(KeyCode.Tab))
            {
                if(!_isCamStayToView)
                {
                    _isCamStayToView = true;
                    
                    StopAllCoroutines();
                    StartCoroutine(CorCamPosToView(_posToView));
                }               
            }

            if (Input.GetKeyUp(KeyCode.Tab))
            {
                StopAllCoroutines();
                StartCoroutine(CorCamPosOrigin());

                _isCamStayToView = false;
            }        
        }
        
        public void EnableCamera()
        {
            ExtendCamera.IsEnable = true;
        }

        public void DisableCamera()
        {
            ExtendCamera.IsEnable = false;
        }
               
        private IEnumerator CorCamPosToView(Transform trans)
        {
            DisableCamera();

            _currentCamPos = ExtendCamera.CustomCamera.transform.position;
            _currentCamRot = ExtendCamera.CustomCamera.transform.rotation;

            _startTime = Time.time;
            print(ExtendCamera.CustomCamera.transform.position.z + "    " + trans.position.z);
            while (!Mathf.Approximately(ExtendCamera.CustomCamera.transform.position.z, trans.position.z))
            {
                float fracComplete = (Time.time - _startTime) / _journeyTime;
                ExtendCamera.CustomCamera.transform.position = Vector3.Slerp(ExtendCamera.CustomCamera.transform.position, trans.position, fracComplete);
                ExtendCamera.CustomCamera.transform.rotation = Quaternion.Slerp(ExtendCamera.CustomCamera.transform.rotation, trans.rotation, fracComplete);

                print("CorCamPosToView");

                yield return null;
            }
        }              

        private IEnumerator CorCamPosOrigin()
        {
            DisableCamera();

            _startTime = Time.time;
            while (!Mathf.Approximately(ExtendCamera.CustomCamera.transform.position.z, _currentCamPos.z))
            {
                float fracComplete = (Time.time - _startTime) / _journeyTime;
                ExtendCamera.CustomCamera.transform.position = Vector3.Slerp(ExtendCamera.CustomCamera.transform.position, _currentCamPos, fracComplete);
                ExtendCamera.CustomCamera.transform.rotation = Quaternion.Slerp(ExtendCamera.CustomCamera.transform.rotation, _currentCamRot, fracComplete);

                print("CorCamPosOrigin");

                yield return null;
            }

            ExtendCamera.CustomCamera.transform.position = _currentCamPos;
            ExtendCamera.CustomCamera.transform.rotation = _currentCamRot;

            EnableCamera();
        }

       
    }    
}
