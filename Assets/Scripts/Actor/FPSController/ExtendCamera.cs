﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    [Serializable]
    public class CameraViewSettings
    {
        public Transform Target;
        public float Distance;
    }

    public class ExtendCamera : MonoBehaviour
    {
        [SerializeField]
        private Settings _settings;

        private Vector3 _position;        
        private bool _cachePointerUI;
        private bool _isProcessSetView;

        public Camera CustomCamera { get; private set; }
        public bool IsEnable { get; set; }
        public ObservedValue<Vector3> OnChangeTargetPos;

        public List<CameraViewSettings> cameraViewSettings;
        
        private void Awake()
        {
            CustomCamera = GetComponent<Camera>();
        }

        private void Start()
        {
            IsEnable = true;

            _settings.target.targetDistance = _settings.target.currentDistance;

            if (_settings.axisX.invertAxisX)
            {
                _settings.axisX.invertXValue = -1;
            }
            else
            {
                _settings.axisX.invertXValue = 1;
            }
            if (_settings.axisY.invertAxisY)
            {
                _settings.axisY.invertYValue = -1;
            }
            else
            {
                _settings.axisY.invertYValue = 1;
            }
            if (_settings.zoom.invertAxisZoom)
            {
                _settings.zoom.invertZoomValue = -1;
            }
            else
            {
                _settings.zoom.invertZoomValue = 1;
            }

            _settings.axisX.x = _settings.axisX.initialAngleX;
            _settings.axisY.y = _settings.axisY.initialAngleY;

            transform.Rotate(new Vector3(0f, _settings.axisX.initialAngleX, 0f), Space.World);
            transform.Rotate(new Vector3(_settings.axisY.initialAngleY, 0f, 0f), Space.Self);

            transform.position = ((transform.rotation * new Vector3(0f, 0f, -_settings.target.currentDistance))) + _settings.target.target.position;
            
            OnChangeTargetPos = new ObservedValue<Vector3>(_settings.target.target.position);            
        }

        private void Update()
        {
            if (!IsEnable)
                return;
            
            PressMouseButton();
            ResetCachePointerUI();

            OnChangeTargetPos.Value = _settings.target.target.position;

            if (!_cachePointerUI && _settings.target.target != null)
            {
                if (_settings.control.mouseControl)
                {
                    if ((!_settings.control.clickToRotate || (_settings.control.leftClickToRotate && Input.GetMouseButton(1))) || (_settings.control.rightClickToRotate && Input.GetMouseButton(1)))
                    {
                        StopAllCoroutines();
                        _settings.axisX.xVelocity += (Input.GetAxis(_settings.axisX.mouseAxisX) * _settings.axisX.xSpeed) * _settings.axisX.invertXValue;
                        _settings.axisY.yVelocity -= (Input.GetAxis(_settings.axisY.mouseAxisY) * _settings.axisY.ySpeed) * _settings.axisY.invertYValue;
                    }

                    if (Input.GetMouseButton(2))
                    {
                        StopAllCoroutines();
                        _settings.axisX.xTranslateVelocity += (Input.GetAxis(_settings.axisX.mouseAxisX) * _settings.axisX.xSpeed) * _settings.axisX.invertXValue;
                        _settings.axisY.yTranslateVelocity -= (Input.GetAxis(_settings.axisY.mouseAxisY) * _settings.axisY.ySpeed) * _settings.axisY.invertYValue;
                    }                     
                }

                if (_settings.control.keyboardControl)
                {
                    var x = 0.25f;
                    if (Input.GetKey(KeyCode.A))
                    {
                        _settings.axisX.xTranslateVelocity += (x * _settings.axisX.xSpeed) * _settings.axisX.invertXValue;
                    }
                    else if (Input.GetKey(KeyCode.D))
                    {
                        _settings.axisX.xTranslateVelocity -= (x * _settings.axisX.xSpeed) * _settings.axisX.invertXValue;
                    }
                    else if (Input.GetKey(KeyCode.W))
                    {
                        _settings.axisY.yTranslateVelocity += (x * _settings.axisY.ySpeed) * _settings.axisY.invertYValue;
                    }
                    else if (Input.GetKey(KeyCode.S))
                    {
                        _settings.axisY.yTranslateVelocity -= (x * _settings.axisY.ySpeed) * _settings.axisY.invertYValue;
                    }
                }

                _settings.zoom.zoomVelocity -= (Input.GetAxis(_settings.zoom.mouseAxisZoom) * _settings.zoom.zoomSpeed) * _settings.zoom.invertZoomValue;
            }

            if (_settings.axisX.limitX)
            {
                if ((_settings.axisX.x + _settings.axisX.xVelocity) < (_settings.axisX.xMinLimit + _settings.axisX.xLimitOffset))
                {
                    _settings.axisX.xVelocity = (_settings.axisX.xMinLimit + _settings.axisX.xLimitOffset) - _settings.axisX.x;
                }
                else if ((_settings.axisX.x + _settings.axisX.xVelocity) > (_settings.axisX.xMaxLimit + _settings.axisX.xLimitOffset))
                {
                    _settings.axisX.xVelocity = (_settings.axisX.xMaxLimit + _settings.axisX.xLimitOffset) - _settings.axisX.x;
                }

                _settings.axisX.x += _settings.axisX.xVelocity;

                transform.Rotate(new Vector3(0f, _settings.axisX.xVelocity, 0f), Space.World);
            }
            else
            {
                transform.Rotate(new Vector3(0f, _settings.axisX.xVelocity, 0f), Space.World);
            }
            
            if (_settings.axisY.limitY)
            {
                if ((_settings.axisY.y + _settings.axisY.yVelocity) < (_settings.axisY.yMinLimit + _settings.axisY.yLimitOffset))
                {
                    _settings.axisY.yVelocity = (_settings.axisY.yMinLimit + _settings.axisY.yLimitOffset) - _settings.axisY.y;
                }
                else if ((_settings.axisY.y + _settings.axisY.yVelocity) > (_settings.axisY.yMaxLimit + _settings.axisY.yLimitOffset))
                {
                    _settings.axisY.yVelocity = (_settings.axisY.yMaxLimit + _settings.axisY.yLimitOffset) - _settings.axisY.y;
                }

                _settings.axisY.y += _settings.axisY.yVelocity;

                transform.Rotate(new Vector3(_settings.axisY.yVelocity, 0f, 0f), Space.Self);
            }
            else
            {
                transform.Rotate(new Vector3(_settings.axisY.yVelocity, 0f, 0f), Space.Self);
            }

            if ((_settings.target.targetDistance + _settings.zoom.zoomVelocity) < _settings.target.minDistance)
            {
                _settings.zoom.zoomVelocity = _settings.target.minDistance - _settings.target.targetDistance;
            }
            else if ((_settings.target.targetDistance + _settings.zoom.zoomVelocity) > _settings.target.maxDistance)
            {
                _settings.zoom.zoomVelocity = _settings.target.maxDistance - _settings.target.targetDistance;
            }

            _settings.target.targetDistance += _settings.zoom.zoomVelocity;
            _settings.target.currentDistance = Mathf.Lerp(_settings.target.currentDistance, _settings.target.targetDistance, _settings.zoom.smoothingZoom);
            
            _settings.target.target.Translate(new Vector3(-_settings.axisX.xTranslateVelocity * 0.01f, 0, 0));
            _settings.target.target.Translate(new Vector3(0, _settings.axisY.yTranslateVelocity * 0.01f, 0));

            if (_settings.collision.cameraCollision)
            {
                Vector3 vector = this.transform.position - _settings.target.target.position;
                Ray ray = new Ray(_settings.target.target.position, vector.normalized);
                RaycastHit hit;
                if (Physics.SphereCast(ray.origin, _settings.collision.collisionRadius, ray.direction, out hit, _settings.target.currentDistance))
                {
                    _settings.target.currentDistance = hit.distance;
                }
            }                       

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetMouseButton(2))
            {
                _settings.target.target.rotation = transform.rotation;
                transform.position = _settings.target.target.position - (transform.rotation * Vector3.forward * _settings.target.currentDistance);                
            }         
            else
            {
                transform.position = ((transform.rotation * new Vector3(0f, 0f, -_settings.target.currentDistance))) + _settings.target.target.position;
            }
            
            _settings.axisX.xVelocity *= _settings.axisX.dampeningX;
            _settings.axisY.yVelocity *= _settings.axisY.dampeningY;
            _settings.axisX.xTranslateVelocity *= _settings.axisX.dampeningX;
            _settings.axisY.yTranslateVelocity *= _settings.axisY.dampeningY;
            _settings.zoom.zoomVelocity = 0f;
        }

        private void ResetCachePointerUI()
        {
            if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(2) /*|| Input.GetAxis("Mouse ScrollWheel") == 0*/)
            {
                _cachePointerUI = false;
            }
        }

        private void PressMouseButton()
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2) || Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                _cachePointerUI = EventSystem.current.IsPointerOverGameObject();
            }
        }
        
        private IEnumerator CorSmoothTargetPos(Vector3 targetPos, float targetDistance)
        {
            var step = 0.0f;

            while(step < 1)
            {
                _settings.target.target.position = Vector3.Lerp(_settings.target.target.position, targetPos, step);
                _settings.target.targetDistance = Mathf.Lerp(_settings.target.targetDistance , targetDistance, step);
                step += .01f;                
                yield return null;
            }            
        }

        private IEnumerator CorSetView(Transform target, float distance)
        {
            IsEnable = false;
           
            yield return new WaitForSeconds(0.25f);

            _settings.target.currentDistance = _settings.target.targetDistance = distance;

            float fracComplete = 0.0f;
            var startTime = Time.time;
            var journeyTime = 2;
            Ray ray = new Ray(target.position, target.forward * distance);
            var camPos = ray.GetPoint(distance);
            
            while (!Mathf.Approximately(CustomCamera.transform.position.z, camPos.z))
            {
                fracComplete = (Time.time - startTime) / journeyTime;
                _settings.target.target.position = Vector3.Slerp(_settings.target.target.position, target.position, fracComplete);
                CustomCamera.transform.LookAt(_settings.target.target.position);
                CustomCamera.transform.position = Vector3.Slerp(CustomCamera.transform.position, camPos, fracComplete);
                yield return null;
            }
            
            IsEnable = true;
        }
        
        public void SetTargetPos(Vector3 position)
        {
            StartCoroutine(CorSmoothTargetPos(position, 0.5f));
        }

        public void SetView(int index)
        {
            StartCoroutine(CorSetView(cameraViewSettings[index].Target, cameraViewSettings[index].Distance));
        }

        public void SetView(CameraViewSettings cameraViewSettings)
        {
            StartCoroutine(CorSetView(cameraViewSettings.Target, cameraViewSettings.Distance));
        }

        [Serializable]
        public class Settings
        {
            public Target target;
            public Control control;
            public AxisX axisX;
            public AxisY axisY;
            public Zoom zoom;
            public Collision collision;

            [Serializable]
            public class Target
            {
                public Transform target;
                public float targetDistance;
                public float currentDistance = 10f;
                public float maxDistance = 5f;
                public float minDistance = 0.5f;
                public float movingTargetSpeed = 1.5f;              
            }

            [Serializable]
            public class Control
            {
                public bool keyboardControl = true;
                public bool mouseControl = true;
                public bool clickToRotate = true;
                public bool leftClickToRotate;
                public bool rightClickToRotate = true;
                public float panSpeed = 0.05f;
                public float keyBoardSpeed = 10f;
            }

            [Serializable]
            public class AxisX
            {
                public float x;
                public float xLimitOffset;
                public bool limitX;
                public float xMaxLimit = 60f;
                public float xMinLimit = -60f;
                public float xSpeed = 1f;
                public float xVelocity;
                public float xTranslateVelocity;
                public float dampeningX = 0.9f;
                public float initialAngleX;
                public bool invertAxisX;
                public int invertXValue = 1;
                public string kbPanAxisX = "Horizontal";
                public string mouseAxisX = "Mouse X";
            }

            [Serializable]
            public class AxisY
            {
                public float y;
                public float yLimitOffset;
                public bool limitY = true;
                public float yMaxLimit = 60f;
                public float yMinLimit = -60f;
                public float ySpeed = 1f;
                public float yVelocity;
                public float yTranslateVelocity;
                public float dampeningY = 0.9f;
                public float initialAngleY;
                public bool invertAxisY;
                public int invertYValue = 1;
                public string kbPanAxisY = "Vertical";
                public string mouseAxisY = "Mouse Y";
            }

            [Serializable]
            public class Zoom
            {
                public float zoomSpeed = 5f;
                public float zoomVelocity;
                public int invertZoomValue = 1;
                public bool invertAxisZoom;
                public bool kbUseZoomAxis;
                public string kbZoomAxisName = string.Empty;
                public string mouseAxisZoom = "Mouse ScrollWheel";
                public float smoothingZoom = 0.1f;
            }

            [Serializable]
            public class Collision
            {
                public bool cameraCollision;
                public float collisionRadius = 0.25f;
            }
        }       
    }
}
