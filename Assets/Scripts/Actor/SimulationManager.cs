﻿using UnityEngine;
using System.Collections;
using System;

namespace AmazyingPhysics
{
    public class SimulationManager : MonoBehaviour
    {
        private PickableInventoryBase[] _items;

        public event Action<bool> OnActiveSimulation;

        public bool IsEnableSimulation { get; set; }

        private void StartSimulation()
        {
            _items = DynamicRoot.Instance.GetComponentsInChildren<PickableInventoryBase>();

            foreach (var item in _items)
            {
                item.MakeJoint();
                item.EnableSimulation();
            }

            Timer.Instance.StartTimer();
            IsEnableSimulation = true;
        }

        private void StopSimulation()
        {
            foreach (var item in _items)
            {
                if(item != null)
                    item.DisableSimulation();
            }

            Timer.Instance.StopTimer();
            IsEnableSimulation = false;
        }

        public void LaunchSimulation(bool value)
        {
            if (value)
            {
                StartSimulation();
            }
            else
            {
                StopSimulation();
            }

            if (OnActiveSimulation != null)
                OnActiveSimulation(value);
        }
    }
}