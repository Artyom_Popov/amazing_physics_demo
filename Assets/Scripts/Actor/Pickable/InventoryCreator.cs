﻿using UnityEngine;
using System.Collections;
using Zenject;
using AmazyingPhysics;
using AmazyingPhysics.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AmazyingPhysics
{
    public class InventoryCreator : MonoBehaviour
    {
        public CreatedInventory CreatedInventory { get; set; }

        private string _pathItem = "Items/mdl_inv_";
        private string _nameItem = "mdl_inv_";
        
        public PickableInventoryBase CurrentInventory { get; set; }
        public event Action OnCreate;

        private GameObject _inventoryActionsSimModePanelPref;

        private void Start()
        {
            CreatedInventory = new CreatedInventory();
            _inventoryActionsSimModePanelPref = Resources.Load<GameObject>("InventoryActionsSimModePanel");
        }

        public PickableInventoryBase Create(Inventory inventory, int experimentId)
        {
            if (OnCreate != null)
            {
                OnCreate();
            }

            GameObject prefab = Resources.Load<GameObject>(_pathItem + inventory.Id.ToInventoryNameId());

            if (prefab == null)
            {
                Debug.LogError("item is null");
            }

            GameObject go = Instantiate(prefab, Vector3.zero, Quaternion.identity, DynamicRoot.Instance.DynamicRootTransform);
            SetItem(go);

            CreatedInventory.Add(CurrentInventory);
            CurrentInventory.InventorySettings.Id = inventory.Id;
            CurrentInventory.InventorySettings.Name = _nameItem + inventory.Id.ToInventoryNameId();
            CurrentInventory.InventorySettings.ParentInventories = inventory.ParentInventory;
            CurrentInventory.SetActiveColliders(false);
            CurrentInventory.InventorySettings.Rigidbody.isKinematic = true;
            CurrentInventory.InventorySettings.CanStayOnTable = inventory.CanStayOnTable;
            CurrentInventory.InventorySettings.ExperimentId = experimentId;
            CurrentInventory.InventorySettings.CreatedInventory = CreatedInventory;
            CurrentInventory.InventorySettings.Transform.position = new Vector3(0, -5, 0);
            CurrentInventory.Initialize();

            InitInventoryActionsSimModePanel();

            return CurrentInventory;
        }

        private void InitInventoryActionsSimModePanel()
        {
            InventoryActionsSimModePanel inventoryActionsSimModePanel = null;

            if (CurrentInventory.InventorySettings.CameraView.Distance != 0 || CurrentInventory.InventorySettings.AnchorUI != null)
            {
                var inventoryActionsSimModePanelGo = Instantiate(_inventoryActionsSimModePanelPref);
                inventoryActionsSimModePanel = inventoryActionsSimModePanelGo.GetComponent<InventoryActionsSimModePanel>();
                inventoryActionsSimModePanel.SetParent(CurrentInventory.InventorySettings.AnchorUI);             
                CurrentInventory.InventorySettings.InventoryActionsSimModePanel = inventoryActionsSimModePanel;
            }
            else
            {
                return;
            }

            if (CurrentInventory.InventorySettings.CameraView.Distance != 0)
            {
                inventoryActionsSimModePanel.CamView.Constructor(CurrentInventory.InventorySettings.CameraView);              
            }

            if (CurrentInventory.InventorySettings.AnchorUI != null)
            {
                inventoryActionsSimModePanel.ActionsMenu.Constructor(CurrentInventory);
            }
        }


        public void Delete(PickableInventoryBase inventory)
        {
            DeleteFromParent(inventory);
            DeleteChilds(inventory);
        }

        private void DeleteChilds(PickableInventoryBase inventory)
        {
            if (inventory.InventoryChilds.Count > 0)
            {
                foreach (var child in inventory.InventoryChilds)
                {
                    if(child != null)
                    {
                        DeleteChilds(child);
                    }                   
                }
            }
            
            CreatedInventory.Remove(inventory);
            Destroy(inventory.gameObject);
        }

        private void DeleteFromParent(PickableInventoryBase inventory)
        {
            if (inventory.InventoryParent != null)
            {
                var parent = CreatedInventory.GetInventory(inventory.InventoryParent.InventorySettings.Id);
                parent.InventoryChilds.Remove(inventory);
            }
        }
        
        private void SetItem(GameObject item)
        {
            PickableInventoryBase pickableItem = item.GetComponent<PickableInventoryBase>();

            if(pickableItem == null)
            {
                Debug.LogError("объект не содержит скрипт PickableItemBase");
            }

            CurrentInventory = pickableItem;
        }                
    }

    public class CreatedInventory
    {
        public List<PickableInventoryBase> Inventories { get; set; }

        public CreatedInventory()
        {
            Inventories = new List<PickableInventoryBase>();
        }

        public void Add(PickableInventoryBase inventory)
        {
            Inventories.Add(inventory);
            Debug.Log("добавлен предмет: " + inventory.InventorySettings.Name);
        }

        public void Remove(PickableInventoryBase inventory)
        {
            var isContain = Inventories.Contains(inventory);
            if (isContain)
            {
                Inventories.Remove(inventory);
                Debug.Log("удален предмет: " + inventory.InventorySettings.Name);
            }
            //else
            //{
            //    Debug.LogError("предмета " + inventory.InventorySettings.Name + " не существует");
            //}
        }

        public PickableInventoryBase GetInventory(int inventoryId)
        {
            var inventory = Inventories
                .Where(x => x.InventorySettings.Id == inventoryId)
                .FirstOrDefault();

            var isContains = inventory != null ? true : false;

            if (isContains)
            {
                return inventory;
            }
            else
            {
                Debug.Log("предмета с индексом " + inventoryId + " не существует");
                return null;
            }
        }

        public bool IsContains(int inventoryId)
        {
            var inventory = Inventories
                .Where(x => x.InventorySettings.Id == inventoryId)
                .FirstOrDefault();

            var isContains = inventory != null ? true : false;

            return isContains;
        }      
    }
}
