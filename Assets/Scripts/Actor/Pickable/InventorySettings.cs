﻿using System;
using UnityEngine;

namespace AmazyingPhysics
{
    [Serializable]
    public class InventorySettings
    {
        [HideInInspector] public int Id; // Id     
        [HideInInspector] public string Name; // имя     
        [HideInInspector] public Transform Transform; // трансформ главного объекта 
        [HideInInspector] public SkinnedMeshRenderer[] SkinnedMeshRenderers; // компонент рендерер с морфами 
        [HideInInspector] public MeshRenderer[] MeshRenderers; // компонент рендерер без морфов       
        [HideInInspector] public Material[] Materials; // все материалы объекта
        [HideInInspector] public Rigidbody Rigidbody; // ригидбоди
        [HideInInspector] public Collider[] Colliders; // все коллайдеры объекта
        [HideInInspector] public Joint Joint; // джоинт объекта
        [HideInInspector] public Transform[] Pivots;
        [HideInInspector] public Transform[] Connectors; // коннекторы объекта
        [HideInInspector] public Transform ChildsContainer; // контейнер для дочерних объектов
        [HideInInspector] public ParentInventory[] ParentInventories; // информация о родительских предметах и коннекторах
        [HideInInspector] public bool CanStayOnTable; // флаг возможности поставить объект на стол
        [HideInInspector] public Highlighting InventoryHighlighting;
        [HideInInspector] public int ExperimentId;
        [HideInInspector] public CreatedInventory CreatedInventory { get; set; }
        [HideInInspector] public Transform AnchorUI;
        [HideInInspector] public InventoryActionsSimModePanel InventoryActionsSimModePanel { get; set; }
        public ActiveAxis DirAxis; // оси перемещения
        public ActiveAxis RotAxis; // оси вращения        
        public CameraViewSettings CameraView;
    }

    [Serializable]
    public class Highlighting
    {
        // [HideInInspector] public Highlighter HighLighter;
        // [HideInInspector] public Color HighLightColor;
        // [HideInInspector] public bool IsHighLight;

        // public void HighLightUpdate()
        // {
        //     if (IsHighLight)
        //         HighLighter.On(HighLightColor);
        // }
    }  
}