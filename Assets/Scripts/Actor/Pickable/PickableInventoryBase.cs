﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AmazyingPhysics
{
    public abstract class PickableInventoryBase : MonoBehaviour
    {
        #region Fields

        [HideInInspector] public PickableInventoryBase InventoryParent;
        [HideInInspector] public List<PickableInventoryBase> InventoryChilds = new List<PickableInventoryBase>();
        public InventorySettings InventorySettings = new InventorySettings();

        private string _meshes =        "Meshes";
        private string _colliders =     "Colliders";
        private string _connectors =    "Connectors";
        private string _pivots =        "Pivots";
        private string _childs =        "Childs";
        private string _anchorUI =      "AnchorUI";

        private SaveSettings _saveSettings;
        
        protected bool IsEnableSimulation { get; set; }

        public event Action OnConnect;// событие для подсоединения предмета к другому предмету
        public event Action<bool> OnConnect2;// событие для подсоединения предмета к другому предмету

        public List<IinventoryAct> InventoryAct { get; set; }

        #endregion

        private void Awake()
        {           
            InventorySettings.Transform = transform;
            InventorySettings.Rigidbody = GetComponent<Rigidbody>();            
            InitRenderersAndMaterials(_meshes);
            InventorySettings.Colliders = GetColliders(_colliders);
            InventorySettings.Pivots = GetInventoryComnponents<Transform>(_pivots);
            InventorySettings.Connectors = GetInventoryComnponents<Transform>(_connectors);
            InventorySettings.ChildsContainer = GetComponentsInChildren<Transform>().Where(x => x.name == _childs).First();
            InventorySettings.AnchorUI = GetComponentsInChildren<Transform>().Where(x => x.name == _anchorUI).FirstOrDefault();

            _saveSettings = new SaveSettings();            
        }
        
        public virtual void Initialize()
        {
        }

        #region Renderers и Materials

        /// <summary>
        /// записать в нстройки информацию о исходных рендерерах и материалах
        /// </summary>
        /// <param name="root"></param>
        private void InitRenderersAndMaterials(string nameRoot)
        {
            var root = GetComponentsInChildren<Transform>().Where(m => m.name == nameRoot).First();

            InventorySettings.MeshRenderers = root.GetComponentsInChildren<MeshRenderer>();

            List<Material> mats = new List<Material>();

            if (InventorySettings.MeshRenderers.Length == 0)
            {
                InventorySettings.SkinnedMeshRenderers = root.GetComponentsInChildren<SkinnedMeshRenderer>();

                for (int i = 0; i < InventorySettings.SkinnedMeshRenderers.Length; i++)
                {
                    var buffer = InventorySettings.SkinnedMeshRenderers[i].materials;

                    foreach (var item in buffer)
                    {
                        mats.Add(item);
                    }
                }
            }
            else
            {
                for (int i = 0; i < InventorySettings.MeshRenderers.Length; i++)
                {
                    var buffer = InventorySettings.MeshRenderers[i].materials;

                    foreach (var item in buffer)
                    {
                        mats.Add(item);
                    }
                }
            }

            InventorySettings.Materials = mats.ToArray();           
        }

        /// <summary>
        /// установить материал во все рендереры предмета
        /// </summary>
        /// <param name="material"></param>
        public void SetMaterial(Material material)
        {
            if (InventorySettings.MeshRenderers.Length != 0)
            {
                for (int i = 0; i < InventorySettings.MeshRenderers.Length; i++)
                {
                    Material[] mats = new Material[InventorySettings.MeshRenderers[i].materials.Length];

                    for (int j = 0; j < InventorySettings.MeshRenderers[i].materials.Length; j++)
                    {
                        mats[j] = material;
                    }

                    InventorySettings.MeshRenderers[i].materials = mats;
                }
            }
            else
            {
                for (int i = 0; i < InventorySettings.SkinnedMeshRenderers.Length; i++)
                {
                    Material[] mats = new Material[InventorySettings.SkinnedMeshRenderers[i].materials.Length];

                    for (int j = 0; j < InventorySettings.SkinnedMeshRenderers[i].materials.Length; j++)
                    {
                        mats[j] = material;
                    }

                    InventorySettings.SkinnedMeshRenderers[i].materials = mats;
                }
            }

            foreach (var child in InventoryChilds)
            {
                child.SetMaterial(material);
            }            
        }

        /// <summary>
        /// назначить исходные материалы на предмет
        /// </summary>
        public void SetSourceMaterials()
        {
            int countPrevRend = 0;

            if (InventorySettings.MeshRenderers.Length != 0)
            {
                for (int i = 0; i < InventorySettings.MeshRenderers.Length; i++)
                {
                    Material[] matsByRendId = new Material[InventorySettings.MeshRenderers[i].materials.Length];
                    var sortedMats = InventorySettings.Materials.Skip(countPrevRend).Take(matsByRendId.Length).ToArray();

                    for (int j = 0; j < InventorySettings.MeshRenderers[i].materials.Length; j++)
                    {
                        matsByRendId[j] = sortedMats[j];
                    }

                    InventorySettings.MeshRenderers[i].materials = matsByRendId;
                    countPrevRend += InventorySettings.MeshRenderers[i].materials.Length;
                }
            }
            else
            {
                for (int i = 0; i < InventorySettings.SkinnedMeshRenderers.Length; i++)
                {
                    Material[] matsByRendId = new Material[InventorySettings.SkinnedMeshRenderers[i].materials.Length];
                    var sortedMats = InventorySettings.Materials.Skip(countPrevRend).Take(matsByRendId.Length).ToArray();

                    for (int j = 0; j < InventorySettings.SkinnedMeshRenderers[i].materials.Length; j++)
                    {
                        matsByRendId[j] = sortedMats[j];
                    }

                    InventorySettings.SkinnedMeshRenderers[i].materials = matsByRendId;
                    countPrevRend += InventorySettings.SkinnedMeshRenderers[i].materials.Length;
                }
            }

            foreach (var child in InventoryChilds)
            {
                child.SetSourceMaterials();
            }
        }

        #endregion

        #region Select и Deselect

        public virtual void Select()
        {
        }

        public virtual void Deselect()
        {
        }

        #endregion

        #region Positions и Connections

        /// <summary>
        /// Sets the position.
        /// </summary> установить позицию по индексу Pivots
        /// <param name="indexPivot">The index.</param>
        /// <param name="position">The position.</param>
        public void SetPosition(int indexPivot, Vector3 position)
        {
            InventorySettings.Transform.localPosition = position - InventorySettings.Pivots[indexPivot].localPosition;
            InventorySettings.Transform.localRotation = Quaternion.identity;
        }

        /// <summary>
        /// Sets the position on table.
        /// </summary> установить объект на стол
        /// <param name="position">The position.</param>
        public void SetPositionOnTable(Vector3 position)
        {
            SetPosition(0, position);
        }

        /// <summary>
        /// Sets the position to objects.
        /// </summary> установить позицию объекта к указанной точке
        /// <param name="position">The position.</param>
        public void SetPositionToObjects(Vector3 position)
        {
            SetPosition(1, position);
        }
        
        /// <summary>
        /// настроить позицию предмета во время прикрепления к другому предмету
        /// </summary>
        /// <param name="position">позиция мышки</param>
        /// <returns></returns>
        public virtual void TunePosition(Vector3 position)
        {
        }

        public virtual void TunePosition(Transform transRoot)
        {
        }

        public virtual void TunePosition(InventorySettings inventorySettings)
        {
        }

        /// <summary>
        /// получить центр координат предмета
        /// </summary>
        public virtual Vector3 CenterItem
        {
            get
            {
                Vector3 center = Vector3.zero;

                if(InventorySettings.MeshRenderers.Length == 0)
                {
                    center = InventorySettings.SkinnedMeshRenderers[0].bounds.center;
                }
                else
                {
                    center = InventorySettings.MeshRenderers[0].bounds.center;
                }

                return center;
            }            
        }

        /// <summary>
        /// проверка на возможность соединения с другим предметом
        /// </summary>
        /// <param inventoryId="inventoryId">Id предмета, к которому пытаемся подсоединиться</param>
        /// <returns></returns>
        public bool CanBeConnect(int inventoryId)
        {
            bool value = false;

            foreach (var item in InventorySettings.ParentInventories)
            {
                if (item.Id == inventoryId)
                {
                    value = true;
                    break;
                }
            }

            return value;
        }
        
        /// <summary>
        /// унаследоваться от предмета к которому возможно присоединиться
        /// </summary>
        /// <param name="transParent">трансформ родителя</param>
        /// <param name="position">позиция коннектора</param>
        public virtual void SetParerntRelativePointerItem(PickableInventoryBase inventory, Transform[] connectors)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);
            SetPosition(1, connectors[0].localPosition);            
        }

        public virtual void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);
            SetPosition(1, inventory.InventorySettings.Connectors[0].localPosition);
        }

        /// <summary>
        /// установить соединение с другим предметом
        /// </summary>
        /// <param inventoryId="inventoryId">Id предмета, к которому подсоединяемся</param>
        /// <returns></returns>
        public virtual void SetConnection(int inventoryId)
        {
            if (OnConnect != null)
            {
                OnConnect();
            }

            if (OnConnect2 != null)
            {
                OnConnect2(true);
            }       
        }
        
        #endregion

        #region Physics и Simulation
        
        /// <summary>
        /// сохранить настройки предмета до включения симуляции
        /// </summary>   
        public void SaveItemState()
        {
            _saveSettings.position = InventorySettings.Transform.localPosition;
            _saveSettings.rotation = InventorySettings.Transform.localRotation;

            if (InventoryParent != null)
            {
                _saveSettings.parentTransform = InventoryParent.InventorySettings.ChildsContainer;
            }                
        }

        /// <summary>
        /// загрузить настройки предмета после выключения симуляции
        /// </summary>
        public void LoadItemState()
        {
            if (InventoryParent != null)
            {
                InventorySettings.Transform.parent = _saveSettings.parentTransform;
            }                

            InventorySettings.Transform.localPosition = _saveSettings.position;
            InventorySettings.Transform.localRotation = _saveSettings.rotation;

            if (InventorySettings.SkinnedMeshRenderers != null)
            {
                for (int i = 0; i < InventorySettings.SkinnedMeshRenderers.Length; i++)
                {
                    for (int j = 0; j < InventorySettings.SkinnedMeshRenderers[i].sharedMesh.blendShapeCount; j++)
                    {
                        InventorySettings.SkinnedMeshRenderers[i].SetBlendShapeWeight(j, 0);
                    }
                }
            }            
        }

        /// <summary>
        /// включить симуляцию
        /// </summary>
        public virtual void EnableSimulation()
        {
            IsEnableSimulation = true;
        }

        /// <summary>
        /// выключить симуляцию
        /// </summary>
        public virtual void DisableSimulation()
        {
            IsEnableSimulation = false;
        }   
        
        /// <summary>
        /// активация/деактивация коллайдеров предмета
        /// </summary>
        /// <param name="value"></param>
        public void SetActiveColliders(bool value)
        {
            foreach (var item in InventorySettings.Colliders)
            {
                item.enabled = value;
            }
        }

        /// <summary>
        /// установить джоинт с другим предметом
        /// </summary>
        public virtual void MakeJoint()
        {
        }
        
        #endregion

        public virtual List<IinventoryAct> GetInventoryActions()
        {
            //InventoryAct = new List<IinventoryAct>();
            return null;
        }
        
        /// <summary>
        /// получить массив компонентов типа Т только предков,
        /// сам объект не входит в данный массив
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        private T[] GetInventoryComnponents<T>(string name)
        {
            var root = transform.FindChildTransform(name);
            var buffer = root.GetComponentsInChildren<Transform>().ToList();
            buffer.RemoveAt(0);

            List<T> components = new List<T>();

            foreach (var item in buffer)
            {
                T component = item.GetComponent<T>();       
                         
                if (component != null && component is T)
                {
                    components.Add(component);                                 
                }               
            }

            return components.ToArray();
        }

        private Collider[] GetColliders(string name)
        {
            var root = transform.FindChildTransform(name);
            var buffer = root.GetComponentsInChildren<Transform>().ToList();
            buffer.RemoveAt(0);

            List<Collider> components = new List<Collider>();

            foreach (var item in buffer)
            {
                var component = item.GetComponent<Collider>();

                if (component != null)
                {
                    components.Add(component);
                }
            }

            return components.ToArray();
        }

        /// <summary>
        /// контейнер для хранения данных о предете перед включением симуляции
        /// </summary>
        private class SaveSettings
        {
            public Vector3 position;
            public Quaternion rotation;
            public Transform parentTransform;
        }
    }
}
