﻿using System.Linq;
using UnityEngine;

namespace AmazyingPhysics
{
    public class MechanicStandClip : PickableInventoryBase
    {
        [SerializeField]
        private Transform _part1, _part2;

        private ObservedValue<int> OnChildConnect;

        private void Update()
        {
            if (InventoryChilds.Count > 0)
            {
                if (InventoryChilds.First().InventorySettings.Id == 53 || InventoryChilds.First().InventorySettings.Id == 92)
                {
                    InventoryChilds.First().TunePosition(InventorySettings);
                }

                OnChildConnect.Value = InventoryChilds.First().InventorySettings.Id;
            }
            else
            {
                OnChildConnect.Value = -1;
            }
        }

        public override void Initialize()
        {
            OnChildConnect = new ObservedValue<int>(-1);
            OnChildConnect.OnValueChanged += (int inventoryId) =>
            {
                switch (inventoryId)
                {
                    case -1:
                        SetState(0);
                        break;
                    //case 2:
                    //    SetState(15);
                    //    break;
                    case 53:
                    case 55:
                        SetState(17);
                        break;
                    case 92:
                        SetState(50);
                       
                        break;
                    default:
                        break;
                }                
            };
        }

        public override void TunePosition(Vector3 position)
        {            
            float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).y;          
            delta = Mathf.Clamp(delta, 0.065f, 0.55f);
            SetPositionToObjects(new Vector3(0, delta, 0));            
        }

        public override void EnableSimulation()
        {
            SaveItemState();            
        }

        public override void DisableSimulation()
        {
            LoadItemState();
        }

        private void SetState(float angle)
        {
            _part1.localRotation = Quaternion.AngleAxis(-angle, transform.up);
            _part2.localRotation = Quaternion.AngleAxis(angle, transform.up);
        }
    }
}


