﻿using UnityEngine;
using System.Linq;

namespace AmazyingPhysics
{
    public class BindingBallisticGun : PickableInventoryBase
    {
        [SerializeField]
        private BallisticGun _ballisticGun;

        [SerializeField]
        private BallisticGunHelper _helper;

        private KinematicSphere _kinematicSphere;

        private void Update()
        {
            if (IsEnableSimulation && !_kinematicSphere.IsTouchTable)
            {
                _helper.SetPos(_kinematicSphere.InventorySettings.Transform.position);
            }
        }

        public override void Initialize()
        {
            InventoryChilds.Add(_ballisticGun);
            _ballisticGun.InventorySettings.Id = 103;
            _ballisticGun.InventoryParent = this;

            _ballisticGun.Initialize();

            _kinematicSphere = _ballisticGun.InventoryChilds.First() as KinematicSphere;
            _helper.Rotator.OnRotateAngle += Rotator_OnRotateAngle;
        }

        private void Rotator_OnRotateAngle(float angle)
        {
            InventoryChilds.First().InventorySettings.Transform.localRotation = 
                Quaternion.AngleAxis(
                    -angle, 
                    Vector3.forward);
        }

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, true);
            SetPosition(2, inventory.InventorySettings.Connectors[0].localPosition);
        }

        //public override void TunePosition(Vector3 position)
        //{
        //    float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).y;
        //    delta = Mathf.Clamp(delta, 0.065f, 0.55f);
        //    SetPositionToObjects(new Vector3(0, delta, 0));
        //}

        public override void EnableSimulation()
        {
            _helper.Helper2.SetActive(true);
            IsEnableSimulation = true;
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;
            _helper.Helper2.SetActive(false);
        }
    }
}


