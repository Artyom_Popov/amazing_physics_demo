﻿using System.Linq;
using UnityEngine;

namespace AmazyingPhysics
{
    public class DirectBoard : PickableInventoryBase
    {
        [SerializeField]
        private DirectBoardHelper _helper;

        private void Update()
        {
            if (IsEnableSimulation && InventoryChilds.Count > 0)
            {
                _helper.SetPos(InventoryChilds.First().InventorySettings.Pivots[3].position);
            }
        }

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);
            TunePosition(inventory.InventorySettings);
        }

        public override void TunePosition(InventorySettings inventorySettings)
        {
            var angle = Mathf.Asin((inventorySettings.Transform.position.y - 0.975f) / 1) * Mathf.Rad2Deg;
            InventorySettings.Transform.localRotation = Quaternion.AngleAxis(angle, InventorySettings.Transform.forward);

            Vector3 delta = InventorySettings.Transform.position - InventorySettings.Pivots[1].position;
            InventorySettings.Transform.position = inventorySettings.Connectors[0].position + delta;
        }     

        public override void EnableSimulation()
        {
            if (InventoryChilds.Count > 0)
            {
                _helper.SetStartPos(InventoryChilds.First().InventorySettings.Pivots[3].position);
            }

            IsEnableSimulation = true;
            _helper.gameObject.SetActive(true);        
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;            
            _helper.gameObject.SetActive(false);
        }
    }
}


