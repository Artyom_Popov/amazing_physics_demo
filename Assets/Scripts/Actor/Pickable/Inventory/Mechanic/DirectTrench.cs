﻿using UnityEngine;
using System.Linq;

namespace AmazyingPhysics
{
    public class DirectTrench : PickableInventoryBase
    {
        [SerializeField]
        private DirectTrenchHelper _helper;

        private bool _isContainKineticSphere;
        private Transform _transKineticSphere;
        private KinematicSphere _kinematicSphere;

        private void Update()
        {
            if (IsEnableSimulation)
            {
                if (_isContainKineticSphere && !_kinematicSphere.IsTouchTable)
                {
                    _helper.SetPos(_transKineticSphere.position);
                }
            }
        }

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);
            TunePosition(inventory.InventorySettings);    
        }
        
        public override void TunePosition(InventorySettings inventorySettings)
        {
            var angle = Mathf.Asin((inventorySettings.Transform.position.y - 0.975f) / 1) * Mathf.Rad2Deg;            
            InventorySettings.Transform.localRotation = Quaternion.AngleAxis(angle, Vector3.forward);
            
            Vector3 delta = InventorySettings.Transform.position - InventorySettings.Pivots[1].position;
            InventorySettings.Transform.position = inventorySettings.Connectors[0].position + delta;            
        }

        public override void EnableSimulation()
        {
            var obj = InventoryChilds.Where(x => x.InventorySettings.Id == 93).FirstOrDefault();

            if(obj != null)
            {
                _kinematicSphere = obj as KinematicSphere;                
                _isContainKineticSphere = true;
                _transKineticSphere = obj.InventorySettings.Transform;
                _helper.SetStartPos(_transKineticSphere);
                _helper.gameObject.SetActive(true);
            }            

            IsEnableSimulation = true;
        }

        public override void DisableSimulation()
        {
            _isContainKineticSphere = false;
            _helper.gameObject.SetActive(false);

            IsEnableSimulation = false;
        }
    }
}


