﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public class TestTube : PickableInventoryBase
    {
        public float _positionY = 0.15f;
        public float _aniPositionY = 0.0f;

        private bool _isConnected;

        private Animation _animation;

        private float smoothTime = 0.3F;
        private float yVelocity = 0.0F;

        public override void Initialize()
        {
            _animation = GetComponent<Animation>();
            OnConnect += Connect;
        }

        private void Connect()
        {
            _animation.Play("AnimationTestTube");
            _isConnected = true;
        }

        private void Update()
        {
            if (_isConnected)
            {
                var actualPosition = InventorySettings.Transform.localPosition;
                var targetPositionY = Mathf.SmoothDamp(actualPosition.y, _positionY + _aniPositionY, ref yVelocity, smoothTime);
                var targetPosition = new Vector3(actualPosition.x, targetPositionY, actualPosition.z);

                InventorySettings.Transform.localPosition = targetPosition;
            }
        }
    }
}


