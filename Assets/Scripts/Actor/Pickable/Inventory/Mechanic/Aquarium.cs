﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AmazyingPhysics
{
    public class Aquarium : PickableInventoryBase
    {
        public ProceduralMaterial _wavesMaterial;
        public Text _timer;

        private bool _isEnabled;

        private float _waterAmount;

        private bool _isWaterAded;
        private bool _isFloatAded;

        public GameObject _ball;
        public GameObject _float;
        private Transform _Float;

        private float _length = 70.0f;

        private bool _isTimer;
        private IEnumerator _timerCoroutine;

        public override void EnableSimulation()
        {
            _isEnabled = true;

            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(true);
        }

        public override void DisableSimulation()
        {
            _isEnabled = false;

            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(false);
        }

        public override List<IinventoryAct> GetInventoryActions()
        {
            InventoryAct = new List<IinventoryAct>();

            if (_isEnabled)
            {
                if (InventorySettings.ExperimentId == 21)
                {
                    if (!_isFloatAded)
                    {
                        InventoryAct.Add(new ButtonInventoryFactory("Опустить поплавок", InstantiateFloat));
                    }
                    else
                    {
                        InventoryAct.Add(new ButtonInventoryFactory("Бросить шарик", () => StartCoroutine(CoroutineInstantiateBall())));
                        InventoryAct.Add(new SliderInventoryFactory(new[] { "Расстояние до поплавка", "см." }, SetFloatPosition, new SliderSettings(_length, 40.0f, 80.0f)));
                    }
                }
            }

            return InventoryAct;
        }

        public void SetWater(float change)
        {
            _isWaterAded = true;

            StartCoroutine(CoroutineSetWater(_waterAmount += change, 1.5f));
        }

        IEnumerator CoroutineSetWater(float targetAmount, float duration)
        {
            float actualAmount = InventorySettings.SkinnedMeshRenderers[1].GetBlendShapeWeight(0);

            for (float t = 0; t < 1; t += Time.deltaTime / duration)
            {
                var amount = Mathf.Lerp(actualAmount, targetAmount, t);
                InventorySettings.SkinnedMeshRenderers[1].SetBlendShapeWeight(0, amount);
                yield return null;
            }

            InventorySettings.SkinnedMeshRenderers[1].SetBlendShapeWeight(0, targetAmount);
        }

        private void SetWaves()
        {
            _isTimer = true;

            StopAllCoroutines();

            _timerCoroutine = CoroutineSetSeconds(7.0f);

            StartCoroutine(_timerCoroutine);
            StartCoroutine(CoroutineSetWaves(1.0f, 10.0f));
        }

        IEnumerator CoroutineSetWaves(float targetAmount, float duration)
        {
            float actualAmount = 0.0f;

            for (float t = 0; t < 1; t += Time.deltaTime / duration)
            {
                var amount = Mathf.Lerp(actualAmount, targetAmount, t);

                _wavesMaterial.SetProceduralFloat("waveWay", amount);
                _wavesMaterial.RebuildTextures();

                yield return null;
            }

            _wavesMaterial.SetProceduralFloat("waveWay", targetAmount);
            _wavesMaterial.RebuildTextures();
        }

        IEnumerator CoroutineSetSeconds(float targetAmount)
        {
            _timer.enabled = true;

            var time = 0.0f;

            while (time < targetAmount - 0.1f)
            {
                yield return new WaitForSeconds(0.1f);
                time += 0.1f;
                _timer.text = time.ToString("F1");
            }
        }

        IEnumerator CoroutineInstantiateBall()
        {
            var ball = Instantiate(_ball).GetComponent<KinematicSphere>();
            ball.InventorySettings.Transform.SetParent(InventorySettings.Transform);
            ball.InventorySettings.Transform.localPosition = new Vector3(0.4f, 0.5f, 0.0f);
            ball.InventorySettings.Rigidbody.isKinematic = false;

            yield return new WaitForSeconds(0.2f);
            SetWaves();
        }

        private void InstantiateFloat()
        {
            var Float = Instantiate(_float).GetComponent<MechanicFloat>();
            _Float = Float.InventorySettings.Transform;
            _Float.SetParent(InventorySettings.Transform);
            _Float.localPosition = new Vector3(-0.3f, 0.11f, 0.0f);
            _isFloatAded = true;
        }

        private void SetFloatPosition(float length)
        {
            _length = length;

            var xPosition = 0.4f - (length / 100); 
            _Float.localPosition = new Vector3(xPosition, _Float.localPosition.y, _Float.localPosition.z);
        }

        private void Update()
        {
            if (_isTimer)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    StopCoroutine(_timerCoroutine);
                    _isTimer = false;
                }
            }
        }
    }
}


