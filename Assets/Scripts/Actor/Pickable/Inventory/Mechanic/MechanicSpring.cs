﻿using System.Linq;
using UnityEngine;

namespace AmazyingPhysics
{
    public class MechanicSpring : PickableInventoryBase
    {
        [SerializeField]
        private MechanicSpringHelper _helper;

        private void Update()
        {
            if (IsEnableSimulation && InventoryChilds.Count > 0)
            {
                var dif = (InventorySettings.Connectors[0].position.y - InventoryChilds.First().InventorySettings.Transform.position.y) * 400;
                InventorySettings.SkinnedMeshRenderers[0].SetBlendShapeWeight(0, dif);
                _helper.SetPos(InventoryChilds.First().InventorySettings.Pivots[2].position);
            }
        }

        public override void EnableSimulation()
        {
            if (InventoryChilds.Count > 0)
            {
                _helper.SetStartPos(InventoryChilds.First().InventorySettings.Pivots[2].position);
            }

            IsEnableSimulation = true;
            _helper.gameObject.SetActive(true);
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;
            InventorySettings.SkinnedMeshRenderers[0].SetBlendShapeWeight(0, 0);
            _helper.gameObject.SetActive(false);
        }
    }
}


