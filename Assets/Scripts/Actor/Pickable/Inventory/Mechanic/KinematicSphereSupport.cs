﻿using UnityEngine;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public class KinematicSphereSupport : PickableInventoryBase
    {
        [SerializeField]
        private LineRenderer _lineRenderer;

        [SerializeField]
        private KinematicSphereSupportHelper _helper;
        
        private void Update()
        {
            _lineRenderer.SetPosition(0, InventorySettings.Pivots[1].position);
            _lineRenderer.SetPosition(1, InventorySettings.Pivots[2].position);            
        }
        
        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);
            SetPosition(1, inventory.InventorySettings.Connectors[0].localPosition);                    
        }

        public override List<IinventoryAct> GetInventoryActions()
        {
            var bufferLength = InventorySettings.Pivots[1].localPosition.y;

            _helper.gameObject.SetActive(true);
            _helper.SetStartPos(InventorySettings.Pivots[3].position);
            _helper.SetPos(InventorySettings.Pivots[1].position);

            InventoryAct = new List<IinventoryAct>();
            InventoryAct.Add(new SliderInventoryFactory(
                    new[] { "Длина нити", "L" }, 
                    (float value) => 
                    {
                        SetLength(value);
                        _helper.SetPos(InventorySettings.Pivots[1].position);
                    }, 
                    new SliderSettings(bufferLength, 0.1f, 0.4f))
                );
            InventoryAct.Add(new ButtonInventoryFactory("Запустить колебания маятника", () =>
            {
                _helper.gameObject.SetActive(false);
                CustomMakeJoint();
                SaveItemState();
                InventorySettings.Transform.parent = DynamicRoot.Instance.DynamicRootTransform;
                InventorySettings.Rigidbody.isKinematic = false;
                InventorySettings.Rigidbody.AddForce(InventorySettings.Transform.forward * 10, ForceMode.Force);
            }));
          
            return InventoryAct;
        }

        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
            _helper.gameObject.SetActive(false);
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(true);
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;
            if (InventoryParent != null)
            {
                InventorySettings.Transform.parent = InventoryParent.InventorySettings.ChildsContainer;
            }

            InventorySettings.Rigidbody.isKinematic = true;
            LoadItemState();
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(false);
        }

        private void CustomMakeJoint()
        {
            if (InventoryParent != null && InventoryParent.InventorySettings.Id == 1)
            {
                if (InventorySettings.Joint != null)
                {
                    Destroy(InventorySettings.Joint);
                }

                var distance = InventorySettings.Pivots[1].position.y - InventorySettings.Transform.position.y;
                var hingeJoint = InventorySettings.Transform.gameObject.AddComponent<HingeJoint>();
                hingeJoint.connectedBody = InventoryParent.InventorySettings.Rigidbody;
                hingeJoint.anchor = new Vector3(0, distance, 0);
                hingeJoint.axis = Vector3.right;
                InventorySettings.Joint = hingeJoint;
            }          
        }

        private void SetLength(float length)
        {
            InventorySettings.Pivots[1].localPosition = new Vector3(0, length, 0);
            SetPosition(1, InventoryParent.InventorySettings.Connectors[0].localPosition);
        }
    }
}


