﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class LeverRing : PickableInventoryBase
    {
        public void SetPosition(float posX)
        {
            InventorySettings.Transform.localPosition = new Vector3(posX, 0, 0);
        }

        public void SetJoint(Rigidbody connectedRigidbodyBody, float anchorPosX)
        {
            InventorySettings.Joint = GetComponentInChildren<ConfigurableJoint>();
            InventorySettings.Joint.connectedBody = connectedRigidbodyBody;
            InventorySettings.Joint.connectedAnchor = new Vector3(anchorPosX, 0, 0);
        }

        public override void MakeJoint()
        {
            if (InventorySettings.Joint != null)
            {
                Destroy(InventorySettings.Joint);
            }

            var fixedJoint = InventorySettings.Transform.gameObject.AddComponent<FixedJoint>();
            fixedJoint.connectedBody = InventoryParent.InventorySettings.Rigidbody;

            InventorySettings.Joint = fixedJoint;
        }

        public override void EnableSimulation()
        {
            SaveItemState();

            InventorySettings.Transform.transform.parent = DynamicRoot.Instance.DynamicRootTransform;
            InventorySettings.Rigidbody.isKinematic = false;
        }

        public override void DisableSimulation()
        {
            if (InventoryParent != null)
            {
                InventorySettings.Transform.parent = InventoryParent.InventorySettings.ChildsContainer;
            }

            InventorySettings.Rigidbody.isKinematic = true;

            LoadItemState();
        }
    }
}


