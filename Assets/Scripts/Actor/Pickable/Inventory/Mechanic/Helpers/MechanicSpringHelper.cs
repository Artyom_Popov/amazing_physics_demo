﻿using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class MechanicSpringHelper : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _axisY;

        [SerializeField]
        private Text _txtAxisY;
        
        private Canvas _canvas;

        private void Start()
        {
            _canvas = GetComponent<Canvas>();
        }

        private void OnDisable()
        {
            _axisY.sizeDelta = new Vector2(_axisY.rect.width, 0);
            _txtAxisY.text = "";            
        }

        public void SetStartPos(Vector3 pos)
        {
            _axisY.position = new Vector3(_axisY.position.x, pos.y, _axisY.position.z);
        }
        
        public void SetPos(Vector3 pos)
        {
            if (_axisY.position.y > pos.y)
            {
                var disY = Vector3.Distance(new Vector3(0, pos.y, 0), new Vector3(0, _axisY.position.y, 0));
                _axisY.sizeDelta = new Vector2(_axisY.rect.width, (disY) * 1000 / _canvas.scaleFactor);
                _txtAxisY.text = disY.ToString("0.00") + "m";                  
            }            
        }
    }
}


