﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class BallisticGunHelper : MonoBehaviour
    {
        public BallisticGunRotator Rotator;

        public GameObject Helper2;

        [SerializeField]
        private RectTransform _axis1, _axis2, _axis3;

        [SerializeField]
        private Text _txtDistance;

        [SerializeField]
        private Canvas _canvas;

        private void Start()
        {
            _axis3.sizeDelta = new Vector2(_axis1.rect.width, _axis1.rect.height);
            Helper2.SetActive(false);
        }

        public void SetPos(Vector3 pos)
        {
            var d = Vector3.Distance(pos, _axis2.position);
            _axis2.sizeDelta = new Vector2(_axis2.rect.width, ((d) * 1000 / _canvas.scaleFactor));
            _txtDistance.text = d.ToString("0.00") + "m";            
        }

        public void SetStartPos(Transform trans)
        {
            _axis1.position = trans.position + trans.right * 0.04f;
        }
    }
}

