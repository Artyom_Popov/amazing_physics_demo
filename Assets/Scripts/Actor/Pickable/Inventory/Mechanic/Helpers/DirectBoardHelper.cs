﻿using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class DirectBoardHelper : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _axisY;

        [SerializeField]
        private Text _txtAxisY;

        [SerializeField]
        private Canvas _canvas;
        
        private void OnDisable()
        {
            _axisY.sizeDelta = new Vector2(_axisY.rect.width, 0);
            _txtAxisY.text = "";            
        }

        public void SetStartPos(Vector3 pos)
        {
            _axisY.position = pos;
        }
        
        public void SetPos(Vector3 pos)
        {
            var disY = Vector3.Distance(pos, _axisY.position);
            _axisY.sizeDelta = new Vector2(_axisY.rect.width, (disY) * 1000 / _canvas.scaleFactor);
            _txtAxisY.text = disY.ToString("0.00") + "m";
        }
    }
}


