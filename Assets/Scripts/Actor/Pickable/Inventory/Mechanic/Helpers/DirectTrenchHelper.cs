﻿using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class DirectTrenchHelper : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _axis1, _axis2, _axis3;

        [SerializeField]
        private Text _txtDistance;

        [SerializeField]
        private Canvas _canvas;

        private void Start()
        {
            _axis3.sizeDelta = new Vector2(_axis1.rect.width, _axis1.rect.height);
        }

        public void SetPos(Vector3 pos)
        {
            var dis = Vector3.Distance(pos, _axis2.position);
            _axis2.sizeDelta = new Vector2(_axis2.rect.width, (dis * 1000 / _canvas.scaleFactor));
            _txtDistance.text = dis.ToString("0.00") + "m";
        }

        public void SetStartPos(Transform trans)
        {
            _axis1.position = trans.position + trans.right * 0.04f;
        }
    }
}