﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class BallisticGunRotator : MonoBehaviour, IPointerDownHandler, IDragHandler
    {
        [SerializeField]
        private Image _circle;

        [SerializeField]
        private Text _txtAngle;

        private Transform _transform;
        private Camera _camera;       
        private Vector3 _previousMousePosition;
        private Vector3 _originalTargetPosition;
        private Vector3 _planeNormal;        
        private Vector3 _deltaPosition;
        private Vector3 _mousePosition;
        private float _rotateSpeedMultiplier = 200;
        private float _bufferAngle;
        
        public event Action<float> OnRotateAngle;

        void Awake()
        {
            _transform = transform.parent;
            _camera = Camera.main;

            _txtAngle.text = "0";
        }

        public void OnPointerDown(PointerEventData data)
        {
            _originalTargetPosition = _transform.position;
            _planeNormal = (_camera.transform.position - _transform.position).normalized;                
            _mousePosition = _previousMousePosition = Vector3.zero;
            _deltaPosition = _transform.position - _camera.ScreenToWorldPoint(data.pointerCurrentRaycast.screenPosition);           
        }

        public void OnDrag(PointerEventData data)
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            _mousePosition = LinePlaneIntersect(ray.origin, ray.direction, _originalTargetPosition, _planeNormal);
           
            if (_previousMousePosition != Vector3.zero && _mousePosition != Vector3.zero)
            {
                Vector3 projected = (IsParallel(_transform.forward, _planeNormal)) ? _planeNormal : Vector3.Cross(_transform.forward, _planeNormal);
                float rotateAmount = (MagnitudeInDirection(_mousePosition - _previousMousePosition, projected) * _rotateSpeedMultiplier) / GetDistanceMultiplier();
                _bufferAngle += rotateAmount;
                _bufferAngle = Mathf.Clamp(_bufferAngle, 0, 90);
                
                if (_bufferAngle > 0 && _bufferAngle < 90)
                {
                    _transform.Rotate(_transform.forward, rotateAmount, Space.World);
                    _transform.localRotation = Quaternion.Euler(_transform.localEulerAngles.x, _transform.localEulerAngles.y, _bufferAngle);
                    _circle.fillAmount = 0.0027f * _transform.localEulerAngles.z;

                    if (OnRotateAngle != null)
                    {
                        OnRotateAngle(_bufferAngle);
                        _txtAngle.text = _bufferAngle.ToString("0");
                    }
                }
            }

            _previousMousePosition = _mousePosition;         
        }

        private Vector3 LinePlaneIntersect(Vector3 linePoint, Vector3 lineVec, Vector3 planePoint, Vector3 planeNormal)
        {
            float distance = LinePlaneDistance(linePoint, lineVec, planePoint, planeNormal);
            
            if (distance != 0f)
            {
                return linePoint + (lineVec * distance);
            }

            return Vector3.zero;
        }

        private float LinePlaneDistance(Vector3 linePoint, Vector3 lineVec, Vector3 planePoint, Vector3 planeNormal)
        {
            //calculate the distance between the linePoint and the line-plane intersection point
            float dotNumerator = Vector3.Dot((planePoint - linePoint), planeNormal);
            float dotDenominator = Vector3.Dot(lineVec, planeNormal);

            //line and plane are not parallel
            if (dotDenominator != 0f)
            {
                return dotNumerator / dotDenominator;
            }

            return 0;
        }
        
        private bool IsParallel(Vector3 direction, Vector3 otherDirection, float precision = .0001f)
        {
            return Vector3.Cross(direction, otherDirection).sqrMagnitude < precision;
        }

        private float MagnitudeInDirection(Vector3 vector, Vector3 direction, bool normalizeParameters = true)
        {
            if (normalizeParameters) direction.Normalize();
            return Vector3.Dot(vector, direction);
        }

        private float GetDistanceMultiplier()
        {
            return Mathf.Max(.01f, Mathf.Abs(MagnitudeInDirection(_transform.position - _camera.transform.position, _camera.transform.forward)));
        }        
    }
}

