﻿using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class CurvedTrenchHelper : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _axisX, _axisY;

        [SerializeField]
        private Text _txtAxisX, _txtAxisY;
        
        private Canvas _canvas;

        private void Start()
        {
            _canvas = GetComponent<Canvas>();
        }

        private void OnDisable()
        {
            _axisX.sizeDelta = new Vector2(_axisX.rect.width, 0);
            _axisY.sizeDelta = new Vector2(_axisY.rect.width, 0);
            _txtAxisX.text = _txtAxisY.text = "";            
        }
        
        public void SetPos(Vector3 pos)
        {
            if (_axisY.position.y > pos.y)
            {
                var disX = Vector3.Distance(new Vector3(pos.x, 0, 0), new Vector3(_axisX.position.x, 0, 0))/* - 0.02f*/;
                _axisX.sizeDelta = new Vector2(_axisX.rect.width, ((disX) * 1000 / _canvas.scaleFactor));
                _txtAxisX.text = disX.ToString("0.00") + "m";
                //_txtAxisX.rectTransform.localPosition = new Vector2(_axisX.localPosition.x + _axisX.sizeDelta.y, _axisX.localPosition.y);

                var disY = Vector3.Distance(new Vector3(0, pos.y, 0), new Vector3(0, _axisY.position.y, 0))/* + 0.02f*/;
                _axisY.sizeDelta = new Vector2(_axisY.rect.width, (disY + 0.04f) * 1000 / _canvas.scaleFactor);
                _txtAxisY.text = disY.ToString("0.00") + "m";
                //_txtAxisY.rectTransform.localPosition = new Vector2(_axisY.localPosition.x, _axisY.localPosition.y - _axisY.sizeDelta.y);                
            }            
        }
    }
}


