﻿using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class MetronomHelper : MonoBehaviour
    {
        [SerializeField]
        private Text _txtTime;

        private float _startTime;
        private bool _isCalcTime;

        private void Update()
        {
            if(_isCalcTime)
            {
                _txtTime.text = Mathf.Round(Time.time - _startTime).ToString() + " sec";
            }
        }

        public void StartCalcTime()
        {
            _startTime = Time.time;
            _isCalcTime = true;
        }

        public void StopCalcTime()
        {
            _isCalcTime = false;
            _txtTime.text = "";
        }
    }
}


