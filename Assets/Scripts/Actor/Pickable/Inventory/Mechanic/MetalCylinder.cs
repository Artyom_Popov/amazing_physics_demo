﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace AmazyingPhysics
{
    public class MetalCylinder : PickableInventoryBase
    {
        private Vector3 _buferConnectorPos; // хранение координат сдвига относительно желоба

        //Thermopart
        private bool _isEnabled;
        private Vector3 _targetPosition;
        private float _speed;
        private bool _istoTarget;
        private Action _action;

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);
            InventorySettings.Transform.localPosition = inventory.InventorySettings.Connectors[0].localPosition - InventorySettings.Pivots[1].localPosition;            
            InventorySettings.Transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 90));

            _buferConnectorPos = inventory.InventorySettings.Connectors[0].localPosition;
        }

        public override void TunePosition(Vector3 position)
        {
            var delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;
            var resultPos = new Vector3(delta, _buferConnectorPos.y, _buferConnectorPos.z);
            InventorySettings.Transform.localPosition = resultPos - InventorySettings.Pivots[1].localPosition;
        }

        public override void EnableSimulation()
        {
            if(InventorySettings.ExperimentId != 25)
            {
                SaveItemState();
                InventorySettings.Transform.parent = DynamicRoot.Instance.DynamicRootTransform;
                InventorySettings.Rigidbody.isKinematic = false;
            }

            _isEnabled = true;
        }

        public override void DisableSimulation()
        {
            if (InventorySettings.ExperimentId != 25)
            {
                InventorySettings.Rigidbody.isKinematic = true;
                LoadItemState();
            }
            
            _isEnabled = false;
        }

        public override List<IinventoryAct> GetInventoryActions()
        {
            print(InventorySettings.ExperimentId);
            InventoryAct = new List<IinventoryAct>();

            return InventoryAct;
        }

        private void MoveToTarget(Vector3 targetPosition, float speed, Action action = null)
        {
            _targetPosition = targetPosition;
            _speed = speed;
            _action = action;
            _istoTarget = true;
        }

        private void Update()
        {
            if (_istoTarget)
            {
                var myTrasform = InventorySettings.Transform;

                myTrasform.position = Vector3.MoveTowards(myTrasform.position, _targetPosition, _speed * Time.deltaTime);

                if (myTrasform.position == _targetPosition)

                {
                    _istoTarget = false;

                    if (_action != null)
                    {
                        _action();
                    }
                }
            }
        }
    }
}


