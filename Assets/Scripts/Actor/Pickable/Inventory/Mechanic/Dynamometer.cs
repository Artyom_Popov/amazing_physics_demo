﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public class Dynamometer : PickableInventoryBase
    {
        [SerializeField]
        private Transform _movingPart;
        private Vector3 _movingPartStartPos;
        private Vector3 _childBodyStartPos;
        private Vector3 _buferConnectorPos;
        private int _bufferParentId;
        private Vector3 _deltaMovingPart/* = new Vector3(0, 0.2325f, 0)*/;
        //private Vector3 _deltaMovingPart = new Vector3(0.0f, 0.1280f, 0.0f);
        private bool _isTopOnBoard;
        private Vector3 _downPosOnBoard;

        private void Update()
        {
            if (IsEnableSimulation)
            {
                if (InventoryChilds.Count > 0)
                {
                    _movingPart.localPosition = InventorySettings.Transform.InverseTransformPoint(InventoryChilds.First().InventorySettings.Transform.position) + _deltaMovingPart;
                }
            }
        }
        
        public override void Initialize()
        {
            _movingPartStartPos = _movingPart.localPosition;           
        }

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);

            if (inventory.InventorySettings.Id == 1)
            {
                InventorySettings.Transform.localPosition = inventory.InventorySettings.Connectors[0].localPosition - InventorySettings.Pivots[1].localPosition;
                InventorySettings.Transform.localRotation = Quaternion.identity;
                _bufferParentId = 1;
            }
            else if (inventory.InventorySettings.Id == 92)
            {
                InventorySettings.Transform.localRotation = Quaternion.Euler(new Vector3(-90, -90, 0));
                var delta = inventory.InventorySettings.Connectors[0].localPosition - InventorySettings.Pivots[2].localPosition;
                delta.x = delta.x - 0.2f;
                delta.y = delta.y + 0.005f;
                delta.z = 0;
                InventorySettings.Transform.localPosition = inventory.InventorySettings.Connectors[0].localPosition + delta;
                _bufferParentId = 92;
            }
        }

        public override void TunePosition(Vector3 position)
        {
            if (_bufferParentId == 92)
            {
                var delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;
                var resultPos = new Vector3(delta, InventorySettings.Transform.localPosition.y, InventorySettings.Transform.localPosition.z);
                InventorySettings.Transform.localPosition = resultPos;
            }
        }

        public override List<IinventoryAct> GetInventoryActions()
        {
            if (InventoryParent != null)
            {
                InventoryAct = new List<IinventoryAct>();

                if (InventoryParent.InventorySettings.Id == 1)
                {                    
                    InventoryAct.Add(new ButtonInventoryFactory("положить на доску", () => StartCoroutine(CorPutOnBoard())));                    

                }
                if (InventoryParent.InventorySettings.Id == 92)
                {
                    if(_isTopOnBoard)
                    {                        
                        InventoryAct.Add(new ButtonInventoryFactory("Повторить перемещение", () =>
                        {
                            InventoryChilds.First().InventorySettings.Rigidbody.isKinematic = true;
                            InventoryChilds.First().InventorySettings.Rigidbody.freezeRotation = false;
                            InventoryChilds.First().SetParerntRelativePointerItem(this);
                            InventorySettings.Transform.localPosition = _downPosOnBoard;

                            StartCoroutine(CorMoveAlongOnBoard());
                        }));
                    }
                    else
                    {
                        InventoryAct.Add(new ButtonInventoryFactory("Тащить вдоль доски", () =>
                        {
                            StartCoroutine(CorMoveAlongOnBoard());
                        }));
                    }                   
                }                
            }

            return InventoryAct;
        }

        public override void EnableSimulation()
        {
            //SaveItemState();

            if (InventoryChilds.Count > 0)
            {
                _childBodyStartPos = _movingPart.position - InventoryChilds.First().InventorySettings.Transform.position;

                //Расстояние для грузика 100 грамм.
                if (InventoryChilds.First().InventorySettings.Id == 3)
                {
                    _deltaMovingPart = new Vector3(0.0f, 0.1280f, 0.0f);
                }

                //Расстояние для бруска деревяного с ушком.
                if (InventoryChilds.First().InventorySettings.Id == 66)
                {
                    _deltaMovingPart = new Vector3(0.0f, 0.2325f, 0.0f);
                }

                if (InventorySettings.CreatedInventory.GetInventory(92) != null)
                {
                    InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(true);
                }                
            }

            IsEnableSimulation = true;                       
        }
        
        public override void DisableSimulation()
        {
            IsEnableSimulation = false;

            StopAllCoroutines();

            if(InventoryParent.InventorySettings.Id == 92)
            {
                InventoryParent.InventoryChilds.Clear();
            }

            _movingPart.localPosition = _movingPartStartPos;
            //LoadItemState();
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(false);
            _isTopOnBoard = false;
        }
        
        private IEnumerator CorPutOnBoard()
        {
            var board = InventorySettings.CreatedInventory.GetInventory(92);

            if(board != null)
            {
                InventoryChilds.First().InventorySettings.Rigidbody.isKinematic = true;
                InventoryChilds.First().InventorySettings.Rigidbody.freezeRotation = false;
                InventoryChilds.First().SetParerntRelativePointerItem(this);
               
                SetParerntRelativePointerItem(board);
                InventoryParent = board;
                board.InventoryChilds.Add(this);

                _downPosOnBoard = InventorySettings.Transform.localPosition;
            }

            yield return 0;
        }

        private IEnumerator CorMoveAlongOnBoard()
        {
            //_downPosOnBoard = InventorySettings.Transform.localPosition;

            var step = 0.1f;
            InventoryChilds.First().MakeJoint();
            InventoryChilds.First().InventorySettings.Transform.parent = DynamicRoot.Instance.DynamicRootTransform;
            InventoryChilds.First().InventorySettings.Rigidbody.freezeRotation = true;
            InventoryChilds.First().InventorySettings.Rigidbody.isKinematic = false;
            while (InventorySettings.Transform.localPosition.x < 0.4f)
            {
                var posX = InventorySettings.Transform.localPosition.x + step * Time.deltaTime;
                InventorySettings.Transform.localPosition = new Vector3 (
                    posX, 
                    InventorySettings.Transform.localPosition.y,
                    InventorySettings.Transform.localPosition.z
                );
                yield return null;
            }

            _isTopOnBoard = true;
        }
    }
}


