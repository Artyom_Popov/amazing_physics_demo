﻿using System.Linq;
using UnityEngine;

namespace AmazyingPhysics
{
    public class MechanicDoubleStandClip : PickableInventoryBase
    {
        [SerializeField]
        private PickableInventoryBase _part1;
        [SerializeField]
        private PickableInventoryBase _part2;

        private void Update()
        {
            if (InventoryChilds.Count > 0)
            {
                if (InventoryChilds.First().InventorySettings.Id == 53 || InventoryChilds.First().InventorySettings.Id == 92)
                {
                    InventoryChilds.First().TunePosition(InventorySettings);
                }
            }
        }

        public override void Initialize()
        {
            _part1.InventorySettings.Id = 104;
            _part1.InventoryParent = this;
            InventoryChilds.Add(_part1);

            _part2.InventorySettings.Id = 104;
            _part2.InventoryParent = this;
            InventoryChilds.Add(_part2);
        }

        public override void TunePosition(Vector3 position)
        {            
            float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).y;          
            delta = Mathf.Clamp(delta, 0.065f, 0.55f);
            SetPositionToObjects(new Vector3(0, delta, 0));            
        }

        public override void EnableSimulation()
        {
            SaveItemState();            
        }

        public override void DisableSimulation()
        {
            LoadItemState();
        }
    }
}


