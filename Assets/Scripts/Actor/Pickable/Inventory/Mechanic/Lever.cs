﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class Lever : PickableInventoryBase
    {
        [SerializeField]
        private GameObject _prefRing;
        private LeverRing[] _rings;

        public override void Initialize()
        {
            InventorySettings.Id = -1;
            SetActiveColliders(false);
            _rings = new LeverRing[4];
            _rings[0] = CreateRings(0.1f, 0.17f);
            _rings[1] = CreateRings(0.2f, 0.17f);
            _rings[2] = CreateRings(-0.1f, -0.17f);
            _rings[3] = CreateRings(-0.2f, -0.17f);
        }

        private LeverRing CreateRings(float posX, float anchorPosX)
        {
            var go = Instantiate(_prefRing);
            go.transform.SetParent(InventorySettings.ChildsContainer, false);
            var ring = go.GetComponent<LeverRing>();
            ring.InventorySettings.Id = 30;
            ring.InventoryParent = this;            
            InventoryChilds.Add(ring);
            ring.SetPosition(posX);           
            return ring;
        }

        public override void EnableSimulation()
        {
            SaveItemState();            
            InventorySettings.Transform.transform.parent = DynamicRoot.Instance.DynamicRootTransform;
            InventorySettings.Rigidbody.isKinematic = false;
        }

        public override void DisableSimulation()
        {
            if(InventoryParent != null)
            {
                InventorySettings.Transform.parent = InventoryParent.InventorySettings.ChildsContainer;
            }
            
            InventorySettings.Rigidbody.isKinematic = true;
            if (InventorySettings.Joint != null)
            {
                Destroy(InventorySettings.Joint);
            }

            LoadItemState();
        }

        public override void MakeJoint()
        {
            if (InventoryParent != null)
            {
                HingeJoint hingeJoint = InventorySettings.Transform.gameObject.AddComponent<HingeJoint>();
                hingeJoint.connectedBody = InventoryParent.InventorySettings.Rigidbody;                
                hingeJoint.axis = Vector3.forward;
                hingeJoint.useLimits = true;
                JointLimits limits = hingeJoint.limits;
                limits.min = -80;
                limits.bounciness = 0;
                limits.bounceMinVelocity = 0.2f;
                limits.max = 80;
                hingeJoint.limits = limits;                
                InventorySettings.Joint = hingeJoint;
            }
        }
    }
}


