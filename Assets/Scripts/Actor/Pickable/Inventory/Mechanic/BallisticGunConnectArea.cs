﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class BallisticGunConnectArea : PickableInventoryBase
    {
        private void Start()
        {
            InventorySettings.Id = 105;
        }

        public override void EnableSimulation()
        {
            SetActiveColliders(false);
        }

        public override void DisableSimulation()
        {
            SetActiveColliders(true);
        }
    }
}


