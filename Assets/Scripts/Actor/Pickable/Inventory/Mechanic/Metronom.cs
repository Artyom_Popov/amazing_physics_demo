﻿using UnityEngine;
using System.Collections;

namespace AmazyingPhysics
{
    public class Metronom : PickableInventoryBase
    {
        [SerializeField]
        private GameObject _movingPart;

        [SerializeField]
        private MetronomHelper _helper;

        private Quaternion _delta = Quaternion.Euler(0, 0, 45);
        
        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
            StartCoroutine(CorMetronom());
            _helper.StartCalcTime();
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;
            StopAllCoroutines();
            _movingPart.transform.localRotation = Quaternion.identity;
            _helper.StopCalcTime();
        }

        private IEnumerator CorMetronom()
        {
            var min = -60;
            var max = 60;
            var lerpTime = 0.5f;
            var step = 1.5f;

            while(true)
            {
                if(lerpTime > 1)
                {
                    var buffer = max;
                    max = min;
                    min = buffer;

                    lerpTime = 0.0f;
                }

                lerpTime += step * Time.deltaTime;

                var angle = Mathf.LerpAngle(min, max, lerpTime);

                _movingPart.transform.localRotation = Quaternion.Lerp(_movingPart.transform.localRotation, Quaternion.Euler(0, 0, angle), lerpTime);
                
                yield return null;
            }
        }
    }
}


