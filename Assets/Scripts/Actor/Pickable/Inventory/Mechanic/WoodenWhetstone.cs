﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class WoodenWhetstone : PickableInventoryBase
    {
        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);            
            InventorySettings.Transform.localPosition = inventory.InventorySettings.Connectors[0].localPosition - InventorySettings.Pivots[1].localPosition;
            InventorySettings.Transform.localRotation = Quaternion.identity;
        }

        public override void MakeJoint()
        {
            if (InventorySettings.Joint != null)
            {
                Destroy(InventorySettings.Joint);
            }

            SpringJoint springJoint = InventorySettings.Transform.gameObject.AddComponent<SpringJoint>();
            springJoint.connectedBody = InventoryParent.InventorySettings.Rigidbody;
            //springJoint.anchor = new Vector3(0, 0.25f, 0);
            springJoint.spring = 200;
            InventorySettings.Joint = springJoint;
            //InventorySettings.Rigidbody.freezeRotation = true;
        }

        public override void EnableSimulation()
        {
            SaveItemState();
            InventorySettings.Transform.parent = DynamicRoot.Instance.DynamicRootTransform;
            InventorySettings.Rigidbody.freezeRotation = true;
            InventorySettings.Rigidbody.isKinematic = false;
        }

        public override void DisableSimulation()
        {
            InventorySettings.Rigidbody.isKinematic = true;
            LoadItemState();            
        }
    }
}


