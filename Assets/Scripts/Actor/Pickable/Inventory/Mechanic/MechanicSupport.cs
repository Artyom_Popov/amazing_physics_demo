﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;

namespace AmazyingPhysics
{
    public class MechanicSupport : PickableInventoryBase
    {
        [SerializeField]
        private BenchHelper _helper;

        public override void Initialize()
        {
            InventoryAct = new List<IinventoryAct>();            
            _helper.SwapSide();
        }

        public override void EnableSimulation()
        {
            _helper.gameObject.SetActive(true);
            var b = InventoryChilds.Where(x => x.InventorySettings.Id != 37).ToArray();
            List<PickableInventoryBase> orderInventoryChilds = b.OrderBy(x => x.transform.localPosition.y).ToList();            
            orderInventoryChilds.Insert(0, this);
            _helper.Init(orderInventoryChilds.ToArray());
            _helper.Callouts[0].SetInteractable(false);
        }

        public override void DisableSimulation()
        {
            _helper.Deactivate();
            _helper.gameObject.SetActive(false);                      
        }       
    }
}


