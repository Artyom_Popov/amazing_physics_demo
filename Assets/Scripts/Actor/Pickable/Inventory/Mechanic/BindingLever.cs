﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class BindingLever : PickableInventoryBase
    {
        [SerializeField]
        private Lever _lever;

        public override void Initialize()
        {
            InventoryChilds.Add(_lever);
            _lever.InventoryParent = this;
            _lever.Initialize();
        }

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, true);
            SetPosition(1, inventory.InventorySettings.Connectors[0].localPosition);
        }
    }
}


