﻿using UnityEngine;
using System;

namespace AmazyingPhysics
{
    public class KinematicSphere : PickableInventoryBase
    {
        [SerializeField]
        private TrailRenderer _trailRenderer;

        private Vector3 _buferConnectorPos;
        private int _bufferParentId;

        public bool IsTouchTable;
        private ObservedValue<bool> OnTouch;

        private void Start()
        {
            _trailRenderer.Clear();

            OnTouch = new ObservedValue<bool>(false);
            OnTouch.OnValueChanged += (bool value) =>
            {
                if (value)
                {
                    Timer.Instance.PinTimer();
                }
            };
        }

        private void Update()
        {
            OnTouch.Value = IsTouchTable;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if(collision.gameObject.tag == "Table")
            {
                IsTouchTable = true;
            }
            else if (collision.gameObject.tag == "Pickable")
            {
                var metalCylinder = collision.gameObject.GetComponent<MetalCylinder>();

                if(metalCylinder != null)
                {
                    IsTouchTable = true;
                }                
            }
        }

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);
            InventorySettings.Transform.localPosition = inventory.InventorySettings.Connectors[0].localPosition;
            InventorySettings.Transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 90));
            _buferConnectorPos = inventory.InventorySettings.Connectors[0].localPosition;
            _bufferParentId = inventory.InventorySettings.Id;
        }

        public override void TunePosition(Vector3 position)
        {
            if(_bufferParentId == 53)
            {
                var delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;
                var resultPos = new Vector3(delta, _buferConnectorPos.y, _buferConnectorPos.z);
                InventorySettings.Transform.localPosition = resultPos;
            }           
        }

        public override void EnableSimulation()
        {
            SaveItemState();
            InventorySettings.Transform.parent = DynamicRoot.Instance.DynamicRootTransform;            
            InventorySettings.Rigidbody.isKinematic = false;

            if(InventoryParent != null && InventoryParent.InventorySettings.Id == 103)
            {
                InventorySettings.Rigidbody.AddForce(-InventorySettings.Transform.transform.right * 40, ForceMode.Force);
            }

            _trailRenderer.Clear();
            _trailRenderer.enabled = true;            
        }

        public override void DisableSimulation()
        {
            if (InventoryParent != null)
            {
                InventorySettings.Transform.parent = InventoryParent.InventorySettings.ChildsContainer;
            }

            InventorySettings.Rigidbody.isKinematic = true;
            LoadItemState();
            
            _trailRenderer.enabled = false;
            IsTouchTable = false;
        }        
    }
}


