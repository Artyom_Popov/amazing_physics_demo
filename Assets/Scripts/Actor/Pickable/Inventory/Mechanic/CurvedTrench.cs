﻿using UnityEngine;
using Zenject;
using AmazyingPhysics;
using System.Linq;

namespace AmazyingPhysics
{
    public class CurvedTrench : PickableInventoryBase
    {
        [SerializeField]
        private CurvedTrenchHelper _helper;

        private bool _isContainKineticSphere;
        private Transform _transKineticSphere;
        private KinematicSphere _kinematicSphere;
        

        private void Update()
        {
            if (IsEnableSimulation)
            {
                if (_isContainKineticSphere && !_kinematicSphere.IsTouchTable)
                {
                    _helper.SetPos(_transKineticSphere.position);
                }

                
            }
        }

        public override void Initialize()
        {
           
        }

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory, Transform[] connectors)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);           
            SetPosition(1, connectors[0].localPosition);
        }

        public override void TunePosition(Transform transRoot)
        {
            var angle = Mathf.Asin((transRoot.position.y - 1) / 1) * Mathf.Rad2Deg;
            InventorySettings.Transform.localRotation = Quaternion.AngleAxis(angle, InventorySettings.Transform.forward);

            SetPosition(1, transRoot.localPosition);
        }

        public override void EnableSimulation()
        {
            var obj = InventoryChilds.Where(x => x.InventorySettings.Id == 93).FirstOrDefault();

            if (obj != null)
            {
                _kinematicSphere = obj as KinematicSphere;
                _isContainKineticSphere = true;
                _transKineticSphere = obj.InventorySettings.Transform;                
                _helper.gameObject.SetActive(true);
            }

            IsEnableSimulation = true;            
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;
            _helper.gameObject.SetActive(false);
        }
    }
}


