﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class BallisticGun : PickableInventoryBase
    {
        [SerializeField]
        private KinematicSphere _bullet;
        
        public override void Initialize()
        {
            InventoryChilds.Add(_bullet);
            _bullet.InventoryParent = this;
        }        
    }
}


