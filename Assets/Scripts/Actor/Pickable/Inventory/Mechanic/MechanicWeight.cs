﻿using UnityEngine;
using System.Collections;

namespace AmazyingPhysics
{
    public class MechanicWeight : PickableInventoryBase
    {
        private bool _isEnabled;        
        private SpringJoint _springJoint;

        public override void MakeJoint()
        {
            if (InventoryParent != null)
            {
                if (InventoryParent.InventorySettings.Id == 3)
                {
                    var hingeJoint = InventorySettings.Transform.gameObject.AddComponent<HingeJoint>();
                    hingeJoint.axis = transform.forward;
                    hingeJoint.connectedBody = InventoryParent.InventorySettings.Rigidbody;
                    InventorySettings.Joint = hingeJoint;
                }
                else if (InventoryParent.InventorySettings.Id == 2)
                {
                    SpringJoint springJoint = InventorySettings.Transform.gameObject.AddComponent<SpringJoint>();
                    springJoint.connectedBody = InventoryParent.InventoryParent.InventorySettings.Rigidbody;
                    springJoint.anchor = new Vector3(0, 0, .015f);
                    springJoint.spring = 20;
                    InventorySettings.Joint = springJoint;
                    InventorySettings.Rigidbody.freezeRotation = true;
                }
                else if (InventoryParent.InventorySettings.Id == 30)
                {
                    var hingeJoint = InventorySettings.Transform.gameObject.AddComponent<HingeJoint>();
                    hingeJoint.axis = transform.forward;
                    hingeJoint.connectedBody = InventoryParent.InventoryParent.InventorySettings.Rigidbody;
                    InventorySettings.Joint = hingeJoint;
                }
                else if (InventoryParent.InventorySettings.Id == 18)
                {
                    _springJoint = InventorySettings.Transform.gameObject.AddComponent<SpringJoint>();
                    _springJoint.connectedBody = InventoryParent.InventoryParent.InventorySettings.Rigidbody;
                    _springJoint.anchor = new Vector3(0, 0.1605f, 0);
                    _springJoint.autoConfigureConnectedAnchor = false;
                    _springJoint.connectedAnchor = new Vector3(-0.1289f, 0.0f, 0.01426f);
                    _springJoint.spring = 200;
                    InventorySettings.Joint = _springJoint;
                    InventorySettings.Rigidbody.freezeRotation = true;
                }
            }
        }

        public override void EnableSimulation()
        {
            _isEnabled = true;

            SaveItemState();

            InventorySettings.Transform.transform.parent = DynamicRoot.Instance.DynamicRootTransform;
            InventorySettings.Rigidbody.isKinematic = false;

            var childs = InventoryParent.InventoryParent.InventoryParent.InventoryChilds;          
        }

        public override void DisableSimulation()
        {
            if (InventoryParent != null)
            {
                InventorySettings.Transform.parent = InventoryParent.InventorySettings.ChildsContainer;
            }

            InventorySettings.Rigidbody.isKinematic = true;
            if (InventorySettings.Joint != null)
            {
                Destroy(InventorySettings.Joint);
            }

            LoadItemState();
        }

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);
            SetPosition(1, inventory.InventorySettings.Connectors[0].localPosition);
        }

        private void OnTriggerEnter(Collider other)
        {

        }
    }
}


