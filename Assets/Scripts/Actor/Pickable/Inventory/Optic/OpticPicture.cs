﻿using UnityEngine;
using Zenject;
using AmazyingPhysics;

namespace AmazyingPhysics
{
    public class OpticPicture : PickableInventoryBase
    {
        private Material _matPicture;

        private void Start()
        {
            _matPicture = InventorySettings.MeshRenderers[0].materials[3];
        }

        public override void TunePosition(Vector3 position)
        {
            float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;            
            delta = Mathf.Clamp(delta, -0.43f, 0.43f);
            SetPosition(1, new Vector3(delta, 0, 0));            
        }
        
        public override void EnableSimulation()
        {            
            _matPicture.SetColor("_EmissionColor", Color.white);
            RendererExtensions.UpdateGIMaterials(InventorySettings.MeshRenderers[0]);
            DynamicGI.UpdateEnvironment();

            //var mats = InventorySettings.MeshRenderers[0].materials;            
            //mats[3] = _matPicture;
            //InventorySettings.MeshRenderers[0].materials = mats;
        }

        public override void DisableSimulation()
        {
            
            _matPicture.SetColor("_EmissionColor", Color.black);
            RendererExtensions.UpdateGIMaterials(InventorySettings.MeshRenderers[0]);
            DynamicGI.UpdateEnvironment();

            //var mats = InventorySettings.MeshRenderers[0].materials;
            //mats[3] = _matPicture;
            //InventorySettings.MeshRenderers[0].materials = mats;
        }
    }
}


