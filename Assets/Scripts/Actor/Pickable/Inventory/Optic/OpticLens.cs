﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System;

namespace AmazyingPhysics
{
    public class OpticLens : PickableInventoryBase
    {
        [SerializeField]
        private Camera _focusCam;

        private LensSettings _lensSettings;
        
        private void Start()
        {
            _lensSettings = new LensSettings(_focusCam);
        }
        
        private void Update()
        {
            if(IsEnableSimulation)
            {
                _lensSettings.CalculateOpticParams(InventorySettings.Transform.position);                
            }       
        }

        public override void TunePosition(Vector3 position)
        {
            float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;
            delta = Mathf.Clamp(delta, -0.43f, 0.43f);
            SetPosition(1, new Vector3(delta, 0, 0));
        }        
        
        public override void EnableSimulation()
        {
            if(InventorySettings.CreatedInventory.GetInventory(9) != null)
            {
                _lensSettings.Target = InventorySettings.CreatedInventory.GetInventory(9).InventorySettings.Transform;
            }

            if (InventorySettings.CreatedInventory.GetInventory(14) != null)
            {
                _lensSettings.Screen = InventorySettings.CreatedInventory.GetInventory(14).InventorySettings.Transform;
            }

            _focusCam.gameObject.SetActive(true);
            IsEnableSimulation = true;        
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;
            _lensSettings.Target = _lensSettings.Screen = null;
            _focusCam.gameObject.SetActive(false);
        }        
    }

    [Serializable]
    public class LensSettings
    {
        private Camera _camera;        
        private DepthOfFieldDeprecated[] _dof;        
        private float _halfSideScreen = 0.1f;
        private float F = 0.1f;
        private Transform _dofPoint;

        public Transform Target;
        public Transform Screen;

        public LensSettings(Camera camera)
        {
            _camera = camera;
            _dof = camera.GetComponents<DepthOfFieldDeprecated>();             
            _dofPoint = camera.transform.GetChild(0);
        }
        
        public void CalculateOpticParams(Vector3 posLens)
        {
            if (Target != null && Screen != null)
            {
                // расстояние от экрана до линзы
                float f = Vector3.Distance(posLens, Screen.position);

                // fov
                _camera.fieldOfView = Mathf.Atan2(_halfSideScreen, f) * Mathf.Rad2Deg * 2;

                // Вычисляем фокусную дистанци = Фокус линзы * Расстояние от экрана до линзы / Фокус линзы - Расстояние от экрана до линзы
                var dof = (F * f) / (f - F);

                if (dof > 0)
                {
                    foreach (var item in _dof)
                    {
                        item.focalPoint = dof;
                        _dofPoint.localPosition = new Vector3(_dofPoint.localPosition.x, _dofPoint.localPosition.y, item.focalPoint);
                    }                    
                }
            }
        }        
    }
}