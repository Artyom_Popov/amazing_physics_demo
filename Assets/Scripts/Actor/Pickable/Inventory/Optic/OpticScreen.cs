﻿using System.Collections.Generic;
using UnityEngine;

namespace AmazyingPhysics
{
    public class OpticScreen : PickableInventoryBase
    {
        [SerializeField]
        private Material _matAddLensImage;

        public float delta;
        
        private ProceduralMaterial _matScreen;
        private ObservedValue<Vector3> OnChangeDifractionPos = new ObservedValue<Vector3>(Vector3.zero);
        private ObservedValue<Vector3> OnChangeScreenPos = new ObservedValue<Vector3>(Vector3.zero);
        private Transform _difractionTarget;
        private Material[] _sourceMats;

        private void Update()
        {
            if (IsEnableSimulation)
            {
                if (_difractionTarget != null)
                {
                    OnChangeDifractionPos.Value = _difractionTarget.position;
                    OnChangeScreenPos.Value = InventorySettings.Transform.position;
                }
            }
        }

        public override void Initialize()
        {
            if (InventorySettings.ExperimentId == 17)
            {
                var mats = InventorySettings.MeshRenderers[0].materials;
                _sourceMats = InventorySettings.MeshRenderers[0].materials;
                mats[1] = _matAddLensImage;
                InventorySettings.MeshRenderers[0].materials = mats;
            }
            else if (InventorySettings.ExperimentId == 33)
            {
                _matScreen = InventorySettings.MeshRenderers[0].materials[1] as ProceduralMaterial;
            }
        }       

        public override void TunePosition(Vector3 position)
        {
            float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;            
            delta = Mathf.Clamp(delta, -0.43f, 0.43f);
            SetPosition(1, new Vector3(delta, 0, 0));            
        }
        
        public override void EnableSimulation()
        {
            if (InventorySettings.ExperimentId == 17)
            {
                var mats = InventorySettings.MeshRenderers[0].materials;
                mats[1] = _matAddLensImage;
                InventorySettings.MeshRenderers[0].materials = mats;
            }
            else if (InventorySettings.ExperimentId == 33)
            {
                var inventory = InventorySettings.CreatedInventory.GetInventory(10);
                if (inventory != null)
                {
                    _difractionTarget = inventory.InventorySettings.Transform;
                }                    

                if(_difractionTarget != null)
                {
                    _matScreen.SetProceduralBoolean("addLightPicture", true);
                    _matScreen.RebuildTextures();

                    OnChangeDifractionPos.OnValueChanged += SetDifractionParams;
                    OnChangeScreenPos.OnValueChanged += SetDifractionParams;
                }               
            }

            InventorySettings.InventoryActionsSimModePanel.CamView.SetActive(true);
            
            IsEnableSimulation = true;
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;

            if (InventorySettings.ExperimentId == 17)
            {
                InventorySettings.MeshRenderers[0].materials = _sourceMats;                
            }
            else if (InventorySettings.ExperimentId == 33)
            {
                if (_difractionTarget != null)
                {
                    _matScreen.SetProceduralBoolean("addLightPicture", false);
                    _matScreen.RebuildTextures();

                    OnChangeDifractionPos.OnValueChanged -= SetDifractionParams;
                    OnChangeScreenPos.OnValueChanged -= SetDifractionParams;
                }

                _difractionTarget = null;
            }

            InventorySettings.InventoryActionsSimModePanel.CamView.SetActive(false);          
        }
        
        private void SetDifractionParams(Vector3 pos)
        {
            var distance = Vector3.Distance(InventorySettings.Transform.position, _difractionTarget.position);
            _matScreen.SetProceduralFloat("distance", distance);
            _matScreen.RebuildTextures();              
        }        
    }
}


