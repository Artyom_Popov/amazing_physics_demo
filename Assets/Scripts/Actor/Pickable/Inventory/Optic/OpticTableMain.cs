﻿using UnityEngine;
using Zenject;
using AmazyingPhysics;
using System.Linq;

namespace AmazyingPhysics
{
    public class OpticTableMain : PickableInventoryBase
    {
        public override void Initialize()
        {
            var child = InventorySettings.ChildsContainer.GetComponentInChildren<PickableInventoryBase>();
            child.Initialize();
            child.InventoryParent = this;
            InventoryChilds.Add(child);
        }

        public override void TunePosition(Vector3 position)
        {
            float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;
            delta = Mathf.Clamp(delta, -0.43f, 0.43f);
            SetPosition(1, new Vector3(delta, 0, 0));
        }

        //public override void EnableSimulation()
        //{
        //    InventoryChilds.First().EnableSimulation();
        //}

        //public override void DisableSimulation()
        //{
        //    InventoryChilds.First().DisableSimulation();
        //}
    }
}


