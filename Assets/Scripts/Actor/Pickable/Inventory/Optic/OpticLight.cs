﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class OpticLight : PickableInventoryBase
    {
        [SerializeField]
        private Light _light;

        private Material _matLight;

        public override void Initialize()
        {
            _matLight = InventorySettings.MeshRenderers[0].materials[1];
        }

        public override void SetParerntRelativePointerItem(PickableInventoryBase inventory, Transform[] connectors)
        {
            InventorySettings.Transform.SetParent(inventory.InventorySettings.ChildsContainer, false);
            SetPosition(1, connectors[0].localPosition);
        }

        public override void TunePosition(Vector3 position)
        {
            float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;
            delta = Mathf.Clamp(delta, -0.43f, 0.43f);
            SetPosition(1, new Vector3(delta, 0, 0));
        }

        public override void EnableSimulation()
        {
            _light.intensity = 0.75f;
            _matLight.SetColor("_EmissionColor", Color.white * 3);
            RendererExtensions.UpdateGIMaterials(InventorySettings.MeshRenderers[0]);
            DynamicGI.UpdateEnvironment();
        }

        public override void DisableSimulation()
        {
            _light.intensity = 0;
            _matLight.SetColor("_EmissionColor", Color.black);
            RendererExtensions.UpdateGIMaterials(InventorySettings.MeshRenderers[0]);
            DynamicGI.UpdateEnvironment();
        }
    }
}


