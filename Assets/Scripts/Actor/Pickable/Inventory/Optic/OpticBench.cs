﻿using UnityEngine;
using Zenject;
using AmazyingPhysics;
using System.Linq;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public class OpticBench : PickableInventoryBase
    {
        [SerializeField]
        private BenchHelper helper1;
        
        public override void EnableSimulation()
        {
            helper1.gameObject.SetActive(true);
            var orderInventoryChilds = InventoryChilds.OrderBy(x => x.transform.localPosition.x).ToArray();           
            helper1.Init(orderInventoryChilds);            
        }

        public override void DisableSimulation()
        {
            helper1.Deactivate();
            helper1.gameObject.SetActive(false);            
        }        
    }
}


