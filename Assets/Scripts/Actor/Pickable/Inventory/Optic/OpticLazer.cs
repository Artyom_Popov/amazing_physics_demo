﻿using UnityEngine;
using Zenject;
using AmazyingPhysics;

namespace AmazyingPhysics
{
    public class OpticLazer : PickableInventoryBase
    {
        [SerializeField]
        private Transform _lazerPointOrigin;
        private PrismaSettings _prismaSettings;

        private void Update()
        {
            if (IsEnableSimulation)
            {
                _prismaSettings.CalcLazer();
            }
        }

        public override void Initialize()
        {
            var line = GetComponent<LineRenderer>();
            _prismaSettings = new PrismaSettings(line, _lazerPointOrigin, 1.7f);
        }

        public override void TunePosition(Vector3 position)
        {
            float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;            
            delta = Mathf.Clamp(delta, -0.43f, 0.43f);
            SetPosition(1, new Vector3(delta, 0, 0));            
        }
        
        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
            _prismaSettings.Line.enabled = true;
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;
            _prismaSettings.Line.enabled = false;
            if (_prismaSettings.PrismHelper != null)
            {
                _prismaSettings.PrismHelper.gameObject.SetActive(false);
                _prismaSettings.PrismHelper = null;
            }            
        }

        // класс описывает расчеты физики лазера
        public class PrismaSettings
        {
            public LineRenderer Line;
            public float _refraction; // коэффициент преломления

            private Transform _lazerPointOrigin;

            private Vector3[] _linePositions;
            private LayerMask _defaultLayerMask = 1 << LayerMask.NameToLayer("Default");
            private LayerMask _prismInsideLayerMask = 1 << LayerMask.NameToLayer("PrismInside");

            public PrismHelper PrismHelper;

            public PrismaSettings(LineRenderer line, Transform lazerPointOrigin, float refraction)
            {
                Line = line;
                Line.enabled = false;
                _lazerPointOrigin = lazerPointOrigin;
                _refraction = refraction;

                Line.positionCount = 4;
                _linePositions = new Vector3[Line.positionCount];
            }            

            public void CalcLazer()
            {
                Ray fromLazerOriginToPrism = new Ray(_lazerPointOrigin.position, _lazerPointOrigin.forward);
                RaycastHit outerPointPrism;
                bool isCastOuterPointPrism = Physics.Raycast(fromLazerOriginToPrism, out outerPointPrism);

                if (isCastOuterPointPrism)
                {
                    //Debug.DrawLine(_lazerPointOrigin.position, outerPointPrism.point, Color.red);

                    if(PrismHelper == null)
                    {
                        var prism = outerPointPrism.transform.GetComponent<OpticPrism>();
                        PrismHelper = prism.helper;
                        PrismHelper.gameObject.SetActive(true);
                    }                                      

                    var backNormal = -outerPointPrism.normal;
                    var cross = Vector3.Cross(_lazerPointOrigin.forward, backNormal).normalized;                   

                    // угол падения
                    var incidenceAngle = Vector3.Angle(_lazerPointOrigin.forward, backNormal);
                    //print("угол падения: " + incidenceAngle);

                    if (cross.y < 0)
                        incidenceAngle = -incidenceAngle;                  

                    // синус фи
                    var sinY = (Mathf.Sin((incidenceAngle) * Mathf.PI / 180)) / _refraction;

                    // угол преломления
                    var refractionAngle = ((Mathf.Asin(sinY) * 180) / Mathf.PI);
                    //print("угол преломления: " + refractionAngle);

                    // поворот на угол 
                    var deltaAngle = Quaternion.AngleAxis(refractionAngle, _lazerPointOrigin.up);

                    // направление преломления
                    var refractionDirection = deltaAngle * _lazerPointOrigin.forward;

                    #region PrismHelper

                    PrismHelper.LineNormal.position = PrismHelper.ImgRefraction.transform.position = PrismHelper.ImgIncidence.transform.position = outerPointPrism.point;
                    PrismHelper.ImgIncidence.fillAmount = - incidenceAngle / 360;                   
                    PrismHelper.ImgRefraction.fillAmount = - refractionAngle / 360;
                    //PrismHelper.LineIncidence.position = outerPointPrism.point;
                    //PrismHelper.LineIncidence.rotation = Quaternion.LookRotation(_lazerPointOrigin.position - outerPointPrism.point) * Quaternion.Euler(90, 0, 180);

                    //PrismHelper.LineRefraction.position = outerPointPrism.point;
                    //PrismHelper.LineRefraction.rotation = Quaternion.LookRotation(refractionDirection) * Quaternion.Euler(90, 0, 180);

                    PrismHelper.TxtIncidence.text = Mathf.Abs(incidenceAngle).ToString("0");
                    PrismHelper.TxtRefraction.text = Mathf.Abs(refractionAngle).ToString("0");

                    #endregion

                    Ray fromOuterPointToInsidePointPrism = new Ray(outerPointPrism.point, refractionDirection);
                    RaycastHit insidePointPrism;
                    bool isCastInsidePointPrism = Physics.Raycast(fromOuterPointToInsidePointPrism, out insidePointPrism, Mathf.Infinity, _prismInsideLayerMask);

                    if (isCastInsidePointPrism)
                    {
                        //Debug.DrawLine(outerPointPrism.point, insidePointPrism.point, Color.yellow);                     

                        Ray fromInsidePointPrismToScreen = new Ray(insidePointPrism.point, _lazerPointOrigin.forward);
                        RaycastHit pointScreen;
                        bool isCastPointScreen = Physics.Raycast(fromInsidePointPrismToScreen, out pointScreen, Mathf.Infinity, _defaultLayerMask);

                        if (isCastPointScreen)
                        {
                            Debug.DrawLine(insidePointPrism.point, pointScreen.point, Color.blue);

                            _linePositions[0] = _lazerPointOrigin.position;
                            _linePositions[1] = outerPointPrism.point;
                            _linePositions[2] = insidePointPrism.point;
                            _linePositions[3] = pointScreen.point;
                            Line.SetPositions(_linePositions);
                        }
                    }
                }
            }
        }
    }
}


