﻿using UnityEngine;
using Zenject;
using AmazyingPhysics;

namespace AmazyingPhysics
{
    public class OpticTableRotator : PickableInventoryBase
    {
        [SerializeField]
        private BallisticGunRotator _helper;

        public override void Initialize()
        {
            InventorySettings.Id = 102;            
            SetActiveColliders(false);
            InventorySettings.Rigidbody.isKinematic = true;           
           
            _helper.OnRotateAngle += (float angle) => InventorySettings.Transform.rotation = Quaternion.Euler(0, -angle, 0);
        }        

        public override void EnableSimulation()
        {
            _helper.transform.parent.parent.parent.gameObject.SetActive(true);
        }

        public override void DisableSimulation()
        {
            _helper.transform.parent.parent.parent.gameObject.SetActive(false);
        }
    }
}


