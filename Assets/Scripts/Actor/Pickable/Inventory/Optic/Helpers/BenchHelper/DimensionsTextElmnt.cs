using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class DimensionsTextElmnt : MonoBehaviour
    {
        [SerializeField]
        private Text _text;

        public void SetParams(string text, Vector3 position)
        {
            _text.text = text;
            transform.position = position;
        }
    }
}
