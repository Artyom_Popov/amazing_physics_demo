﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AmazyingPhysics
{
    public class UiTransition : MonoBehaviour, IPointerDownHandler, IDragHandler
    {
        private Vector2 originalLocalPointerPosition;
        private Vector3 originalPanelLocalPosition;
        private RectTransform panelRectTransform;
        private RectTransform parentRectTransform;
        private Camera _camera;
        private float _bufferScaleValue;
        private float _bufferRotationValue;
        private float _stepScale = 100000;
        private float _stepRotation = 5;

        void Awake()
        {
            panelRectTransform = transform.parent as RectTransform;
            parentRectTransform = panelRectTransform.parent as RectTransform;

            if (_camera == null)
                _camera = Camera.main;
        }

        public void OnPointerDown(PointerEventData data)
        {
            originalPanelLocalPosition = panelRectTransform.localPosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, data.position, data.pressEventCamera, out originalLocalPointerPosition);

            // масштаб
            if (Input.GetMouseButton(1))
            {
                _bufferScaleValue = GetSqrtVectorForScale(data.position);
            }
            else if (Input.GetMouseButton(0))
            {
                // вращение
                _bufferRotationValue = originalLocalPointerPosition.x;
            }
        }

        public void OnDrag(PointerEventData data)
        {
            if (panelRectTransform == null || parentRectTransform == null)
                return;

            Vector2 localPointerPosition;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, data.position, data.pressEventCamera, out localPointerPosition))
            {
                // перемещение
                if (Input.GetMouseButton(2))
                {
                    Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;
                    panelRectTransform.localPosition = originalPanelLocalPosition + offsetToOriginal;

                    ClampToWindow();
                }
                // вращение
                else if (Input.GetMouseButton(0))
                {
                    SetSRotation(localPointerPosition);
                }
                // масштаб
                else if (Input.GetMouseButton(1))
                {
                    SetScale(data.position);
                }
            }
        }

        private void ClampToWindow()
        {
            Vector3 pos = panelRectTransform.localPosition;

            Vector3 minPosition = parentRectTransform.rect.min - panelRectTransform.rect.min;
            Vector3 maxPosition = parentRectTransform.rect.max - panelRectTransform.rect.max;

            pos.x = Mathf.Clamp(panelRectTransform.localPosition.x, minPosition.x, maxPosition.x);
            pos.y = Mathf.Clamp(panelRectTransform.localPosition.y, minPosition.y, maxPosition.y);

            panelRectTransform.localPosition = pos;
        }

        private void SetScale(Vector2 pointerPosition)
        {
            var sqrtPointerPosition = GetSqrtVectorForScale(pointerPosition);
            var delta = (sqrtPointerPosition - _bufferScaleValue) / _stepScale;

            var bufferScale = panelRectTransform.localScale.x + delta;

            var clampScale = Mathf.Clamp(bufferScale, 0, 1000f);
            var scale = new Vector2(clampScale, clampScale);
            panelRectTransform.localScale = scale;

            _bufferScaleValue = sqrtPointerPosition;
        }

        private float GetSqrtVectorForScale(Vector2 vector)
        {
            return ((Vector2)_camera.WorldToScreenPoint(panelRectTransform.position) - vector).sqrMagnitude;
        }

        private void SetSRotation(Vector2 pointerPosition)
        {
            var delta = (pointerPosition.x - _bufferRotationValue) / _stepRotation;

            if (transform.localPosition.y < pointerPosition.y)
            {
                delta = -delta;
            }

            var buffer = panelRectTransform.localEulerAngles.z + delta;

            panelRectTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, buffer));

            _bufferRotationValue = pointerPosition.x;
        }
    }
}

