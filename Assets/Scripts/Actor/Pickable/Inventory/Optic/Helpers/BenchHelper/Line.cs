using UnityEngine;

namespace AmazyingPhysics
{
    public class Line : MonoBehaviour
    {
        private LineRenderer _lineRenderer;
        private Transform _transStart;
        private Transform _transEnd;
        private DimensionsTextElmnt _dimensionsTextElmnt;

        private void Start()
        {
            _lineRenderer = GetComponent<LineRenderer>();
            _lineRenderer.positionCount = 2;
        }

        private void LateUpdate()
        {
            if (_transStart != null && _transEnd != null)
            {
                _lineRenderer.SetPosition(0, new Vector3(_transStart.position.x - .0001f, _transStart.position.y -.0001f, _transStart.position.z - .0001f));
                _lineRenderer.SetPosition(1, new Vector3(_transEnd.position.x - .0001f, _transEnd.position.y - .0001f, _transEnd.position.z - .0001f));

                if(_dimensionsTextElmnt != null)
                {
                    var pos = Vector3.Lerp(_transStart.position, _transEnd.position, 0.5f);
                    var dimension = (Vector3.Distance(_transStart.position, _transEnd.position)).ToString("0.00") + "m";
                    _dimensionsTextElmnt.SetParams(dimension, pos);
                }
            }
        }

        public void SetStartPoint(Transform start)
        {
            _transStart = start;
        }

        public void SetEndPoint(Transform end)
        {
            _transEnd = end;
        }

        public void SetPoints(Transform start, Transform end)
        {
            _transStart = start;
            _transEnd = end;
        }

        public void SetDimensionsTextElmnt(DimensionsTextElmnt dimensionsTextElmnt)
        {
            _dimensionsTextElmnt = dimensionsTextElmnt;
        }
    }
}
