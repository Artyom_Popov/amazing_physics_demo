using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class Callout : MonoBehaviour, IPointerDownHandler, IDragHandler
    {
        [SerializeField]
        private GameObject _prefLine;
        
        [SerializeField]
        private Button _button;

        public RectTransform ControlPoint;        
        public RectTransform SecondPoint;
        
        private Transform _root;
        private Line _line;
        private Vector2 _originalLocalPointerPosition;
        private Vector3 _originalPanelLocalPosition;
        private RectTransform _panelRectTransform;
        private RectTransform _parentRectTransform;
        private float _posY;
        private ActiveAxis _axis;
        
        public event Action<float> OnDragPosX;
        
        private void Awake()
        {
            var go = Instantiate(_prefLine);
            go.transform.SetParent(transform, false);

            _line = go.GetComponent<Line>();
            _line.SetPoints(ControlPoint, SecondPoint);

            _axis = ActiveAxis.X;
        }
        
        public void Init(Transform root)
        {
            _root = root;
            
            _panelRectTransform = transform as RectTransform;
            _parentRectTransform = _panelRectTransform.parent as RectTransform;            
        }

        public void Init(Transform root, ActiveAxis axis)
        {
            _root = root;
            _axis = axis;

            _panelRectTransform = transform as RectTransform;
            _parentRectTransform = _panelRectTransform.parent as RectTransform;
        }

        public void OnPointerDown(PointerEventData data)
        {
            _originalPanelLocalPosition = _panelRectTransform.localPosition;            
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_parentRectTransform, data.position, data.pressEventCamera, out _originalLocalPointerPosition);

            if (_posY == 0)
            {
                _posY = _panelRectTransform.localPosition.y;
            }
        }

        public void OnDrag(PointerEventData data)
        {
            Vector2 localPointerPosition;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_parentRectTransform, data.position, data.pressEventCamera, out localPointerPosition))
            {
                Vector3 offsetToOriginal = localPointerPosition - _originalLocalPointerPosition;
                _panelRectTransform.localPosition = _originalPanelLocalPosition + offsetToOriginal;                
            }

            ClampToWindow();
            
            if (OnDragPosX != null)
            {
                var value = 0.0f;

                if (_axis == ActiveAxis.X)
                {
                    value = _root.InverseTransformPoint(transform.position).x;
                }
                else if (_axis == ActiveAxis.Y)
                {
                    value = _root.InverseTransformPoint(transform.position).y;
                }
                
                OnDragPosX(value);
            }
        }

        private void ClampToWindow()
        {
            Vector3 pos = _panelRectTransform.localPosition;

            Vector3 minPosition = _parentRectTransform.rect.min - _panelRectTransform.rect.min;
            Vector3 maxPosition = _parentRectTransform.rect.max - _panelRectTransform.rect.max;

            pos.x = Mathf.Clamp(_panelRectTransform.localPosition.x, minPosition.x, maxPosition.x);
            pos.y = _posY;

            _panelRectTransform.localPosition = pos;
        }

        public void SetInteractable(bool value)
        {
            _button.image.raycastTarget = value;
        }
    }
}
