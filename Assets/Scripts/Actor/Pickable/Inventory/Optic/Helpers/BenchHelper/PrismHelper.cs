using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class PrismHelper : MonoBehaviour
    {
        public RectTransform LineNormal, LineIncidence, LineRefraction;        
        public Text TxtIncidence, TxtRefraction;
        public Image ImgIncidence, ImgRefraction;
    }
}
