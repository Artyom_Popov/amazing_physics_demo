using System.Collections.Generic;
using UnityEngine;

namespace AmazyingPhysics
{
    public class BenchHelper : MonoBehaviour
    {
        [SerializeField]
        private ActiveAxis _axis;

        [SerializeField]
        private GameObject _prefCallout;

        [SerializeField]
        private GameObject _prefLine;

        [SerializeField]
        private GameObject _prefDimensionTextElmnt;

        public List<Callout> Callouts = new List<Callout>();
        private List<Line> _lines = new List<Line>();

        public void Init(PickableInventoryBase[] roots)
        {
            foreach (var root in roots)
            {
                CreateCallout(root.transform);
            }            

            for (int i = 0; i < Callouts.Count-1; i++)
            {
                var go = Instantiate(_prefLine);
                go.transform.SetParent(transform, false);
                go.transform.position = Callouts[i].ControlPoint.position;
                var line = go.GetComponent<Line>();
                line.SetPoints(Callouts[i].ControlPoint, Callouts[i+1].ControlPoint);

                go = Instantiate(_prefDimensionTextElmnt);
                go.transform.SetParent(line.transform, false);
                var dimensionsTextElmnt = go.GetComponent<DimensionsTextElmnt>();
                line.SetDimensionsTextElmnt(dimensionsTextElmnt);

                _lines.Add(line);
            }
        }

        public void Deactivate()
        {
            foreach (var callout in Callouts)
            {
                Destroy(callout.gameObject);
            }

            Callouts.Clear();

            foreach (var line in _lines)
            {
                Destroy(line.gameObject);
            }

            _lines.Clear();
        }

        public void SwapSide()
        {
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, -transform.localScale.z);
        }

        private void CreateCallout(Transform root)
        {
            var go = Instantiate(_prefCallout);
            go.transform.SetParent(transform.GetChild(0), false);

            var callout = go.GetComponent<Callout>();
            callout.Init(transform.parent.parent, _axis);

            var shift = 0.02f;

            if (_axis == ActiveAxis.X)
            {
                callout.ControlPoint.position = new Vector3(root.position.x, callout.ControlPoint.position.y, root.position.z + shift);
            }
            else if (_axis == ActiveAxis.Y)
            {
                callout.ControlPoint.position = new Vector3(root.position.x, root.position.y + shift, callout.ControlPoint.position.z);
            }

            //callout.ControlPoint.position = new Vector3(root.position.x, callout.ControlPoint.position.y, root.position.z + shift);
            callout.ControlPoint.localPosition = callout.ControlPoint.localPosition - callout.SecondPoint.localPosition;

            callout.OnDragPosX += (float pos) =>
            {
                var vector = Vector3.zero;
                
                if (_axis == ActiveAxis.X)
                {
                    vector = new Vector3(pos, root.localPosition.y, root.localPosition.z);
                }
                else if (_axis == ActiveAxis.Y)
                {
                    vector = new Vector3(root.localPosition.x, pos, root.localPosition.z);
                }
                
                root.localPosition = vector;
            };

            Callouts.Add(callout);
        }
    }
}
