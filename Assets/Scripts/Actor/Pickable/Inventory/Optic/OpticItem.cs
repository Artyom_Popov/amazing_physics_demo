﻿using UnityEngine;
using Zenject;
using AmazyingPhysics;

namespace AmazyingPhysics
{
    public class OpticItem : PickableInventoryBase
    {
        public override void TunePosition(Vector3 position)
        {
            float delta = InventorySettings.Transform.parent.InverseTransformPoint(position).x;            
            delta = Mathf.Clamp(delta, -0.43f, 0.43f);
            SetPosition(1, new Vector3(delta, 0, 0));            
        }
    }
}


