﻿using UnityEngine;
using Zenject;
using AmazyingPhysics;

namespace AmazyingPhysics
{
    public class OpticPrism : PickableInventoryBase
    {
        public PrismHelper helper;

        public override void EnableSimulation()
        {
            InventorySettings.InventoryActionsSimModePanel.CamView.SetActive(true);
        }

        public override void DisableSimulation()
        {
            InventorySettings.InventoryActionsSimModePanel.CamView.SetActive(false);
        }
    }
}


