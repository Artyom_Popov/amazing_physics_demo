﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public class CoilCore : PickableInventoryBase
    {
        public override void TunePosition(Vector3 position)
        {
            SetPositionToObjects(new Vector3(0.11f, 0, 0));
        }
        
        public override void EnableSimulation()
        {
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(true);
        }

        public override void DisableSimulation()
        {
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(false);

            var parent = InventoryParent as Coil;
            if (parent != null)
            {
                parent.CoilCoreValue = 1;
            }
        }

        public override List<IinventoryAct> GetInventoryActions()
        {
            InventoryAct = new List<IinventoryAct>();

            var parent = InventoryParent as Coil;

            InventoryAct.Add(new SliderInventoryFactory(
                new[] { "Позиция", "" },
                (float v) => 
                {
                    InventorySettings.Transform.localPosition = new Vector3(v, InventorySettings.Transform.localPosition.y, InventorySettings.Transform.localPosition.z);

                    if(parent != null)
                    {
                        parent.CoilCoreValue *= (1+v/2);
                    }
                }, 
                new SliderSettings(InventorySettings.Transform.localPosition.x, 0, 0.11f))
            );
            return InventoryAct;
        }
    }
}


