﻿using UnityEngine;
using System.Linq;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class Coil : PickableInventoryBase, IElectricElmntBase
    {
        public Transform Point1, Point2;
        public float CoilCoreValue { get; set; }

        public Canvas _canvas;
        public GameObject CoilCoreControl;
        public GameObject MagnetControl;
        public Slider _controlCoinCore;

        public override void Initialize()
        {
            ElectricElmntId = gameObject.GetInstanceID();
            ElectricType = ElectricType.Resistor;            
            InitPlugs(GoPlus, GoMinus);

            CoilCoreValue = 1;            
        }
        
        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
        }

        public override void DisableSimulation()
        {
        }
        
        #region Electric part

        public GameObject GoPlus;
        public GameObject GoMinus;

        public int ElectricElmntId { get; set; }
        public ElectricType ElectricType { get; set; }
        public int ConsistentConnectionId { get; set; }

        public float Voltage { get; set; }
        public float Current { get; set; }

        [SerializeField]
        private float _resistance;
        public float Resistance
        {
            get
            {
                return _resistance;
            }
            set
            {
                _resistance = value;
            }
        }

        public Plug Plus { get; set; }
        public Plug Minus { get; set; }
        
        public void InitPlugs(GameObject goPlus, GameObject goMinus) 
        {
            Plus = goPlus.AddComponent<Plug>();
            Plus.Init(ElectricElmntId, "Coil", PlugType.Plus);

            Minus = goMinus.AddComponent<Plug>();
            Minus.Init(ElectricElmntId, "Coil", PlugType.Minus);            
        }

        public Plug GetTypePlug(PlugType type)
        {
            return type == PlugType.Plus ? Minus : Plus;
        }

        #endregion
    }
}


