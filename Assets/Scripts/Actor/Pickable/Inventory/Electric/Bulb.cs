﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class Bulb : PickableInventoryBase, IElectricElmntBase
    {     
        [SerializeField]
        private Light _light;

        private Material _matLight;
        private float _intensity;

        private void Update()
        {
            if (IsEnableSimulation)
            {
                _intensity = Mathf.Clamp(Current, 0, 1.1f);
                _light.intensity = _intensity;
                _matLight.SetColor("_EmissionColor", Color.white * Current * 9);
                RendererExtensions.UpdateGIMaterials(InventorySettings.MeshRenderers[0]);
                DynamicGI.UpdateEnvironment();
            }
        }

        public override void Initialize()
        {
            ElectricElmntId = gameObject.GetInstanceID();
            ElectricType = ElectricType.Bulb;            
            InitPlugs(GoPlus, GoMinus);

            _matLight = InventorySettings.MeshRenderers[0].materials[4];
            _matLight.SetColor("_EmissionColor", Color.black);
            RendererExtensions.UpdateGIMaterials(InventorySettings.MeshRenderers[0]);
            DynamicGI.UpdateEnvironment();
        }        

        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;

            _light.intensity = 0;
            _matLight.SetColor("_EmissionColor", Color.black);
            RendererExtensions.UpdateGIMaterials(InventorySettings.MeshRenderers[0]);
            DynamicGI.UpdateEnvironment();
        }

        #region Electric part

        public GameObject GoPlus;
        public GameObject GoMinus;

        public int ElectricElmntId { get; set; }
        public ElectricType ElectricType { get; set; }
        public int ConsistentConnectionId { get; set; }

        public float Voltage { get; set; }
        public float Current { get; set; }

        [SerializeField]
        private float _resistance;
        public float Resistance
        {
            get
            {
                return _resistance;
            }
            set
            {
                _resistance = value;
            }
        }

        public Plug Plus { get; set; }
        public Plug Minus { get; set; }

        public void InitPlugs(GameObject goPlus, GameObject goMinus)
        {
            Plus = goPlus.AddComponent<Plug>();
            Plus.Init(ElectricElmntId, "Bulb", PlugType.Plus);

            Minus = goMinus.AddComponent<Plug>();
            Minus.Init(ElectricElmntId, "Bulb", PlugType.Minus);
        }

        public Plug GetTypePlug(PlugType type)
        {
            return type == PlugType.Plus ? Minus : Plus;
        }

        #endregion
    }
}


