﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public interface IVoltageSource : IElectricElmntBase
    {
        event Action OnVoltageSource;
    }

    public class VoltageSource : PickableInventoryBase, IVoltageSource
    { 
        [SerializeField]
        private Text _txtVoltageBoard;
        private ObservedValue<float> OnChangeVoltage;

        public event Action OnVoltageSource;

        private void Update()
        {
            OnChangeVoltage.Value = Voltage;
        }

        public override void Initialize()
        {
            ElectricElmntId = gameObject.GetInstanceID();
            ElectricType = ElectricType.VoltageSource;
            Resistance = 0.5f;            
            InitPlugs(GoPlus, GoMinus);

            OnChangeVoltage = new ObservedValue<float>(Voltage);
            OnChangeVoltage.OnValueChanged += (float value) =>
            {
                _txtVoltageBoard.text = String.Format("{0} V", value.ToString("0.0"));
            };

            Voltage = 5;
        }

        public override void EnableSimulation()
        {
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(true);          
        }

        public override void DisableSimulation()
        {
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(false);           
        }


        public override List<IinventoryAct> GetInventoryActions()
        {
            InventoryAct = new List<IinventoryAct>();
            InventoryAct.Add(new SliderInventoryFactory(new[] { "Напряжение", "V" }, SetVoltage, new SliderSettings(Voltage, 0, 12)));
            return InventoryAct;
        }

        private void SetVoltage(float value)
        {
            Voltage = value;
            
            if (OnVoltageSource != null)
            {
                OnVoltageSource();
            }
        }

        #region Electric part

        public GameObject GoPlus;
        public GameObject GoMinus;

        public int ElectricElmntId { get; set; }
        public ElectricType ElectricType { get; set; }
        public int ConsistentConnectionId { get; set; }

        public float Voltage { get; set; }
        public float Current { get; set; }
        public float Resistance { get; set; }

        public Plug Plus { get; set; }
        public Plug Minus { get; set; }

        public void InitPlugs(GameObject goPlus, GameObject goMinus)
        {
            Plus = goPlus.AddComponent<Plug>();
            Plus.Init(ElectricElmntId, "VoltageSource", PlugType.Plus);

            Minus = goMinus.AddComponent<Plug>();
            Minus.Init(ElectricElmntId, "VoltageSource", PlugType.Minus);
        }

        public Plug GetTypePlug(PlugType type)
        {
            return type == PlugType.Plus ? Minus : Plus;
        }
        
        #endregion
    }
}