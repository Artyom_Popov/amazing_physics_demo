﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class Ammeter : PickableInventoryBase, IElectricElmntBase
    {
        [SerializeField]
        private Transform _arrow;
        
        private Quaternion _startRotation;        
        private float _lengthArrow = 0.1f;
        private float dampTime = .2f;
        private float velocity = 0;
        private float currentAngle;

        private void LateUpdate()
        {
            if (IsEnableSimulation)
            {
                var angle = (Current / _lengthArrow);                
                angle = Mathf.Clamp(angle, 0, 120);
                currentAngle = Mathf.SmoothDamp(currentAngle, angle, ref velocity, dampTime);                             
            }
            else
            {
                currentAngle = Mathf.SmoothDamp(currentAngle, 0, ref velocity, dampTime);                
            }

            _arrow.localRotation = _startRotation * Quaternion.Euler(0, currentAngle, 0);
        }

        public override void Initialize()
        {
            _startRotation = _arrow.localRotation;

            ElectricElmntId = gameObject.GetInstanceID();
            ElectricType = ElectricType.Ammeter;
            Resistance = 0.001f;         
            InitPlugs(GoPlus, GoMinus);
        }

        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;            
        }

        #region Electric part

        public GameObject GoPlus;
        public GameObject GoMinus;

        public int ElectricElmntId { get; set; }
        public ElectricType ElectricType { get; set; }
        public int ConsistentConnectionId { get; set; }

        public float Voltage { get; set; }
        public float Current { get; set; }
        public float Resistance { get; set; }

        public Plug Plus { get; set; }
        public Plug Minus { get; set; }

        public void InitPlugs(GameObject goPlus, GameObject goMinus)
        {
            Plus = goPlus.AddComponent<Plug>();
            Plus.Init(ElectricElmntId, "Ammeter", PlugType.Plus);

            Minus = goMinus.AddComponent<Plug>();
            Minus.Init(ElectricElmntId, "Ammeter", PlugType.Minus);
        }

        public Plug GetTypePlug(PlugType type)
        {
            return type == PlugType.Plus ? Minus : Plus;
        }

        #endregion
    }
}


