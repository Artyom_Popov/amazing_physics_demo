﻿using UnityEngine;
using System;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class MagnetFieldDevice : PickableInventoryBase, IElectricElmntBase
    {
        private string _sbsVarMagnetField = "fieldForm";
        private ProceduralMaterial _matFieldForm;
        
        public override void Initialize()
        {
            ElectricElmntId = gameObject.GetInstanceID();
            ElectricType = ElectricType.Resistor;            
            InitPlugs(GoPlus, GoMinus);
            Resistance = 0.5f;

            _matFieldForm = InventorySettings.MeshRenderers[0].materials[5] as ProceduralMaterial;
        }

        private void Update()
        {     
            if(IsEnableSimulation)
            {
                var value = Mathf.Clamp01(Voltage/12);
                value = Mathf.Max(value, _matFieldForm.GetProceduralFloat(_sbsVarMagnetField));
                _matFieldForm.SetProceduralFloat(_sbsVarMagnetField, value);
                _matFieldForm.RebuildTextures();
            }
        }

        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;

            _matFieldForm.SetProceduralFloat(_sbsVarMagnetField, 0);
            _matFieldForm.RebuildTextures();
        }

        #region Electric part

        public GameObject GoPlus;
        public GameObject GoMinus;

        public int ElectricElmntId { get; set; }
        public ElectricType ElectricType { get; set; }
        public int ConsistentConnectionId { get; set; }

        public float Resistance { get; set; }
        public float Voltage { get; set; }
        public float Current { get; set; }
        
        public Plug Plus { get; set; }
        public Plug Minus { get; set; }
        
        public void InitPlugs(GameObject goPlus, GameObject goMinus) 
        {
            Plus = goPlus.AddComponent<Plug>();
            Plus.Init(ElectricElmntId, "Coil", PlugType.Plus);

            Minus = goMinus.AddComponent<Plug>();
            Minus.Init(ElectricElmntId, "Coil", PlugType.Minus);            
        }

        public Plug GetTypePlug(PlugType type)
        {
            return type == PlugType.Plus ? Minus : Plus;
        }

        #endregion
    }
}


