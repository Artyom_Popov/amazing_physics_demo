﻿using UnityEngine;
using System;

namespace AmazyingPhysics
{
    public class Resistor : PickableInventoryBase, IElectricElmntBase
    {
        public override void Initialize()
        {
            ElectricElmntId = gameObject.GetInstanceID();
            ElectricType = ElectricType.Resistor;            
            InitPlugs(GoPlus, GoMinus);
        }

        #region Electric part

        public GameObject GoPlus;
        public GameObject GoMinus;

        public int ElectricElmntId { get; set; }
        public ElectricType ElectricType { get; set; }
        public int ConsistentConnectionId { get; set; }

        public float Voltage { get; set; }
        public float Current { get; set; }

        [SerializeField]
        private float _resistance;
        public float Resistance
        {
            get
            {
                return _resistance;
            }
            set
            {
                _resistance = value;
            }
        }

        public Plug Plus { get; set; }
        public Plug Minus { get; set; }
        
        public void InitPlugs(GameObject goPlus, GameObject goMinus) 
        {
            Plus = goPlus.AddComponent<Plug>();
            Plus.Init(ElectricElmntId, "Resistor", PlugType.Plus);

            Minus = goMinus.AddComponent<Plug>();
            Minus.Init(ElectricElmntId, "Resistor", PlugType.Minus);            
        }

        public Plug GetTypePlug(PlugType type)
        {
            return type == PlugType.Plus ? Minus : Plus;
        }

        #endregion
    }
}


