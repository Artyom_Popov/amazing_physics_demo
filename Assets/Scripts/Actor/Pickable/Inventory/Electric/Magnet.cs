﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public class Magnet : PickableInventoryBase
    {
        private Vector3 _startPos, _endPos;
        private MilliAmmetr _milliAmmetr;
        private bool _isInsideCoil;
        private bool _isPolar;
        private IElectricElmntBase _coil;
        
        public override void Initialize()
        {
            _startPos = new Vector3(0.11f, 0, 0);
            _endPos = Vector3.zero;
            
            //OnConnect2 += ConnectToCoil;            
        }

        public override List<IinventoryAct> GetInventoryActions()
        {
            InventoryAct = new List<IinventoryAct>();

            InventoryAct.Add(new ToggleInventoryFactory(new[] { "южный полюс", "северный полюс" }, ChangeDirMagnet, _isPolar));

            if (!_isInsideCoil)
            {
                InventoryAct.Add(new ButtonInventoryFactory("ввести медленно", () => MoveStates(0)));
                InventoryAct.Add(new ButtonInventoryFactory("ввести быстро", () => MoveStates(1)));
            }
            
            if(_isInsideCoil)
            {
                InventoryAct.Add(new ButtonInventoryFactory("вывести медленно", () => MoveStates(2)));
                InventoryAct.Add(new ButtonInventoryFactory("вывести быстро", () => MoveStates(3)));
            }

            print(_isInsideCoil);

            return InventoryAct;
        }
        
        public override void TunePosition(Vector3 position)
        {
            SetPositionToObjects(_startPos);
        }

        public override void MakeJoint()
        {
        }

        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(true);
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(false);
        }
        
        private void MoveStates(int indexState)
        {            
            if(_milliAmmetr == null)
            {
                var milliAmmetr = InventorySettings.CreatedInventory.GetInventory(56);

                if(milliAmmetr != null)
                {
                    _milliAmmetr = milliAmmetr.GetComponent<MilliAmmetr>();
                }
            }

            if (_coil == null)
            {
                var coil = InventorySettings.CreatedInventory.GetInventory(50);

                if (coil != null)
                {
                    _coil = coil as IElectricElmntBase;
                }
            }

            StopAllCoroutines();
            StartCoroutine(CorMoveStates(indexState));
        }

        private IEnumerator CorMoveStates(int indexState)
        {
            var slowSpeed = 0.005f;
            var fastSpeed = 0.025f;           
            _isInsideCoil = false;

            StopAllCoroutines();

            switch (indexState)
            {
                // ввести магнит медленно
                case 0:
                    StartCoroutine(CorSetMilliAmmetr(2, slowSpeed));
                    StartCoroutine(CorMoveState(_endPos, slowSpeed));                    
                    _isInsideCoil = true;
                    break;
                // ввести магнит быстро
                case 1:
                    StartCoroutine(CorSetMilliAmmetr(5, fastSpeed));
                    StartCoroutine(CorMoveState(_endPos, fastSpeed));                    
                    _isInsideCoil = true;
                    break;
                // вывести магнит медленно
                case 2:
                    StartCoroutine(CorSetMilliAmmetr(-2, slowSpeed));
                    StartCoroutine(CorMoveState(_startPos, slowSpeed));                    
                    break;
                // вывести магнит быстро
                case 3:
                    StartCoroutine(CorSetMilliAmmetr(-5, 0.05f));
                    StartCoroutine(CorMoveState(_startPos, fastSpeed));                    
                    break;
                case 4:
                    break;
            }

            yield return null;
        }

        private IEnumerator CorMoveState(Vector3 target, float step)
        {
            float increment = 0;

            while (increment < 1)
            {
                InventorySettings.Transform.localPosition = Vector3.Lerp(InventorySettings.Transform.localPosition, target, increment);
                increment += step;                
                yield return null;
            }            
        }

        private void ChangeDirMagnet(bool value)
        {
            var angle = value == true ? 180 : 0;
            _isPolar = !_isPolar;
            InventorySettings.Transform.localRotation = Quaternion.AngleAxis(angle, InventorySettings.Transform.up);            
        }

        private void MoveState(int stateId)
        {
            //Settings.Transform.localPosition = Vector3.Lerp(Settings.Transform.localPosition, Vector3.zero, );
        }
        
        private IEnumerator CorSetMilliAmmetr(float value, float step)
        {
            if (!IsEnableSimulation)
            {
                yield break;
            }

            if (_milliAmmetr == null && _coil == null)
            {
                yield break;
            }

            var elmnt1 = _milliAmmetr as IElectricElmntBase;
            var elmnt2 = _coil;
            bool checkConnection = ElectricCircuitCore.IsConnectElmntsEachOther(elmnt1, elmnt2);

            if(!checkConnection)
            {
                yield break;
            }

            float increment = 0;
            float current = 0;
            int sign = -1;

            if (_isPolar)
            {
                sign = 1;
            }
         
            while (!Mathf.Approximately(current, value))
            {
                current = Mathf.Lerp(current, value, increment);
                _milliAmmetr.SetAngle(sign * current, increment);
                increment += step * 2;
                yield return null;
            }


            increment = 0;

            while (!Mathf.Approximately(current, 0))
            {
                current = Mathf.Lerp(current, 0, increment);
                _milliAmmetr.SetAngle(sign * current, increment);
                increment += step * 0.5f;
                yield return null;
            }
        }        
    }
}


