﻿using UnityEngine;

namespace AmazyingPhysics
{
    public class MilliAmmetr : PickableInventoryBase, IElectricElmntBase
    {
        [SerializeField]
        private Transform _arrow;
        
        private Quaternion _startRotation;        
        private float _lengthArrow = 0.1f;             

        public override void Initialize()
        {
            ElectricElmntId = gameObject.GetInstanceID();
            ElectricType = ElectricType.Ammeter;
            Resistance = 0.001f;         
            InitPlugs(GoPlus, GoMinus);

            _startRotation = _arrow.localRotation;            
        }

        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;            
        }

        public void SetAngle(float current, float increment)
        {
            var angle = (current / (_lengthArrow));
            _arrow.localRotation = Quaternion.Lerp(_arrow.localRotation, _startRotation * Quaternion.Euler(0, angle, 0), increment);
        }

        #region Electric part

        public GameObject GoPlus;
        public GameObject GoMinus;

        public int ElectricElmntId { get; set; }
        public ElectricType ElectricType { get; set; }
        public int ConsistentConnectionId { get; set; }

        public float Voltage { get; set; }
        public float Current { get; set; }
        public float Resistance { get; set; }

        public Plug Plus { get; set; }
        public Plug Minus { get; set; }

        public void InitPlugs(GameObject goPlus, GameObject goMinus)
        {
            Plus = goPlus.AddComponent<Plug>();
            Plus.Init(ElectricElmntId, "Ampermeter", PlugType.Plus);

            Minus = goMinus.AddComponent<Plug>();
            Minus.Init(ElectricElmntId, "Ampermeter", PlugType.Minus);
        }

        public Plug GetTypePlug(PlugType type)
        {
            return type == PlugType.Plus ? Minus : Plus;
        }

        #endregion
    }
}


