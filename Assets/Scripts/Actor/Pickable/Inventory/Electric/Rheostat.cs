﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace AmazyingPhysics
{
    public interface IRheostat : IElectricElmntBase
    {
        event Action OnRheostat;
    }

    public class Rheostat : PickableInventoryBase, IRheostat
    {
        public override void Initialize()
        {
            ElectricElmntId = gameObject.GetInstanceID();
            ElectricType = ElectricType.Rheostat;            
            InitPlugs(GoPlus, GoMinus);

            Resistance = 4;
            SetResistance(Resistance);
        }

        public override void EnableSimulation()
        {
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(true);
        }

        public override void DisableSimulation()
        {
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(false);
        }

        public override List<IinventoryAct> GetInventoryActions()
        {
            InventoryAct = new List<IinventoryAct>();            
            InventoryAct.Add(new SliderInventoryFactory(
                new[] { "Сопротивление", "Ohm" }, 
                SetResistance, 
                new SliderSettings(Resistance, 1, 8))
            );
            return InventoryAct;
        }

        private void SetResistance(float value)
        {
            MovingPart.transform.localPosition = new Vector3((value - 4) / 71.4f, 0, 0);
            Resistance = value;
            
            if (OnRheostat != null)
            {
                OnRheostat();
            }
        }

        #region Electric part

        public GameObject GoPlus;
        public GameObject GoMinus;
        public GameObject MovingPart;
        
        public int ElectricElmntId { get; set; }
        public ElectricType ElectricType { get; set; }
        public int ConsistentConnectionId { get; set; }

        public float Voltage { get; set; }
        public float Current { get; set; }

        [SerializeField]
        private float _resistance;

        public event Action OnRheostat;

        public float Resistance
        {
            get
            {
                return _resistance;
            }
            set
            {
                _resistance = value;
            }
        }

        public Plug Plus { get; set; }
        public Plug Minus { get; set; }        
        
        public void InitPlugs(GameObject goPlus, GameObject goMinus) 
        {
            Plus = goPlus.AddComponent<Plug>();
            Plus.Init(ElectricElmntId, "Resistor", PlugType.Plus);

            Minus = goMinus.AddComponent<Plug>();
            Minus.Init(ElectricElmntId, "Resistor", PlugType.Minus);
        }

        public Plug GetTypePlug(PlugType type)
        {
            return type == PlugType.Plus ? Minus : Plus;
        }

        #endregion
    }
}


