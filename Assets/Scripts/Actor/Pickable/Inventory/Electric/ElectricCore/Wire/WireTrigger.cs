﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AmazyingPhysics
{
    public class WireTrigger : MonoBehaviour
    {
        public event Action OnUpWire;
        public event Action OnEnetrWire;
        public event Action OnExitWire;
        
        private void OnMouseUp()
        {
            if (OnUpWire != null)
                OnUpWire();
            
            //print("up");
        }

        private void OnMouseEnter()
        {
            if (OnEnetrWire != null)
                OnEnetrWire();
            
            //print("enter");
        }

        private void OnMouseExit()
        {
            if (OnExitWire != null)
                OnExitWire();

            //print("exit");
        }
    }   
}

