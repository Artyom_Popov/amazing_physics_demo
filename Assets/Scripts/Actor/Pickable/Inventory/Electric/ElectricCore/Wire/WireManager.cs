﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;

namespace AmazyingPhysics
{ 
    public class WireManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject _prefPlugLine;

        [SerializeField]
        private GameObject _prefWire;

        private PlugLine _plugLine;

        public Material _matNormal;
        public Material _matEnter;
        public Material _matSelect;

        private Plug _bufferStartPlug;

        public Dictionary<int, Wire> Wires;

        private int _counterId = 0;
        private int _selectId = -1;
        private int _enterId = -1;
        private bool _isEnter;

        public event Action<Plug[]> OnDeleteWire;

        private void Start()
        {
            var line = Instantiate(_prefPlugLine);
            line.transform.parent = transform;
            _plugLine = line.GetComponent<PlugLine>();

            Wires = new Dictionary<int, Wire>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Delete))
            {
                DeleteWire(_selectId);
            }

            if (Input.GetMouseButtonUp(0))
            {
                ResetSelectWire();
            }
        }

        public void CreatePlugLine(Plug startPlug, Transform raycastHitTransform)
        {
            _plugLine.CreatePlugLine(startPlug.transform.GetChild(0), raycastHitTransform);
            _bufferStartPlug = startPlug;
        }

        public void DisablePlugLine()
        {
            _plugLine.Disable();
        }

        public void CreateWire(Plug endPlug)
        {
            var start = _plugLine.GetStartPoint();
            _plugLine.Disable();            

            var id = GenerateId();

            var goWire = Instantiate(_prefWire);
            goWire.transform.parent = transform;

            var wire = goWire.GetComponent<Wire>();
            wire.Spline.SetMaterial(_matNormal);
            wire.CreateWire(start, endPlug.transform.GetChild(0));
            wire.Id = wire.Spline.Id = id;
            ++_bufferStartPlug.CountWireConnection;
            ++endPlug.CountWireConnection;
            wire.Plugs = new[] { _bufferStartPlug, endPlug };
            wire.Spline.OnUpWire += Spline_OnUpWire;
            wire.Spline.OnEnetrWire += Spline_OnEnetrWire;
            wire.Spline.OnExitWire += Spline_OnExitWire;
            wire.OnDeleteWire += DeleteWire;

            Wires.Add(id, wire);
        }

        private void Spline_OnEnetrWire(int wireId)
        {
            if(wireId != _selectId)
            {
                Wires[wireId].Spline.SetMaterial(_matEnter);
                _enterId = wireId;
                _isEnter = true;
            }           
        }

        private void Spline_OnExitWire(int wireId)
        {
            if (wireId != _selectId)
            {
                Wires[_enterId].Spline.SetMaterial(_matNormal);                
            }

            _isEnter = false;
        }

        private void Spline_OnUpWire(int wireId)
        {
            if (_selectId != -1)
            {
                Wires[_selectId].Spline.SetMaterial(_matNormal);                
            }

            Wires[wireId].Spline.SetMaterial(_matSelect);
            _selectId = wireId;
        }

        private void ResetSelectWire()
        {
            if(_selectId != -1 && !_isEnter)
            {
                Wires[_selectId].Spline.SetMaterial(_matNormal);
                _selectId = -1;
            }            
        }

        private void DeleteWire(int wireId)
        {
            if (OnDeleteWire != null)
            {
                Wire buffer;
                var isContains = Wires.TryGetValue(wireId, out buffer);
                if(isContains)
                {
                    OnDeleteWire(buffer.Plugs);
                    Destroy(buffer.gameObject);

                    _selectId = -1;
                }               
            }
        }
        
        private int GenerateId()
        {
            return _counterId++;
        }
    }
}
