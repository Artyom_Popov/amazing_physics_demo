﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AmazyingPhysics
{
    public class Spline : MonoBehaviour
    {
        public LineRenderer _line;
        public float _width = 0.002f;
        public int _numberOfPoints;
        public List<GameObject> _points;
        public Vector3[] _controlPoints;      

        public int Id { get; set; }
        public bool IsSelect { get; set; }
        public event Action<int> OnUpWire;
        public event Action<int> OnEnetrWire;
        public event Action<int> OnExitWire;

        private void Start()
        {
            _line.SetWidth(_width, _width);           
            _points = new List<GameObject>();
        }

        private void Update()
        {
            if (_line == null || _controlPoints == null || _controlPoints.Length < 2)
            {
                return;
            }
            
            // общее количество точек
            var count = _numberOfPoints * (_controlPoints.Length - 1);
        
            _line.SetVertexCount(count);
            GeneratePoints(count);

            Vector3 p0;
            Vector3 p1;
            Vector3 m0;
            Vector3 m1;

            for (int j = 0; j < _controlPoints.Length - 1; j++)
            {
                p0 = _controlPoints[j];
                p1 = _controlPoints[j + 1];
                if (j > 0)
                {
                    m0 = 0.5f * (_controlPoints[j + 1] - _controlPoints[j - 1]);
                }
                else
                {
                    m0 = _controlPoints[j + 1] - _controlPoints[j];
                }
                if (j < _controlPoints.Length - 2)
                {
                    m1 = 0.5f * (_controlPoints[j + 2] - _controlPoints[j]);
                }
                else
                {
                    m1 = _controlPoints[j + 1] - _controlPoints[j];
                }
            
                Vector3 position;
                float t;
                float pointStep = 1.0f / _numberOfPoints;

                if (j == _controlPoints.Length - 2)
                {
                    pointStep = 1.0f / (_numberOfPoints - 1.0f);                
                }
                for (int i = 0; i < _numberOfPoints; i++)
                {
                    t = i * pointStep;
                    position = (2.0f * t * t * t - 3.0f * t * t + 1.0f) * p0
                       + (t * t * t - 2.0f * t * t + t) * m0
                       + (-2.0f * t * t * t + 3.0f * t * t) * p1
                       + (t * t * t - t * t) * m1;

                    var index = i + j * _numberOfPoints;

                    if (position.y < 1)
                    {
                        position = new Vector3(position.x, 1.001f, position.z);
                    }

                    _line.SetPosition(index, position);
                    _points[index].transform.position = position;
                    _points[index].transform.LookAt(_points[index + 1].transform);
                }
            }
        }

        private void GeneratePoints(int count)
        {
            if(_points.Count < count)
            {
                var dif = count - _points.Count;

                for (int i = 0; i < dif+1; i++)
                {
                    var point = new GameObject("point");

                    var collider = point.AddComponent<CapsuleCollider>();
                    collider.center = new Vector3(0, 0, 0.0025f);
                    collider.height = 0.005f;
                    collider.direction = 2;
                    collider.radius = _width;
                    collider.isTrigger = true;
                    
                    var trigger = point.AddComponent<WireTrigger>();                    
                    trigger.OnUpWire += () => OnUpWire(Id);
                    trigger.OnEnetrWire += () => OnEnetrWire(Id);
                    trigger.OnExitWire += () => OnExitWire(Id);
                    point.transform.parent = transform;

                    _points.Add(point);
                }
            }
            else if(_points.Count > count)
            {
                for (int i = _points.Count - 1; i >= count; i--)
                {
                    _points[i].SetActive(false);
                }
            }
        }

        public void SetMaterial(Material material)
        {
            _line.material = material;
        }
    }
}
