﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;

namespace AmazyingPhysics
{
    public class Wire : MonoBehaviour
    {
        private NavMeshPath _path;        
        private Transform _start, _end;
        private List<Vector3> _points;

        public int Id { get; set; }
        public Spline Spline { get; private set; }
        public Plug[] Plugs { get; set; }
        public event Action<int> OnDeleteWire;

        private void Awake()
        {
            _points = new List<Vector3>();
            Spline = GetComponent<Spline>();            
        }

        private void Update()
        {
            if (_path != null)
            {
                if(_start == null | _end == null)
                {
                    if(OnDeleteWire != null)
                    {
                        OnDeleteWire(Id);
                    }
                    return;
                }

                NavMesh.CalculatePath(_start.GetChild(0).GetChild(0).position, _end.GetChild(0).GetChild(0).position, NavMesh.AllAreas, _path);

                _points.Clear();
                _points.Add(_start.position);
                _points.Add(_start.GetChild(0).position);

                var step = Vector3.Distance(_start.position, _start.GetChild(0).position);
                List<Vector3> v = new List<Vector3>();
                for (int i = 0; i < _path.corners.Length - 1; i++)
                {
                    var d = Vector3.Distance(_path.corners[i], _path.corners[i + 1]);
                    var c = (int)(d / step);
                    var p = (float)(1 / (float)c);
                   
                    for (int j = 0; j < c; j++)
                    {
                        var l = Vector3.Lerp(_path.corners[i], _path.corners[i + 1], (j + 1) * p);                        
                        v.Add(l);
                    }
                }

                _points.AddRange(v.ToArray());    
                _points.Add(_end.GetChild(0).position);
                _points.Add(_end.position);
                Spline._controlPoints = _points.ToArray();
            }    
        }

        public void CreateWire(Transform start, Transform end)
        {
            _start = start;
            _end = end;
            _path = new NavMeshPath();
        }                
    }
}
