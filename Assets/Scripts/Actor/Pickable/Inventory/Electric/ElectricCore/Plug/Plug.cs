﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AmazyingPhysics
{
    public enum PlugType
    {
        Default,
        Plus,
        Minus
    }

    public enum CurrentDirection
    {
        Default,
        In,
        Out
    }

    [Serializable]
    public class Plug : MonoBehaviour
    {
        public int ElectricElmntId;
        public string Name;
        public int ElectricNodeId;
        public PlugType PlugType;
        public CurrentDirection CurrentDirection;
        public int CountWireConnection;
        public List<Plug> ConnectionPlug;

        public void Init(int id, string name, PlugType plugType)
        {
            ElectricElmntId = id;
            Name = name;
            PlugType = plugType;
            CountWireConnection = 0;
            ConnectionPlug = new List<Plug>();
        }
    }

    public class PlugComparer : IEqualityComparer<Plug>
    {
        public bool Equals(Plug x, Plug y)
        {
            //Check whether the compared objects reference the same data.
            if (System.Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (System.Object.ReferenceEquals(x, null) || System.Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.ElectricElmntId == y.ElectricElmntId && x.Name == y.Name;
        }
        
        public int GetHashCode(Plug product)
        {
            //Check whether the object is null
            if (System.Object.ReferenceEquals(product, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashProductName = product.Name == null ? 0 : product.Name.GetHashCode();

            //Get hash code for the Code field.
            int hashProductCode = product.ElectricElmntId.GetHashCode();

            //Calculate the hash code for the product.
            return hashProductName ^ hashProductCode;
        }
    }
}

