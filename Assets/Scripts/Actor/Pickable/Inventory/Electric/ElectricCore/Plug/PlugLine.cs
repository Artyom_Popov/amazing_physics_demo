﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class PlugLine : MonoBehaviour
{
    public LineRenderer _line;
    private Transform _transStart;
    private Transform _transEnd;
   
    private void Start()
    {
        _line.positionCount = 2;
        _line.enabled = false;
    }

    void Update()
    {
        if (_transStart != null && _transEnd != null)
        {
            _line.SetPosition(0, _transStart.position);
            _line.SetPosition(1, _transEnd.position);
        }
    }

    public void CreatePlugLine(Transform start, Transform end)
    {
        _line.enabled = true;
        _transStart = start;
        _transEnd = end;
    }

    public Transform GetStartPoint()
    {
        return _transStart;
    }

    public void Disable()
    {
        _transStart = _transEnd = null;
        _line.SetPosition(0, Vector3.zero);
        _line.SetPosition(1, Vector3.zero);
        _line.enabled = false;
    }
}
