﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AmazyingPhysics
{
    public enum ElectricType
    {
        VoltageSource,
        CurrentSource,
        Voltmeter,
        Ammeter,
        Resistor,
        Rheostat,
        Bulb,
        CircuitBreaker
    }

    public interface IElectricElmntBase
    {
        int ElectricElmntId { get; set; }
        ElectricType ElectricType { get; set; }
        int ConsistentConnectionId { get; set; }

        float Current { get; set; }
        float Voltage { get; set; }
        float Resistance { get; set; }
        
        Plug Plus { get; set; }        
        Plug Minus { get; set; }
        
        void InitPlugs(GameObject goPlus, GameObject goMinus);

        Plug GetTypePlug(PlugType type);         
    }
}
