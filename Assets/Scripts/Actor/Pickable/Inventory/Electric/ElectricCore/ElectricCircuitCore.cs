﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AmazyingPhysics
{
    /// <summary>
    /// ядро управления соединениями в электрической цепи между элементами;
    /// реализован расчет сопротивления, напряжения, тока для каждого элемента цепи;
    /// </summary>
    public class ElectricCircuitCore
    {
        #region Field    
            
        public readonly WireManager _wireManager;
        private ConsistentCircuitPortion _startConsistentCircuitPortion; // начальный участок электрической цепи
        private int _parallelConnectionIdCounter = 0;
        private int _consistentConnectionIdCounter = 0;        
        private ElectricNodeManager _electricNodeManager;
        private PointerPlug _pointerPlug; // выбранная клема   
        private IElectricElmntBase _voltageSource; // источник напряжения      
        private float _current; // суммарный ток в цепи
        private int _restrictor; // ограничитель просчета параллельных участков цепи

        #endregion

        public List<IElectricElmntBase> CreatedElectricElmnts { get; set; } // контейнер всех элементов в сцене      

        public ElectricCircuitCore(WireManager wireManager)
        {
            CreatedElectricElmnts = new List<IElectricElmntBase>();                        
            _startConsistentCircuitPortion = new ConsistentCircuitPortion();
            _electricNodeManager = new ElectricNodeManager();
            _pointerPlug = new PointerPlug();
            _parallelConnectionIdCounter = 0;
            _consistentConnectionIdCounter = 0;

            _wireManager = wireManager;

            // подписка на удаление клемм из узлов при удалении провода
            _wireManager.OnDeleteWire += _electricNodeManager.Remove;
    }

        public void EnableSimulation()
        {
            _restrictor = 0;

            InitCircuit();
            CalcCircuit();
            //ShowPartsCircuit(_startConsistentCircuitPortion);
        }

        public void DisableSimulation()
        {
            CreatedElectricElmnts = new List<IElectricElmntBase>();
            _startConsistentCircuitPortion = new ConsistentCircuitPortion();            
            _parallelConnectionIdCounter = 0;
            _consistentConnectionIdCounter = 0;
        }

        /// <summary>
        /// получить ссылку на клемму
        /// </summary>
        /// <param name="plug">The plug.</param>
        /// <param name="raycastHitTransform">The raycast hit transform.</param>
        public void GetPlug(Plug plug, Transform raycastHitTransform)
        {
            _pointerPlug.Current = plug;

            if (_pointerPlug.Previous != null && _pointerPlug.Previous == _pointerPlug.Current
                || _pointerPlug.Previous != null && _pointerPlug.Previous.ElectricElmntId == _pointerPlug.Current.ElectricElmntId)
            {
                ResetSelectPlug();
                return;
            }

            if (_pointerPlug.IsFirstPlug)
            {
                _pointerPlug.IsFirstPlug = false;
                _pointerPlug.Previous = plug;

                _wireManager.CreatePlugLine(plug, raycastHitTransform);
            }
            else
            {               
                var value = _electricNodeManager.Add(_pointerPlug.Current, _pointerPlug.Previous);

                if(value)
                {
                    _wireManager.CreateWire(plug);
                }

                ResetSelectPlug();
            }
        }

        /// <summary>
        /// сбросить выделенные клеммы
        /// </summary>
        public void ResetSelectPlug()
        {
            _pointerPlug.Reset();
            _wireManager.DisablePlugLine();
        }
        
        /// <summary>
        /// инициализация всех элементов цепи
        /// </summary>
        private void InitCircuit()
        {
            // найти источник напряжения - с него начинается расчет цепи
            _voltageSource = CreatedElectricElmnts
                .Where(q => q.ElectricType == ElectricType.VoltageSource)
                .FirstOrDefault();

            if (_voltageSource == null)
                return;

            IVoltageSource iVoltageSource = _voltageSource as IVoltageSource;
            iVoltageSource.OnVoltageSource += CalcCircuit;

            // инициализация базового участка последовательного соединения
            _startConsistentCircuitPortion = InitializeConsistentPortion(GenerateParallelConnectionId());

            // вставить источник напряжения в цепь
            var voltageSourceNode = _electricNodeManager.GetElectricNode(_voltageSource.Minus.ElectricNodeId);
            if(voltageSourceNode != null)
            {
                var voltageSourcePlugs = voltageSourceNode.GetSolvePlugs(_voltageSource.Minus);
                CreateConsistentPortion(_startConsistentCircuitPortion, ref voltageSourcePlugs, _voltageSource.Plus.ElectricNodeId);
            }

            // создать замкнутый контур цепи, начиная с плюса источника напряжения
            var node = _electricNodeManager.GetElectricNode(_voltageSource.Minus.ElectricNodeId);
            var plugs = new[] { _voltageSource.Plus };
            CreateConsistentPortion(_startConsistentCircuitPortion, ref plugs, _voltageSource.Minus.ElectricNodeId);

            #region Rheostat

            var rheostats = CreatedElectricElmnts
                 .Where(q => q.ElectricType == ElectricType.Rheostat)
                 .ToArray();

            if (rheostats != null)
            {
                foreach (var rheostat in rheostats)
                {
                    if (rheostat != null)
                    {
                        IRheostat iRheostat = rheostat as IRheostat;
                        iRheostat.OnRheostat += CalcCircuit;
                    }
                }
            }

            #endregion

            #region CircuitBreaker

            var circuitBreakers = CreatedElectricElmnts
                 .Where(q => q.ElectricType == ElectricType.CircuitBreaker)
                 .ToArray();

            if(circuitBreakers != null)
            {
                foreach (var circuitBreaker in circuitBreakers)
                {
                    if (circuitBreaker != null)
                    {
                        ICircuitBreaker iCircuitBreaker = circuitBreaker as ICircuitBreaker;
                        iCircuitBreaker.OnCircuitBreaker += CalcCircuit;
                    }
                }
            }

            #endregion   
                  
        }

        /// <summary>
        /// создать последовательный участок цепи
        /// </summary>
        /// <param name="consistentCircuitPortion"></param>
        /// <param name="exceptPlugs"></param>
        /// <param name="electricNodeId"></param>
        private void CreateConsistentPortion(ConsistentCircuitPortion consistentCircuitPortion, ref Plug[] exceptPlugs, int electricNodeId)
        {
            Plug[] solvePlugs = null;
           
            for (int i = 0; i < 20; i++)
            {
                var node = _electricNodeManager.GetElectricNode(exceptPlugs.First().ElectricNodeId);
                
                if(node != null)
                {
                    var connectionType = node.CheckConnectionType(exceptPlugs, ref solvePlugs);

                    // нужно проверить участок цепи на целостность
                    
                    if (connectionType == EConnectionType.Consistent)
                    {
                        Plug outPlug;
                        var isConnectElmntInCircuit = AddElectricElmntInConsistentPortion(consistentCircuitPortion, solvePlugs.First(), out outPlug);

                        if (!isConnectElmntInCircuit)
                        {
                            return;
                        }

                        exceptPlugs = new[] { outPlug };
                    }
                    else if (connectionType == EConnectionType.Paralell)
                    {
                        CreateParallelPortion(ref consistentCircuitPortion, solvePlugs, ref exceptPlugs);
                    }

                    if (exceptPlugs.First().ElectricNodeId == electricNodeId)
                    {
                        Debug.Log("ПРЕРВАТЬ ЦЕПЬ!!!  " + electricNodeId);
                        return;
                    }
                }
                else
                {
                    Debug.Log("ОБРЫВ ЦЕПИ!!!");
                    return;
                }                
            }
        }      
     
        /// <summary>
        /// создать параллельный участок цепи
        /// </summary>
        /// <param name="consistentCircuitPortion"></param>
        /// <param name="inPlugs"></param>
        /// <param name="exceptPlugs"></param>
        private void CreateParallelPortion(ref ConsistentCircuitPortion consistentCircuitPortion, Plug[] inPlugs, ref Plug[] exceptPlugs)
        {
            int stopElectricNodeId = -1;
            var solvePlugs = CalcParallelPlugs(inPlugs, ref stopElectricNodeId);
            Debug.Log("STOP: " + stopElectricNodeId);

            List<Plug> buffer = new List<Plug>();
            var parallelConnectionId = GenerateParallelConnectionId();

            if (parallelConnectionId > 20)
                return;

            if (solvePlugs.Count > 1)
            {
                foreach (var plugs in solvePlugs)
                {
                    ConsistentCircuitPortion consistentCircuitPortionBuffer = InitializeConsistentPortion(parallelConnectionId);
                    consistentCircuitPortion.ParallelCircuitPortions.Add(consistentCircuitPortionBuffer);

                    Debug.Log("Main con id " + consistentCircuitPortion.ConsistentConnectionId + " Main par id" + consistentCircuitPortion.ParallelConnectionId);
                    Debug.Log("Second con id " + consistentCircuitPortionBuffer.ConsistentConnectionId + " Second par id" + consistentCircuitPortionBuffer.ParallelConnectionId);

                    var node = _electricNodeManager.GetElectricNode(plugs.First().ElectricNodeId);
                    var bufferExceptPlug = node.GetSolvePlugs(plugs);

                    CreateConsistentPortion(consistentCircuitPortionBuffer, ref bufferExceptPlug, stopElectricNodeId);
                    buffer.AddRange(bufferExceptPlug);                    
                }
            }
            else
            {
                foreach (var plug in solvePlugs.First())
                {
                    ConsistentCircuitPortion consistentCircuitPortionBuffer = InitializeConsistentPortion(parallelConnectionId);
                    consistentCircuitPortion.ParallelCircuitPortions.Add(consistentCircuitPortionBuffer);

                    Debug.Log("___Main con id " + consistentCircuitPortion.ConsistentConnectionId + " Main par id" + consistentCircuitPortion.ParallelConnectionId);
                    Debug.Log("____Second con id " + consistentCircuitPortionBuffer.ConsistentConnectionId + " Second par id" + consistentCircuitPortionBuffer.ParallelConnectionId);

                    var node = _electricNodeManager.GetElectricNode(plug.ElectricNodeId);
                    var bufferExceptPlug = node.GetSolvePlugs(plug);

                    CreateConsistentPortion(consistentCircuitPortionBuffer, ref bufferExceptPlug, stopElectricNodeId);
                    buffer.AddRange(bufferExceptPlug);                    
                }
            }

            exceptPlugs = buffer.Distinct(new PlugComparer()).ToArray();
            //exceptPlugs = buffer.ToArray();
        }

        /// <summary>
        /// инициализировать последовательный участок электрической цепи
        /// </summary>
        /// <param name="parallelConnectionId"></param>
        /// <returns></returns>
        private ConsistentCircuitPortion InitializeConsistentPortion(int parallelConnectionId)
        {
            var consistentCircuitPortion = new ConsistentCircuitPortion()
            {
                ConsistentConnectionId = GenerateConsistentConnectionId(),
                ParallelConnectionId = parallelConnectionId,
                ElectricElmnts = new List<IElectricElmntBase>()
            };

            return consistentCircuitPortion;
        }

        /// <summary>
        /// получить элемент цепи по клемме
        /// </summary>
        /// <param name="inPlug">клемма, по которой осуществлется поиск элемента цепи</param>
        /// <param name="outPlug">клемма - обратная первой клемме - клемма выхода</param>
        /// <returns></returns>
        private IElectricElmntBase GetElectricElmntByPlug(Plug inPlug, out Plug outPlug)
        {
            IElectricElmntBase electricElmntBase = CreatedElectricElmnts
                .Where(x => x.ElectricElmntId == inPlug.ElectricElmntId)
                .First();

            outPlug = inPlug.PlugType == PlugType.Plus ? electricElmntBase.Minus : electricElmntBase.Plus;

            inPlug.CurrentDirection = CurrentDirection.In;
            outPlug.CurrentDirection = CurrentDirection.Out;

            return electricElmntBase;
        }

        /// <summary>
        /// вставить элемент в последовательный участок электрической цепи
        /// </summary>
        /// <param name="consistentCircuitPortion"></param>
        /// <param name="inPlug"></param>
        /// <param name="outPlug"></param>
        private bool AddElectricElmntInConsistentPortion(ConsistentCircuitPortion consistentCircuitPortion, Plug inPlug, out Plug outPlug)
        {
            var electricElmnt = GetElectricElmntByPlug(inPlug, out outPlug);
            var isConnectElmntInCircuit = IsConnectElmntInCircuit(electricElmnt);

            if(isConnectElmntInCircuit)
            {
                consistentCircuitPortion.ElectricElmnts.Add(electricElmnt);
                //Debug.LogFormat("вставлен элемент : {0};", electricElmnt.ElectricElmntId);
                return true;
            }
            
            return false;
        }
        
        /// <summary>
        /// рассчитать группу клем в параллельном участке цепи для дальнейшего построения последовательных участков цепи  
        /// </summary>
        /// <param name="plugs"></param>
        /// <param name="stopElectricNodeId"></param>
        /// <returns></returns>
        private List<Plug[]> CalcParallelPlugs(Plug[] plugs, ref int stopElectricNodeId)
        {
            List<CalcPlug> groupPlugs = new List<CalcPlug>();
            List<Plug[]> exceptionPlugs = new List<Plug[]>();
            //List<int> stopElectricNodes = new List<int>();

            foreach (var plug in plugs)
            {
                CalcPlug calcPlug = new CalcPlug();
                calcPlug.InPlug = plug;
                calcPlug.OutPlug = GetEndElectricNodeIdInConsistentPortion(plug);
                groupPlugs.Add(calcPlug);
            }
            
            var query = groupPlugs
               .Where(d => d.OutPlug != null)
               .GroupBy(x => x.OutPlug.ElectricNodeId)
               .ToDictionary(k => k.Key, v => v.ToArray());

            Debug.LogFormat("количество групп в параллельном участке: {0}", query.Count);

            int stopElectricNodeId1 = -1;
            int stopElectricNodeId2 = -1;

            if (query.Count > 1)
            {
                //stopElectricNodes.AddRange(query.Select(k => k.Key).ToArray());

                foreach (var item in query.Values)
                {
                    exceptionPlugs.Add(item.Select(p => p.InPlug).ToArray());
                }

                var count = exceptionPlugs.Min(m => m.Length);
                var calcPlugs = exceptionPlugs.Where(c => c.Length == count).First();

                stopElectricNodeId1 = GetStopNodeElectricId(calcPlugs);                
                Debug.LogFormat("узел останова параллельного участка цепи: {0}", stopElectricNodeId1);
            }
            else
            {
                foreach (var item in query.Values)
                {
                    var node = _electricNodeManager.GetElectricNode(item.First().OutPlug.ElectricNodeId);
                    var plug = node.GetSolvePlugs(item.Select(p => p.OutPlug).ToArray());
                    stopElectricNodeId2 = plug.First().ElectricNodeId;                    
                    exceptionPlugs.Add(item.Select(p => p.InPlug).ToArray());
                }
                Debug.LogFormat("узел останова последовательного участка цепи: {0}", stopElectricNodeId2);
            }

            stopElectricNodeId = stopElectricNodeId1 != -1 ? stopElectricNodeId1 : stopElectricNodeId2;
            Debug.LogFormat("stopElectricNodeId: {0}", stopElectricNodeId);

            return exceptionPlugs;
        }
        
        private Plug GetEndElectricNodeIdInConsistentPortion(Plug startPlug)
        {
            Plug endPlug;
            GetElectricElmntByPlug(startPlug, out endPlug);

            var node = _electricNodeManager.GetElectricNode(endPlug.ElectricNodeId);

            if (node != null)
            {
                var plugs = node.GetSolvePlugs(endPlug);
                
                if (plugs.Length == 1)
                {
                    endPlug = GetEndElectricNodeIdInConsistentPortion(plugs.First());
                }

                return endPlug;
            }

            return null;
        }

        private Plug GetEndElectricNodeIdInConsistentPortion(Plug startPlug, ref int stopElectricElmntNodeId)
        {
            Plug endPlug;
            var elmnt = GetElectricElmntByPlug(startPlug, out endPlug);
            
            if (elmnt.ElectricType == ElectricType.VoltageSource)
            {
                Debug.Log("find VoltageSource" + elmnt.ElectricType);
                stopElectricElmntNodeId = startPlug.ElectricNodeId;
                return null;
            }

            var node = _electricNodeManager.GetElectricNode(endPlug.ElectricNodeId);

            if (node != null)
            {
                var plugs = node.GetSolvePlugs(endPlug);
                
                if (plugs.Length == 1)
                {
                    endPlug = GetEndElectricNodeIdInConsistentPortion(plugs.First(), ref stopElectricElmntNodeId);
                }

                return endPlug;
            }

            return null;
        }
                
        private int GetStopNodeElectricId(Plug[] plugs)
        {
            if (_restrictor > 20)
            {
                return -1;
            }                

            _restrictor++;


            List<Plug> calcPlugs = new List<Plug>();
            int stopElectricElmntNodeId = -1;

            foreach (var plug in plugs)
            {                
                var buffer = GetEndElectricNodeIdInConsistentPortion(plug, ref stopElectricElmntNodeId);

                if(stopElectricElmntNodeId != -1)
                {
                    Debug.LogFormat("узел останова: {0}", plug.ElectricNodeId);
                    return plug.ElectricNodeId;
                }
                
                calcPlugs.Add(buffer);
            }

            var arrElectricNodeId = calcPlugs.GroupBy(f => f.ElectricNodeId).Select(g => g.Key).ToArray();
            
            if (arrElectricNodeId.Length > 1)
            {
                return plugs.First().ElectricNodeId;
            }
            else
            {
                var node = _electricNodeManager.GetElectricNode(calcPlugs.First().ElectricNodeId);
                var solvePlugs = node.GetSolvePlugs(calcPlugs.Select(p => p).ToArray());               
                var stopNodeElectricId = GetStopNodeElectricId(solvePlugs);               
                return stopNodeElectricId;               
            }
        }
        
        /// <summary>
        /// расчитать сопротивление участка цепи
        /// </summary>
        /// <param name="consistentCircuitPortion"></param>
        /// <returns></returns>
        private float CalcResistanceCircuit(ConsistentCircuitPortion consistentCircuitPortion)
        {
            if (consistentCircuitPortion == null)
                return 0;

            float result = 0;
            float consistentResistense = 0;

            foreach (var element in consistentCircuitPortion.ElectricElmnts)
            {
                if (element.ElectricType == ElectricType.CircuitBreaker)
                {
                    ICircuitBreaker circuitBreaker = element as ICircuitBreaker;

                    if (circuitBreaker != null && !circuitBreaker.IsOn)
                    {
                        return 0;
                    }
                }
               
                else if (element.ElectricType != ElectricType.VoltageSource)
                {
                    if(!IsConnectElmntInCircuit(element))
                    {
                        return 0;
                    }

                    consistentResistense = consistentResistense + element.Resistance;

                    //Debug.Log("элемент: " + element.ElectricElmntId + " сопротивление: " + element.Resistance);
                }
            }

            result = consistentResistense;            

            if (consistentCircuitPortion.ParallelCircuitPortions.Count > 0)
            {
                Dictionary<int, List<float>> resistenses = new Dictionary<int, List<float>>();
               
                foreach (var consistentCircuit in consistentCircuitPortion.ParallelCircuitPortions)
                {
                    var resistensePortionCircuit = CalcResistanceCircuit(consistentCircuit);
                    
                    if(resistenses.ContainsKey(consistentCircuit.ParallelConnectionId))
                    {
                        resistenses[consistentCircuit.ParallelConnectionId].Add(resistensePortionCircuit);                        
                    }
                    else
                    {
                        var bufferResistence = new List<float>();                       
                        bufferResistence.Add(resistensePortionCircuit);
                        resistenses.Add(consistentCircuit.ParallelConnectionId, bufferResistence);
                    }                   
                }

                Dictionary<int, float> parallelResistence = new Dictionary<int, float>();

                foreach (var resistense in resistenses)
                {
                    float b = 0;

                    foreach (var r in resistense.Value)
                    {
                        if (r != 0)
                        {
                            b = b + 1 / r;                            
                        }
                    }

                    if (b != 0)
                    {
                        b = 1 / b;
                        parallelResistence.Add(resistense.Key, b);                
                    }                    
                }

                foreach (var consistentCircuit in consistentCircuitPortion.ParallelCircuitPortions)
                {
                    foreach (var resistense in parallelResistence)
                    {
                        if(resistense.Key == consistentCircuit.ParallelConnectionId)
                        {
                            consistentCircuit.ParallelResistance = resistense.Value;
                        }
                    }                    
                }

                result = result + parallelResistence.Select(s => s.Value).Sum();
            }

            consistentCircuitPortion.Resistance = result;

            //Debug.Log("id участка цепи: " + consistentCircuitPortion.ConsistentConnectionId + " сопротивление: " + result);

            return result;
        }

        /// <summary>
        /// расчитать ток и напряжение участка цепи
        /// </summary>
        /// <param name="consistentCircuitPortion"></param>
        /// <param name="voltage"></param>
        /// <param name="current"></param>
        private void CalcCurrentVoltageCircuit(ConsistentCircuitPortion consistentCircuitPortion, float current, float voltage)
        {
            if (consistentCircuitPortion == null)
                return;

            //Debug.LogFormat("id: {0}; ток: {1}; напряжение: {2}", consistentCircuitPortion.ConsistentConnectionId, current, voltage);
            
            foreach (var element in consistentCircuitPortion.ElectricElmnts)
            {
                if(element.ElectricType == ElectricType.Ammeter)
                {
                    element.Current = current;
                }               
                else if (element.ElectricType != ElectricType.VoltageSource)
                {
                    element.Current = current;
                    element.Voltage = element.Resistance * element.Current;
                    
                    voltage = voltage - element.Voltage;                    
                }

                //Debug.LogFormat("Элемент: {0}; Ток: {1}; Напряжение: {2}", element.ElectricElmntId, element.Current, element.Voltage);
            }

            consistentCircuitPortion.Current = current;
            consistentCircuitPortion.Voltage = consistentCircuitPortion.Resistance * consistentCircuitPortion.Current;

            if (consistentCircuitPortion.ParallelCircuitPortions.Count > 0)
            {
                foreach (var consistentCircuit in consistentCircuitPortion.ParallelCircuitPortions)
                {
                    if (consistentCircuit.Resistance != 0)
                    {
                        var bufferVoltage = current * consistentCircuit.ParallelResistance;
                        var bufferCurrent = bufferVoltage / consistentCircuit.Resistance;
                        CalcCurrentVoltageCircuit(consistentCircuit, bufferCurrent, bufferVoltage);

                        //Debug.Log("id consistent: " + consistentCircuit.ConsistentConnectionId + " id parallel: " + consistentCircuit.ParallelConnectionId);
                    }
                }
            }            
        }
        
        /// <summary>
        /// соединен ли данный элемент в электрическую цепь
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private bool IsConnectElmntInCircuit(IElectricElmntBase element)
        {
            return _electricNodeManager.IsContains(element.Plus) && _electricNodeManager.IsContains(element.Minus) ? true : false;
        }
        
        /// <summary>
        /// расчитать сопротивление, ток, напряжение для всех элементов цепи
        /// </summary>
        private void CalcCircuit()
        {
            if (_voltageSource == null)
                return;

            var resistense = CalcResistanceCircuit(_startConsistentCircuitPortion);            
            float current = 0;

            if(resistense != 0)
            {
                current = _voltageSource.Voltage / resistense;
            }

            _current = current;

            CalcCurrentVoltageCircuit(_startConsistentCircuitPortion, _current, _voltageSource.Voltage);

            Debug.LogFormat("общее сопротивление: {0}; суммарная сила тока: {1}; напряжение источника: {2};", resistense, current, _voltageSource.Voltage);
        }

        /// <summary>
        /// Determines whether [is connect elmnts each other] [the specified elmnt1].
        /// </summary>
        /// <param name="elmnt1">The elmnt1.</param>
        /// <param name="elmnt2">The elmnt2.</param>
        /// <returns>
        ///   <c>true</c> if [is connect elmnts each other] [the specified elmnt1]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsConnectElmntsEachOther(IElectricElmntBase elmnt1, IElectricElmntBase elmnt2)
        {
            var check1 = elmnt1.Plus.ElectricNodeId != 0 && elmnt1.Plus.ElectricNodeId == elmnt2.Plus.ElectricNodeId || elmnt1.Plus.ElectricNodeId != 0 && elmnt1.Plus.ElectricNodeId == elmnt2.Minus.ElectricNodeId ? true : false;
            var check2 = elmnt1.Minus.ElectricNodeId != 0 && elmnt1.Minus.ElectricNodeId == elmnt2.Plus.ElectricNodeId || elmnt1.Minus.ElectricNodeId != 0 && elmnt1.Minus.ElectricNodeId == elmnt2.Minus.ElectricNodeId ? true : false;

            if(check1 && check2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int GenerateConsistentConnectionId()
        {
            return _consistentConnectionIdCounter++;
        }

        private int GenerateParallelConnectionId()
        {
            return _parallelConnectionIdCounter++;
        }

        /// <summary>
        /// показать строение цепи со всеми элементами
        /// </summary>
        /// <param name="consistentCircuitPortion"></param>
        private void ShowPartsCircuit(ConsistentCircuitPortion consistentCircuitPortion)
        {
            foreach (var item in consistentCircuitPortion.ElectricElmnts)
            {
                Debug.Log("id con portion: " + consistentCircuitPortion.ConsistentConnectionId + "id par portion: " + consistentCircuitPortion.ParallelConnectionId + " elmnt ---------- " + item.ElectricElmntId);
            }

            foreach (var item in consistentCircuitPortion.ParallelCircuitPortions)
            {
                ShowPartsCircuit(item);
            }
        }
        
        /// <summary>
        /// описывает результат расчета входящей и выходящей клемм для расчета участка цепи
        /// </summary>
        public class CalcPlug
        {
            public Plug InPlug;
            public Plug OutPlug;
        }

        /// <summary>
        /// описывает действия для двух клемм, которые объединяют провод
        /// </summary>
        public class PointerPlug
        {
            public Plug Current { get; set; }
            public Plug Previous { get; set; }
            public bool IsFirstPlug { get; set; }

            public PointerPlug()
            {
                IsFirstPlug = true;
            }

            public void Reset()
            {
                IsFirstPlug = true;
                Current = Previous = null;
            }
        }
    }

    /// <summary>
    /// тип соединения в электрической цепи
    /// </summary>
    public enum EConnectionType
    {
        Default,
        Consistent,
        Paralell,
        Error
    }
    
    /// <summary>
    /// контейнер последовательного соединения замкнутого контура цепи
    /// </summary>
    public class ConsistentCircuitPortion
    {
        public int ConsistentConnectionId { get; set; }
        public int ParallelConnectionId { get; set; }
        public float Voltage { get; set; }
        public float Current { get; set; }
        public float Resistance { get; set; }
        public float ParallelResistance { get; set; } // общее сопротивление на параллельном участке цепи, в которое входит данное соединение
        public List<IElectricElmntBase> ElectricElmnts { get; set; }
        public List<ConsistentCircuitPortion> ParallelCircuitPortions { get; set; }

        public ConsistentCircuitPortion()
        {
            ElectricElmnts = new List<IElectricElmntBase>();
            ParallelCircuitPortions = new List<ConsistentCircuitPortion>();
        }
    }

    /// <summary>
    /// управляет узлами в электрической цепи
    /// </summary>
    public class ElectricNodeManager
    {
        private int _counterId;

        public Dictionary<int, ElectricNode> ElectricNodes { get; set; }

        public ElectricNodeManager()
        {
            ElectricNodes = new Dictionary<int, ElectricNode>();
            _counterId = 0;
        }

        public bool Add(Plug plugPrevious, Plug plugCurrent)
        {
            bool value = true;

            var isPrevious = IsContains(plugPrevious);
            var isCurrent = IsContains(plugCurrent);

            if(!isPrevious && !isCurrent)
            {
                // создать новый узел и записать данные клеммы
                ElectricNode electricNode = new ElectricNode();
                electricNode.Plugs.AddRange(new[] { plugPrevious, plugCurrent });
                var id = GenerateNodeId();
                plugPrevious.ElectricNodeId = plugCurrent.ElectricNodeId = id;
                ElectricNodes.Add(id, electricNode);
            }
            else if(isPrevious && !isCurrent)
            {
                // найти id узла предыдущей клеммы и записать в узел текущую клемму
                var electricNode = ElectricNodes[plugPrevious.ElectricNodeId];
                plugCurrent.ElectricNodeId = plugPrevious.ElectricNodeId;
                electricNode.Plugs.Add(plugCurrent);
            }
            else if (!isPrevious && isCurrent)
            {
                // найти id узла текущей клеммы и записать в узел предыдущую клемму
                var electricNode = ElectricNodes[plugCurrent.ElectricNodeId];
                plugPrevious.ElectricNodeId = plugCurrent.ElectricNodeId;
                electricNode.Plugs.Add(plugPrevious);
            }
            else
            {
                // здесь нужно объединить узлы, т.к. можно объединить независимо по две клеммы, т.е. два узла,
                // а затем их возможно нужно будет объединить и здесь нужно слить два созданных узла в один
                return false;
            }

            return value;    
        }

        public void Remove(int electricNodeId)
        {
            ElectricNode electricNode;
            var isContains = ElectricNodes.TryGetValue(electricNodeId, out electricNode);
            if (isContains)
            {
                ElectricNodes.Remove(electricNodeId);
            }
            else
            {
                Debug.LogWarning("узла с индексом " + electricNodeId + " не существует");                
            }
        }

        public void Remove(Plug[] plugs)
        {
            foreach (var plug in plugs)
            {
                var node = GetElectricNode(plug.ElectricNodeId);

                if(node != null)
                {
                    --plug.CountWireConnection;

                    if(plug.CountWireConnection == 0)
                    {
                        node.Plugs.Remove(plug);
                        //Debug.LogFormat("удалить клемму: {0} - {1}", plug.ElectricElmntId, plug.PlugType);

                        if (node.Plugs.Count <= 0)
                        {
                            ElectricNodes.Remove(plug.ElectricNodeId);
                            //Debug.LogFormat("удалить узел: {0}", plug.ElectricNodeId);
                        }
                    }                                     
                }                
            }
        }

        public ElectricNode GetElectricNode(int electricNodeId)
        {
            ElectricNode electricNode;
            var isContains = ElectricNodes.TryGetValue(electricNodeId, out electricNode);
            if (isContains)
            {
                return electricNode;
            }
            else
            {
                Debug.LogWarning("узла с индексом " + electricNodeId + " не существует");
                return null;
            }           
        }

        /// <summary>
        /// Содержится ли клемма в узле
        /// </summary>
        /// <param name="plug">The plug.</param>
        /// <returns>
        ///   <c>true</c> if the specified plug is contains; otherwise, <c>false</c>.
        /// </returns>
        public bool IsContains(Plug plug)
        {
            var query = ElectricNodes
                .Where(x => x.Value.Plugs.Contains(plug))
                .Select(s => s.Value)
                .FirstOrDefault();

            if(query == null)
            {
                //Debug.LogFormat("клемма {0}-{1} не подсединена к узлу: {2}", plug.ElectricElmntId, plug.PlugType, plug.ElectricNodeId);
                return false;
            }
            {
                //Debug.LogFormat("клемма {0} подсединена к узлу: {2}", plug.ElectricElmntId, plug.PlugType, plug.ElectricNodeId);
                return true;
            }       
        }
        
        private int GenerateNodeId()
        {
            return ++_counterId;
        }
    }

    /// <summary>
    /// описывает узел в электрической цепи
    /// </summary>
    public class ElectricNode
    {
        public List<Plug> Plugs { get; set; }
        
        public ElectricNode()
        {
            Plugs = new List<Plug>();
        }
        
        public EConnectionType CheckConnectionType(Plug[] exceptPlugs, ref Plug[] solvePlugs)
        {
            var query = Plugs
                .Where(x => !exceptPlugs.Contains(x))
                .ToArray();

            if(query == null)
            {
                return EConnectionType.Error;
            }

            solvePlugs = query;

            //foreach (var plug in exceptPlugs)
            //{
            //    Debug.LogFormat("exceptPlugs : id elmnt - {0}; type plug - {1}", plug.ElectricElmntId, plug.PlugType);
            //}

            //foreach (var plug in solvePlugs)
            //{
            //    Debug.LogFormat("solvePlugs : id elmnt - {0}; type plug - {1}", plug.ElectricElmntId, plug.PlugType);
            //}

            if (query.Length > 1)
            {
                return EConnectionType.Paralell;
            }
            else
            {
                return EConnectionType.Consistent;
            }
        }

        public Plug GetSolvePlug(Plug exceptPlug)
        {
            var plug = Plugs
                .Where(p => p != exceptPlug)
                .First();

            return plug;
        }
        
        public Plug[] GetSolvePlugs(Plug exceptPlug)
        {
            var query = Plugs
                   .Where(p => p != exceptPlug)
                   .ToArray();

            return query;
        }

        public Plug[] GetSolvePlugs(Plug[] exceptPlugs)
        {
            var query = Plugs
                 .Where(x => !exceptPlugs.Contains(x))
                 .ToArray();

            return query;
        }
    }
}
