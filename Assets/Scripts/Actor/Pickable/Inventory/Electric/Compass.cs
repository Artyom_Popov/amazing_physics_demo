﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

namespace AmazyingPhysics
{
    public class Compass : PickableInventoryBase
    {
        #region Field

        [SerializeField]
        private Transform Arrow;

        [SerializeField]
        private CalcCircle _calcCircle;

        private Coil _coil;
        private ObservedValue<float> OnCurrent;
        private bool _isCalc;
        private Transform _point1;
        private Transform _point2;
        private Transform _point3;
        private Vector3 _worldDirection;
        private Vector3 _centreCircle;

        private float _targetAngle;
        private float _calcAngle;
        private bool _isSwapAngleDir;        
        private float _minimum;
        private float _maximum;
        private float _step = 0;
        private float _lerpTime = 0;
        private float _counter = 0;

        private Quaternion _targetRot;
        private Quaternion _worldRot;
        private Quaternion _delta = Quaternion.Euler(0, 0, 0);

        #endregion

        private void Update()
        {
            CalcSideCircleRelativeCompassPos();
        }

        private void LateUpdate()
        {
            if (IsEnableSimulation && _coil != null)
            {
                // если в зоне действия магнитного поля
                if (_coil.Current != 0)
                {
                    _calcCircle.SetPoints(_point1.position, _point2.position, _point3.position); // рассчитать окружность, описывающую магнитное поле
                    _centreCircle = _calcCircle.Centre; // получить центр окружности магнитного поля
                    _step = (_coil.CoilCoreValue * _coil.Current * .04f) / DistanceBetweenCoilAndCompass(); // устанавить силу затухания колебания стрелки на основе силы тока в катушке                                       

                    // рассчитать угол поворота между центром окружности и компассом
                    _targetRot = Quaternion.LookRotation(new Vector3(_centreCircle.x, 1, _centreCircle.z) - new Vector3(_point3.position.x, 1, _point3.position.z));
                    Debug.DrawRay(_point3.position, _targetRot * Vector3.right, Color.green);
                    
                    //блок описывает плавное затухание стрелки компасса при подападании компасса в магнитное поле
                    if (_counter > 0)
                    {
                        _calcAngle = Mathf.LerpAngle(_minimum, _maximum, _lerpTime);

                        if (_lerpTime > 1.0f)
                        {
                            float temp = _maximum;
                            _maximum = _minimum;
                            _minimum = temp;

                            if (_minimum > _maximum)
                            {
                                _maximum += _step;
                                _minimum -= _step;
                            }
                            else
                            {
                                _maximum -= _step;
                                _minimum += _step;

                            }

                            _counter -= _step;
                            _lerpTime = 0.0f;
                        }

                        _lerpTime += _step * Time.deltaTime;

                        Arrow.rotation = Quaternion.Lerp(Arrow.rotation, _targetRot * Quaternion.Euler(new Vector3(0, _calcAngle, 0)) * _delta, _lerpTime);
                    }
                    else
                    {
                        Arrow.rotation = _targetRot * _delta;
                    }
                }
                // если не в зоне магнитного поля
                else
                {
                    if (_counter > 0)
                    {
                        _calcAngle = Mathf.LerpAngle(_minimum, _maximum, _lerpTime);

                        if (_lerpTime > 1.0f)
                        {
                            float temp = _maximum;
                            _maximum = _minimum;
                            _minimum = temp;

                            if (_minimum > _maximum)
                            {
                                _maximum += _step;
                                _minimum -= _step;
                            }
                            else
                            {
                                _maximum -= _step;
                                _minimum += _step;
                            }

                            _counter -= _step;
                            _lerpTime = 0.0f;
                        }

                        _lerpTime += _step * Time.deltaTime;
                        
                        Arrow.rotation = Quaternion.Lerp(Arrow.rotation, _worldRot * Quaternion.Euler(new Vector3(0, _calcAngle, 0)) * _delta, _lerpTime);
                    }
                }

                OnCurrent.Value = _coil.Current;
            }
            else
            {
                if (_counter > 0)
                {
                    _calcAngle = Mathf.LerpAngle(_minimum, _maximum, _lerpTime);

                    if (_lerpTime > 1.0f)
                    {
                        float temp = _maximum;
                        _maximum = _minimum;
                        _minimum = temp;

                        if (_minimum > _maximum)
                        {
                            _maximum += _step;
                            _minimum -= _step;
                        }
                        else
                        {
                            _maximum -= _step;
                            _minimum += _step;
                        }

                        _counter -= _step;
                        _lerpTime = 0.0f;
                    }

                    _lerpTime += _step * Time.deltaTime;
                    
                    Arrow.rotation = Quaternion.Lerp(Arrow.rotation, _worldRot * Quaternion.Euler(new Vector3(0, _calcAngle, 0)), _lerpTime);
                }
            }            
        }
        
        public override void Initialize()
        {
            _point3 = transform;

            OnCurrent = new ObservedValue<float>(0);
            OnCurrent.OnValueChanged += ListenCurrent;

            _worldDirection = new Vector3(0, 162, 0);
            _worldRot = Quaternion.Euler(_worldDirection);            
        }

        public override void EnableSimulation()
        {
            var coil = InventorySettings.CreatedInventory.GetInventory(50);
            _coil = coil.GetComponent<Coil>();
            _point1 = _coil.Point1;
            _point2 = _coil.Point2;

            _maximum = _counter = 40;
            _minimum = -40;

            _isCalc = true;

            IsEnableSimulation = true;
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;

            _coil = null;

            _counter = 0;

            _maximum = _counter = 50;
            _minimum = -50;
            _step = 0.75f;
        }

        /// <summary>
        /// Calculates the side circle relative compass position.
        /// </summary>
        private void CalcSideCircleRelativeCompassPos()
        {
            if (_point1 == null)
                return;

            var d1 = _point1.right;
            var d2 = (new Vector3(_point1.position.x, 1, _point1.position.z) - new Vector3(_point3.position.x, 1, _point3.position.z)).normalized;

            var side = AngleDir(d1, d2, Vector3.up);

            Debug.DrawRay(_point1.position, d1, Color.red);
            Debug.DrawRay(_point1.position, d2, Color.blue);

            if (side == 1)
            {               
                if (_coil != null && _coil.Plus.CurrentDirection == CurrentDirection.In)
                {
                    _delta = Quaternion.AngleAxis(180, Arrow.transform.up);
                }
                else
                {
                    _delta = Quaternion.AngleAxis(0, Arrow.transform.up);
                }
            }
            else if (side == -1)
            {                
                if (_coil != null && _coil.Plus.CurrentDirection == CurrentDirection.In)
                {
                    _delta = Quaternion.AngleAxis(0, Arrow.transform.up);
                }
                else
                {
                    _delta = Quaternion.AngleAxis(180, Arrow.transform.up);
                }
            }
        }
        
        private float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
        {
            Vector3 right = Vector3.Cross(up, fwd);
            float dir = Vector3.Dot(right, targetDir);

            if (dir > 0f)
            {
                return 1f;
            }
            else if (dir < 0f)
            {
                return -1f;
            }
            else
            {
                return 0f;
            }
        }

        private void ListenCurrent(float current)
        {
            if(current == 0)
            {
                _maximum = _counter = 10;
                _minimum = -10;
                _step = 1.5f;
            }
        }

        private float DistanceBetweenCoilAndCompass()
        {
            if(_coil != null)
            {
                return Vector3.Distance(_coil.transform.position, transform.position);
            }

            return 0;
        }
    }
}