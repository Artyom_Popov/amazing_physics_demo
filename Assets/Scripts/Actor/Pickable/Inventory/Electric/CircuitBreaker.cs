﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;

namespace AmazyingPhysics
{
    public interface ICircuitBreaker : IElectricElmntBase
    {
        bool IsOn { get; }
        event Action OnCircuitBreaker;
    }

    public class CircuitBreaker : PickableInventoryBase, ICircuitBreaker
    {
        private float _startRot;
        private float _speed = 100;
        //private bool _isState;
        
        public override void Initialize()
        {
            ElectricElmntId = InventorySettings.Id;
            ElectricType = ElectricType.CircuitBreaker;            
            InitPlugs(GoPlus, GoMinus);
            Resistance = 0.001f;   
            
            _startRot = MovingPart.transform.localEulerAngles.z;
        }

        public override List<IinventoryAct> GetInventoryActions()
        {
            InventoryAct = new List<IinventoryAct>();
            InventoryAct.Add(new ToggleInventoryFactory(
                new[] {"включить", "отключить" },
                (bool v) =>
                {
                    StopAllCoroutines();
                    StartCoroutine(CorSetState(v));
                },
                IsOn)
            );

            return InventoryAct;
        }
        
        public override void EnableSimulation()
        {
            IsEnableSimulation = true;
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(true);
        }

        public override void DisableSimulation()
        {
            IsEnableSimulation = false;
            InventorySettings.InventoryActionsSimModePanel.ActionsMenu.SetActive(false);
        }

        private IEnumerator CorSetState(bool value)
        {
            IsOn = !IsOn;
            float dampTime = .2f;
            float velocity = -1;
            float rotZ = MovingPart.transform.localEulerAngles.z;            
            float target = value ? 0 : _startRot;
            while ((int)velocity != 0)
            {
                rotZ = Mathf.SmoothDamp(rotZ, target, ref velocity, dampTime);
                MovingPart.transform.localRotation = Quaternion.AngleAxis(rotZ, MovingPart.transform.forward);               
                yield return null;
            }

            if (OnCircuitBreaker != null)
            {
                OnCircuitBreaker();               
            }            
        }

        #region Electric part

        [SerializeField]
        private float _resistance;

        public GameObject GoPlus;
        public GameObject GoMinus;
        public GameObject MovingPart;

        public int ElectricElmntId { get; set; }
        public ElectricType ElectricType { get; set; }
        public int ConsistentConnectionId { get; set; }

        public float Voltage { get; set; }
        public float Current { get; set; }

        public event Action OnCircuitBreaker;

        public float Resistance
        {
            get
            {
                return _resistance;
            }
            set
            {
                _resistance = value;
            }
        }

        public Plug Plus { get; set; }
        public Plug Minus { get; set; }

        public bool IsOn { get; private set; }
      
        public void InitPlugs(GameObject goPlus, GameObject goMinus) 
        {
            Plus = goPlus.AddComponent<Plug>();
            Plus.Init(ElectricElmntId, "Resistor", PlugType.Plus);

            Minus = goMinus.AddComponent<Plug>();
            Minus.Init(ElectricElmntId, "Resistor", PlugType.Minus);
        }

        public Plug GetTypePlug(PlugType type)
        {
            return type == PlugType.Plus ? Minus : Plus;
        }

        #endregion
    }
}


