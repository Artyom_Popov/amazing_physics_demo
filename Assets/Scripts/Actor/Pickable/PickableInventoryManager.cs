﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AmazyingPhysics
{
    public enum EPickableState
    {
        Default,
        MoveOnTable,
        MoveOnHideAreaTable,
        StayOnTable,
        DeleteCreatedInventory,
        TuneConnection,
        Connect,
        Transform,
        Property,
        Erase,
        Deselect,
        Gizmo
    }

    public class PickableInventoryManager : MonoBehaviour
    {
        #region Fields
        
        private string _pickableTag =   "Pickable";
        private string _tableTag =      "Table";
        private string _hideAreaTable = "HideAreaTable";
        private string _plugTag =       "Plug"; 
        private int _pointerInventoryId;
        private bool _IsMoveItem;
        private bool _isStayOnTableItem;        
        private bool _isConnectedItem;
        private bool _isSelectItem;
        private bool _isFocus;
        private float _timeFocus = 0.1f;
        private float _rayLength;
        private float _rayLengthMax = 3;
        private int _layerGizmo = 1 << 12;
        private ObservedValue<EPickableState> _observedPickableState;        
        private GameObject _goHitPoint;
        private SimulationManager _simulationManager;
        private CameraManager _cameraManager;
        private Gizmo _gizmo;
        private MouseHoverState _mouseHoverState;
        private Material _matMoveOnTable;
        private Material _matConnectToInventory;

        #endregion

        /// <summary>
        /// управляет созданными предметами
        /// </summary>
        /// <value>
        /// The inventory creator.
        /// </value>
        public InventoryCreator InventoryCreator { get; private set; }

        /// <summary>
        /// управляет выделенными предметами
        /// </summary>
        /// <value>
        /// The inventory pointer.
        /// </value>
        public InventoryPointer InventoryPointer { get; private set; }

        public event Action<PickableInventoryBase> OnSelectInventory;
        public event Action OnDeselectInventory;
        public event Action<Plug, Transform> OnSelectPlug;
        public event Action OnMissPlug;

        public void Constructor(SimulationManager simulationManager, CameraManager cameraManager, Gizmo gizmo, MouseHoverState mouseHoverState)
        {
            _simulationManager = simulationManager;
            _cameraManager = cameraManager;
            _gizmo = gizmo;
            _mouseHoverState = mouseHoverState;

            // подписание на отключение гизмо при включении симуляции
            _simulationManager.OnActiveSimulation += (bool value) =>
            {
                if(value)
                {
                    _gizmo.SetTarget(null);
                }
            };

            _gizmo.OnDelete += DeleteInventoryPointer;
        }

        private void Start()
        {
            InventoryCreator = gameObject.AddComponent<InventoryCreator>();
            InventoryCreator.OnCreate += () => DeleteCreatedInventory();
            InventoryPointer = new InventoryPointer();

            _observedPickableState = new ObservedValue<EPickableState>(EPickableState.Default);
            _observedPickableState.OnValueChanged += PickableState;

            _matMoveOnTable = Resources.Load<Material>("Materials/mtl_MoveOnTable") as Material;
            _matConnectToInventory = Resources.Load<Material>("Materials/mtl_ConnectToInventory") as Material;
            
            InitHitPoint();
        }
        
        private void Update()
        {
            bool isPointerUI = EventSystem.current.IsPointerOverGameObject();

            if (isPointerUI)
                return;
            
            Ray ray = _gizmo.CameraRay = _cameraManager.ExtendCamera.CustomCamera.ScreenPointToRay(Input.mousePosition);

            RaycastHit gizmoHit;
            bool isCastGizmo = Physics.Raycast(ray, out gizmoHit, Mathf.Infinity, _layerGizmo);

            if(isCastGizmo)
            {
                if(Input.GetMouseButtonDown(0))
                {
                    _gizmo.GizmoHit = gizmoHit;
                    _gizmo.IsStartMoveGizmo = true;                    
                }
                SetPickableState(EPickableState.Gizmo);
                return;
            }

            SetPickableState(EPickableState.Default);

            RaycastHit hit;
            bool isCast = Physics.Raycast(ray, out hit);
            
            // отрисовать точку пересечения указателя мыши с поверхностью объекта
            DrawHitPoint(hit);
            
            if (isCast)
            {
                if(hit.collider.tag == _plugTag)
                {
                    if (!_simulationManager.IsEnableSimulation)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            if (OnSelectPlug != null)
                            {
                                var plug = hit.collider.GetComponent<Plug>();

                                if (plug != null)
                                {
                                    OnSelectPlug(plug, _goHitPoint.transform);
                                }
                            }
                        }
                    }
                }
                else if (hit.transform.tag == _hideAreaTable)
                {
                    // двигать предмет по поверхности стола
                    MoveOnHideAreaTable(hit.point);                    
                }
                else if (hit.transform.tag == _tableTag)
                {
                    // двигать предмет по поверхности стола
                    MoveOnTable(hit.point);

                    if (Input.GetMouseButtonDown(0) && !_simulationManager.IsEnableSimulation)
                    {
                        // поставить предмет на стол
                        StayOnTable();

                        DeselectInventoryPointer();

                        if (!_simulationManager.IsEnableSimulation)
                        {
                            MissPlug();
                        }
                    }                                  
                }
                else if (hit.transform.tag == _pickableTag)
                {
                    if(!_simulationManager.IsEnableSimulation)
                    {
                        if (InventoryCreator.CurrentInventory == null)
                        {
                            if (Input.GetMouseButtonDown(0))
                            {
                                SelectInventoryPointer(hit);
                            }
                        }
                        else
                        {
                            InventoryConnection(hit);
                        }

                        if (Input.GetMouseButtonDown(0))
                        {
                            MissPlug();
                        }
                    }                                  
                }
                else
                {
                    // если предмет не присоединен то отключить его отображение
                    HideCreatedInventory();

                    if (Input.GetMouseButtonDown(0) && !_simulationManager.IsEnableSimulation)
                    {
                        MissPlug();
                        _gizmo.SetTarget(null);
                        DeselectInventoryPointer();
                    }
                }
            }   
            else
            {
                // если предмет не присоединен то отключить его отображение
                HideCreatedInventory();

               
            }

            if (Input.GetKeyUp(KeyCode.Escape))
            {
                DeleteCreatedInventory();
            }
        }

        /// <summary>
        /// установить текущее состояние менеджера
        /// </summary>
        /// <param name="state">The state.</param>
        private void SetPickableState(EPickableState state)
        {
            _observedPickableState.Value = state;
            
        }

        /// <summary>
        /// описание состояний менеджера
        /// </summary>
        /// <param name="pickableState">State of the pickable.</param>
        private void PickableState(EPickableState pickableState)
        {
            switch (pickableState)
            {
                case EPickableState.Default:

                    if (/*InventoryPointer.PointerItemIsNull() && */InventoryCreator.CurrentInventory == null)
                    {
                        _mouseHoverState.SetCrosshair(EItemStage.Default);
                        print(pickableState);
                    }                    

                    break;
                case EPickableState.MoveOnTable:       
                    
                    if(InventoryCreator.CurrentInventory.InventorySettings.CanStayOnTable)
                    {
                        InventoryCreator.CurrentInventory.SetMaterial(_matConnectToInventory);
                    }
                    else
                    {
                        InventoryCreator.CurrentInventory.SetMaterial(_matMoveOnTable);
                    }                    
                    foreach (var child in InventoryCreator.CurrentInventory.InventoryChilds)
                    {
                        child.SetMaterial(_matMoveOnTable);
                    }
                    InventoryCreator.CurrentInventory.InventorySettings.Transform.parent = DynamicRoot.Instance.DynamicRootTransform;
                    ClearLastPointerInventoryId();
                    _gizmo.SetTarget(null);
                    _mouseHoverState.SetCrosshair(EItemStage.MoveOnTable);

                    break;
                case EPickableState.MoveOnHideAreaTable:

                    InventoryCreator.CurrentInventory.SetMaterial(_matMoveOnTable);
                    InventoryCreator.CurrentInventory.InventorySettings.Transform.parent = DynamicRoot.Instance.DynamicRootTransform;
                    ClearLastPointerInventoryId();
                    _mouseHoverState.SetCrosshair(EItemStage.MoveOnAbstractArea);

                    break;
                case EPickableState.StayOnTable:

                    InventoryCreator.CurrentInventory.SetActiveColliders(true);                    
                    InventoryCreator.CurrentInventory.SetSourceMaterials();                    
                    foreach (var child in InventoryCreator.CurrentInventory.InventoryChilds)
                    {
                        child.SetActiveColliders(true);
                        child.SetSourceMaterials();
                    }
                    InventoryCreator.CurrentInventory = null;
                    _mouseHoverState.SetCrosshair(EItemStage.Default);

                    break;
                case EPickableState.TuneConnection:

                    InventoryCreator.CurrentInventory.SetMaterial(_matConnectToInventory);
                    foreach (var child in InventoryCreator.CurrentInventory.InventoryChilds)
                    {
                        child.SetMaterial(_matConnectToInventory);                       
                    }

                    break;
                case EPickableState.Connect:

                    _mouseHoverState.SetCrosshair(EItemStage.Default);

                    break;
                case EPickableState.Transform:
                    break;
                case EPickableState.Property:
                    break;
                case EPickableState.Erase:
                    break;
                case EPickableState.Deselect:
                    break;
                case EPickableState.Gizmo:

                    _mouseHoverState.SetCrosshair(EItemStage.Gizmo);

                    break;
                default:
                    break;
            }

            print("pickableState: " + pickableState);
        }

        /// <summary>
        /// очистить Id выделенного предмета
        /// </summary>
        private void ClearLastPointerInventoryId()
        {
            _pointerInventoryId = -1;            
        }

        /// <summary>
        /// двигать предмет по столу
        /// </summary>
        /// <param name="position">позиция точки рейкаста</param>
        private void MoveOnTable(Vector3 position)
        {
            if (InventoryCreator.CurrentInventory == null)
                return;

            InventoryCreator.CurrentInventory.SetPositionOnTable(position);            
            SetPickableState(EPickableState.MoveOnTable);                        
        }

        /// <summary>
        /// двигать предмет по виртуальной области стола
        /// </summary>
        /// <param name="position">позиция точки рейкаста</param>
        private void MoveOnHideAreaTable(Vector3 position)
        {
            if (InventoryCreator.CurrentInventory == null)
                return;           

            InventoryCreator.CurrentInventory.SetPositionOnTable(position);
            SetPickableState(EPickableState.MoveOnHideAreaTable);           
        }

        /// <summary>
        /// поставить предмет на стол
        /// </summary>
        private void StayOnTable()
        {
            if (InventoryCreator.CurrentInventory != null && InventoryCreator.CurrentInventory.InventorySettings.CanStayOnTable)
            {
                SetPickableState(EPickableState.StayOnTable);                                
            }
        }

        /// <summary>
        /// настройка соединения и соединение предмета с другим предметом
        /// </summary>
        /// <param name="hit">The hit.</param>
        private void InventoryConnection(RaycastHit hit)
        {
           //if (InventoryCreator.CurrentInventory == null)
           //     return;

            _gizmo.SetTarget(null);

            PickableInventoryBase pointerItem = InventoryPointer.GetPointerItem(hit.transform.gameObject);

            //Debug.Log(pointerItem.InventorySettings.Id);

            //if (pointerItem.itemSettings.IsChildConnection)
            //    return;

            //_mouseHoverState.SetCrosshair(EItemStage.ConnectFalse);

            if (InventoryCreator.CurrentInventory.CanBeConnect(pointerItem.InventorySettings.Id))
            {
                if (pointerItem.InventorySettings.Id != _pointerInventoryId)
                {
                    SetPickableState(EPickableState.TuneConnection);

                    // унаследоваться от предмету к которому возможно присоединиться
                    //InventoryCreator.CurrentInventory.SetParerntRelativePointerItem(pointerItem, pointerItem.InventorySettings.Connectors);
                    InventoryCreator.CurrentInventory.SetParerntRelativePointerItem(pointerItem);

                    // записать значение предмета на котором указатель мыши
                    _pointerInventoryId = pointerItem.InventorySettings.Id;

                    _mouseHoverState.SetCrosshair(EItemStage.Connect);
                }                

                InventoryCreator.CurrentInventory.TunePosition(hit.point);

                if (Input.GetMouseButtonDown(0))
                {
                    SetPickableState(EPickableState.Connect);

                    // записать инфо о родителе в предмет                
                    InventoryCreator.CurrentInventory.InventoryParent = pointerItem;

                    // записать инфо о предмете в родитель               
                    pointerItem.InventoryChilds.Add(InventoryCreator.CurrentInventory);
                    
                    InventoryCreator.CurrentInventory.SetConnection(pointerItem.InventorySettings.Id);                   
                    //InventoryCreator.CreatedInventory.Add(InventoryCreator.CurrentInventory);                    

                    DropCreatedInventory();
                }                
            }           
        }

        /// <summary>
        /// установить вид камеры на выделенный предмет
        /// </summary>
        /// <param name="hit">The hit.</param>
        /// <returns></returns>
        private IEnumerator CorSetFocusToSelectedInventory()
        {
            _isFocus = true;
            //var pointFocus = InventoryPointer.CurrentInventory.CenterItem;
            //yield return new WaitForSeconds(_timeFocus);            
            //_cameraManager.ExtendCamera.SetTargetPos(pointFocus);
            _cameraManager.ExtendCamera.SetView(InventoryPointer.CurrentInventory.InventorySettings.CameraView);
            _isFocus = false;

            yield return null;
        }

        public void SetFocusToSelectedInventory()
        {
            if(InventoryPointer.PointerItemIsNull())
            {
                return;
            }

            StopAllCoroutines();
            StartCoroutine(CorSetFocusToSelectedInventory());
        }

        //public void SetFocusToSelectedInventory()
        //{
        //    if (InventoryPointer.PointerItemIsNull())
        //    {
        //        return;
        //    }

        //    StopAllCoroutines();
        //    StartCoroutine(CorSetFocusToSelectedInventory());
        //}

        /// <summary>
        /// выделить предмет
        /// </summary>
        private void SelectInventoryPointer(RaycastHit hit)
        {
            if (!InventoryPointer.PointerItemIsNull())
            {
                var buffer = InventoryPointer.GetPointerItem(hit.transform.gameObject);

                if (buffer != null && buffer.InventorySettings.Id != _pointerInventoryId)
                {
                    InventoryPointer.CurrentInventory.Deselect();
                    InventoryPointer.SetNullPointerItem();

                    InventoryPointer.SetPointerItem(hit.transform.gameObject);
                    InventoryPointer.CurrentInventory.Select();

                    SetGizmo(InventoryPointer.CurrentInventory);

                    if(OnSelectInventory != null)
                    {
                        OnSelectInventory(InventoryPointer.CurrentInventory);
                    }

                    _pointerInventoryId = InventoryPointer.CurrentInventory.InventorySettings.Id;
                }
            }
            else
            {
                InventoryPointer.SetPointerItem(hit.transform.gameObject);
                InventoryPointer.CurrentInventory.Select();

                SetGizmo(InventoryPointer.CurrentInventory);

                if (OnSelectInventory != null)
                {
                    OnSelectInventory(InventoryPointer.CurrentInventory);
                }

                _pointerInventoryId = InventoryPointer.CurrentInventory.InventorySettings.Id;
            }            
        }

        public void SelectInventoryPointer(PickableInventoryBase inventory)
        {
            if (!InventoryPointer.PointerItemIsNull())
            {
                if (InventoryPointer.CurrentInventory.InventorySettings.Id != _pointerInventoryId)
                {
                    InventoryPointer.CurrentInventory.Deselect();
                    InventoryPointer.SetNullPointerItem();

                    InventoryPointer.CurrentInventory = inventory;
                    InventoryPointer.CurrentInventory.Select();

                    _pointerInventoryId = InventoryPointer.CurrentInventory.InventorySettings.Id;
                }
            }
            else
            {
                InventoryPointer.CurrentInventory = inventory;
                InventoryPointer.CurrentInventory.Select();

                _pointerInventoryId = InventoryPointer.CurrentInventory.InventorySettings.Id;
            }
        }

        private void SetGizmo(PickableInventoryBase inventory)
        {
            if(!_simulationManager.IsEnableSimulation)
            {
                _gizmo.SetTarget(inventory.InventorySettings.Transform);
                _gizmo.SetActiveDirAxis(inventory.InventorySettings.DirAxis);
                _gizmo.SetActiveRotAxis(inventory.InventorySettings.RotAxis);
            }           
        }

        /// <summary>
        /// снять выделение с выделенного предмета
        /// </summary>
        public void DeselectInventoryPointer()
        {
            if (!InventoryPointer.PointerItemIsNull())
            {
                InventoryPointer.CurrentInventory.SetSourceMaterials();                
                InventoryPointer.SetNullPointerItem();
                ClearLastPointerInventoryId();
                _gizmo.SetTarget(null);

                if (OnSelectInventory != null)
                {
                    OnSelectInventory(null);
                }
            }
        }

        /// <summary>
        /// скрыть отображение созданного предмета
        /// </summary>
        private void HideCreatedInventory()
        {            
            if (InventoryCreator.CurrentInventory != null)
            {
                InventoryCreator.CurrentInventory.InventorySettings.Transform.position = new Vector3(0, 0, -1);
                InventoryCreator.CurrentInventory.InventorySettings.Transform.parent = DynamicRoot.Instance.DynamicRootTransform;
                ClearLastPointerInventoryId();
                _gizmo.SetTarget(null);
            }                
        }

        /// <summary>
        /// дропнуть созданный предмет
        /// </summary>
        private void DropCreatedInventory()
        {
            if (InventoryCreator.CurrentInventory != null)
            {
                InventoryCreator.CurrentInventory.SetActiveColliders(true);
                InventoryCreator.CurrentInventory.SetSourceMaterials();
                foreach (var child in InventoryCreator.CurrentInventory.InventoryChilds)
                {
                    child.SetActiveColliders(true);
                    child.SetSourceMaterials();
                }
                InventoryCreator.CurrentInventory = null;
                ClearLastPointerInventoryId();                
            }
        }

        /// <summary>
        /// удалить созданный предмет
        /// </summary>
        public void DeleteCreatedInventory()
        {
            if (InventoryCreator.CurrentInventory != null)
            {
                InventoryCreator.Delete(InventoryCreator.CurrentInventory);                
                InventoryCreator.CurrentInventory = null;
                SetPickableState(EPickableState.DeleteCreatedInventory);                
            }
        }

        /// <summary>
        /// удалить выделенный предмет
        /// </summary>
        /// <param name="hit">The hit.</param>
        public void DeleteInventoryPointer()
        {
            if (!InventoryPointer.PointerItemIsNull())
            {
                InventoryCreator.Delete(InventoryPointer.CurrentInventory);                
                ClearLastPointerInventoryId();
                _gizmo.SetTarget(null);
                if (OnSelectInventory != null)
                {
                    OnSelectInventory(null);
                }

                //InventoryCreator.CreatedInventory.Remove(InventoryPointer.CurrentInventory);
                //Destroy(InventoryPointer.CurrentInventory.gameObject);
                //ClearLastPointerInventoryId();
                //_gizmo.SetTarget(null);
                //if (OnSelectInventory != null)
                //{
                //    OnSelectInventory(null);
                //}
            }            
        }

        /// <summary>
        /// перемещение предмета
        /// </summary>
        /// <param name="hit">The hit.</param>
        public void InventoryTransform(RaycastHit hit)
        {            
            if (Input.GetMouseButtonUp(0))
            {
                InventoryPointer.SetPointerItem(hit.transform.gameObject);

                // установить значения в GizmoManager
                //_cameraManager.Gizmo.SetTarget(hit.transform);
                //_cameraManager.Gizmo.SetMoveAxis(InventoryPointer.CurrentInventory.InventorySettings.MoveAxis);
                //_cameraManager.Gizmo.SetRotateAxis(InventoryPointer.CurrentInventory.InventorySettings.RotateAxis);
            }                
        }
        
        /// <summary>
        /// свойства предмета
        /// </summary>
        /// <param name="hit">The hit.</param>
        private void InventoryProperty(RaycastHit hit)
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (InventoryPointer.PointerItemIsNull())
                {
                    InventoryPointer.SetPointerItem(hit.transform.gameObject);
                }

                InventoryPointer.ShowItemsettings();

                Debug.Log("item settings");
            }            
        }

        /// <summary>
        /// Initializes the hit point.
        /// </summary>
        private void InitHitPoint()
        {
            if (_goHitPoint == null)
            {
                _goHitPoint = new GameObject("HitPoint");
                _goHitPoint.transform.parent = DynamicRoot.Instance.DynamicRootTransform;
            }
        }

        /// <summary>
        /// Draws the hit point.
        /// </summary>
        /// <param name="hit">The hit.</param>
        private void DrawHitPoint(RaycastHit hit)
        {            
            _goHitPoint.transform.position = hit.point;
            //_goHitPoint.transform.rotation = Quaternion.LookRotation(hit.normal);
            
            Debug.DrawRay(_goHitPoint.transform.position, _goHitPoint.transform.up * .1f, Color.green);
            Debug.DrawRay(_goHitPoint.transform.position, _goHitPoint.transform.right * .1f, Color.red);
            Debug.DrawRay(_goHitPoint.transform.position, _goHitPoint.transform.forward * .1f, Color.blue);
        }

        /// <summary>
        /// если при нажатии левого указателя мыши не попали на клемму
        /// </summary>
        private void MissPlug()
        {
            if (OnMissPlug != null)
                OnMissPlug();
        }
    }
}