﻿using UnityEngine;
using System.Collections;
using Zenject;
using AmazyingPhysics;
using AmazyingPhysics.Data;
using System;

namespace AmazyingPhysics
{
    public class InventoryPointer
    {
        public PickableInventoryBase CurrentInventory { get; set; }

        public event Action<string> OnItemSettings;
        
        public void SetPointerItem(GameObject item)
        {
            CurrentInventory = item.GetComponent<PickableInventoryBase>();

            if(CurrentInventory == null)
            {
                Debug.Log("объект не содержит скрипт PickableItemBase");
            }
        }

        public PickableInventoryBase GetPointerItem(GameObject item)
        {
            return item.GetComponent<PickableInventoryBase>();            
        }

        public void SetNullPointerItem()
        {
            Debug.Log("CurrentInventory = null");
            CurrentInventory = null;
        }

        public bool PointerItemIsNull()
        {
            bool isNull = true;

            if (CurrentInventory != null)
                isNull = false;

            return isNull;
        }

        public void ShowItemsettings()
        {
            if(OnItemSettings != null)
            {
                OnItemSettings(CurrentInventory.InventorySettings.Name);
            }
        }       
    }
}
