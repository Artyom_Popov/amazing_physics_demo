﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace AmazyingPhysics
{
    public class Gizmo : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private GameObject _root;

        [SerializeField]
        private Button _btnDelete, _btnFocus;       
        
        private Axis _selectedAxis;
        private Vector3 _selectedDirAxis;
        private Transform _target;
        private Camera _camera;
        private AxisObject _directionAxis;
        private AxisObject _rotationAxis;

        private Vector3 _previousMousePosition;
        private Vector3 _originalTargetPosition;
        private Vector3 _planeNormal;
        private Vector3 _projectedAxis;        
        private Vector3 _mousePosition;
        private float _rotateSpeedMultiplier = 200f;
        private Vector3 _initialScale;

        #endregion

        public Ray CameraRay { get; set; }
        public RaycastHit GizmoHit  { get; set; }
        public bool IsStartMoveGizmo { get; set; }
        public event Action OnDelete;
        public event Action OnFocus;

        private void Start()
        {
            _directionAxis = new AxisObject();
            _rotationAxis = new AxisObject();
            _previousMousePosition = Vector3.zero;
            _mousePosition = Vector3.zero;            
            _initialScale = _root.transform.localScale;

            var axis = _root.GetComponentsInChildren<Transform>();
            _directionAxis.X = axis.Where(a => a.name == "x").Select(g => g.gameObject).First();
            _directionAxis.Y = axis.Where(a => a.name == "y").Select(g => g.gameObject).First();
            _directionAxis.Z = axis.Where(a => a.name == "z").Select(g => g.gameObject).First();
            _rotationAxis.X = axis.Where(a => a.name == "rx").Select(g => g.gameObject).First();
            _rotationAxis.Y = axis.Where(a => a.name == "ry").Select(g => g.gameObject).First();
            _rotationAxis.Z = axis.Where(a => a.name == "rz").Select(g => g.gameObject).First();

            _btnDelete.onClick.AddListener(() =>
            {
                if(OnDelete != null)
                {
                    OnDelete();
                }
            });

            _btnFocus.onClick.AddListener(() =>
            {
                if (OnFocus != null)
                {
                    OnFocus();
                }
            });
        }

        private void Update()
        {
            if(IsStartMoveGizmo)
            {
                StartCoroutine(CorTransformSelected());
                IsStartMoveGizmo = false;
            }           
        }

        private void LateUpdate()
        {
            if (_target != null)
            {
                Plane plane = new Plane(_camera.transform.forward, _camera.transform.position);
                float dist = plane.GetDistanceToPoint(_root.transform.position);
                _root.transform.localScale = Vector3.one * dist;
            }
        }

        public void Constructor(Camera camera)
        {
            _camera = camera;
        }

        public void SetTarget(Transform target)
        {
            if (target != null)
            {
                _target = target;

                _root.transform.position = target.position;
                _root.transform.rotation = target.rotation;

                _root.SetActive(true);
            }
            else
            {
                _root.SetActive(false);
            }
        }
        
        private void SetAxis()
        {
            switch (GizmoHit.collider.name)
            {
                case "x":
                    _selectedAxis = Axis.DirX;                    
                    break;
                case "y":
                    _selectedAxis = Axis.DirY;                    
                    break;
                case "z":
                    _selectedAxis = Axis.DirZ;                    
                    break;
                case "rx":
                    _selectedAxis = Axis.RotX;                    
                    break;
                case "ry":
                    _selectedAxis = Axis.RotY;                    
                    break;
                case "rz":
                    _selectedAxis = Axis.RotZ;                   
                    break;
                default:
                    _selectedAxis = Axis.None;                    
                    break;
            }

            _selectedDirAxis = GizmoHit.transform.up;
            _originalTargetPosition = _target.position;
            _planeNormal = (_camera.transform.position - _target.position).normalized;                    
            _mousePosition = _previousMousePosition = Vector3.zero;          
        }

        public void SetActiveDirAxis(ActiveAxis axis)
        {
            SetActiveAxis(axis, _directionAxis);
        }

        public void SetActiveRotAxis(ActiveAxis axis)
        {
            SetActiveAxis(axis, _rotationAxis);
        }

        private void SetActiveAxis(ActiveAxis axis, AxisObject axisObjects)
        {
            axisObjects.SetActiveAxis(false);

            switch (axis)
            {
                case ActiveAxis.X:
                    axisObjects.X.SetActive(true);
                    break;
                case ActiveAxis.Y:
                    axisObjects.Y.SetActive(true);
                    break;
                case ActiveAxis.Z:
                    axisObjects.Z.SetActive(true);
                    break;
                case ActiveAxis.XY:
                    axisObjects.X.SetActive(true);
                    axisObjects.Y.SetActive(true);
                    break;
                case ActiveAxis.YZ:
                    axisObjects.Y.SetActive(true);
                    axisObjects.Z.SetActive(true);
                    break;
                case ActiveAxis.ZX:
                    axisObjects.Z.SetActive(true);
                    axisObjects.X.SetActive(true);
                    break;
                case ActiveAxis.XYZ:
                    axisObjects.SetActiveAxis(true);
                    break;
                default:
                    break;
            }
        }
        
        private IEnumerator CorTransformSelected()
        {
            SetAxis();

            if(_selectedAxis == Axis.None)
            {
                yield break;
            }
            
            while (!Input.GetMouseButtonUp(0))
            {
                _mousePosition = LinePlaneIntersect(CameraRay.origin, CameraRay.direction, _originalTargetPosition, _planeNormal);

                if (_previousMousePosition != Vector3.zero && _mousePosition != Vector3.zero)
                {
                    switch (_selectedAxis)
                    {
                        case Axis.DirX:
                        case Axis.DirY:
                        case Axis.DirZ:

                            float moveAmount = Vector3.Dot(_mousePosition - _previousMousePosition, _selectedDirAxis);
                            _target.Translate((_selectedDirAxis) * moveAmount, Space.World);

                            _root.transform.position = _target.position;

                            break;
                        case Axis.RotX:
                        case Axis.RotY:
                        case Axis.RotZ:

                            Vector3 projected = (IsParallel(_selectedDirAxis, _planeNormal)) ? _planeNormal : Vector3.Cross(_selectedDirAxis, _planeNormal);                           
                            float rotateAmount = (MagnitudeInDirection(_mousePosition - _previousMousePosition, projected) * _rotateSpeedMultiplier) / GetDistanceMultiplier();
                            _target.Rotate(_selectedDirAxis, rotateAmount, Space.World);

                            _root.transform.rotation = _target.rotation;
                            
                            break;
                        default:
                            break;
                    }
                }                    

                _previousMousePosition = _mousePosition;

                yield return null;
            }
        }

        public class AxisObject
        {
            public GameObject X, Y, Z;

            public void SetActiveAxis(bool value)
            {
                X.SetActive(value);
                Y.SetActive(value);
                Z.SetActive(value);
            }
        }        

        #region Geometry

        private bool IsParallel(Vector3 direction, Vector3 otherDirection, float precision = .0001f)
        {
            return Vector3.Cross(direction, otherDirection).sqrMagnitude < precision;
        }

        private float MagnitudeInDirection(Vector3 vector, Vector3 direction, bool normalizeParameters = true)
        {
            if (normalizeParameters) direction.Normalize();
            return Vector3.Dot(vector, direction);
        }

        private float GetDistanceMultiplier()
        {
            return Mathf.Max(.01f, Mathf.Abs(MagnitudeInDirection(_target.position - _camera.transform.position, _camera.transform.forward)));
        }

        private float LinePlaneDistance(Vector3 linePoint, Vector3 lineVec, Vector3 planePoint, Vector3 planeNormal)
        {
            //calculate the distance between the linePoint and the line-plane intersection point
            float dotNumerator = Vector3.Dot((planePoint - linePoint), planeNormal);
            float dotDenominator = Vector3.Dot(lineVec, planeNormal);

            //line and plane are not parallel
            if (dotDenominator != 0f)
            {
                return dotNumerator / dotDenominator;
            }

            return 0;
        }

        private Vector3 LinePlaneIntersect(Vector3 linePoint, Vector3 lineVec, Vector3 planePoint, Vector3 planeNormal)
        {
            float distance = LinePlaneDistance(linePoint, lineVec, planePoint, planeNormal);

            //line and plane are not parallel
            if (distance != 0f)
            {
                return linePoint + (lineVec * distance);
            }

            return Vector3.zero;
        }

        #endregion
    }

    public enum Axis
    {
        None,
        DirX,
        DirY,
        DirZ,
        RotX,
        RotY,
        RotZ
    }

    public enum ActiveAxis
    {
        None,
        X,
        Y,
        Z,
        XY,
        YZ,        
        ZX,
        XYZ
    }
}
