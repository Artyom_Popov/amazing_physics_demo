﻿using UnityEngine;

public class CalcCircle : MonoBehaviour
{
    public Vector3 Centre { get; set; }
    public Transform CentrePoint;

    private Vector3 V1, V2, V3;
    private Vector3 oV1, oV2, oV3;
    
    private void Update()
    {
        if (V1 != oV1 || V2 != oV2 || V3 != oV3)
        {
            FindCircleCentre(V1, V2, V3);

            oV1 = V1;
            oV2 = V2;
            oV3 = V3;

            CentrePoint.position = Centre;
        }
    }

    public void SetPoints(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        V1 = v1;
        V2 = v2;
        V3 = v3;
    }

    private void FindCircleCentre(Vector3 pt1, Vector3 pt2, Vector3 pt3)
    {
        Centre = Vector3.zero;
        
        if (!IsPerpendicular(pt1, pt2, pt3))
            CalcCircleCentre(pt1, pt2, pt3);
        else if (!IsPerpendicular(pt1, pt3, pt2))
            CalcCircleCentre(pt1, pt3, pt2);
        else if (!IsPerpendicular(pt2, pt1, pt3))
            CalcCircleCentre(pt2, pt1, pt3);
        else if (!IsPerpendicular(pt2, pt3, pt1))
            CalcCircleCentre(pt2, pt3, pt1);
        else if (!IsPerpendicular(pt3, pt2, pt1))
            CalcCircleCentre(pt3, pt2, pt1);
        else if (!IsPerpendicular(pt3, pt1, pt2))
            CalcCircleCentre(pt3, pt1, pt2);        
    }
    
    private bool IsPerpendicular(Vector3 pt1, Vector3 pt2, Vector3 pt3)
    {
        // Check the given point are perpendicular to x or y axis
        float yDelta_a = pt2.z - pt1.z;
        float xDelta_a = pt2.x - pt1.x;
        float yDelta_b = pt3.z - pt2.z;
        float xDelta_b = pt3.x - pt2.x;

        // checking whether the line of the two pts are vertical
        if (Mathf.Abs(xDelta_a) <= float.Epsilon && Mathf.Abs(yDelta_b) <= float.Epsilon)
            return false;

        if (Mathf.Abs(yDelta_a) <= float.Epsilon)
            return true;

        else if (Mathf.Abs(yDelta_b) <= float.Epsilon)
            return true;

        else if (Mathf.Abs(xDelta_a) <= float.Epsilon)
            return true;

        else if (Mathf.Abs(xDelta_b) <= float.Epsilon)
            return true;

        else
            return false;
    }

    private float CalcCircleCentre(Vector3 pt1, Vector3 pt2, Vector3 pt3)
    {
        float yDelta_a = pt2.z - pt1.z;
        float xDelta_a = pt2.x - pt1.x;
        float yDelta_b = pt3.z - pt2.z;
        float xDelta_b = pt3.x - pt2.x;

        if (Mathf.Abs(xDelta_a) <= float.Epsilon && Mathf.Abs(yDelta_b) <= float.Epsilon)
        {
            Centre = new Vector3( 0.5f * (pt2.x + pt3.x),
                                  1,
                                  0.5f * (pt1.z + pt2.z));         
            return 1;
        }

        // IsPerpendicular() assure that xDelta(s) are not zero
        float aSlope = yDelta_a / xDelta_a;
        float bSlope = yDelta_b / xDelta_b;
        if (Mathf.Abs(aSlope - bSlope) <= float.Epsilon)    // checking whether the given points are colinear.
        {
            return -1;
        }

        // calc centre
        Centre = new Vector3( (aSlope * bSlope * (pt1.z - pt3.z) + bSlope * (pt1.x + pt2.x) - aSlope * (pt2.x + pt3.x)) / (2 * (bSlope - aSlope)),
                              1,
                              -1 * (Centre.x - (pt1.x + pt2.x) / 2) / aSlope + (pt1.z + pt2.z) / 2);      
        return 1;
    }    
}
