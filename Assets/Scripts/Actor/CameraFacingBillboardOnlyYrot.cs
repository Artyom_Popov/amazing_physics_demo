﻿using UnityEngine;
using System.Collections;

namespace AmazyingPhysics
{
    public class Test2 : MonoBehaviour
    {
        public Camera m_Camera;
        private Vector3 v;

        public ActiveAxis axis;

        private void Start()
        {
            m_Camera = Camera.main;
            v = transform.localEulerAngles;
        }

        void Update()
        {
            transform.LookAt(m_Camera.transform.position, Vector3.up);

            var v = m_Camera.transform.position - transform.position;
            var q = Quaternion.LookRotation(v);

            if(axis == ActiveAxis.Y)
            {
                transform.localRotation = Quaternion.Euler(v.x, q.eulerAngles.y, v.z) * Quaternion.Euler(0, 180, -90);
            }
            else if (axis == ActiveAxis.X)
            {
                transform.localRotation = Quaternion.Euler(q.eulerAngles.y, v.y, v.z) * Quaternion.Euler(180, 0, -90);
            }            
        }
    }
}